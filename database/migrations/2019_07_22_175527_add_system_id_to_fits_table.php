<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSystemIdToFitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fits', function (Blueprint $table) {
            $table->integer('system_id')->default(0)->after('id');//system_id la id cua phu kien tren phan mem
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fits', function (Blueprint $table) {
            $table->dropColumn('system_id');
        });
    }
}
