<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comment', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('parent_id')->default(0);
            $table->string('type',191)->nullable();
            $table->integer('type_id')->nullable();
            $table->text('url')->nullable();
            $table->integer('user_id')->default(0);
            $table->integer('admin_id')->default(0);
            $table->tinyInteger('gender')->default(1);
            $table->string('name');
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->integer('like')->default(1);
            $table->integer('rank')->default(5);
            $table->text('content');
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
            $table->unique('id','id_UNIQUE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comment');
    }
}
