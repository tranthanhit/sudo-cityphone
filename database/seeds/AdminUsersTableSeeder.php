<?php

use Illuminate\Database\Seeder;

class AdminUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin_users')->insert(['id'=>1,'name'=>'admin', 'email'=>'info@sudo.vn', 'password'=>bcrypt('sudovn')]);
    }
}
