<?php 

namespace App\MyClass;
/**
* 
*/
class MyForm
{

    /**
     * @param string $name - tên trường dữ liệu
     * @param string $value - Giá trị hiện tại
     * @param int $required - có bắt buộc hay không (1 | 0)
     * @param string $title - Tên label
     * @param string $placeholder
     * @param int $slug - có trường slug không (1 | 0)
     * @param string $slugField - tên trường slug để map (trong db hay để luôn là slug)
     * @return array
     */
    function title($title){
        return ['store'=>'title','title'=>$title];
    }
    function text($name = '', $value = '', $required = 0, $title = 'Tiêu đề', $placeholder = '',$slug = 0, $slugField = 'slug',$extra = '') {
        return ['store'=>'text','name'=>$name,'value'=>$value,'required'=>$required,'title'=>$title,'placeholder'=>$placeholder,'slug'=>$slug,'slugField'=>$slugField,'extra'=>$extra];
    }
    function slug($name = '', $value = '', $required = 1, $title = 'Đường dẫn') {
        return ['store'=>'slug','name'=>$name,'value'=>$value,'required'=>$required, 'title'=>$title];
    }
    function password($name = '', $value = '', $required = 0, $title = 'Tiêu đề', $placeholder = '') {
        return ['store'=>'password','name'=>$name,'value'=>$value,'required'=>$required,'title'=>$title,'placeholder'=>$placeholder];
    }
    function textarea($name = '', $value = '', $required = 0, $title = '', $placeholder = '') {
        return ['store'=>'textarea','name'=>$name,'value'=>$value,'required'=>$required,'title'=>$title,'placeholder'=>$placeholder];
    }
    function select($name = '', $value = '', $required = 0, $title = '', $options = [], $disabled = []) {
        return ['store'=>'select','name'=>$name,'value'=>$value,'required'=>$required,'title'=>$title,'options'=>$options,'disabled'=>$disabled];
    }
    function multi_cate($name = '', $value = [],$required = 0, $title = '', $options = []) {
        return ['store'=>'multi_cate','name'=>$name,'value'=>$value,'required'=>$required,'title'=>$title,'options'=>$options];
    }
    function tags($name = '', $value = '', $required = 0, $title = '') {
        return ['store'=>'tags','name'=>$name,'value'=>$value,'required'=>$required,'title'=>$title];
    }
    function editor($name = '', $value = '', $required = 0, $title = '') {
        return ['store'=>'editor','name'=>$name,'value'=>$value,'required'=>$required,'title'=>$title];
    }
    function checkbox($name = '', $value = '', $checked = 1, $title) {
        return ['store'=>'checkbox','name'=>$name,'value'=>$value, 'checked' => $checked, 'title'=>$title];
    }
    function hidden($name = '', $value = '') {
        return ['store'=>'hidden','name'=>$name,'value'=>$value];
    }
    function image($name = '', $value = '', $required = 0, $title = 'Ảnh đại diện', $title_btn = 'Chọn làm ảnh đại diện',$helper_text = '') {
        return ['store'=>'image','name'=>$name,'value'=>$value,'required'=>$required,'title'=>$title,'title_btn'=>$title_btn,'helper_text'=>$helper_text];
    }
    function slide($name = '', $value = [], $required = 0, $title = 'Ảnh slide', $title_btn = 'Chọn làm ảnh slide',$helper_text = '') {
        return ['store'=>'slide','name'=>$name,'value'=>$value,'required'=>$required,'title'=>$title,'title_btn'=>$title_btn,'helper_text'=>$helper_text];
    }
    function datepicker($name = '', $value = '', $required = 0, $title = 'Thời điểm') {
        return ['store'=>'datepicker','name'=>$name,'value'=>$value,'required'=>$required,'title'=>$title];
    }
    function datetimepicker($name = '', $value = '', $required = 0, $title = 'Thời điểm') {
        return ['store'=>'datetimepicker','name'=>$name,'value'=>$value,'required'=>$required,'title'=>$title];
    }
    function related($name = '', $value = '', $required = 0, $title = 'Tin liên quan', $placeholder = 'Tìm theo tên ...',$relate_table = 'news', $relate_id = 'id', $relate_name = 'name') {
        return ['store'=>'related','name'=>$name,'value'=>$value,'required'=>$required,'title'=>$title,'placeholder'=>$placeholder,'relate_table'=>$relate_table,'relate_id'=>$relate_id,'relate_name'=>$relate_name];
    }
    function custom($template,$value='') {
        return ['store'=>'custom','template'=>$template,'value'=>$value];
    }
    function customMenu($name = '', $value='', $title = 'Cấu hình menu', $data_table=['news_categories','pages']){
        return ['store'=>'custommenu', 'name'=>$name, 'template' => 'admin.layouts.menu', 'value' => $value, 'title'=>$title,'data_table'=>$data_table];
    }
    function action($store = 'store',$preview = '') {
        return ['store'=>$store,'preview'=>$preview];
    }
    function start_group($id,$name) {
        return ['store'=>'start_group','id'=>$id,'name'=>$name];
    }
    function end_group() {
        return ['store'=>'end_group'];
    }
    function custom_forum($template){
        return ['store'=>'custom_forum','template'=>$template];
    }
    function custom_warranty($template){
        return ['store'=>'custom_warranty','template'=>$template];
    }
    function custom_attribute($template){
        return ['store'=>'custom_attribute','template'=>$template];
    }
}

?>