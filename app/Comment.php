<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = "comment";

    public static function add($request)
    {
        $created_at = $updated_at = date('Y-m-d H:i:s');
    	$comment = new Comment;
        $comment->name = $request->name;
        $comment->type = $request->type;
        $comment->type_id = $request->type_id;
        $comment->parent_id = 0;
        $comment->admin_id = 0;
        $comment->content = $request->content;
        $comment->email = $request->email;
        $comment->phone = $request->phone;
        $comment->like = 0;
        $comment->status = 2;
        $comment->gender = 1;
        $comment->rank = $request->rating;
        $comment->created_at = $created_at;
        $comment->updated_at = $updated_at;
        $comment->save();
    }
}
