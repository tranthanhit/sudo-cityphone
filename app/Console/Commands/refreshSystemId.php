<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class refreshSystemId extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'refreshSystemId';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Lam moi toan bo system_id cua phones va fits';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::table('phones')->where('system_id','>',0)->update(['system_id'=>0]);
        DB::table('fits')->where('system_id','>',0)->update(['system_id'=>0]);
        $this->info("Da lam moi system_id");
    }
}
