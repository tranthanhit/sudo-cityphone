<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
class Syncdb extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'DB {table}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $table = $this->argument('table');
        switch ($table) {
            case 'metaseo_phone':
                $metaseo_old = DB::connection('mysql_old')->table('meta_seo')->where('met_type','phone')->get();
                foreach($metaseo_old as $k => $v){
                    // $id = $v->met_id;
                    $type = 'phones';
                    $type_id = $v->met_post_id;
                    $title = $v->met_title;
                    $description = $v->met_description;
                    $robots = $v->met_robots;
                    $data_insert = compact('type','title','description','type_id','robots');
                    DB::connection('mysql')->table('meta_seo')->insert($data_insert);
                }
                $this->info("Done");
                break;
            case 'metaseo_services':
                $metaseo_old = DB::connection('mysql_old')->table('meta_seo')->where('met_type','fix_phone')->get();
                foreach($metaseo_old as $k => $v){
                    // $id = $v->met_id;
                    $type = 'services';
                    $type_id = $v->met_post_id;
                    $title = $v->met_title;
                    $description = $v->met_description;
                    $robots = $v->met_robots;
                    $data_insert = compact('type','title','description','type_id','robots');
                    DB::connection('mysql')->table('meta_seo')->insert($data_insert);
                }
                $this->info("Done");
                break;
            case 'metaseo_tablet':
                $metaseo_old = DB::connection('mysql_old')->table('meta_seo')->where('met_type','tablet')->get();
                foreach($metaseo_old as $k => $v){
                    // $id = $v->met_id;
                    $type = 'tablets';
                    $type_id = $v->met_post_id;
                    $title = $v->met_title;
                    $description = $v->met_description;
                    $robots = $v->met_robots;
                    $data_insert = compact('type','title','description','type_id','robots');
                    DB::connection('mysql')->table('meta_seo')->insert($data_insert);
                }
                $this->info("Done");
                break;
            case 'metaseo_fit':
                $metaseo_old = DB::connection('mysql_old')->table('meta_seo')->where('met_type','fittings')->get();
                foreach($metaseo_old as $k => $v){
                    // $id = $v->met_id;
                    $type = 'fits';
                    $type_id = $v->met_post_id;
                    $title = $v->met_title;
                    $description = $v->met_description;
                    $robots = $v->met_robots;
                    $data_insert = compact('type','title','description','type_id','robots');
                    DB::connection('mysql')->table('meta_seo')->insert($data_insert);
                }
                $this->info("Done");
                break;
            case 'metaseo_news':
                $metaseo_old = DB::connection('mysql_old')->table('meta_seo')->where('met_type','news')->get();
                foreach($metaseo_old as $k => $v){
                    // $id = $v->met_id;
                    $type = 'news';
                    $type_id = $v->met_post_id;
                    $title = $v->met_title;
                    $description = $v->met_description;
                    $robots = $v->met_robots;
                    $data_insert = compact('type','title','description','type_id','robots');
                    DB::connection('mysql')->table('meta_seo')->insert($data_insert);
                }
                $this->info("Done");
                break;
            case 'category':
                $metaseo_old = DB::connection('mysql_old')->table('meta_seo')->where('met_type','categories')->get();
                foreach($metaseo_old as $k => $v){
                    $category_type = DB::connection('mysql_old')->table('categories')->where('cat_id',$v->met_post_id)->first();
                    if($category_type){
                        switch ($category_type->cat_type) {
                            case 'phone':
                                $type = "phone_categories";
                                break;
                            case 'tablet':
                                $type = "tablet_categories";
                                break;
                            case 'fix_phone':
                                $type = "service_categories";
                                break;
                            case 'news':
                                $type = "news_categories";
                                break;
                            case 'fittings':
                                $type = "fit_categories";
                                break;
                            default:
                                
                                break;
                        }
                    }
                    $type_id = $v->met_post_id;
                    $title = $v->met_title;
                    $description = $v->met_description;
                    $robots = $v->met_robots;

                    $data_insert = compact('type','title','description','type_id','robots');
                    DB::connection('mysql')->table('meta_seo')->insert($data_insert);
                }
                $this->info("Done");
                break;
            case 'phone_attribute':
                $phone_attribute_old = DB::connection('mysql_old')->table('phone_attribute')->get();
                foreach($phone_attribute_old as $key => $value){
                    $phone_z = DB::connection('mysql_old')->table('phone')->where('pho_id',$value->pha_phone_id)->first();
                    $phone_id = $value->pha_phone_id;
                    $index = $value->pha_index;
                    $storage = $value->pha_storage;
                    $color_name = $value->pha_color_name;
                    $color_code = $value->pha_color_code;
                    $aspect = $value->pha_aspect;
                    $image = $value->pha_image;
                    $price = 0;
                    if($phone_z){
                        $data_insert = compact('phone_id','index','storage','color_name','color_code','aspect','price','image');
                        DB::connection('mysql')->table('phone_attribute')->insert($data_insert);
                    }
                   
                }
                $this->info("Done");
                break;
            case 'phones':
                $phone_cate = DB::connection('mysql_old')->table('categories')->where('cat_type','phone')->get();
                foreach ($phone_cate as $key => $value) {
                    $id = $value->cat_id;
                    $parent_id = $value->cat_parent_id;
                    $name = $value->cat_name;
                    $slug = $value->cat_alias;
                    $detail = $value->cat_description;
                    $image = $value->cat_image;
                    $order = $value->cat_order;
                    $status = ($value->cat_active == 1)?1:2;
                    $created_at = $updated_at = null;
                    $data_insert = compact('id','parent_id','name','slug','detail','image','order','status','created_at','updated_at');
                    DB::connection('mysql')->table('phone_categories')->insert($data_insert);
                }
                $phone_old = DB::connection('mysql_old')->table('phone')->get();
                foreach ($phone_old as $key => $value) {
                    $id = $value->pho_id;
                    $category_id = $value->pho_cat_id;
                    $old = $value->pho_old;
                    $name = $value->pho_name;
                    $slug = $value->pho_alias;
                    $price = $value->pho_price;
                    $price_old = $value->pho_price_old;
                    $price_sg = $value->pho_price_sg;
                    $warranty = $value->pho_warranty;
                    $warranty_option = $value->pho_warranty_options;
                    $promotion = $value->pho_promotion;
                    $image = $value->pho_image;
                    $slides = $value->pho_slides;
                    $videos = $value->pho_videos;
                    $related_phone = $value->pho_phone_related;
                    $related_fit = $value->pho_fittings;
                    $forum = $value->pho_forum;
                    $related_news = $value->pho_news;
                    $package = $value->pho_package;
                    $detail = $value->pho_detail;
                    $detail_installment = $value->pho_detail_installment;
                    $instock_status = $value->pho_instock_status;
                    $tags = $value->pho_tags;
                    $general_os = $value->pho_general_os;
                    $general_language = $value->pho_general_language;
                    $display_type = $value->pho_display_type;
                    $display_color = $value->pho_display_color;
                    $display_standard = $value->pho_display_standard;
                    $display_resolution = $value->pho_display_resolution;
                    $display_width = $value->pho_display_width;
                    $display_induction = $value->pho_display_induction;
                    $cam_primary = $value->pho_cam_primary;
                    $cam_second = $value->pho_cam_second;
                    $cam_flash = $value->pho_cam_flash;
                    $cam_feature = $value->pho_cam_feature;
                    $cam_filming = $value->pho_cam_filming;
                    $cam_videocall = $value->pho_cam_videocall;
                    $cpuram_speed = $value->pho_cpuram_speed;
                    $cpuram_core = $value->pho_cpuram_core;
                    $cpuram_chipset = $value->pho_cpuram_chipset;
                    $cpuram_ram = $value->pho_cpuram_ram;
                    $cpuram_graphic = $value->pho_cpuram_graphic;
                    $storage_contacts = $value->pho_storage_contacts;
                    $storage_rom = $value->pho_storage_rom;
                    $storage_memorycard = $value->pho_storage_memorycard;
                    $storage_memorycardsup = $value->pho_storage_memorycardsupport;
                    $design_style = $value->pho_design_style;
                    $design_size = $value->pho_design_size;
                    $design_weight = $value->pho_design_weight;
                    $pin_type = $value->pho_pin_type;
                    $pin_capacity = $value->pho_pin_capacity;
                    $pin_removable = $value->pho_pin_removable;
                    $connect_3g = $value->pho_connect_3g;
                    $connect_4g = $value->pho_connect_4g;
                    $connect_simtype = $value->pho_connect_simtype;
                    $connect_duasim = $value->pho_connect_duasim;
                    $connect_wifi = $value->pho_connect_wifi;
                    $connect_gps = $value->pho_connect_gps;
                    $connect_bluetouth = $value->pho_connect_bluetouth;
                    $connect_gprs = $value->pho_connect_gprs;
                    $connect_nfc = $value->pho_connect_nfc;
                    $connect_jack = $value->pho_connect_jack;
                    $connect_usb = $value->pho_connect_usb;
                    $connect_other = $value->pho_connect_other;
                    $connect_chargprot = $value->pho_connect_chargprot;
                    $entertainment_movie = $value->pho_entertainment_movie;
                    $entertainment_player = $value->pho_entertainment_player;
                    $entertainment_recording = $value->pho_entertainment_recording;
                    $entertainment_fm = $value->pho_entertainment_fm;
                    $entertainment_other = $value->pho_entertainment_other;
                    $showhome = $value->pho_showhome;
                    $order = $value->pho_order;
                    $status = $status = ($value->pho_active == 1)?1:2;
                    $created_at = date('Y-m-d H:i:s',$value->pho_date_add);
                    // $updated_at = date('Y-m-d H:i:s',$value->pho_date_edit);
                    $updated_at = ($value->pho_date_edit != null)?date('Y-m-d H:i:s',$value->pho_date_edit):$created_at;
                    $new = $value->pho_new;
                    $hot = $value->pho_hot;
                    $data_insert = compact('id','category_id','old','name','slug','price','price_old','price_sg','warranty','warranty_option','promotion','image','slides','videos','related_phone','related_fit','forum','related_news','package','detail','detail_installment','instock_status','tags','general_os','general_language','display_type','display_color','display_standard','display_resolution','display_width','display_induction','cam_primary','cam_second','cam_flash','cam_feature','cam_filming','cam_videocall','cpuram_speed','cpuram_core','cpuram_chipset','cpuram_ram','cpuram_graphic','storage_contacts','storage_rom','storage_memorycard','storage_memorycardsup','design_style','design_size','design_weight','pin_type','pin_capacity','pin_removable','connect_3g','connect_4g','connect_simtype','connect_duasim','connect_wifi','connect_gps','connect_bluetouth','connect_gprs','connect_nfc','connect_jack','connect_usb','connect_other','connect_chargprot','entertainment_movie','entertainment_player','entertainment_recording','entertainment_fm','entertainment_other','showhome','order','status','new','hot','created_at','updated_at');
                        DB::connection('mysql')->table('phones')->insert($data_insert);
                }
                
                $this->info("Done");
                break;
            case 'tablets':
                $phone_cate = DB::connection('mysql_old')->table('categories')->where('cat_type','tablet')->get();
                foreach ($phone_cate as $key => $value) {
                    $id = $value->cat_id;
                    $parent_id = $value->cat_parent_id;
                    $name = $value->cat_name;
                    $slug = $value->cat_alias;
                    $detail = $value->cat_description;
                    $image = $value->cat_image;
                    $order = $value->cat_order;
                    $status = ($value->cat_active == 1)?1:2;
                    $created_at = $updated_at = null;
                    $data_insert = compact('id','parent_id','name','slug','detail','image','order','status','created_at','updated_at');
                    DB::connection('mysql')->table('tablet_categories')->insert($data_insert);
                }
                $phone_old = DB::connection('mysql_old')->table('tablet')->get();
                foreach ($phone_old as $key => $value) {
                    $id = $value->tab_id;
                    $category_id = $value->tab_cat_id;
                    $old = $value->tab_old;
                    $name = $value->tab_name;
                    $slug = $value->tab_alias;
                    $price = $value->tab_price;
                    // $price_old = isset($value->tab_price_old)?$value->tab_price_old:"";
                    $price_sg = $value->tab_price_sg;
                    $warranty = $value->tab_warranty;
                    $warranty_option = isset($value->tab_warranty_options)?$value->tab_warranty_options:"";
                    $promotion = $value->tab_promotion;
                    $image = $value->tab_image;
                    $slides = $value->tab_slides;
                    $videos = $value->tab_videos;
                    $related_phone = $value->tab_tablet_related;
                    $related_fit = $value->tab_fittings;
                    $forum = isset($value->tab_forum)?$value->tab_forum:"";
                    $related_news = $value->tab_news;
                    $package = $value->tab_package;
                    $detail = $value->tab_detail;
                    $detail_installment = isset($value->tab_detail_installment)?$value->tab_detail_installment:"";
                    $instock_status = $value->tab_instock_status;
                    $tags = $value->tab_tags;
                    $general_os = $value->tab_general_os;
                    $general_language = $value->tab_general_language;
                    $display_type = $value->tab_display_type;
                    $display_color = $value->tab_display_color;
                    $display_standard = $value->tab_display_standard;
                    $display_resolution = $value->tab_display_resolution;
                    $display_width = $value->tab_display_width;
                    $display_induction = $value->tab_display_induction;
                    $cam_primary = $value->tab_cam_primary;
                    $cam_second = $value->tab_cam_second;
                    $cam_flash = $value->tab_cam_flash;
                    $cam_feature = $value->tab_cam_feature;
                    $cam_filming = $value->tab_cam_filming;
                    $cam_videocall = $value->tab_cam_videocall;
                    $cpuram_speed = $value->tab_cpuram_speed;
                    $cpuram_core = $value->tab_cpuram_core;
                    $cpuram_chipset = $value->tab_cpuram_chipset;
                    $cpuram_ram = $value->tab_cpuram_ram;
                    $cpuram_graphic = $value->tab_cpuram_graphic;
                    $storage_contacts = $value->tab_storage_contacts;
                    $storage_rom = $value->tab_storage_rom;
                    $storage_memorycard = $value->tab_storage_memorycard;
                    $storage_memorycardsup = $value->tab_storage_memorycardsupport;
                    $design_style = $value->tab_design_style;
                    $design_size = $value->tab_design_size;
                    $design_weight = $value->tab_design_weight;
                    $pin_type = $value->tab_pin_type;
                    $pin_capacity = $value->tab_pin_capacity;
                    $pin_removable = $value->tab_pin_removable;
                    $connect_3g = $value->tab_connect_3g;
                    $connect_4g = $value->tab_connect_4g;
                    $connect_simtype = $value->tab_connect_simtype;
                    $connect_duasim = $value->tab_connect_duasim;
                    $connect_wifi = $value->tab_connect_wifi;
                    $connect_gps = $value->tab_connect_gps;
                    $connect_bluetouth = $value->tab_connect_bluetouth;
                    $connect_gprs = $value->tab_connect_gprs;
                    $connect_nfc = $value->tab_connect_nfc;
                    $connect_jack = $value->tab_connect_jack;
                    $connect_usb = $value->tab_connect_usb;
                    $connect_other = $value->tab_connect_other;
                    $connect_chargprot = $value->tab_connect_chargprot;
                    $entertainment_movie = $value->tab_entertainment_movie;
                    $entertainment_player = $value->tab_entertainment_player;
                    $entertainment_recording = $value->tab_entertainment_recording;
                    $entertainment_fm = $value->tab_entertainment_fm;
                    $entertainment_other = $value->tab_entertainment_other;
                    $showhome = $value->tab_showhome;
                    $order = $value->tab_order;
                    $status = $status = ($value->tab_active == 1)?1:2;
                    $created_at = date('Y-m-d H:i:s',$value->tab_date_add);
                    // $updated_at = date('Y-m-d H:i:s',$value->pho_date_edit);
                    $updated_at = ($value->tab_date_edit != null)?date('Y-m-d H:i:s',$value->tab_date_edit):$created_at;
                    $new = $value->tab_new;
                    $hot = $value->tab_hot;
                    $data_insert = compact('id','category_id','old','name','slug','price','price_old','price_sg','warranty','warranty_option','promotion','image','slides','videos','related_phone','related_fit','forum','related_news','package','detail','detail_installment','instock_status','tags','general_os','general_language','display_type','display_color','display_standard','display_resolution','display_width','display_induction','cam_primary','cam_second','cam_flash','cam_feature','cam_filming','cam_videocall','cpuram_speed','cpuram_core','cpuram_chipset','cpuram_ram','cpuram_graphic','storage_contacts','storage_rom','storage_memorycard','storage_memorycardsup','design_style','design_size','design_weight','pin_type','pin_capacity','pin_removable','connect_3g','connect_4g','connect_simtype','connect_duasim','connect_wifi','connect_gps','connect_bluetouth','connect_gprs','connect_nfc','connect_jack','connect_usb','connect_other','connect_chargprot','entertainment_movie','entertainment_player','entertainment_recording','entertainment_fm','entertainment_other','showhome','order','status','new','hot','created_at','updated_at');
                        DB::connection('mysql')->table('tablets')->insert($data_insert);
                }
                $phone_attribute_old = DB::connection('mysql_old')->table('tablet_attribute')->get();
                foreach($phone_attribute_old as $key => $value){
                    $phone_z = DB::connection('mysql_old')->table('tablet')->where('tab_id',$value->tab_phone_id)->first();
                    $tablet_id = $value->tab_phone_id;
                    $index = $value->tab_index;
                    $storage = $value->tab_storage;
                    $color_name = $value->tab_color_name;
                    $color_code = $value->tab_color_code;
                    $aspect = isset($value->tab_aspect)?$value->tab_aspect:"";
                    // $price = $value->tab_price;
                    $price = 0;

                    $image = isset($value->tab_image)?$value->tab_image:"";
                    if($phone_z){
                        $data_insert = compact('tablet_id','index','storage','color_name','color_code','aspect','price','image');
                        DB::connection('mysql')->table('tablet_attribute')->insert($data_insert);
                    }
                    
                }
                $this->info("Done");
                break;      
            case 'fits':
                $phone_cate = DB::connection('mysql_old')->table('categories')->where('cat_type','fittings')->get();
                foreach ($phone_cate as $key => $value) {
                    $id = $value->cat_id;
                    $parent_id = $value->cat_parent_id;
                    $name = $value->cat_name;
                    $slug = $value->cat_alias;
                    $detail = $value->cat_description;
                    $image = $value->cat_image;
                    $order = $value->cat_order;
                    $status = ($value->cat_active == 1)?1:2;
                    $created_at = $updated_at = null;
                    $data_insert = compact('id','parent_id','name','slug','detail','image','order','status','created_at','updated_at');
                    DB::connection('mysql')->table('fit_categories')->insert($data_insert);
                }
                $fit_old = DB::connection('mysql_old')->table('fittings')->get();
                foreach($fit_old as $key => $value){
                    $id = $value->fit_id;
                    $category_id = $value->fit_cat_id;
                    $name = $value->fit_name;
                    $slug = $value->fit_alias;
                    $image = $value->fit_image;
                    $price = $value->fit_price;
                    $warranty = $value->fit_warranty;
                    $info = $value->fit_info;
                    $related_fit = $value->fit_related;
                    $detail = $value->fit_detail;
                    $instock_status = $value->fit_instock_status;
                    $tags = $value->fit_tags;
                    $showhome = $value->fit_showhome;
                    $order = $value->fit_order;
                    $promotion = $value->fit_promotion;
                    $package = $value->fit_package;
                    $slides = $value->fit_slides;
                    $status = $status = ($value->fit_active == 1)?1:2;
                    $created_at = date('Y-m-d H:i:s',$value->fit_date_add);
                    $updated_at = ($value->fit_date_edit != null)?date('Y-m-d H:i:s',$value->fit_date_edit):$created_at;
                    $data_insert = compact('id','category_id','name','slug','image','price','warranty','info','related_fit','detail','instock_status','tags','showhome','order','promotion','package','slides','status','created_at','updated_at');
                    DB::connection('mysql')->table('fits')->insert($data_insert);
                }   
                $this->info("Done");
                break;
            case 'services':
                $phone_cate = DB::connection('mysql_old')->table('categories')->where('cat_type','fix_phone')->get();
                foreach ($phone_cate as $key => $value) {
                    $id = $value->cat_id;
                    $parent_id = $value->cat_parent_id;
                    $name = $value->cat_name;
                    $slug = $value->cat_alias;
                    $detail = $value->cat_description;
                    $image = $value->cat_image;
                    $order = $value->cat_order;
                    $status = ($value->cat_active == 1)?1:2;
                    $created_at = $updated_at = null;
                    $data_insert = compact('id','parent_id','name','slug','detail','image','order','status','created_at','updated_at');
                    DB::connection('mysql')->table('service_categories')->insert($data_insert);
                }
                $service_old = DB::connection('mysql_old')->table('fix_phone')->get();
                foreach($service_old as $key => $value){
                    
                    $id = $value->fix_id;
                    $category_id = $value->fix_cat_id;
                    $name = $value->fix_name;
                    $slug = $value->fix_alias;
                    $image = $value->fix_image;
                    $price = is_numeric ($value->fix_price)?$value->fix_price:0;
                    $price_table = $value->fix_price_table;
                    $option  = $value->fix_option;
                    $warranty = $value->fix_warranty;
                    $promotion = $value->fix_promotion;
                    $info = $value->fix_info;
                    $related_service = $value->fix_phone_related;
                    $related_news = $value->fix_news;
                    $related_fit = $value->fix_fittings;
                    $detail = $value->fix_detail;
                    $instock_status = $value->fix_instock_status;
                    $tags = $value->fix_tags;
                    $showhome = $value->fix_showhome;
                    $order = $value->fix_order;
                    // $package = $value->fix_package;
                    $slides = $value->fix_slides;
                    $videos = $value->fix_video;
                    $status = $status = ($value->fix_active == 1)?1:2;
                    $created_at = date('Y-m-d H:i:s',$value->fix_date_add);
                    $updated_at = ($value->fix_date_edit != null)?date('Y-m-d H:i:s',$value->fix_date_edit):$created_at;
                    $data_insert = compact('id','category_id','name','slug','image','price','price_table','option','warranty','promotion','info','related_service','related_news','related_fit','detail','instock_status','tags','showhome','order','package','slides','videos','status','created_at','updated_at');
                    DB::connection('mysql')->table('services')->insert($data_insert);
                }
                $this->info("Done");
                break;
            case 'news':
                $phone_cate = DB::connection('mysql_old')->table('categories')->where('cat_type','news')->get();
                foreach ($phone_cate as $key => $value) {
                    $id = $value->cat_id;
                    $parent_id = $value->cat_parent_id;
                    $name = $value->cat_name;
                    $slug = $value->cat_alias;
                    $detail = $value->cat_description;
                    $image = $value->cat_image;
                    $order = $value->cat_order;
                    $status = ($value->cat_active == 1)?1:2;
                    $created_at = $updated_at = null;
                    $data_insert = compact('id','parent_id','name','slug','detail','image','order','status','created_at','updated_at');
                    DB::connection('mysql')->table('news_categories')->insert($data_insert);
                }
                $news_old = DB::connection('mysql_old')->table('news')->get();
                foreach ($news_old as $key => $value) {
                    $id = $value->new_id;
                    $category_id = $value->new_id_cate;
                    $name = $value->new_name;
                    $slug = $value->new_alias;
                    $image = $value->new_image;
                    $detail = $value->new_detail;
                    $tags = $value->new_tags;
                    $status = $status = ($value->new_active == 1)?1:2;
                    $created_at = date('Y-m-d H:i:s',$value->new_date_add);
                    $updated_at = ($value->new_date_edit != null)?date('Y-m-d H:i:s',$value->new_date_edit):$created_at;
                    $data_insert = compact('id','category_id','name','detail','slug','image','status','tags','created_at','updated_at');
                    DB::connection('mysql')->table('news')->insert($data_insert);
                }
                $this->info("Done");
                break;
            case 'pages':
                $page_old = DB::connection('mysql_old')->table('page')->get();
                foreach($page_old as $key => $value){
                    $id = $value->pag_id;
                    $name = $value->pag_title;
                    $slug = $value->pag_alias;
                    $related_phone = $value->pag_relate_product;
                    $detail = $value->pag_detail;
                    $status = ($value->pag_active == 1)?1:2;
                    $created_at = $updated_at = date('Y-m-d H:i:s',$value->pag_date);
                    $data_insert = compact('id','name','slug','related_phone','detail','status','created_at','updated_at');
                    DB::connection('mysql')->table('pages')->insert($data_insert);
                }
                $this->info("Done!");
                break;
            case 'sync':
                $sync_old = DB::connection('mysql_old')->table('sync')->get();
                foreach($sync_old as $key => $value){
                    $old = $value->syn_old;
                    $new = $value->syn_new;
                    $created_at = $updated_at = null;
                    $data_insert = compact('old','new','created_at','updated_at');
                    DB::connection('mysql')->table('sync')->insert($data_insert);
                }
                $this->info("Done");
                break;
            case 'internal':
                $internal_old = DB::connection('mysql_old')->table('internal_link')->get();
                foreach($internal_old as $key => $value){
                    $id = $value->id;
                    $anchor = $value->anchor;
                    $title = $value->title;
                    $link = $value->link;
                    $status = $status = ($value->active == 1)?1:2;
                    $created_at = $updated_at = date('Y-m-d H:i:s',$value->last_choose);
                    $data_insert = compact('id','anchor','title','link','status','created_at','updated_at');
                    DB::connection('mysql')->table('internal_link')->insert($data_insert);
                }
                $this->info("Done!");
                break;         
            case 'order':
                $order_old = DB::connection('mysql_old')->table('orders')->get();
                foreach($order_old as $key => $value){
                    $id = $value->ord_id;
                    $name = $value->ord_name;
                    $gender = $value->ord_gender;
                    $phone = $value->ord_phone;
                    $email = $value->ord_email;
                    $address = $value->ord_address;
                    $note = $value->ord_note;
                    $created_at = $updated_at = date('Y-m-d H:i:s',$value->ord_date);
                    $status = $value->ord_active;
                    $data_insert = compact('id','name','gender','phone','email','address','note','status','created_at','updated_at');
                    DB::connection('mysql')->table('orders')->insert($data_insert);
                }
                
                foreach($order_old as $key => $value){
                    $order_id = $value->ord_id;
                    $info = $value->ord_info;
                    $product_id = $value->ord_product_id;
                    $type = $value->ord_type;
                    switch ($type) {
                        case 'phone':
                            $type = "phones";
                            break;
                        case 'phoneiment':
                            $type = "phoneiment";
                            break;
                        case 'tablet':
                            $type = "tablets";
                            break;
                        case 'tabletiment':
                            $type = "tabletiment";
                            break;
                        case 'fittings':
                            $type = "fits";
                            break;
                        case 'services':
                            $type = "services";
                            break;
                        case 'servicesiment':
                            $type = "servicesiment";
                            break;
                        default:
                            # code...
                            break;
                    }
                    $link = $value->ord_link;
                    $location = $value->ord_location;
                    $price = $value->ord_price;
                    $data_insert = compact('order_id','info','product_id','type','link','location','price');
                    DB::connection('mysql')->table('order_detail')->insert($data_insert);
                }
                $this->info("Done");
                break;
            
            case 'comment':
                $comment_old = DB::connection('mysql_old')->table('comment')->get();
                $rep_old = DB::connection('mysql_old')->table('reply')->get();
                foreach($comment_old as $key => $value){
                    switch ($value->com_table) {
                        case 'fix_phone':
                            $type = "services";
                            break;
                        case 'phone':
                            $type = "phones";
                            break;
                        case 'fittings':
                            $type = "fits";
                            break;
                        case 'tablet':
                            $type = "tablets";
                            break;
                        default:
                            $type = "";
                            break;
                    }
                    $id = $value->com_id;
                    $type_id = $value->com_id_baiviet;
                    $name = $value->com_name;
                    $email = $value->com_email;
                    $like = $value->com_like;
                    $rank = $value->com_rank;
                    $content = $value->com_detail;
                    $created_at = $updated_at = date('Y-m-d H:i:s',$value->com_date);
                    $status = ($value->com_active == 1)?1:2;
                    $data_insert = compact('type','type_id','name','email','like','rank','content','created_at','updated_at','status');
                    DB::connection('mysql')->table('comment')->insert($data_insert);
                }
                foreach($rep_old as $key => $value){
                    $type_id = $value->rep_id_baiviet;
                    $name = $value->rep_name;
                    $email = $value->rep_email;
                    $like = $value->rep_like;
                    $parent_id = $value->rep_id_coment;
                    $content = $value->rep_detail;
                    $admin_id = $value->rep_admin_id;
                    $created_at = $updated_at = date('Y-m-d H:i:s',$value->rep_date);
                    $status = ($value->rep_active == 1)?1:2;
                    $data_insert = compact('type_id','name','email','like','parent_id','content','admin_id','created_at','updated_at','status');
                    DB::connection('mysql')->table('comment')->insert($data_insert);
                }
                break; 
            case 'slides':
                $slides_old = DB::connection('mysql_old')->table('slides')->get();
                foreach($slides_old as $k => $v){
                    $id = $v->sli_id;
                    $type = $v->sli_type;
                    $name = $v->sli_name;
                    $image = $v->sli_image;
                    $link = $v->sli_link;
                    $order = $v->sli_order;
                    $created_at = $updated_at = null;
                    $status = ($v->sli_active == 1)?1:2;
                    $data_insert = compact('id','type','name','link','image','order','status','created_at','updated_at');
                    DB::connection('mysql')->table('slides')->insert($data_insert);
                }
                break;

            case 'order_location':
                $order_detail = DB::table('order_detail')->get();
                foreach ($order_detail as $key => $value) {
                    $location = ($value->location == 0 || $value->location == null)?1:$value->location;
                    $order_id = $value->order_id;
                    $order = DB::table('orders')->where('status','<>',4)->where('id',$order_id)->first();
                    if($order){
                        DB::table('orders')->where('id', $order->id)->update(['location_id' => $location]);
                    }
                }
                break;
            
            case 'order_type':
                $order_detail = DB::table('order_detail')->get();
                foreach ($order_detail as $key => $value) {
                    $type = $value->type;
                    $order_id = $value->order_id;
                    $order = DB::table('orders')->where('status','<>',4)->where('id',$order_id)->first();
                    if($order){
                        DB::table('orders')->where('id', $order->id)->update(['type' => $type]);
                    }
                }
                break;
            case 'move_order_detail':
                $order_detail = DB::table('order_detail')->get();
                foreach ($order_detail as $key => $value) {
                    $info = $value->info;
                    $link = $value->link;
                    $price = $value->price;
                    $order_id = $value->order_id;
                    $order = DB::table('orders')->where('status','<>',4)->where('id',$order_id)->first();
                    if($order){
                        DB::table('orders')->where('id', $order->id)->update(['info' => $info,'link' => $link,'price' => $price]);
                    }
                }
                break;
            default:
                $this->info("DEFAULT");
                break;
        }
    }
}
