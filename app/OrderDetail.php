<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    //
    protected $table = "order_detail";
    public $timestamps = false;
    protected $fillable = [
        'order_id', 'product_id', 'info', 'type', 'link', 'location', 'price', 'status',
    ];

    public static function addOrderDetail($order_id,$request)
    {
    	switch ($request->name) {
    		case 'phones':
    			$info = $request->product_name." - Màu: ".$request->info[0]['color_name']." - DL: ".$request->info[0]['storage']." - Tình Trạng: ".$request->info[0]['aspect']." - Bảo hành: ".$request->warranty;
    		break;
            case 'phoneiment':
                $info = $request->product_name." - Màu: ".$request->info[0]['color_name']." - DL: ".$request->info[0]['storage']." - Tình Trạng: ".$request->info[0]['aspect']." - Bảo hành: ".$request->warranty." - ".$request->pay_before." - ".$request->pay_month;
            break;
            case 'fits':
                $info = $request->product_name." - Bảo hành: ".$request->warranty;
            break;
            case 'tablets':
                $info = $request->product_name." - Màu: ".$request->info[0]['color_name']." - DL: ".$request->info[0]['storage']." - Tình Trạng: ".$request->info[0]['aspect']." - Bảo hành: ".$request->warranty;
            break;
            case 'tabletiment':
                $info = $request->product_name." - Màu: ".$request->info[0]['color_name']." - DL: ".$request->info[0]['storage']." - Tình Trạng: ".$request->info[0]['aspect']." - Bảo hành: ".$request->warranty." - ".$request->pay_before." - ".$request->pay_month;
            break;
            case 'services':
                $info = "Dịch vụ: ".$request->product_name." - Linh kiện: ".$request->service_name." - Giá: ".$request->price_serice;
            break;
            case 'servicesiment':
                $info = "Dịch vụ: ".$request->product_name." - Linh kiện: ".$request->service_name." - Giá: ".$request->price_serice;
            break;
    	}
    	
    	OrderDetail::create([
            'order_id' => $order_id,
            'product_id' => $request->product_id,
            'info' => $info,
            'type' => $request->name,
            'link' => $request->link,
            'location' => $request->cuslocation,
            'price' => $request->price,
            'status' => 1,
        ]);
    }
}
