<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    public function getImage($size = '') {
        if($this->image != '') {
            return image_by_link($this->image,$size);
        }else
            return '/assets/img/no-image.png';
    }

    public function getUrl() {
        return route('web.price.index',$this->slug);
    }
}
