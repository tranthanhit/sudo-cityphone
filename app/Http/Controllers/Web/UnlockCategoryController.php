<?php

namespace App\Http\Controllers\Web;

use App\Fit;
use App\News;
use App\Unlock;
use App\UnlockCategory;
use DB;
use Illuminate\Http\Request;
use App\Comment;

class UnlockCategoryController extends Controller
{
    public function showall()
    {
        $unlocks = Unlock::where('status', 1)->where('instock_status', '!=', 5)->select('id', 'name', 'slug', 'image', 'price', 'promotion')->orderBy('instock_status')->orderBy('price', 'DESC')->take(20)->get();
        $unlock_categories = UnlockCategory::where('parent_id', 0)->where('status', 1)->orderBy('order', 'asc')->select('id', 'name', 'slug')->take(7)->get();
        $location = (isset($request->setlocation) == false) ? 1 : $request->setlocation;


        $config_promotion = DB::table('options')->select('value')->where('name', 'promotion')->first();
        if ($config_promotion) {
            $config_promotion = json_decode(base64_decode($config_promotion->value), true);
        }
        $config_promotion = $config_promotion["promotion_default_service"];
        $config_promotion = preg_split('/\n|\r\n/', $config_promotion);

        $config_seo = DB::table('options')->select('value')->where('name', 'seo_category')->first();

        if ($config_seo) {
            $config_seo = json_decode(base64_decode($config_seo->value), true);
        }
        $meta_seo = $this->meta_seo('', 0, [
            'title' => (@$config_seo["title_unlock"] != null) ? @$config_seo["title_unlock"] : "Unlock, mở mạng" . ' - Cityphone',
            'description' => (@$config_seo["desc_unlock"] != null) ? @$config_seo["desc_unlock"] : "Unlock, mở mạng",
            'url' => route('web.unlock_categories.showall')
        ]);

        $breadcrumbs = [
            ['name' => 'Unlock mở mạng', 'url' => route('web.unlock_categories.showall')],
        ];
        return view('web.unlock_categories.showall', compact('unlocks', 'unlock_categories', 'config_promotion', 'meta_seo', 'config_seo', 'location', 'breadcrumbs'));
    }

    public function index(Request $request, $slug = null)
    {
        $location = (isset($request->setlocation) == false) ? 1 : $request->setlocation;
        $unlock_category = UnlockCategory::where('slug', $slug)->where('status',1)->first();
        $unlock_categories = UnlockCategory::where('parent_id', 0)->where('status', 1)->orderBy('order', 'asc')->select('id', 'name', 'slug')->take(7)->get();
        $unlock_category_childs = UnlockCategory::where('parent_id', $unlock_category->id)->where('status', 1)->orderBy('order', 'asc')->select('id', 'name', 'slug')->take(7)->get();
        $unlock_category_childs_id = [];
        array_push($unlock_category_childs_id, $unlock_category->id);
        foreach ($unlock_category_childs as $value) {
            array_push($unlock_category_childs_id, $value->id);
        }
        $unlocks = Unlock::wherein('category_id', $unlock_category_childs_id)->where('status', 1)->where('instock_status', '!=', 5)->select('id', 'name', 'slug', 'image', 'price', 'promotion')->orderBy('price', 'DESC')->take(20)->get();

        $config_promotion = DB::table('options')->select('value')->where('name', 'promotion')->first();
        if ($config_promotion) {
            $config_promotion = json_decode(base64_decode($config_promotion->value), true);
        }
        $config_promotion = $config_promotion["promotion_default_service"];
        $config_promotion = preg_split('/\n|\r\n/', $config_promotion);

        $meta_seo = $this->meta_seo('unlock_categories', $unlock_category->id, [
            'title' => $unlock_category->name . ' - Cityphone',
            'description' => cutString(removeHTML($unlock_category->detail), 150),
            'url' => route('web.unlock_categories.showall', $unlock_category->slug),
        ]);
        $breadcrumbs = [
            ['name' => 'Unlock mở mạng', 'url' => route('web.unlock_categories.showall')],
            ['name' => $unlock_category->name, 'url' => route('web.unlock_categories.show', ['slug' => $unlock_category->slug])],
        ];
        return view('web.unlock_categories.index', compact('unlocks', 'unlock_category', 'unlock_category_childs', 'config_promotion', 'meta_seo', 'location', 'breadcrumbs','unlock_categories'));
    }
}
