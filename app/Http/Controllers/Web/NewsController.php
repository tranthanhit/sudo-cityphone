<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;

use App\News;
use App\Phone;
use App\NewsCategory;
use App\Comment;
use DB;
use App\Service;
class NewsController extends Controller
{
    public function showall()
    {
        $categories = NewsCategory::where('status',1)->get();

        $new_category_childs_id = [];
        foreach ($categories as $value) {
            array_push($new_category_childs_id, $value->id);
        }

        $news = News::wherein('category_id', $new_category_childs_id)->where('category_id','!=',0)->where('status', 1)->select('id', 'name', 'slug', 'image', 'detail','category_id','created_at')->orderBy('id', 'DESC')->with('category')->paginate(10);

        $config_seo = DB::table('options')->select('value')->where('name','seo_category')->first();
        if($config_seo){
            $config_seo = json_decode(base64_decode($config_seo->value),true);
        }

        $news_highlight = News::where('status',1)->orderBy('type_id','desc')
            ->leftJoin('pins', 'news.id', '=', 'pins.type_id')->where('type','news')->where('place','highlight')
            ->limit(5)->get();
         
        $list_services_highlight = Service::where('status',1)
            ->leftJoin('pins', 'services.id', '=', 'pins.type_id')->where('type','services')->where('place','highlight')->orderBy('pins.value','asc')
            ->limit(5)->get();
        $meta_seo = $this->meta_seo('',0,[
            'title' => ($config_seo["title_news"] != null)?$config_seo["title_news"]:"Tin tức - CityPhone",
            'description'=> ($config_seo["desc_news"] != null)?$config_seo["desc_news"]:"Tin tức - CityPhone",
            'url' => route('web.news_categories.showall')
        ]);
        
        // $news_phone = Phone::where('status',1)->where('instock_status','!=',5)->orderBy('id','desc')->limit(5)->get();
        $news_km = News::where('status',1)->where('category_id',116)->orderBy('updated_at','desc')->limit(5)->get();
       
     
        $breadcrumbs = [
            ['name'=>'Tin tức','url'=>route('web.news_categories.showall')],
        ];
        return view('web.news_categories.showall', compact('news','categories','meta_seo','news_highlight','news_km','meta_seo','breadcrumbs','list_services_highlight'));
    }

    public function index($slug = null)
    {
        $category = NewsCategory::where('slug',$slug)->first();
        $categories = NewsCategory::where('status',1)->get();
        $news = News::where('category_id',$category->id)->where('category_id','!=',0)->where('status', 1)->select('id', 'name', 'slug', 'image', 'detail','category_id','created_at')->orderBy('created_at', 'DESC')->with('category')->paginate(10);
        $meta_seo = $this->meta_seo('news_categories',$category->id,[
            'title' => $category->name.' - CityPhone',
            'description'=>($category->detail != null)?removeHTML(cutString($category->detail,150)):$category->name ,
            'url' => route('web.news_categories.show',$category->slug)
        ]);
       
        $news_km = News::where('status',1)->where('category_id',116)->orderBy('updated_at','desc')->limit(5)->get();

        $breadcrumbs = [
            ['name'=>'Tin tức','url'=>route('web.news_categories.showall')],
            ['name'=>$category->name,'url'=>route('web.news_categories.show', ['slug' => $category->slug])],
        ];
        return view('web.news_categories.index', compact('news','category', 'categories','meta_seo','news_km','breadcrumbs'));
    }
    public function show($slug)
    {

        $news = News::where('slug',$slug)->where('status',1)->firstOrFail();
        $new_category = NewsCategory::where('id', $news->category_id)->first();

        $meta_seo = $this->meta_seo('news',$news->id,[
            'title' => $news->name.' - CityPhone',
            'description'=>($news->detail != null)?cutString(removeHTML($news->detail),150):$news->name ,
            'url' => route('web.news.show',$news->slug),
            'image'=> $news->getImage(),
        ]);
        $admin_bar_edit = route('news.edit',$news->id);

        $news_related = News::where('status',1)->where('id','!=',$news->id)->where('category_id',$news->category_id)->orderBy('updated_at','DESC')->limit(3)->get();


        $rating = $news->getRatings();
        if($rating->count) {
            $rank_count = $rating->count;
            $rank_peren = round($rating->avg,1);
        } else {
            $rank_count = 1;
            $rank_peren = 5;
        }

        if ($new_category == null) {
            $breadcrumbs = [
                ['name'=>'Tin tức','url'=>route('web.news_categories.showall')],
            ];
        } else {
            $breadcrumbs = [
                ['name'=>'Tin tức','url'=>route('web.news_categories.showall')],
                ['name'=>$new_category->name,'url'=>route('web.news_categories.show', ['slug' => $new_category->slug])],
            ];
        }

        return view('web.news.index', compact('news','meta_seo','admin_bar_edit','new_category','news_related', 'rank_count','rank_peren','breadcrumbs'));

    }
    public function vote_star(Request $request)
    {
        $comment = new Comment;
        $comment->name = "Vote Star";
        $comment->type = $request->type;
        $comment->type_id = $request->type_id;
        $comment->parent_id = 0;
        $comment->admin_id = 0;
        $comment->content = "Vote Star";
        $comment->email = "";
        $comment->like = 0;
        $comment->status = 1;
        $comment->gender = 1;
        $comment->rank = $request->rank;
        $comment->save();

        $comment_info = DB::table('comment')->where('type', $request->type)->where('type_id',$request->type_id)->where('status',1)->select('id','rank')->get();
        $rank_count = count($comment_info);
        $rating = 0;
        foreach ($comment_info as $value) {
            $rating += $value->rank;
        }
        if ($rank_count != 0) {
            $rank_count = $rank_count;
            $rank_percen = round($rating/$rank_count);
        } else {
            $rank_count = 0;
            $rank_percen = 0;
        }
        return [$rank_count,$rank_percen];
    }
}
