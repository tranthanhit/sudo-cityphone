<?php

namespace App\Http\Controllers\Web;
use DB;
use Illuminate\Http\Request;
use App\Service;
use App\ServiceCategory;
use App\ServiceCategoryMap;
use App\Classe;
class ServiceCategoryController extends Controller
{
    public function showall()
    {
        $config_seo_category = $this->getOptions('seo_category');
        $home_color = 1;
       
        $services = Service::where('status', 1)->where('instock_status','!=',5)->select('id', 'name', 'slug', 'image', 'price', 'promotion');

        $services_categories = ServiceCategory::where('parent_id', 0)->where('status', 1)->orderBy('order','asc')->select('id','name', 'slug')->take(10)->get();
        $location = (isset($request->setlocation) == false)?1:$request->setlocation;

        $service_categories_brand = collect(ServiceCategory::where('status',1)->select('brand')->get() )->unique('brand');
        $brand = @$_GET['brand'] ;
        if(isset($brand) && $brand != '' ){
            $service_categorie = ServiceCategory::where('status',1)->where('brand',$brand)->pluck('id','id')->toArray();
            if(empty($service_categorie)){
                return back();
            }
          
            $services =  $services->whereIn('category_id',$service_categorie);
        }
        $services = $services->orderBy('instock_status')->orderBy('price','DESC')->take(12)->get();

        $config_promotion = DB::table('options')->select('value')->where('name','promotion')->first();
        if($config_promotion){
            $config_promotion = json_decode(base64_decode($config_promotion->value),true);
        }
        $config_promotion = $config_promotion["promotion_default_service"];
        $config_promotion = preg_split('/\n|\r\n/',$config_promotion);
        
        $config_seo = DB::table('options')->select('value')->where('name','seo_category')->first();

        if($config_seo){
            $config_seo = json_decode(base64_decode($config_seo->value),true);
        }
        $meta_seo = $this->meta_seo('',0,[
            'title' => ($config_seo_category["title_service"] != null)?$config_seo_category["title_service"]:"Dịch vụ sửa chữa".' - Cityphone',
            'description'=> ($config_seo_category["desc_service"] != null)?$config_seo_category["desc_service"]:"Dịch vụ sửa chữa",
            'url' => route('web.services_categories.showall')
        ]);

        $breadcrumbs = [
            ['name'=>'Sửa chữa điện thoại','url'=>route('web.services_categories.showall')],
        ];
        return view('web.services_categories.showall', compact('services', 'services_categories', 'config_promotion','meta_seo','config_seo','location','breadcrumbs','home_color','service_categories_brand'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($slug = null)
    {   
         $config_seo_category = $this->getOptions('seo_category');
        $home_color = 1;
        $location = (isset($request->setlocation) == false)?1:$request->setlocation;
        $service_category = ServiceCategory::where('slug', $slug)->where('status',1)->first();
        if(is_null($service_category)){
           return view('web.page_not_found');
        }
        $service_category_parent = ServiceCategory::where('id',$service_category->parent_id)->where('status',1)->first();

        if($service_category->parent_id == 0){
            $services_categories = ServiceCategory::where('status',1)->where('parent_id', $service_category->id)
            ->get();
        }else{
            $services_categories = ServiceCategory::where('status',1)->where('parent_id', $service_category->parent_id)
           ->get();
        }
        $service_category_childs = ServiceCategory::where('parent_id', $service_category->id)->where('status', 1)->orderBy('order','asc')->select('id','name', 'slug')->take(7)->get();
        $service_category_childs_id = [];
        array_push($service_category_childs_id, $service_category->id);
        foreach ($service_category_childs as $value) {
            array_push($service_category_childs_id, $value->id);
        }
        $services = Service::wherein('category_id', $service_category_childs_id)->where('status', 1)->where('instock_status','!=',5)->orderBy('price', 'DESC');

        $classes = Classe::where('status',1)->whereIn('id',$services->pluck('class_id','class_id')->toArray())->get();

        if(isset($_GET['request']) && $_GET['request'] != '' ){
            $class = Classe::where('status',1)->where('slug',$_GET['request'])->first();
            if(empty($class)){
                return back();
            }
            $services =  $services->where('class_id',$class->id);
        }
        $services =   $services->take(12)->get();

        $config_promotion = DB::table('options')->select('value')->where('name','promotion')->first();
        if($config_promotion){
            $config_promotion = json_decode(base64_decode($config_promotion->value),true);
        }
        $config_promotion = $config_promotion["promotion_default_service"];
        $config_promotion = preg_split('/\n|\r\n/',$config_promotion);

        $meta_seo = $this->meta_seo('service_categories',$service_category->id,[
            'title' => $service_category->name ?? $config_seo_category["title_service"] .' - Cityphone',
            'description'=> cutString(removeHTML($service_category->detail ?? $config_seo_category["desc_service"]),150),
            'url' => route('web.services_categories.show',$service_category->slug),
        ]);


        $breadcrumbs = [
            ['name'=>'Sửa chữa điện thoại','url'=>route('web.services_categories.showall')],
           
        ];
        if($service_category_parent) {
            $breadcrumbs[] = ['name'=>$service_category_parent->name,'url'=>route('web.services_categories.show', ['slug'=>$service_category_parent->slug])];
        }
        $slug = $service_category->slug;
        return view('web.services_categories.index', compact('services' , 'service_category', 'config_promotion','meta_seo','location','breadcrumbs','services_categories','home_color','classes','slug'));
    }
}
