<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;

use App\Order;
use Cart;
use DB;
use Mail;
use App\Mail\OrderEmail;
class OrderController extends Controller
{
   public function cart(Request $request)
   {
    $cart_content = Cart::content();
    $meta_seo = $this->meta_seo('',0,[
        'title' => 'Giỏ hàng của bạn',
        'description'=> 'Giỏ hàng của bạn',
        'url' => url('/gio-hang.html'),
        'image' => url('/').'/assets/img/logo.png'
    ]);
    $breadcrumbs = [
        ['name'=>'Giỏi hàng','url'=>'#'],
    ];
    return view('web.orders.cart',compact('cart_content','meta_seo','breadcrumbs'));
   }
   // xóa giỏi hàng
   public function removeItemCart(Request $request)
   {
       if($request->ajax()){
            $rowid = $request->rowid;
            Cart::remove($rowid);
            $cart_count = Cart::count();
            return response(json_encode(compact('cart_count'))); 
       }
   }
   //thêm bớt vào giói hàng
   public function updateItemCart(Request $request)
   {
        if($request->ajax()){
            $rowid = $request->rowid;
            $qty = $request->qty;
            Cart::update($rowid,$qty);
            $cart_count = Cart::count();
            $cart_that = Cart::get($rowid);
            $subtotal = $cart_that->subtotal();
            $qty = $cart_that->qty;
            return response(json_encode(compact('cart_count','subtotal','qty')));
        }
   }
   //dat hang
   public function checkout()
   {
        $home_color = 1;
        $cart_content = Cart::content();
        $meta_seo = $this->meta_seo('',0,[
            'title' => 'Đặt hàng',
            'description'=> 'Đặt hàng',
            'url' => url('/dat-hang.html'),
            'image' => url('/').'/assets/img/logo.png'
        ]);
        $breadcrumbs = [
            ['name'=>'Đặt hàng','url'=>'#'],
        ];
        return view('web.orders.checkout',compact('cart_content','meta_seo','home_color','breadcrumbs'));
   }
   public function postOrder(Request $request)
   {
        $data_order = $request->all();
        $sex = (isset($data_order["sex"]))?$data_order["sex"]:1;
        $payment = (isset($data_order["pay"]))?$data_order["pay"]:'cod';
        $note = (isset($data_order["note"]))?$data_order["note"]:'';
        $name = $data_order["name"];
        $phone = $data_order["phone"];
        $email = $data_order["email"];
        $address = $data_order["address"];
        if(Cart::count()){
            $order_id = DB::table('orders')->insertGetId(
                [
                    'name' => $name,
                    'phone' => $phone,
                    'email' => $email,
                    'note' => $note,
                    'gender' => $sex,
                    'payment' => $payment,
                    'address' => $address,
                    'type'=>'fits',
                    // 'qty' => ,
                    'status'=>0,
                    'created_at' => now(),
                    'updated_at' => now(),
                ]
            );
            foreach(Cart::content() as $value){
                DB::table('order_detail')->insert([
                    'order_id' => $order_id,
                    'product_id' => $value->id,
                    'quantity' => $value->qty,
                    'price' => $value->price,
                    'total' => $value->price * $value->qty,
                    'type' => 'fits',
                ]);
            }
        }
        // Cart::destroy();
        $this->send_email('',new OrderEmail([
            'name' => $name,
            'phone' => $phone,
            'email' => $email,
            'note' => $note,
            'gender' => $sex,
            'payment' => $payment,
            'address' => $address,
            'type'=>'fits',
            'quantity' => $value->qty,
            'total' => $value->price * $value->qty,
        ]));
        // $this->send_email($email,new TestEmail(['email'=>$email,'name'=>'sudo'.config('app.name')]));
        return view('web.orders.order_success');
   }
}
