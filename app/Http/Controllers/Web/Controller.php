<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as BaseController;
use Illuminate\Support\Facades\Input;
use DB;
use View;
use Cache;
use Session;
use App\Comment;

use Image;
use Storage;
use Mail;

class Controller extends BaseController
{
    public function __construct()
    {

        if ( empty($_SERVER['HTTP_USER_AGENT']) ) {
            $is_mobile = false;
        } elseif ( strpos($_SERVER['HTTP_USER_AGENT'], 'Mobile') !== false // many mobile devices (all iPhone, iPad, etc.)
            || strpos($_SERVER['HTTP_USER_AGENT'], 'Android') !== false
            || strpos($_SERVER['HTTP_USER_AGENT'], 'Silk/') !== false
            || strpos($_SERVER['HTTP_USER_AGENT'], 'Kindle') !== false
            || strpos($_SERVER['HTTP_USER_AGENT'], 'BlackBerry') !== false
            || strpos($_SERVER['HTTP_USER_AGENT'], 'Opera Mini') !== false
            || strpos($_SERVER['HTTP_USER_AGENT'], 'Opera Mobi') !== false ) {
            $is_mobile = true;
        } else {
            $is_mobile = false;
        }
     
        
        // if (Cache::has('menu_primary')) {
        //     $menu_primary = Cache::get('menu_primary');
        // } else {
            $menu_primary = $this->ChildrenCategoryOption('general', 'menu_primary');
        //     Cache::forever('menu_primary', $menu_primary);
        // }

    

        // if (Cache::has('menu_header')) {
        //     $menu_footer = Cache::get('menu_header');
        // } else {
            $menu_header = DB::table('options')->select('value')->where('name','menu_header')->first();
        //     Cache::forever('menu_header', $menu_header);
        // }
      
        // dd($menu_header);

        //constructor cho web
        //parent construct
        //parent::__construct();
        $config_general = $this->getOptions('general');
        $config_promotion = $this->getOptions('promotion');
        $config_metacode = $this->getOptions('meta_code');
        $config_boxchat = $this->getOptions('box_chat');

        $config_seo_category = $this->getOptions('seo_category');

        $cate_phone = Cache::rememberForever('cate_phone', function() {
            return DB::table('phone_categories')->where('status',1)->orderBy('order','asc')->get();
        });

        $cate_tablet = Cache::rememberForever('cate_tablet', function() {
            return DB::table('tablet_categories')->where('status',1)->orderBy('order','asc')->get();
        });

        $fit_categories = Cache::rememberForever('fit_categories', function() {
            return DB::table('fit_categories')->where('parent_id',0)->where('status',1)->orderBy('order','asc')->get();
        });

        $service_categories = Cache::rememberForever('service_categories', function() {
            return DB::table('service_categories')->where('parent_id',0)->where('status',1)->orderBy('order','asc')->get();
        });

        $unlock_categories = Cache::rememberForever('unlock_categories', function() {
            return DB::table('unlock_categories')->where('parent_id',0)->where('status',1)->orderBy('order','asc')->get();
        });
        
        $news_categories = Cache::rememberForever('news_categories', function() {
            return DB::table('news_categories')->where('parent_id',0)->where('status',1)->orderBy('order','asc')->get();
        });


       
        if(Cache::has('footer_list')) {
            $footer_list = Cache::get('footer_list');
        }else {
            $footer_pin_array = DB::table('pins')->where('type', 'pages')->where('place','footer')->orderBy('value','DESC')->get();
            $footer_list_array = [];
            foreach($footer_pin_array as $footer_pin) {
                array_push($footer_list_array, $footer_pin->type_id);
            }
            $footer_list = DB::table('pages')->wherein('id',$footer_list_array)->where('status',1)->get();
            Cache::forever('footer_list',$footer_list);
        }
      

        $locations = Cache::rememberForever('locations', function() {
            return DB::table('location')->where('status',1)->orderBy('id','asc')->get();
        });
        $address = Cache::rememberForever('address', function() {
            return DB::table('address')->where('status',1)->get();
        });

      

        if (request()->cookie('location_name')) {
            $location_name = request()->cookie('location_name');
        } else {
            $location_name = "Hà Nội";
        }
        if (request()->cookie('location')) {
            $location_id = request()->cookie('location');
        } else {
            $location_id = 1;
        }
      

        $get_ip = get_client_ip();
        

        if (request()->cookie('vote')) {
            $vote = request()->cookie('vote');
            $vote_array = explode(',', $vote);
            if ($get_ip == $vote_array[0]) {
                $vote_star = $vote_array[1];
            } else {
                $vote_star = 0;
            }
        } else {
            $vote_star = 0;
        }
       

        if(isset($_COOKIE['location'])) {
            $active_location = false;
            foreach ($locations as $value) {
                if($value->id == $_COOKIE['location']) {
                    $active_location = true;
                    break;
                }
            }
            if(!$active_location) {
                setcookie('location',1,(time()+365*24*60*60),'/');
                $_COOKIE['location'] = 1;
            }
        }

        $location = Input::get('location',0);
        if ($location != 0 ) {
            setcookie('location',$location,(time()+365*24*60*60),'/');
            if (isset($_SERVER['REQUEST_URI']))
                return redirect(str_replace('?'.$_SERVER['QUERY_STRING'], '', $_SERVER['REQUEST_URI']))->send();
            if (isset($_SERVER['HTTP_REFERER']))
                return redirect($_SERVER['HTTP_REFERER'])->send();
            return  response()->redirect('/')->send();
        }

        View::share('is_mobile', $is_mobile);
        View::share(compact('vote_star'));
        View::share(compact('get_ip'));
        View::share(compact('location_id','location_name'));
        View::share('locations',$locations);
        View::share('address',$address);
        View::share('footer_list',$footer_list);
        View::share(compact('config_general','config_promotion','cate_phone','cate_tablet','fit_categories','service_categories','news_categories','config_metacode','config_boxchat','unlock_categories','config_seo_category'));
        View::share('menu_header', $menu_header);
        View::share('menu_primary', $menu_primary);

      

    }

    /**
     * @param string $type - tên kiểu trong bảng meta_seo
     * @param int $id - id trong bảng meta_seo
     * @param array $options - các trường mặc định hoặc không có trong bảng meta_seo: title | description | robots | type | url | image |
     */
    public function meta_seo($type='',$id=0,$options) {
        $meta_seo = [];
        if(count($options)) {
            foreach ($options as $key=>$value) {
                $meta_seo[$key] = $value;
            }
        }
        if ($type != '' && $id != 0) {
            $data_seo = DB::table('meta_seo')->where('type',$type)->where('type_id',$id)->first();
            if($data_seo) {
                if($data_seo->title!=''){
                    $meta_seo['title'] = $data_seo->title;
                }
                if($data_seo->description!=''){
                    $meta_seo['description'] = $data_seo->description;
                }
                $meta_seo['robots'] = $data_seo->robots;
            }
        }
        return $meta_seo;
    }

   

    public function ChildrenCategoryOption($setting, $data)
    {
        $category = DB::table('options')->select('value')->where('name', $setting)->first();
        $category = json_decode(base64_decode($category->value), true);
        $data = json_decode($category[$data]);
        foreach ($data as $value) {
            if (!empty($value->children)) {
                $children = $value->children;
                $value->children = $children;
            }
        }
        return $data;
    }
    public function send_email($email = '' ,$fun_mail)
    {
        $option = Cache::remember('setting_mail_configs', 300, function() {
            return DB::table('options')->select('value')->where('name','mail_configs')->first();
        });
        if(empty($option)){
            $data = [];
        }else{
            $data = json_decode(base64_decode($option->value),true);
        }
        $config_general = $this->getOptions('general');
        config([
            'mail.driver'           => $data['driver']??config('mail.driver'),
            'mail.host'             => $data['host']??config('mail.host'),
            'mail.port'             => $data['port']??config('mail.port'),
            'mail.from.address'     => $data['from_address']??config('mail.from.address'),
            'mail.from.name'        => $data['from_name']??config('mail.from.name'),
            'mail.encryption'       => $data['encryption']??config('mail.encryption'),
            'mail.username'         => $data['username']??config('mail.username'),
            'mail.password'         => $data['password']??config('mail.password'),
            'mail.sendmail'         => $data['sendmail']??config('mail.sendmail')
        ]);
        if($email == ''){
            $email = $config_general['email_order'];
        }
        try {
            // new TestEmail(['email'=>$email,'name'=>'sudo'.config('app.name')])
            Mail::to($email)->send($fun_mail);
            return [
                'status' => 1,
                'message' => 'Gửi thành công',
            ];
            // if (verify_email_org($email)) {
            // } return [
            //     'status' => 2,
            //     'message' => 'Email không tồn tại'
            // ];
        } catch (Exception $e) {
            return [
                'status' => 2,
                'message' => 'Gửi thất bại',
                'error' => $e->message(),
            ];
        }


    }

    public function getOptions($name) {
        $cache_name = 'options_'.$name;
        if(Cache::has($cache_name)) {
            return Cache::get($cache_name);
        }else {
            $config = DB::table('options')->select('value')->where('name',$name)->first();
            if($config) {
                $config = json_decode(base64_decode($config->value),true);
                Cache::forever($cache_name,$config);
                return $config;
            }
        }
        return;
    }

}
