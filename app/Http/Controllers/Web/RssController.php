<?php
namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use Barryvdh\Debugbar\Facade as Debugbar;
use App\Http\Controllers\Web\Controller;
use App\Phone;
use App\Tablet;
use App\Fit;
use App\Service;
use App\News;
class RssController extends Controller
{	
	public function index()
	{
		$meta_seo = $this->meta_seo('rss',0,[
            'title' => 'RSS - Cityphone',
            'url' => route('rss'),
        ]);
		return view('web.rss.rss',compact('meta_seo'));
	}

	public function build($template) {
        $content = '
        	<rss version="2.0">
        	<channel>
        		'.$template.'
			</channel>
			</rss>
        ';
        return response($content, '200')->header('Content-Type', 'text/xml');
    }

    public function phone() {
    	$template = '
			<title>Cityphone - Điện thoại</title>
			<copyright>'.route('web.home').'</copyright>
			<generator>Cityphone</generator>
			<link>'.route('web.phone.index').'</link>
			<pubDate>'.date(DATE_RFC822).'</pubDate>
			<lastBuildDate>'.date(DATE_RFC822).'</lastBuildDate>
    	';
    	$items = Phone::where('status',1)->where('instock_status','!=',5)->orderBy('id','DESC')->with(['phone_category'=>function($query) {
    		$query->where('status',1);
    	}])->select('id','name','slug','category_id','detail','created_at')->paginate(30);
    	foreach($items as $item) {
    		$template .='
				<item>
					<title>'.$item->name.'</title>
					<description>
						<![CDATA[
							'.cutString($item->detail,300).'
						]]>
					</description>
					<link>'.$item->getUrl().'</link>
					<guid isPermaLink="false">'.$item->getUrl().'</guid>
					<pubDate>'.$item->created_at.'</pubDate>
				</item>
    		';
    	}
    	return $this->build($template);
    }

    public function talbet() {
    	$template = '
			<title>Cityphone - Máy tính bảng</title>
			<copyright>'.route('web.home').'</copyright>
			<generator>Cityphone</generator>
			<link>'.route('web.tablet.index').'</link>
			<pubDate>'.date(DATE_RFC822).'</pubDate>
			<lastBuildDate>'.date(DATE_RFC822).'</lastBuildDate>
    	';
    	$items = Tablet::where('status',1)->where('instock_status','!=',5)->orderBy('id','DESC')->with(['tablet_category'=>function($query){
    		$query->where('status',1);
    	}])->select('id','name','slug','category_id','detail','created_at')->paginate(30);
    	foreach($items as $item) {
    		$template .='
				<item>
					<title>'.$item->name.'</title>
					<description>
						<![CDATA[
							'.cutString($item->detail,300).'
						]]>
					</description>
					<link>'.$item->getUrl().'</link>
					<guid isPermaLink="false">'.$item->getUrl().'</guid>
					<pubDate>'.$item->created_at.'</pubDate>
				</item>
    		';
    	}
    	return $this->build($template);
    }

    public function fit() {
    	$template = '
			<title>Cityphone - Phụ kiện</title>
			<copyright>'.route('web.home').'</copyright>
			<generator>Cityphone</generator>
			<link>'.route('web.fits_categories.showall').'</link>
			<pubDate>'.date(DATE_RFC822).'</pubDate>
			<lastBuildDate>'.date(DATE_RFC822).'</lastBuildDate>
    	';
    	$items = Fit::where('status',1)->where('instock_status','!=',5)->orderBy('id','DESC')->with(['fit_category'=>function($query){
    		$query->where('status',1);
    	}])->select('id','name','slug','category_id','detail','created_at')->paginate(30);
    	foreach($items as $item) {
    		$template .='
				<item>
					<title>'.$item->name.'</title>
					<description>
						<![CDATA[
							'.cutString($item->detail,300).'
						]]>
					</description>
					<link>'.$item->getUrl().'</link>
					<guid isPermaLink="false">'.$item->getUrl().'</guid>
					<pubDate>'.$item->created_at.'</pubDate>
				</item>
    		';
    	}
    	return $this->build($template);
    }

    public function service() {
    	$template = '
			<title>Cityphone - Dịch vụ</title>
			<copyright>'.route('web.home').'</copyright>
			<generator>Cityphone</generator>
			<link>'.route('web.services_categories.showall').'</link>
			<pubDate>'.date(DATE_RFC822).'</pubDate>
			<lastBuildDate>'.date(DATE_RFC822).'</lastBuildDate>
    	';
    	$items = Service::where('status',1)->where('instock_status','!=',5)->orderBy('id','DESC')->with(['service_category'=>function($query){
    		$query->where('status',1);
    	}])->select('id','name','slug','category_id','detail','created_at')->paginate(30);
    	foreach($items as $item) {
    		$template .='
				<item>
					<title>'.$item->name.'</title>
					<description>
						<![CDATA[
							'.cutString($item->detail,300).'
						]]>
					</description>
					<link>'.$item->getUrl().'</link>
					<guid isPermaLink="false">'.$item->getUrl().'</guid>
					<pubDate>'.$item->created_at.'</pubDate>
				</item>
    		';
    	}
    	return $this->build($template);
    }

    public function news() {
    	$template = '
			<title>Cityphone - Tin tức</title>
			<copyright>'.route('web.home').'</copyright>
			<generator>Cityphone</generator>
			<link>'.route('web.news_categories.showall').'</link>
			<pubDate>'.date(DATE_RFC822).'</pubDate>
			<lastBuildDate>'.date(DATE_RFC822).'</lastBuildDate>
    	';
    	$items = News::where('status',1)->orderBy('id','DESC')->with(['category'=>function($query){
    		$query->where('status',1);
    	}])->select('id','name','slug','category_id','detail','created_at')->paginate(30);
    	foreach($items as $item) {
    		$template .='
				<item>
					<title>'.$item->name.'</title>
					<description>
						<![CDATA[
							'.cutString($item->detail,300).'
						]]>
					</description>
					<link>'.$item->getUrl().'</link>
					<guid isPermaLink="false">'.$item->getUrl().'</guid>
					<pubDate>'.$item->created_at.'</pubDate>
				</item>
    		';
    	}
    	return $this->build($template);
    }
}