<?php

namespace App\Http\Controllers\Web;
use DB;
use Illuminate\Http\Request;
use App\Service;
use App\ServiceCategory;
use App\News;
use App\Fit;
use App\FitCategory;
use App\Comment;
use Storage;
class ServiceController extends Controller
{
    public function index($slug = null)
    {   
        $location = (isset($request->setlocation) == false)?1:$request->setlocation;

        $service = Service::where('slug', $slug)->where('status',1)->firstOrFail();

        $location_cookie = @$_COOKIE['location'];
       
        $regional_prices ='';
        if(isset($location_cookie) && $location_cookie !=1){
            $regional_prices = DB::table('regional_prices')->where('location_id',$location_cookie)->where('service_id', $service->id)->first();
        }
        $amount = $service->amount +1;

        $amount = Service::where('slug', $slug)->where('status',1)->update([
            'amount'=>$amount,
        ]);

        if (isset($service->slides) && $service->slides != '') {
            $slides = explode(",",$service->slides);
        }else {
            $slides = '';
        }
        $rating_star = $service->getRatings();

        $related_new_array = explode(',', $service->related_news);
        $related_news = News::getRelated($related_new_array,4);

        $related_service_array = explode(',', $service->related_service);
        $related_services = Service::getRelated($related_service_array,4,$service->category_id);

        $related_fit_array = explode(',', $service->related_fit);
        $related_fits = Fit::getRelated($related_fit_array,4);

        $service_category = ServiceCategory::where('id',$service->category_id)->where('status',1)->first();
        $service_category_parent = ServiceCategory::where('id',$service_category->parent_id)->where('status',1)->first();

        $config_promotion = DB::table('options')->select('value')->where('name','promotion')->first();
        if($config_promotion){
            $config_promotion = json_decode(base64_decode($config_promotion->value),true);
        }
        $promotion = ($service->promotion)?$service->promotion:$config_promotion["promotion_default_service"];
        $promotion = preg_split('/\n|\r\n/',$promotion);
        // comment
        $module_name = $service->name;
        $comments = $service->getCommentInfo();
 
        $comment_customers = $comments->comment_customer;
        $comment_admins = $comments->comment_admins;
        $vote = $comments->vote;
        $rating = $comments->ratings;
        if($rating->count != 0) {
            // số lượt bình luận
            $rank_count = $rating->count;
            // số sao
            $rank_peren = round($rating->avg,1);
        } else {
            $rank_count = 0;
            $rank_peren = 5;
        }
        $meta_seo = $this->meta_seo('services',$service->id,[
            'title' => $service->name.' - Cityphone',
            'description'=> cutString(removeHTML($service->detail),150),
            'url' => route('web.service.show',$service->slug),
            'image' => $service->getImage()
        ]);
        $admin_bar_edit = route('services.edit',$service->id);
        $breadcrumbs = [
            ['name'=>'Sửa chữa điện thoại','url'=>route('web.services_categories.showall')],
        ];
        if($service_category_parent) {
            $breadcrumbs[] = ['name'=>$service_category_parent->name,'url'=>route('web.services_categories.show', ['slug'=>$service_category_parent->slug])];
        }
        if($service_category) {
            $breadcrumbs[] = ['name'=>$service_category->name,'url'=>route('web.services_categories.show', ['slug'=>$service_category->slug])];
        }
        return view('web.services.index', compact(
            'service', 
            'related_news',
            'related_services',
            'related_fits',
            'service_category',
            'promotion',
            'service_category_parent',
            'module_name',
            'comment_customers',
            'comment_admins',
            'vote',
            'rank_count',
            'rank_peren',
            'meta_seo',
            'admin_bar_edit',
            'location',
            'breadcrumbs',
            'rating_star',
            'slides',
            'regional_prices'
        ));
    }
    public function datafeeds()
    {
        $services = Service::where('status',1)->where('price','>',0)->whereNotNull('image')->whereNotNull('detail')->get();
     
        // $fits = Fit::where('status',1)->where('price','>',0)->whereNotNull('image')->whereNotNull('detail')->get();
   

        $data = "id\ttiêu đề\tmô tả\tliên kết\ttình trạng\tgiá\tcòn hàng\tliên kết hình ảnh\tnhãn hiệu\tdanh mục sản phẩm của Google";
        
        $setting_google_shopping = DB::table('options')->where('name','google_shopping')->first();
        $google_shopping = json_decode(base64_decode($setting_google_shopping->value));
       
        foreach($services as $value){
            $description = str_replace(['\n','\t'],'',cutString(removeHTML($value->detail),170));
        
            $url = $value->getUrl();
            $service_category = ServiceCategory::where('status',1)->where('id',$value->category_id)->first();
            
            $price = $value->price;
            $google_shopping_services = DB::table('google_shopping')->where('type','services')->where('type_id',$value->id)->first();
          
            $google_shopping_brand = $google_shopping->brand;
            $google_shopping_category = $google_shopping->category;
            $google_shopping_instock = $google_shopping->instock;
            $google_shopping_itemcondition = $google_shopping->itemcondition;

            if(!empty($google_shopping_services)){
                if($google_shopping_services->brand != null){
                    $google_shopping_brand = $google_shopping_services->brand;
                }
                if($google_shopping_services->category != null){
                    $google_shopping_category = $google_shopping_services->category;
                }   
                if($google_shopping_services->instock != null){
                    $google_shopping_instock = $google_shopping_services->instock;
                }
                if($google_shopping_services->itemcondition != null){
                    $google_shopping_itemcondition = $google_shopping_services->itemcondition;
                }
            }
           
            if(!empty($google_shopping_brand) && !empty($google_shopping_category) && !empty($google_shopping_instock) && !empty($google_shopping_itemcondition)){
                $data .= "\n"."p".$value->id."\t".$value->name."\t".$description."\t".$url."\t".$google_shopping_itemcondition."\t".$price." VND"."\t".$google_shopping_instock."\t".$value->image."\t".$google_shopping_brand."\t".$google_shopping_category;
            }
            unset($google_shopping_brand);
            unset($google_shopping_category);
            unset($google_shopping_instock);
            unset($google_shopping_itemcondition);
        }

        // foreach($fits as $value){
        //     $description = str_replace(['\n','\t'],'',cutString(removeHTML($value->detail),170));
    
        //     $url = $value->getUrl();
        //     $fit_category = FitCategory::where('status',1)->where('id',$value->category_id)->first();
            
        //     $price = $value->price;
        //     $google_shopping_fit = DB::table('google_shopping')->where('type','fits')->where('type_id',$value->id)->first();
          
        //     $google_shopping_brand = $google_shopping->brand;
        //     $google_shopping_category = $google_shopping->category;
        //     $google_shopping_instock = $google_shopping->instock;
        //     $google_shopping_itemcondition = $google_shopping->itemcondition;

        //     if(!empty($google_shopping_fit)){
        //         if($google_shopping_fit->brand != null){
        //             $google_shopping_brand = $google_shopping_fit->brand;
        //         }
        //         if($google_shopping_fit->category != null){
        //             $google_shopping_category = $google_shopping_fit->category;
        //         }   
        //         if($google_shopping_fit->instock != null){
        //             $google_shopping_instock = $google_shopping_fit->instock;
        //         }
        //         if($google_shopping_fit->itemcondition != null){
        //             $google_shopping_itemcondition = $google_shopping_fit->itemcondition;
        //         }
        //     }
           
        //     if(!empty($google_shopping_brand) && !empty($google_shopping_category) && !empty($google_shopping_instock) && !empty($google_shopping_itemcondition)){
        //         $data .= "\n"."p".$value->id."\t".$value->name."\t".$description."\t".$url."\t".$google_shopping_itemcondition."\t".$price." VND"."\t".$google_shopping_instock."\t".$value->image."\t".$google_shopping_brand."\t".$google_shopping_category;
        //     }
        //     unset($google_shopping_brand);
        //     unset($google_shopping_category);
        //     unset($google_shopping_instock);
        //     unset($google_shopping_itemcondition);
        // }

        Storage::disk('local')->put('products.txt', "\xEF\xBB\xBF" . $data);
        echo "Tạo datafeeds thành công ! <a href='http://cityphone.dev1.pro/uploads/products.txt'>Link file datafeeds<a/>";

    }
  
}
