<?php

namespace App\Http\Controllers\Web;
use App\ServiceCategory;
use App\Unlock;
use App\UnlockCategory;
use Illuminate\Http\Request;
use App\MyClass\Categories;
use DB;
use App\Phone;
use App\Fit;
use App\FitCategory;
use App\Service;
use App\News;
use App\Price;
use Illuminate\Support\Facades\Session;
class HomeController extends Controller
{
    public function index(Request $request)
    {
        $home_color = 1;
        $show_modal_home = 2;
        $config_home = DB::table('options')->select('value')->where('name','home')->first();
        $location = (isset($request->setlocation) == false)?1:$request->setlocation;
        if($config_home){
            $config_home = json_decode(base64_decode($config_home->value),true);
        }
        $meta_seo = $this->meta_seo('',0,[
            'title' => ($config_home["meta_title"] != "")?$config_home["meta_title"]:"Trang chủ".' - CityPhone',
            'description'=> ($config_home["meta_description"] != "")?$config_home["meta_description"]:"Trang chủ",
            'url' => url('/'),
            'image' => url('/').'/assets/img/logo.png'
        ]);

        

        $admin_bar_edit = route('admin.setting.home');

        $fit_category = FitCategory::where('status',1)->where('parent_id',0)->orderBy('order','asc')->limit(5)->get();
        $service_category = ServiceCategory::where('status',1)->where('parent_id',0)->orderBy('order','asc')->paginate(5);
        
        $list_s_cate_ip =ServiceCategory::where('status',1)->where('parent_id',0)->where('id',1)->first();
      
        $list_s_cate_ip_child = ServiceCategory::where('status',1)->where('parent_id',$list_s_cate_ip->id)->get();
        $list_s_ip_collect = Service::where('status',1)->whereIn('category_id',$list_s_cate_ip_child->pluck('id','id')->toArray())->orWhere('category_id',$list_s_cate_ip->id)->limit(10)->get();
   
        $list_fits = Fit::where('status',1)->orderBy('created_at','DESC')
            ->leftJoin('pins', 'fits.id', '=', 'pins.type_id')->where('type','fits')->where('place','home')->orderBy('pins.value','asc')
            ->limit(16)->get();
           
        $list_services = Service::where('status',1)->orderBy('created_at','DESC')
            ->leftJoin('pins', 'services.id', '=', 'pins.type_id')->where('type','services')->where('place','highlight')->orderBy('pins.value','asc')
            ->limit(10)->get();
        $list_news = News::where('status',1)->orderBy('created_at','DESC')
            ->leftJoin('pins', 'news.id', '=', 'pins.type_id')->where('type','news')->where('place','home')->orderBy('pins.value','asc')
            ->limit(3)->get();
        
        $list_news_hot = News::where('status',1)->orderBy('created_at','DESC')
        ->leftJoin('pins', 'news.id', '=', 'pins.type_id')->where('type','news')->where('place','hot')->orderBy('pins.value','asc')
        ->limit(4)->get();

        $slides_home = DB::table('slides')->where("status",1)->where('type',1)->get();
        $videos_home = DB::table('slides')->where('status',1)->where('type',2)->orderBy('id','desc')->limit(3)->get();

        $home_prices = Price::leftJoin('pins','prices.id','=','pins.type_id')
        ->where('status',1)->where('type','prices')->where('place','home')->orderBy('pins.value','asc')
        ->limit(21)->get();

        $session_modal = Session::get('session_modal');

        return view('web.home',compact('meta_seo','admin_bar_edit','fit_category','list_fits','list_services','service_category','list_news','slides_home','videos_home','location',
        'list_s_cate_ip_child',
        'list_s_ip_collect',
        'list_news_hot','home_color',
        'home_prices',
        'show_modal_home',
        'session_modal' ));

    }
    public function home_view_more_product(Request $request){
        $type = $request->type;
        $count = $request->count;
        $page = $request->page;
        $record_per_page = $count;
        $from = ($page - 1)*$record_per_page;
        $location = $request->location;
        switch ($type) {
            case 'phones':
                $list_phones = Phone::where('status',1)->leftJoin('pins', $type.'.id', '=', 'pins.type_id')->where('type',$type)->where('place','home')->skip($from)->take($record_per_page)->orderBy('pins.value','asc')->get();
                $list_item = [];
                $render_phone = view('web.phone.phone_item')->with(['list_phones'=>$list_phones,'location'=>$location])->render();
                $list_item =  (array) $render_phone;
                return response(json_encode(compact('list_item')));
            
                break;
            case 'fits':
                $list_fits = Fit::where('status',1)->leftJoin('pins', $type.'.id', '=', 'pins.type_id')->where('type',$type)->where('place','home')->skip($from)->take($record_per_page)->orderBy('pins.value','asc')->get();
                $list_item = [];
                $render_fit = view('web.fits.fit_item')->with(['list_fits'=>$list_fits])->render();
                $list_item =  (array) $render_fit;
                return response(json_encode(compact('list_item')));
                break;
            case 'services':
                $list_services = Service::where('status',1)->leftJoin('pins', $type.'.id', '=', 'pins.type_id')->where('type',$type)->where('place','home')->skip($from)->take($record_per_page)->orderBy('pins.value','asc')->get();
                $list_item = [];
                $render_service = view('web.services.service_item')->with(['list_services'=>$list_services])->render();
                $list_item =  (array) $render_service;
                return response(json_encode(compact('list_item')));
                break;

            default:
                # code...
                break;
        }
        
    }
    public function sync_image(Request $request){
        $test = $request->test;
        return response(json_encode(compact('test')));
    }

    public function page_not_found(){
        return view('web.page_not_found');
    }
}
