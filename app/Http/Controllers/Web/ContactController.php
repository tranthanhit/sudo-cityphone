<?php
namespace App\Http\Controllers\Web;
use Illuminate\Http\Request;
class ContactController extends Controller
{
    public function index()
    {
        $meta_seo = $this->meta_seo('',0,[
            'title' => 'Liên hệ',
            'description'=> 'Liên hệ',
            'url' => url('/lien-he.html'),
            'image' => url('/').'/public/assets/img/logo.png'
        ]);
        $breadcrumbs = [
            ['name'=>'Liên hệ','url'=>'#'],
        ];
        return view('web.contact.index',compact('meta_seo','breadcrumbs'));
    }
}
