<?php
namespace App\Http\Controllers\Web;
use Illuminate\Http\Request;
use App\Price;
class PriceController extends Controller
{
    public function index($slug)
    {
        $price = Price::where('status',1)->where('slug',$slug)->firstOrFail();
        $meta_seo = $this->meta_seo('prices',$price->id,[
            'title' => $price->name,
            'description'=> cutString(removeHTML($price->detail),150),
            'url' => config('app.url').$price->getUrl(),
            'image' => $price->getImage()
        ]);
        return view('web.prices.index',compact('meta_seo','price'));
    }
}
