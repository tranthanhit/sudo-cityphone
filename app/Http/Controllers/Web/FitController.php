<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use DB;
use App\Fit;
use App\FitCategory;
use App\Comment;
use App\Service;
use App\News;
class FitController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($slug = null)
    {   
        $location = (isset($request->setlocation) == false)?1:$request->setlocation;
        $fit = Fit::where('slug',$slug)->where('status',1)->first();
        if (isset($fit->slides) && $fit->slides != '') {
            $slides = explode(",",$fit->slides);
        }else {
            $slides = '';
        }


        $fit_category = FitCategory::where('id',$fit->category_id)->first();

        $rating_star = $fit->getRatings();
        $related_fit_array = explode(',', $fit->related_fit);
        $related_fits = Fit::getRelated($related_fit_array,5,$fit->category_id);

        $config_promotion = DB::table('options')->select('value')->where('name','promotion')->first();
        if($config_promotion){
            $config_promotion = json_decode(base64_decode($config_promotion->value),true);
        }
        $config_promotion = ($fit->info)?$config_promotion = $fit->info:$config_promotion = $config_promotion["promotion_default_fit"];
        $promotion = preg_split('/\n|\r\n/',$config_promotion);
        // comment
        $module_name = $fit->name;
        $comments = $fit->getCommentInfo();
       
        $comment_customers = $comments->comment_customer;
        $comment_admins = $comments->comment_admins;
        $vote = $comments->vote;
        $rating = $comments->ratings;
        if($rating->count != 0) {
            // số lượt bình luận
            $rank_count = $rating->count;
            // số sao
            $rank_peren = round($rating->avg,1);
        } else {
            $rank_count = 0;
            $rank_peren = 5;
        }

        $meta_seo = $this->meta_seo('fits',$fit->id,[
            'title' => $fit->name.' - CityMobile',
            'description'=> cutString(removeHTML($fit->detail),150),
            'url' => route('web.fits.show',$fit->slug),
            'image' => $fit->getImage()
        ]);

        $admin_bar_edit = route('fits.edit',$fit->id);

        $breadcrumbs = [
            ['name'=>'Phụ kiện','url'=>route('web.fits_categories.showall')],
            ['name'=>$fit_category->name,'url'=>route('web.fits_categories.show', ['slug' => $fit_category->slug])],
        ];
        return view('web.fits.index', compact('rating_star','fit','fit_category','related_fits','promotion','module_name','comment_customers','comment_admins','vote','rank_count','rank_peren','meta_seo','admin_bar_edit','location','breadcrumbs','slides'));
    }
}
