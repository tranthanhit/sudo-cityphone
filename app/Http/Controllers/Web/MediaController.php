<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use DB;
use Validator;
use Response;
use Image;
use Storage;

class MediaController extends Controller
{
    protected $imageSize = [
        'large' => 600,
        'medium' => 300,
        'small' => 150,
        'tiny' => 80
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function uploadImage(Request $request)
    {   
        $explore = explode('/', $request->error_src);
        // năm
        $image_year = $explore[6];
        // tháng
        $image_month = $explore[7];
        $image_full_path = explode('.',array_pop($explore));
        // tên ảnh
        $image_name = $image_full_path[0];
        // định dạng
        $image_extention = $image_full_path[1];

        // size ảnh
        $image_size_array = explode('-',$image_name);
        $image_size = array_pop($image_size_array);
        $image_set_width = $this->imageSize[$image_size];

        // lấy tên ảnh không có tiền tố kích thước
        $image_name_no_size = implode('-', $image_size_array);

        // Link nơi lưu trữ ảnh
        $link_img_new = config('filesystems.disks.sop.container_url');

        // Link ảnh cũ đã chứa file cần chuyển
        $image_has_file = $link_img_new.'/'.$image_year.'/'.$image_month.'/'.$image_size.'-'.$image_name_no_size.'.'.$image_extention;
        $image_has_new_file = $request->error_src;
        
        // $image_file = file_get_contents($image_has_file);

        $upload = Storage::disk('sop');
        // $image_old = $upload->exists($image_has_file);

         return $image_has_new_file;
    }

    public function getImage()
    {
        $imageUrl = 'https://object-storage.tyo1.cloud.z.com/v1/zc_6c13a3172446411fab7837a8a5479710/Cityphone/2016/12/small-Xiaomi-Mi5S-cu-xach-tay-gia-re-nhat-Ha-Noi-Cityphone-03-1.jpg';
        // $imageName = explode('/', $imageUrl);

        // $imageName = array_pop($imageName);
        // $saveTo = "./{$imageName}";

        $image = file_get_contents($imageUrl);
        // file_put_contents($saveTo, $image);
        // echo '<img src="' . $saveTo . '" alt="">';
        echo $image;
    }
}
