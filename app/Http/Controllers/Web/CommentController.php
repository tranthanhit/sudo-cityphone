<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Comment;
class CommentController extends Controller
{
    public function getComment(Request $request) {
    	$type = $request->type;
    	$type_id = $request->type_id;

		$comment_customers = Comment::where('parent_id',0)->where('status',1)->where('type',$type)->where('type_id',$type_id)->orderBy('id','DESC')->paginate(5);
        $comment_array_id = $comment_customers->pluck('id');
        $comment_admins = Comment::wherein('comment.parent_id',$comment_array_id)->where('comment.status',1)->join('admin_users','admin_users.id','comment.admin_id')->select('comment.*','admin_users.fullname')->get();

    	$render_item = view('web.comment.comment-item')->with([
            'comment_customers'=>$comment_customers,
            'comment_admins'=>$comment_admins,
        ])->render();
        $items =  (array) $render_item;
        return [$comment_customers,$items];
    }

    public function addComment(Request $request) {
    	$type = $request->type;
    	$type_id = $request->type_id;
    	Comment::add($request);
        return "Thêm bình luận thành công";
    }

    public function searchComment(Request $request) {
    	$type = $request->type;
    	$type_id = $request->type_id;
    	if (isset($request->keyword)) {
            $keyword = $request->keyword;
            $key = str_replace(" ","%",$keyword);
            $key = str_replace("\'","'",$key);
            $key = str_replace("'","''",$key);
        } else {
            $keyword = $key = "";
        }
        $comment_array_id = [];
        $comment_customers = [];

        $comment_customers_query = Comment::where('parent_id',0)->where('status',1)->where('type',$type)->where('type_id',$type_id)->where('name', 'LIKE', "%$key%")->orderBy('id','DESC')->take(5)->get();
        foreach ($comment_customers_query as $value) {
            $comment_array_id[] = $value->id;
            $comment_customers[] = [
            	'id' => $value->id,
            	'name' => $value->name,
            	'id' => $value->id,
            	'like' => $value->like,
            	'rank' => $value->rank,
            	'content' => $value->content,
            	'created_at' => $value->created_at->format('Y-m-d H:m:i'),
            ];
        }
        if (count($comment_customers_query) < 5) {
        	$limit = 5 - count($comment_customers_query);
            $comment_customers_query_2 = Comment::whereNotIn('id',$comment_array_id)->where('parent_id',0)->where('status',1)->where('type', $type)->where('type_id',$type_id)->where('content', 'LIKE', "%$key%")->orderBy('id','DESC')->take($limit)->get();
            foreach ($comment_customers_query_2 as $value) {
	            $comment_array_id[] = $value->id;
	            $comment_customers[] = [
	            	'id' => $value->id,
	            	'name' => $value->name,
	            	'id' => $value->id,
	            	'like' => $value->like,
	            	'rank' => $value->rank,
	            	'content' => $value->content,
	            	'created_at' => $value->created_at->format('Y-m-d H:m:i'),
	            ];
	        }
        }

    	$comment_admins = Comment::wherein('parent_id',$comment_array_id)->where('status',1)->get();
        $render_item = view('web.comment.comment-item')->with([
            'comment_customers'=>json_decode(json_encode($comment_customers)),
            'comment_admins'=>$comment_admins,
        ])->render();
        $items =  (array) $render_item;
        return $items;
    }

    public function CommentLike($comment_id)
    {
        $comment = Comment::where('id', $comment_id)->firstOrFail();
        $comment->like = $comment->like+1;
        $comment->save();
        return $comment->like;
    }
    public function CommentDisLike($comment_id)
    {
        $comment = Comment::where('id', $comment_id)->firstOrFail();
        $comment->like = $comment->like-1;
        $comment->save();
        return $comment->like;
    }
}
