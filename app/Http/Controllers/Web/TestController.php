<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use Mail;
use App\Mail\TestEmail;
use Validator;
use Response;
use Image;
use Storage;
use DB;
use App\Phone;
use App\PhoneCategory;
use App\Tablet;
use App\Service;
use App\Fit;
use App\News;
use App\Exports\OrdersExport;
use App\Exports\ServiceExport;
use Maatwebsite\Excel\Facades\Excel;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
class TestController extends Controller
{

    public function index()
    {
        $phone_attributes = DB::table('product_attribute')->where('type','phones')->get();
        $common_color = $common_ram = $common_storage = $common_aspect = $common_country = [];
        foreach ($phone_attributes as $phone_attribute) {
            $default = json_decode(base64_decode(json_encode([])));
            $color = json_decode(base64_decode($phone_attribute->color??$default),true);
            $ram = json_decode(base64_decode($phone_attribute->ram??$default),true);
            $storage = json_decode(base64_decode($phone_attribute->storage??$default),true);
            $aspect = json_decode(base64_decode($phone_attribute->aspect??$default),true);
            $country = json_decode(base64_decode($phone_attribute->country??$default),true);
            //dump($color);
            //dump($ram);
            //dump($storage);
            //dump($aspect);
            //dump($country);
        }

        die;
        $phones = Phone::select('phones.id','phones.name','color','storage','ram','aspect','country')
        ->leftJoin('product_attribute','phones.id','=','type_id')
        ->where('phones.status',1)
        ->get();
        echo '<table>';
        foreach ($phones as $value) {
            $default = json_decode(base64_decode(json_encode([])));
            $color = json_decode(base64_decode($value->color??$default),true);
            $ram = json_decode(base64_decode($value->ram??$default),true);
            $storage = json_decode(base64_decode($value->storage??$default),true);
            $aspect = json_decode(base64_decode($value->aspect??$default),true);
            $country = json_decode(base64_decode($value->country??$default),true);

            echo '<tr>';
            echo '<td>'.$value->id.'</td>';
            echo '<td>'.$value->name.'</td>';

            echo '<td>';
            foreach ((array)$color as $v) {
                echo $v['color_name'].', ';
            }
            unset($v);
            echo '</td>';
            
            echo '<td>';
            foreach ((array)$ram as $v) {
                echo $v['ram_name'].', ';
            }
            unset($v);
            echo '</td>';
            
            echo '<td>';
            foreach ((array)$storage as $v) {
                echo $v['storage_name'].', ';
            }
            unset($v);
            echo '</td>';
            
            echo '<td>';
            foreach ((array)$aspect as $v) {
                echo $v['tt_name'].', ';
            }
            unset($v);
            echo '</td>';
            
            echo '<td>';
            foreach ((array)$country as $v) {
                echo $v['country_name'].', ';
            }
            unset($v);
            echo '</td>';
            echo '</tr>';
        }
        echo '</table>';
        echo '<style>table td {padding:5px;border:1px solid #aaa;}</style>';
        die;
        /* lấy tất cả các attribute không trùng lặp
        $phone_attributes = DB::table('product_attribute')->where('type','phones')->get();
        $common_color = $common_ram = $common_storage = $common_aspect = $common_country = [];
        foreach ($phone_attributes as $phone_attribute) {
            $default = json_decode(base64_decode(json_encode([])));
            $color = json_decode(base64_decode($phone_attribute->color??$default),true);
            $ram = json_decode(base64_decode($phone_attribute->ram??$default),true);
            $storage = json_decode(base64_decode($phone_attribute->storage??$default),true);
            $aspect = json_decode(base64_decode($phone_attribute->aspect??$default),true);
            $country = json_decode(base64_decode($phone_attribute->country??$default),true);

            foreach ((array)$color as $value) {
                if(!in_array($value['color_name'],$common_color))
                    array_push($common_color,$value['color_name']);
            }
            unset($value);
            
            foreach ((array)$ram as $value) {
                if(!in_array($value['ram_name'],$common_ram))
                    array_push($common_ram,$value['ram_name']);
            }
            unset($value);
            
            foreach ((array)$storage as $value) {
                if(!in_array($value['storage_name'],$common_storage))
                    array_push($common_storage,$value['storage_name']);
            }
            unset($value);
            
            foreach ((array)$aspect as $value) {
                if(!in_array($value['tt_name'],$common_aspect))
                    array_push($common_aspect,$value['tt_name']);
            }
            unset($value);
            
            foreach ((array)$country as $value) {
                if(!in_array($value['country_name'],$common_country))
                    array_push($common_country,$value['country_name']);
            }
            unset($value);
        }
        dump($common_color);
        dump($common_ram);
        dump($common_storage);
        dump($common_aspect);
        dump($common_country);*/
        /*map tạm
        "Đen" => "Đen",
        "hồng" => "Hồng",
        "xanh" => "Xanh",
        "vàng" => "Vàng",
        "đen bóng" => "Đen bóng",
        "Hồng" => "Hồng",
        "Vàng" => "Vàng",
        "Trắng" => "Trắng",
        "Gold" => "Vàng",
        "Xanh" => "Xanh",
        "Xám" => "Xám",
        "Đỏ" => "Đỏ",
        "Tím" => "Tím",
        "Glod" => "Vàng",
        "đen" => "Đen",
        "Black" => "Đen",
        "White" => "Trắng",
        "Den" => "Đen",
        "Hong" => "Hồng",
        "trắng" => "Trắng",
        "Bạc" => "Xám",
        "Pink" => "Hồng",
        "Blue" => "Xanh",
        "gold" => "Vàng",
        "den bóng" => "Đen bóng",
        "Xám đen" => "Đen",
        "Silver" => "Xám",
        "Red" => "Đỏ",
        "đỏ" => "Đỏ",
        "đen nhám" => "Đen nhám",
        "Cam"  => "Cam",
        "Xanh đen" => "Xanh đen",
        "Đồng" => "Đồng",
        "Jet Black" => "Đen nhám"

        "4GB" => "4GB",
        "6GB" => "6GB",
        "3GB" => "3GB",
        "4-64GB" => "4GB",
        "6-64GB" => "6GB",
        "8GB" => "8GB",
        "2GB" => "2GB",
        "6-128GB" => "6GB"

        "16GB" => "16GB",
        "32G" => "32GB",
        "32GB" => "32GB",
        "64GB" => "64GB",
        "64G" => "64GB",
        "128G" => "128GB",
        "128GB" => "128GB",
        "i5-8-256GB" => "i5-8-256GB",
        "i5-8-512gb" => "i5-8-512GB",
        "6-128GB" => "6-128GB",
        "6-64GB" => "6-64GB",
        "8-128GB" => "8-128GB",
        "4-64GB" => "4-64GB",
        "4-128GB" => "4-128GB",
        "256GB" => "256GB",
        "512GB" => "512GB",
        "2-16GB" => "2-16GB",
        "3-32GB" => "3-32GB",
        "6Gb-64Gb" => "6-64GB",
        "6-256GB" => "6-256GB",
        "6-128GB-Lưngtrong" => "6-128GB",
        "6-64G" => "6-64GB",
        "6-128G" => "6-128GB",
        "12-128GB" => "12-128GB",
        "8-256GB" => "8-256GB",
        "12-256GB" => "12-256GB",

        "100%" => "100%",
        "99%" => "99%",
        "95%" => "95%",
        "New" => "New",

        "Hàn Quốc" => "Hàn",
        "Mỹ" => "Mỹ",
        "Hàn" => "Hàn",
        "Nhật" => "Nhật",
        "Hàn, Nhật" => "Hàn, Nhật",
        "Trả BH" => "Trả BH",
        "Quốc tế" => "Quốc tế",
        "Nhật Bản" => "Nhật",
        "HK" => "HK"
        */
    }
    
    /**
     * tạo nhiều dịch vụ sữa chữa
     */
    public function CreateFit()
    {
        $list_fits = DB::table('fits')->where('status',1)->get();
        $created_at = $updated_at = date("Y-m-d H:i:s");
        for($i =1;$i<100;$i++){
            foreach($list_fits as $key=>$value){
                $array = [
                    'category_id'=>$value->category_id,
                    'name'=>$value->name.' '.$i,
                    'slug'=>$value->slug.'-'.$i,
                    'image'=>$value->image,
                    'slides'=>$value->slides,
                    'price_old'=>$value->price_old,
                    'price'=>$value->price,
                    'warranty'=>$value->warranty,
                    'option'=>$value->option,
                    'info'=>$value->info,
                    'instock_status'=>$value->instock_status,
                    'related_fit'=>$value->related_fit,
                    'package'=>$value->package,
                    'detail'=>$value->detail,
                    'tags'=>$value->tags,
                    'status'=>$value->status,
                    'created_at'=>$created_at,
                    'updated_at'=>$created_at,

                ];
               
                $id_insert = DB::table('fits')->insertGetId($array);
            }
        }
    }
    /**
     * tạo nhiều services
     */
    public function CreatedService()
    {
      
        $wp_posts = DB::table('wp_posts')->where('post_type','product')->get();
       $wp_wc_product_meta_lookup_collect =collect( DB::table('wp_wc_product_meta_lookup')->whereIn('product_id',$wp_posts->pluck('ID','ID')->toArray())->get()  ); 
        $created_at = $updated_at = date("Y-m-d H:i:s");
            foreach($wp_posts as $key=>$value){
                $wp_wc_product_meta_lookup = $wp_wc_product_meta_lookup_collect->where('product_id',$value->ID)->first();
                // dump($wp_wc_product_meta_lookup);
                $array = [
                    'category_id'=>4,
                    'class_id'=>4,
                    'name'=>$value->post_title,
                    'slug'=>slugTitle($value->post_title),
                    'image'=>'',
                    'slides'=>'',
                    'videos'=>'',
                    'price'=>@$wp_wc_product_meta_lookup->min_price,

                    'warranty'=>'',
                    'promotion'=>'',
                    'instock_status'=>0,
                    'related_service'=>'',
                    'related_news'=>'',
                    'infor'=>'',
                    'detail'=>$value->post_content,
                    'status'=> $value->post_status = 'publish' ? 1 : 2,
                    'created_at'=>$created_at,
                    'updated_at'=>$updated_at,
                ];
                $id_insert = DB::table('services')->insertGetId($array);
            
        }
    }
}
