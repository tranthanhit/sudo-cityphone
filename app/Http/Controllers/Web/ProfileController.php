<?php

namespace App\Http\Controllers\Web;

use App\News;
use App\User;
use Illuminate\Http\Request;
use DB;

class ProfileController extends Controller
{
    public function update_news()
    {
        $user = DB::table('admin_users')->where('status', 1)->pluck('id', 'id')->toArray();
        $system_logs = DB::table('system_logs')->where('table', 'news')->where('type', 'update')->whereIn('admin_id', $user)->get();
        foreach ($system_logs as $v) {
            $create_new = News::where('id', $v->table_id)->update([
                'user_id' => $v->admin_id,
            ]);
        }
    }

    public function index($id)
    {
        if (!isset($id)) {
            return abort(404);
        }
        $user = DB::table('admin_users')->where('status', 1)->where('id', $id)->first();
        $news = News::where('user_id', $user->id)->where('status', 1)->orderBy('created_at','desc')->paginate(10);
        if (count($news)<=0){
            return abort(404);
        }
        $meta_seo = $this->meta_seo('',0,[
            'title' => 'Hồ sơ của '.$user->fullname ?? $user->name,
            'description'=> 'Mô tả',
            'url' => route('web.profile.show',$user->id)
        ]);
        return view('web.profile.index',compact('meta_seo','user','news'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
