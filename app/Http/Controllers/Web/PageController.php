<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;

use App\Page;
use App\Phone;
use App\Comment;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $page = Page::where('status',1)->where('slug',$slug)->firstOrFail();

        $related_phone_array = explode(',', $page->related_phone);
      
        $meta_seo = $this->meta_seo('pages',$page->id,[
            'title' => $page->name.' - Cityphone',
            'description'=> ($page->detail != null)?removeHTML(cutString($page->detail,150)):$page->name,
            'url' => route('web.pages.show',$page->slug)
        ]);
        $admin_bar_edit = route('pages.edit',$page->id);

        $comment = Comment::where('type', 'pages')->where('type_id',$page->id)->where('status',1)->select('id','rank')->get();
        // dump($comment);die;
        $rank_count = count($comment);
        $rating = 0;
        foreach ($comment as $value) {
            $rating += $value->rank;
        }

        if ($rank_count != 0) {
            $rank_count = $rank_count;
            $rank_peren = round($rating/$rank_count);
        } else {
            $rank_count = 0;
            $rank_peren = 0;
        }


        $breadcrumbs = [
            ['name'=>$page->name,'url'=>route('web.pages.show',['slug'=>$page->slug])],
        ];
        return view('web.pages.index', compact('page','meta_seo','admin_bar_edit', 'rank_count','rank_peren','breadcrumbs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
