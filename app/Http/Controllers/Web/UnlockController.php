<?php
namespace App\Http\Controllers\Web;
use App\Fit;
use App\News;
use App\Unlock;
use App\UnlockCategory;
use Illuminate\Http\Request;
use DB;
class UnlockController extends Controller
{
    public function index($slug = null)
    {
        $location = (isset($request->setlocation) == false) ? 1 : $request->setlocation;

        $unlock = Unlock::where('slug', $slug)->where('status', 1)->firstOrFail();
        $related_new_array = explode(',', $unlock->related_news);
        $related_news = News::getRelated($related_new_array, 4);

        $related_unlock_array = explode(',', $unlock->related_service);
        $related_unlocks = Unlock::getRelated($related_unlock_array, 4, $unlock->category_id);

        $related_fit_array = explode(',', $unlock->related_fit);
        $related_fits = Fit::getRelated($related_fit_array, 4);

        $unlock_category = UnlockCategory::where('id', $unlock->category_id)->where('status',1)->first();
        $unlock_category_parent = UnlockCategory::where('id', $unlock_category->parent_id)->where('status',1)->first();
        $config_promotion = DB::table('options')->select('value')->where('name', 'promotion')->first();

        if ($config_promotion) {
            $config_promotion = json_decode(base64_decode($config_promotion->value), true);
        }

        $promotion = ($unlock->promotion) ? $unlock->promotion : $config_promotion["promotion_default_unlock"];
        $promotion = preg_split('/\n|\r\n/', $promotion);
        $slide_real = [];
        // slide real
        if (isset($unlock->slide_real) && $unlock->slide_real != '' || isset($unlock->slide_real) && $unlock->slide_real != null) {
            $slide_real = explode(',', $unlock->slide_real);
        } 
        // else {
        //     $config_slide_real = DB::table('options')->select('value')->where('name', 'general')->first();
        //     $config_slide_real = json_decode(base64_decode($config_slide_real->value), true);
        //     $slide_real = $config_slide_real['unlock_slide_real'] ?? [];
        // }

        // video
        $link_video_default = $config_promotion["link_video_service"];
        $videos = ($unlock->videos != null) ? explode(";", $unlock->videos) : preg_split('/\n|\r\n/', $link_video_default);

        // comment
        $module_name = $unlock->name;
        $comments = $unlock->getCommentInfo();
        $comment_customers = $comments->comment_customer;
        $comment_admins = $comments->comment_admins;
        $vote = $comments->vote;
        $rating = $comments->ratings;
        if ($rating->count != 0) {
            // số lượt bình luận
            $rank_count = $rating->count;
            // số sao
            $rank_peren = round($rating->avg, 1);
        } else {
            $rank_count = 0;
            $rank_peren = 5;
        }

        $meta_seo = $this->meta_seo('unlocks', $unlock->id, [
            'title' => $unlock->name . ' - Cityphone',
            'description' => cutString(removeHTML($unlock->detail), 150),
            'url' => route('web.unlocks.show', $unlock->slug),
            'image' => $unlock->getImage()
        ]);

        $admin_bar_edit = route('unlocks.edit', $unlock->id);


        $breadcrumbs = [
            ['name' => 'Unlocks mở mạng', 'url' => route('web.unlock_categories.showall')],
        ];
        if ($unlock_category_parent) {
            $breadcrumbs[] = ['name' => $unlock_category_parent->name, 'url' => route('web.unlock_categories.show', ['slug' => $unlock_category_parent->slug])];
        }
        if ($unlock_category) {
            $breadcrumbs[] = ['name' => $unlock_category->name, 'url' => route('web.unlock_categories.show', ['slug' => $unlock_category->slug])];
        }
        return view('web.unlocks.index', compact(
            'unlock',
            'related_news',   
            'related_unlocks',
            'related_fits',
            'unlock_category',
            'promotion',
            'unlock_category_parent',
            'module_name',
            'comment_customers',
            'comment_admins',
            'vote',
            'rank_count',
            'rank_peren',
            'meta_seo',
            'admin_bar_edit',
            'location',
            'videos',
            'slide_real',
            'breadcrumbs'
        ));
    }
}
