<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Fit;
use App\FitCategory;
use DB;
class FitCategoryController extends Controller
{
    
    public function showall(Request $request)
    {
        $config_seo_category = $this->getOptions('seo_category');
        $filter_price = $request->filter_price;
        $sort  = $request->sort;
        $home_color = 1;
        $location = (isset($request->setlocation) == false)?1:$request->setlocation;
        $fit_categories = FitCategory::where('parent_id', 0)->where('status',1)->get();

        $fits = Fit::where('status', 1)->where('instock_status','!=',5)->select('id', 'name', 'slug', 'image', 'price');


        if($filter_price != ''){
            switch ($filter_price){
                case '200.000':
                    $fits = $fits->whereBetween('price',[0,200000]);
                    break;
                case '200.000vs500.000':
                    $fits = $fits->whereBetween('price',[200000,500000]);
                    break;
                case '500.000vs1000.000':
                    $fits = $fits->whereBetween('price',[500000,1000000]);
                    break;
                case '1000.000':
                    $fits = $fits->whereBetween('price',[1000000,10000000]);
                    break;
            }
        }
        if($sort != ''){
            switch ($sort){
                case 'asc':
                    $fits = $fits->orderBy('price','asc');
                    break;
                case 'desc':
                    $fits = $fits->orderBy('price','desc');
                    break;
                case 'default':
                    $fits = $fits->orderBy('id','desc');
                    break;
            }
        }else{
            $fits = $fits->orderBy('id','desc');
        }


        $config_seo = DB::table('options')->select('value')->where('name','seo_category')->first();
        if($config_seo){
            $config_seo = json_decode(base64_decode($config_seo->value),true);
        }
        $meta_seo = $this->meta_seo('',0,[
            'title' => ($config_seo_category["title_fit"] != null)?$config_seo_category["title_fit"]:"Phụ kiện".' - Cityphone',
            'description'=> ($config_seo_category["desc_fit"] != null)?$config_seo_category["desc_fit"]:"Phụ kiện",
            'url' => route('web.fits_categories.showall')
        ]);

        $breadcrumbs = [
            ['name'=>'Phụ kiện','url'=>route('web.fits_categories.showall')],
        ];
        $fits = $fits->orderBy('instock_status')->orderBy('price')->take(12)->get();
        return view('web.fits_categories.showall', compact('fits','meta_seo','location','config_seo','breadcrumbs','fit_categories','home_color','filter_price','sort'));
    }
    public function index(Request $request,$slug = null)
    {
        $config_seo_category = $this->getOptions('seo_category');
        $filter_price = $request->filter_price;
        $sort  = $request->sort;
        $home_color = 1;
        $location = (isset($request->setlocation) == false)?1:$request->setlocation;
        $fit_category = FitCategory::where('slug', $slug)->first();
        $fit_categories = FitCategory::where('parent_id', 0)->where('status',1)->get();
        $fit_category_childs = FitCategory::where('parent_id', $fit_category->id)->where('slug', $slug)->get();

        $fit_category_childs_id = [];
        array_push($fit_category_childs_id, $fit_category->id);
        foreach ($fit_category_childs as $value) {
            array_push($fit_category_childs_id, $value->id);
        }
        $fits = Fit::wherein('category_id', $fit_category_childs_id)->where('status', 1)->where('instock_status','!=',5);

        if($filter_price != ''){
            switch ($filter_price){
                case '200.000':
                    $fits = $fits->whereBetween('price',[0,200000]);
                    break;
                case '200.000vs500.000':
                    $fits = $fits->whereBetween('price',[200000,500000]);
                    break;
                case '500.000vs1000.000':
                    $fits = $fits->whereBetween('price',[500000,1000000]);
                    break;
                case '1000.000':
                    $fits = $fits->whereBetween('price',[1000000,10000000]);
                    break;
            }
        }
        if($sort != ''){
            switch ($sort){
                case 'asc':
                    $fits = $fits->orderBy('price','asc');
                    break;
                case 'desc':
                    $fits = $fits->orderBy('price','desc');
                    break;
                case 'default':
                    $fits = $fits->orderBy('id','desc');
                    break;
            }
        }else{
            $fits = $fits->orderBy('id','desc');
        }
      
        $meta_seo = $this->meta_seo('fit_categories',$fit_category->id,[
            'title' => $fit_category->name ?? $config_seo_category["title_fit"].' - CityPhone',
            'description'=> cutString(removeHTML($fit_category->detail ?? $config_seo_category["desc_fit"]),150),
            'url' => route('web.fits_categories.show',$fit_category->slug),
        ]);

        $fits = $fits->orderBy('instock_status')->orderBy('price')->take(12)->get();

        $breadcrumbs = [
            ['name'=>'Phụ kiện','url'=>route('web.fits_categories.showall')],
            ['name'=>$fit_category->name,'url'=>route('web.fits_categories.show', ['slug' => $fit_category->slug])],
        ];
        $slug = $fit_category->slug;
        return view('web.fits_categories.index', compact('fits','fit_categories', 'fit_category','meta_seo','location','breadcrumbs','home_color','slug','filter_price','sort'));
    }
}
