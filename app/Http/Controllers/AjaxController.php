<?php

namespace App\Http\Controllers;
use App\Unlock;
use App\UnlockCategory;
use Illuminate\Http\Request;

use App\Fit;
use App\FitCategory;
use App\Service;
use App\ServiceCategory;
use App\Phone;
use App\Tablet;
use Validator;
use Response;
use Image;
use Storage;
use DB;
use Cart;
use Cache;
use App\Classe;
class AjaxController
{
    public function fits_categories_all()
    {
        $fits = Fit::where('status', 1)->where('instock_status','!=',5)->select('id', 'name', 'slug', 'image', 'price')->orderBy('instock_status')->orderBy('price')->paginate(20);
        $status = count($fits);
        $render_fit = view('web.fits_categories.fit_item')->with(['fits'=>$fits])->render();
        $fit_item =  (array) $render_fit;
        return [$status,$fit_item];
    }
    public function fits_categories($slug)
    {
        $fit_category = FitCategory::where('slug', $slug)->first();
        $fit_category_childs = FitCategory::where('parent_id', $fit_category->id)->where('slug', $slug)->get();

        $fit_category_childs_id = [];
        array_push($fit_category_childs_id, $fit_category->id);
        foreach ($fit_category_childs as $value) {
            array_push($fit_category_childs_id, $value->id);
        }
        $fits = Fit::wherein('category_id', $fit_category_childs_id)->where('status', 1)->where('instock_status','!=',5)->select('id', 'name', 'slug', 'image', 'price')->orderBy('instock_status')->orderBy('price')->paginate(20);

        $status = count($fits);
        $render_fit = view('web.fits_categories.fit_item')->with(['fits'=>$fits])->render();
        $fit_item =  (array) $render_fit;
        return [$status,$fit_item];
    }

    public function services_categories_all()
    {
        $services = Service::where('status', 1)->where('instock_status','!=',5)->orderBy('instock_status')->orderBy('price', 'DESC')->paginate(20);

        $config_promotion = DB::table('options')->select('value')->where('name','promotion')->first();
        if($config_promotion){
            $config_promotion = json_decode(base64_decode($config_promotion->value),true);
        }
        $config_promotion = $config_promotion["promotion_default_service"];
        $config_promotion = preg_split('/\n|\r\n/',$config_promotion);

        $status = count($services);
        $render_service = view('web.services_categories.service_item')->with(['services'=>$services,'config_promotion'=>$config_promotion])->render();
        $service_item =  (array) $render_service;
        return [$status,$service_item];
    }

    public function services_categories($slug)
    {
        $service_category = ServiceCategory::where('slug', $slug)->first();
        $service_category_childs = ServiceCategory::where('parent_id', $service_category->id)->where('status', 1)->select('id','name', 'slug')->take(7)->get();
        $service_category_childs_id = [];
        array_push($service_category_childs_id, $service_category->id);
        foreach ($service_category_childs as $value) {
            array_push($service_category_childs_id, $value->id);
        }
        $services = Service::wherein('category_id', $service_category_childs_id)->where('status', 1)->where('instock_status','!=',5)->select('id', 'name', 'slug', 'image', 'price','promotion')->orderBy('instock_status')->orderBy('price', 'DESC')->paginate(20);

        $config_promotion = DB::table('options')->select('value')->where('name','promotion')->first();
        if($config_promotion){
            $config_promotion = json_decode(base64_decode($config_promotion->value),true);
        }
        $config_promotion = $config_promotion["promotion_default_service"];
        $config_promotion = preg_split('/\n|\r\n/',$config_promotion);
        
        $status = count($services);
        $render_service = view('web.services_categories.service_item')->with(['services'=>$services,'config_promotion'=>$config_promotion])->render();
        $service_item =  (array) $render_service;
        return [$status,$service_item];
    }

    public function unlock_categories_all()
    {
        $unlocks = Unlock::where('status', 1)->where('instock_status','!=',5)->orderBy('instock_status')->orderBy('price', 'DESC')->paginate(20);

        $config_promotion = DB::table('options')->select('value')->where('name','promotion')->first();
        if($config_promotion){
            $config_promotion = json_decode(base64_decode($config_promotion->value),true);
        }
        $config_promotion = $config_promotion["promotion_default_unlock"];
        $config_promotion = preg_split('/\n|\r\n/',$config_promotion);

        $status = count($unlocks);
        $render_unlock = view('web.unlock_categories.unlock_item')->with(['unlocks'=>$unlocks,'config_promotion'=>$config_promotion])->render();
        $unlock_item =  (array) $render_unlock;
        return [$status,$unlock_item];
    }

    public function unlock_categories($slug)
    {
        $unlock_category = UnlockCategory::where('slug', $slug)->first();
//        $unlock_category_childs = UnlockCategory::where('parent_id', $unlock_category->id)->where('status', 1)->select('id','name', 'slug')->take(7)->get();
//        $unlock_category_childs_id = [];
//        array_push($service_category_childs_id, $unlock_category->id);
//        foreach ($unlock_category_childs as $value) {
//            array_push($unlock_category_childs_id, $value->id);
//        }
        $unlocks = Unlock::where('category_id', $unlock_category->id)->where('status', 1)->where('instock_status','!=',5)->select('id', 'name', 'slug', 'image', 'price','promotion')->orderBy('instock_status')->orderBy('price', 'DESC')->paginate(20);
        $config_promotion = DB::table('options')->select('value')->where('name','promotion')->first();
        if($config_promotion){
            $config_promotion = json_decode(base64_decode($config_promotion->value),true);
        }
        $config_promotion = $config_promotion["promotion_default_unlock"];
        $config_promotion = preg_split('/\n|\r\n/',$config_promotion);

        $status = count($unlocks);
        $render_unlock = view('web.unlock_categories.unlock_item')->with(['unlocks'=>$unlocks,'config_promotion'=>$config_promotion])->render();
        $unlock_item =  (array) $render_unlock;
        return [$status,$unlock_item];
    }

    public function sync_image(Request $request){
        $test = $request->list_product_image;
        $month = date('m');
        $year = date('Y');
        $path = $year.'/'.$month.'/';
        $zz = 1;
        foreach($test as $k => $v){
            $img = Image::make($v["link_img"]);
            
            Storage::disk('sop')->filePut2($img->stream()->__toString(),$path.$v["file_name"]);
        }
        return response(json_encode(compact('test')));
    }

    public function search_ajax(Request $request)
    {
        if (isset($request->keyword)) {
            $keyword = $request->keyword;
            $key = str_replace(" ","%",$keyword);
            $key = str_replace("\'","'",$key);
            $key = str_replace("'","''",$key);
        } else {
            $keyword = $key = "";
        }

        $status = 0;
        $data = [];

        //Tìm 3 điện thoại
        $phones = Phone::join('phone_categories','phone_categories.id','phones.category_id')
                    -> where('phones.name', 'LIKE', "%$key%")
                    // ->where('phone_categories.status',1)
                    ->where('phones.status',1)
                    ->select('phones.*')
                    ->take(3)->get();
        if($phones->count()){// có điện thoại
            $status = 1;
            foreach ($phones as $value) {
                $data[] = [
                    'id' => $value->id,
                    'name' => $value->name,
                    'image' => $value->getImage('tiny'),
                    'url' => $value->getUrl(),
                    'price' => $value->getPrice()
                ];
            }
        }

        if($phones->count() < 3) { //nếu số điện thoại tìm đc nhỏ hơn 3 => lấy thêm máy tính bảng
            $table_take = 3 - $phones->count();
            $tablets = Tablet::join('tablet_categories','tablet_categories.id','tablets.category_id')
                    ->where('tablets.name', 'LIKE', "%$key%")
                    ->where('tablets.status',1)
                    // ->where('tablet_categories.status',1)
                    ->select('tablets.*')
                    ->take(3)->get();
            if($tablets->count() > 0){// có điện thoại
                $status = 1;
                foreach ($tablets as $value) {
                    $data[] = [
                        'id' => $value->id,
                        'name' => $value->name,
                        'image' => $value->getImage('tiny'),
                        'url' => $value->getUrl(),
                        'price' => $value->getPrice()
                    ];
                }
            }
        }

        //Tìm 3 dịch vụ
        $services = Service::join('service_categories','service_categories.id','services.category_id')
                    ->where( 'services.name', 'LIKE', "%$key%")
                    ->where('services.status',1)
                    // ->where('service_categories.status',1)
                    ->select('services.*')
                    ->take(3)->get();
        if($services->count()) {
            $status = 1;
            foreach ($services as $value) {
                $data[] = [
                    'id' => $value->id,
                    'name' => $value->name,
                    'image' => $value->getImage('tiny'),
                    'url' => $value->getUrl(),
                    'price' => $value->getPrice()
                ];
            }
        }
        return response()->json(['status'=>$status,'data'=>$data]);
    }
    //đặt lịch sửa chữa
    public function orderService(Request $request)
    {
        if($request->ajax()){
            extract($request->all(),EXTR_OVERWRITE);

          
            // $regional_prices = DB::table('regional_prices')->where('location_id',$location)->where('service_id', $id)->first();
            

            $created_at = $updated_at = date("Y-m-d H:i:s");
            DB::table('orders')->insertGetid([
                'type_id'=>$id,
                'name'=>$name,
                'gender'=>$gender,
                'phone'=>$phone,
                'email'=>$email,
                'address'=>$address,
                'note'=>$note,
                'status'=>0,
                'location_id'=>$location,
                'type'=>$type,
                'price'=>$price,
                'link'=>$link,
                'created_at' => $created_at,
                'updated_at' => $updated_at
            ]);
            return 1;
        }
    }
    //thêm vào giỏi hàng
    public function Order(Request $request)
    {
        if($request->ajax()){
            extract($request->all(),EXTR_OVERWRITE);
            $data = DB::table($type)->where('id',$id)->first();
            $name = $data->name;
            $price = $data->price??0;
            $image = $data->image;
            $cart_info = [
                "id" => $id,
                "name" => $name,
                "price" => $price,
                "qty" => $qty,
                "options" => [
                    'type' => $type,
                    'image' => $image,
                    'slug' => $data->slug,
                ],
                
            ];
            Cart::add($cart_info);
            $cart_content = Cart::content();
            $cart_count = Cart::count();
            $cart_total = Cart::total();
            return response(json_encode(compact('cart_content','cart_count','cart_total','qty','image','name','price')));
        }
    }
    //trả lời bình luận
    public function replyComment(Request $request)
    {
        if($request->ajax()){
            extract($request->all(),EXTR_OVERWRITE);
           
            $created_at = $updated_at = date("Y-m-d H:i:s");
            $id_insert = DB::table('comment')->insertGetId([
                'type_id'=>$type_id,
                'type'=>$type,
                'parent_id'=>$parent_id ?? 0,
                'gender'=>$comment_author_gender,
                'name'=>$comment_author_name,
                'email'=>$comment_author_email,
                'phone'=>$comment_author_phone,
                'status'=>2,
                'content'=>$comment_editor,
                'created_at' => $created_at,
                'updated_at' => $updated_at
            ]);

            $comments = \App\Comment::where('type',$type)->where('status',1)->where('parent_id',0)->where('type_id',$type_id)->paginate(10);
            $comment_parent_admin_collect =collect( \App\Comment::where('status',1)->whereIn('parent_id',$comments->pluck('id','id')->toArray())->get() ); 

            return view('web.comments.list',compact('comments','comment_parent_admin_collect'));
        }
    }
    //đánh giá sao
    public function rating(Request $request) {
        if($request->ajax()){
            $id = $request->get('id');
            $id = intval($id);
            $point = $request->get('point',5);
            $point = intval($point);
            $type = $request->get('type','services');
            if ($point > 0 && $point <=5) {
                $in_rating[] = [
                    'ip' => get_client_ip(),
                    'type' => $type,
                    'type_id' => $id,
                    'value' => $point,
                ];
                DB::table('ratings')->insert($in_rating);
                if($type == 'services'){
                    setcookie('ratings_services_' . $id,1,(time()+365*24*60*60),'/');
                    Cache::forget('ratings_services_' . $id);
                }elseif($type == 'news'){
                    setcookie('ratings_news_' . $id,1,(time()+365*24*60*60),'/');
                    Cache::forget('ratings_news_' . $id);
                }
                return response()->json(['status'=>1,'message'=>'Cám ơn bạn đã đánh giá!']);
            }else {
                return response()->json(['status'=>0,'message'=>'Sai dữ liệu!']);
            }
        }else {
            return response()->json(['status'=>0,'message'=>'Hành động bị cấm !']);
        }
    }
    // call me
    
    public function callme(Request $request) {
        if($request->ajax()){
            extract($request->all(),EXTR_OVERWRITE);
            $created_at = $updated_at = date("Y-m-d H:i:s");
            if ($phone != '') {
                $in[] = [
                    'name' => $name ?? '',
                    'phone' => $phone ?? '',
                    'note' => $service_name ??'',
                    'type' => 'call_me',
                    'status' =>0,
                    'created_at' => $created_at,
                    'updated_at' => $updated_at
                ];
                DB::table('orders')->insert($in);
                return response()->json(['status'=>1,'message'=>'<p style="color: #00a651;">Cám ơn quý khách đã để lại số điện thoại. Chúng tôi sẽ liên hệ với quý khách trong thời gian sớm nhất !</p>']);
            }else {
                return response()->json(['status'=>0,'message'=>'<p style="color: #ff1e3d;">Sai dữ liệu !</p>']);
            }
        }else {
            return response()->json(['status'=>0,'message'=>'<p style="color: #ff1e3d;">Hành động bị cấm !</p>']);
        }
    }
    /**
     * liên hệ ngay chúng tôi
     */
    function Contact(Request $request)
    {
        if($request->ajax()){
            extract($request->all(),EXTR_OVERWRITE);
            $created_at = $updated_at = date("Y-m-d H:i:s");
            DB::table('orders')->insertGetid([
                'type_id'=>0,
                'name'=>$name,
                'phone'=>$phone,
                'email'=>$email,
                'address'=>$address,
                'note'=>$note,
                'status'=>0,
                'type'=>'contact',
                'created_at' => $created_at,
                'updated_at' => $updated_at
            ]);
            return 1;
        }
    }
    // thông báo nhận ưu đãi
    public function InforEndow(Request $request)
    {
        if($request->ajax()){
            extract($request->all(),EXTR_OVERWRITE);
            $created_at = $updated_at = date("Y-m-d H:i:s");
            DB::table('orders')->insertGetid([
                'name'=>$name_modal,
                'phone'=>$phone_modal,
                'email'=>$email_modal,
                'type'=>'promotion_notice',
                'status'=>0,
                'location_id'=>$location_id ?? 0,
                'created_at' => $created_at,
                'updated_at' => $updated_at
            ]);
            $request->session()->put('session_modal', '1');
            return 1;
        }
    }

    /**
     * show thêm bình luận
     */
    public function viewMoreComment(Request $request)
    {
        if($request->ajax()){
            extract($request->all(),EXTR_OVERWRITE);
            $from = ($page - 1)*$count;
            $comments = \App\Comment::where('status',1)->where('type',$type)->where('type_id',$type_id)->skip($from)->take($count)->get();
            $comment_parent_admin_collect =collect( \App\Comment::where('status',1)->whereIn('parent_id',$comments->pluck('id','id')->toArray())->get() ); 
            $render_phone = view('web.comments.list',compact('comments','comment_parent_admin_collect'))->render();
            return $render_phone ;
        }
    }
    /**
     * xem thêm phụ kiện , dịch vụ
     */
    public function viewFitService(Request $request)
    {
        if($request->ajax()){
            extract($request->all(),EXTR_OVERWRITE);
            $from = ($page-1)*$count;
            if($type == 'fits'){
                $fits = \App\Fit::where('status',1);

                if(isset($filter_price) && $filter_price != ''){
                    switch ($filter_price){
                        case '200.000':
                            $fits = $fits->whereBetween('price',[0,200000]);
                            break;
                        case '200.000vs500.000':
                            $fits = $fits->whereBetween('price',[200000,500000]);
                            break;
                        case '500.000vs1000.000':
                            $fits = $fits->whereBetween('price',[500000,1000000]);
                            break;
                        case '1000.000':
                            $fits = $fits->whereBetween('price',[1000000,10000000]);
                            break;
                    }
                }

                if( isset($sort) && $sort != ''){
                    switch ($sort){
                        case 'asc':
                            $fits = $fits->orderBy('price','asc');
                            break;
                        case 'desc':
                            $fits = $fits->orderBy('price','desc');
                            break;
                        case 'default':
                            $fits = $fits->orderBy('id','desc');
                            break;
                    }
                }else{
                    $fits = $fits->orderBy('id','desc');
                }

                if($cate_id != ''){
                    $fits =  $fits->where('category_id',$cate_id);
                }
                $fits =   $fits->skip($from)->take($count)->get();
                return view('web.fits_categories.data_fit',compact('fits'))->render();
            }
            if($type == 'services'){

                if ( empty($_SERVER['HTTP_USER_AGENT']) ) {
                    $is_mobile = false;
                } elseif ( strpos($_SERVER['HTTP_USER_AGENT'], 'Mobile') !== false // many mobile devices (all iPhone, iPad, etc.)
                    || strpos($_SERVER['HTTP_USER_AGENT'], 'Android') !== false
                    || strpos($_SERVER['HTTP_USER_AGENT'], 'Silk/') !== false
                    || strpos($_SERVER['HTTP_USER_AGENT'], 'Kindle') !== false
                    || strpos($_SERVER['HTTP_USER_AGENT'], 'BlackBerry') !== false
                    || strpos($_SERVER['HTTP_USER_AGENT'], 'Opera Mini') !== false
                    || strpos($_SERVER['HTTP_USER_AGENT'], 'Opera Mobi') !== false ) {
                    $is_mobile = true;
                } else {
                    $is_mobile = false;
                }

                $config_promotion = DB::table('options')->select('value')->where('name','promotion')->first();
                if($config_promotion){
                    $config_promotion = json_decode(base64_decode($config_promotion->value),true);
                }
                $config_promotion = $config_promotion["promotion_default_service"];
                $config_promotion = preg_split('/\n|\r\n/',$config_promotion);

                $services = \App\Service::where('status',1);


                if(isset($brand) && $brand != '' ){
                    $service_categorie = ServiceCategory::where('status',1)->where('brand',$brand)->pluck('id','id')->toArray();
                    $services =  $services->whereIn('category_id',$service_categorie);
                }

                if(isset($request1) && $request1 != '' ){
                    $class = Classe::where('status',1)->where('slug',$request1)->first();
                    $services =  $services->where('class_id',$class->id);
                }

                if($cate_id != ''){
                    $service_category = ServiceCategory::where('id', $cate_id)->where('status',1)->first();
                    $service_category_parent = ServiceCategory::where('id',$service_category->parent_id)->where('status',1)->first();
            
                    if($service_category->parent_id == 0){
                        $services_categories = ServiceCategory::where('status',1)->where('parent_id', $service_category->id)
                        ->get();
                    }else{
                        $services_categories = ServiceCategory::where('status',1)->where('parent_id', $service_category->parent_id)
                       ->get();
                    }
                    $service_category_childs = ServiceCategory::where('parent_id', $service_category->id)->where('status', 1)->orderBy('order','asc')->select('id','name', 'slug')->take(7)->get();
                    $service_category_childs_id = [];
                    array_push($service_category_childs_id, $service_category->id);
                    foreach ($service_category_childs as $value) {
                        array_push($service_category_childs_id, $value->id);
                    }
                    $services = $services->wherein('category_id', $service_category_childs_id);
                }
                $services =   $services->skip($from)->take($count)->get();
                return view('web.layouts.services.data_service_category',compact('services','config_promotion','is_mobile'))->render();
            }
           
        }
    }

    /**
     * lưu session modal
     */
    public function saveSessionModal(Request $request)
    {
        $request->session()->put('session_modal', '1');
        return 1;
    }

}
