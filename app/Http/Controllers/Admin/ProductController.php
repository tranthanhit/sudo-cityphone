<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\MyClass\MyForm;
use App\MyClass\ListData;
use App\MyClass\Categories;
use DB;
use App\Product;

class ProductController extends Controller
{
    public $attributes = [
        'display' => 'Màn hình hiển thị',
        'os' => 'Hệ điều hành',
        'primary_camera' => 'Camera chính',
        'second_camera' => 'Camera trước',
        'cpu' => 'CPU',
        'ram' => 'RAM',
        'rom' => 'ROM',
        'pin' => 'Dung lượng pin'
    ];
    function __construct()
    {
        $this->module_name = 'sản phẩm';
        $this->table_name = 'products';
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->checkRole($this->table_name.'_access');

        $categories = new Categories('products_categories');
        $array_categories = $categories->data_categories();

        $listdata = new ListData($request,$this->table_name);
        $listdata->add('image','Ảnh đại diện','string');
        $listdata->add('name','Tên sản phẩm','string',1);
        $listdata->add('price','Giá','int',1);
        $listdata->add('','Danh mục','string');
        $listdata->add('','Thông tin');
        $listdata->add('status','Trạng thái','status',1,[1=>'Hoạt động',2=>'Không hoạt động',3=>'Thùng rác']);
        $listdata->add('','Sửa','edit');
        $listdata->add('','Xóa','delete');

        $data = $listdata->data();
        return view('admin.layouts.list',compact('data','array_categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->checkRole($this->table_name.'_create');

        $categories = new Categories('products_categories');
        $array_categories = $categories->list_categories();

        $form = new MyForm();
        $data_form[] = $form->text('name','',1,'Tên sản phẩm','',1,'slug');
        $data_form[] = $form->slug('slug','');
        $data_form[] = $form->multi_cate('cate',[],1,'Danh mục',$array_categories);
        $data_form[] = $form->text('price','',0,'Giá');
        $data_form[] = $form->image('image','',0);
        $data_form[] = $form->slide('slides','',0);
        

        $data_form[] = $form->related('related_products','',0,'Chọn 3 sản phẩm liên quan','Tìm theo tên sản phẩm ...','products');
        $data_form[] = $form->related('related_news','',0,'Chọn 4 tin tức liên quan','Tìm theo tiêu đề tin ...','news');
        $data_form[] = $form->textarea('description','',0,'Thông tin cơ bản','Nhập các thông tin cơ bản, xuống dòng với mỗi thông tin mới');
        $data_form[] = $form->editor('detail','',0,'Nội dung');
        $data_form[] = $form->checkbox('status',1,1,'Kích hoạt');
        //specifications
        $data_form[] = $form->start_group('specifications','Thông số kỹ thuật');
        foreach ($this->attributes as $key=>$value) {
            $data_form[] = $form->text($key,'',0,$value);
        }
        $data_form[] = $form->end_group();

        $data_form[] = $form->action('add');
        return view('admin.layouts.create',compact('data_form'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->checkRole($this->table_name.'_create');

        $this->validate_form($request,'name',1,'Bạn chưa nhập tiêu đề');
        $this->validate_form($request,'slug',1,'Đường dẫn không được để trống',1,'Đường dẫn bị trùng');
        $this->validate_form($request,'cate',1,'Bạn chưa chọn danh mục');

        $created_at = $updated_at = date("Y-m-d H:i:s");
        $data_form = $request->all();
        $status = 2;
        extract($data_form,EXTR_OVERWRITE);// đưa mảng về các biến có tên là các key của mảng

        if (isset($slides) && $slides != '') {
            $slides = implode(',', $slides);
        }else {
            $slides = '';
        }
        if (isset($related_products)) {
            $related_products = implode(',',$related_products);
        }else {
            $related_products = '';
        }
        if (isset($related_news)) {
            $related_news = implode(',',$related_news);
        }else {
            $related_news = '';
        }

        $data_insert = compact('name','slug','price','image','slides','description','detail','related_products','related_news','status','created_at','updated_at');
        $id_insert = DB::table($this->table_name)->insertGetId($data_insert);

        foreach ($cate as $key => $value) {
            DB::table('products_categories_map')->insert(['category_id'=>$value,'product_id'=>$id_insert]);
        }

        $product = Product::find($id_insert);
        foreach ($this->attributes as $key=>$value) {
            if (isset($$key) && $$key != '') {
                $product->set_attribute($key,$$key);
            }
        }

        $this->metaSeo($id_insert,$seo_title,$seo_description,$seo_robots);
        $this->systemLogs('Thêm mới '.$this->module_name,'store',$this->table_name,$id_insert,$data_insert);
        return redirect(route($this->table_name.'.'.$redirect,$id_insert))->with(['flash_level'=>'success','flash_message'=>'Thêm mới thành công!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->checkRole($this->table_name.'_edit');

        // $data_edit = DB::table($this->table_name)->where('id',$id)->first();
        $product = Product::find($id);

        $categories = new Categories('products_categories');
        $array_categories = $categories->list_categories();

        $products_categories = DB::table('products_categories_map')->where('product_id',$id)->pluck('category_id')->toArray();

        $form = new MyForm();
        $data_form[] = $form->text('name',$product->name,1,'Tên sản phẩm','',1,'slug');
        $data_form[] = $form->slug('slug',$product->slug);
        $data_form[] = $form->multi_cate('cate',$products_categories,1,'Danh mục',$array_categories);
        $data_form[] = $form->text('price',$product->price,0,'Giá');
        $data_form[] = $form->image('image',$product->image,0);
        $data_form[] = $form->slide('slides',explode(',',$product->slides),0);
        $data_form[] = $form->related('related_products',$product->related_products,0,'Chọn 3 sản phẩm liên quan','Tìm theo tên sản phẩm ...','products');
        $data_form[] = $form->related('related_news',$product->related_news,0,'Chọn 4 tin tức liên quan','Tìm theo tiêu đề tin ...','news');
        $data_form[] = $form->textarea('description',$product->description,0,'Thông tin cơ bản','Nhập các thông tin cơ bản, xuống dòng với mỗi thông tin mới');
        $data_form[] = $form->editor('detail',$product->detail,0,'Nội dung');
        $data_form[] = $form->checkbox('status',$product->status,1,'Kích hoạt');
        //specifications
        $data_form[] = $form->start_group('specifications','Thông số kỹ thuật');
        foreach ($this->attributes as $key=>$value) {
            $data_form[] = $form->text($key,$product->get_attribute($key),0,$value);
        }
        $data_form[] = $form->end_group();
        $data_form[] = $form->action('edit',route('web.'.$this->table_name.'.show',$product->slug));
        return view('admin.layouts.edit',compact('data_form','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->checkRole($this->table_name.'_edit');
        $data_edit = DB::table($this->table_name)->where('id',$id)->first();

        $this->validate_form($request,'name',1,'Bạn chưa nhập tiêu đề');
        $this->validate_form($request,'slug',1,'Đường dẫn không được để trống');
        $this->validate_form($request,'cate',1,'Bạn chưa chọn danh mục');

        $updated_at = date("Y-m-d H:i:s");
        $data_form = $request->all();
        $status = 2;
        extract($data_form,EXTR_OVERWRITE);

        if (isset($slides) && $slides != '') {
            $slides = implode(',', $slides);
        }else {
            $slides = '';
        }
        if (isset($related_products)) {
            $related_products = implode(',',$related_products);
        }else {
            $related_products = '';
        }
        if (isset($related_news)) {
            $related_news = implode(',',$related_news);
        }else {
            $related_news = '';
        }

        $data_update = compact('name','slug','price','image','slides','description','detail','related_products','related_news','status','updated_at');

        DB::table($this->table_name)->where('id',$id)->update($data_update);

        DB::table('products_categories_map')->where('product_id',$id)->delete();
        foreach ($cate as $key => $value) {
            DB::table('products_categories_map')->insert(['category_id'=>$value,'product_id'=>$id]);
        }

        $product = Product::find($id);
        foreach ($this->attributes as $key=>$value) {
            if (isset($$key) && $$key != '') {
                $product->set_attribute($key,$$key);
            }
        }

        $this->metaSeo($id,$seo_title,$seo_description,$seo_robots);
        
        $old = [
            'name'=>$data_edit->name,
            'slug'=>$data_edit->slug,
            'price'=>$data_edit->price,
            'image'=>$data_edit->image,
            'slides'=>$data_edit->slides,
            'description'=>$data_edit->description,
            'detail'=>$data_edit->detail,
            'related_products'=>$data_edit->related_products,
            'related_news'=>$data_edit->related_news,
            'status'=>$data_edit->status,
            'updated_at'=>$data_edit->updated_at
        ];
        $this->systemLogs('Sửa '.$this->module_name,'update',$this->table_name,$id,['old'=>$old,'new'=>$data_update]);

        return redirect(route($this->table_name.'.'.$redirect,$id))->with(['flash_level'=>'success','flash_message'=>'Cập nhật dữ liệu thành công!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($this->hasRole($this->table_name.'_delete')) {
            DB::table($this->table_name)->where('id',$id)->update([
                'status'=>4,
                'name' => DB::raw("CONCAT(name, '--delete--".time()."')"),
                'slug' => DB::raw("CONCAT(slug, '--delete--".time()."')")
            ]);
            $this->systemLogs('Xóa '.$this->module_name,'delete',$this->table_name,$id);
            return response()->json(['status'=>1,'message'=>'Xóa thành công']);
        }else {
            return response()->json(['status'=>0,'message'=>'Xóa không thành công']);
        }
    }
}
