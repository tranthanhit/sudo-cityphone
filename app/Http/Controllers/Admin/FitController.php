<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\MyClass\MyForm;
use App\MyClass\ListData;
use App\MyClass\Categories;
use DB;
use App\Fit;

use App\Exports\FittingExport;
use Maatwebsite\Excel\Facades\Excel;

class FitController extends Controller
{
    function __construct()
    {
        $this->module_name = 'phụ kiện';
        $this->table_name = 'fits';
        $this->has_google_shopping = true;
        parent::__construct();
    }
    public function index(Request $request)
    {
        $this->checkRole($this->table_name.'_access');

        $categories = new Categories('fit_categories');
        $array_categories = $categories->data_select_categories();

        $listdata = new ListData($request,$this->table_name);
        $listdata->add('image','Ảnh đại diện','string');
        $listdata->add('name','Tên','string',1);
        $listdata->add('category_id','Danh mục','array',1,$array_categories);
        $listdata->add('','Thông tin');
        $listdata->add('home','Ghim trang chủ','pins');
        $listdata->add('status','Trạng thái','status',1,[1=>'Hoạt động',2=>'Không hoạt động',3=>'Thùng rác']);
        $listdata->add('','Sửa','edit');
        $listdata->add('','Xóa','delete');

        $data = $listdata->data();


        $t_t = $data['show_data'];

        $system_logs_collect =collect( DB::table('system_logs')->where('table','fits')->whereIn('table_id',$t_t->pluck('id')->toArray())->orderBy('id','DESC')->get() );
        $user_collect = collect(  DB::table('admin_users')->whereIn('id',$system_logs_collect->pluck('admin_id','admin_id')->toArray())->get() );


        return view('admin.layouts.list',compact('data','array_categories','system_logs_collect','user_collect'));        
    }

    public function export(Request $request) {
        $status = $request->get('status',-1);
        $category_id = $request->get('category_id',-1);
        $name = $request->get('name','');

        $data = DB::table('orders');
        if($status != -1) {
            $data = $data->where('status',$status);
        }
        if($category_id != -1) {
            $data = $data->where('category_id',$category_id);
        }
        if($name != '') {
            $data = $data->where('name','LIKE','%'.$name.'%');
        }
        $count = $data->count();

        $time = date("H-i_d-m-Y");
        return (new FittingExport($status,$category_id,$name))->download('phukien_'.$time.'.xlsx');
    }
    public function create()
    {
        //
        $this->checkRole($this->table_name.'_create');

        $categories = new Categories('fit_categories');
        $array_categories = $categories->data_select_categories();
        $array_instock_status = config('app.instock_status');
        $form = new MyForm();
        $data_form[] = $form->select('category_id',0,1,'Danh mục',$array_categories);
        $data_form[] = $form->text('name','',1,'Tên','',1,'slug');
        $data_form[] = $form->slug('slug','');
        $data_form[] = $form->image('image','',0);
        $data_form[] = $form->slide('slides','',0);
        $data_form[] = $form->textarea('info','',0,'Thông tin cơ bản','Xuống dòng với mỗi thông tin');
        $data_form[] = $form->text('package','',0,'Đóng gói sản phẩm','VD: Hộp, cáp, sạc, tai nghe, cây lấy sim, sách hướng dẫn');
        $data_form[] = $form->text('price_old','',0,'Giá cũ');
        $data_form[] = $form->text('price','',0,'Giá');
        $data_form[] = $form->custom('admin.layouts.fit.option');
        $data_form[] = $form->text('warranty','',0,'Bảo hành','VD: 6 tháng');
        $data_form[] = $form->select('instock_status','',0,'Trạng thái hàng',$array_instock_status);
        $data_form[] = $form->checkbox('internal_link',0,1,'Link nội bộ');
        $data_form[] = $form->editor('detail','',0,'Nội dung');
        $data_form[] = $form->tags('tags','',0,'Tags');
        $data_form[] = $form->checkbox('status',1,1,'Kích hoạt');
        $data_form[] = $form->action('add');

        return view('admin.layouts.create',compact('data_form'));
    }
    public function store(Request $request)
    {
        //
        $this->checkRole($this->table_name.'_create');

        $this->validate_form($request,'name',1,'Bạn chưa nhập tiêu đề');
        $this->validate_form($request,'slug',1,'Đường dẫn không được để trống',1,'Đường dẫn bị trùng');
        $created_at = $updated_at = date("Y-m-d H:i:s");
        $data_form = $request->all();
        $status = 2;
        extract($data_form,EXTR_OVERWRITE);
        if (isset($slides) && $slides != '') {
            $slides = implode(',', $slides);
        }else {
            $slides = '';
        }
        if(isset($internal_link)){
            $detail = insert_internal_link($detail);
        }
        if(isset($data_form["option"]) && $data_form["option"] != null){
            $option = base64_encode(json_encode($data_form["option"]));    
        }else{
            $option = "";
        }
      
        $data_insert = compact('category_id','name','slug','image','slides','price_old','price','warranty','option','info','instock_status','package','detail','tags','status','created_at','updated_at');
        $id_insert = DB::table($this->table_name)->insertGetId($data_insert);
        $this->metaSeo($id_insert,$seo_title,$seo_description,$seo_robots);
        $this->googleShopping($id_insert,$google_shopping_brand,$google_shopping_category,$google_shopping_instock,$google_shopping_itemcondition);
       
        $this->systemLogs('Thêm mới '.$this->module_name,'store',$this->table_name,$id_insert,$data_insert);
        return redirect(route($this->table_name.'.'.$redirect,$id_insert))->with(['flash_level'=>'success','flash_message'=>'Thêm mới thành công!']);
    }
    public function show($id)
    {
        //
    }
    public function edit($id)
    {
        //
        $this->checkRole($this->table_name.'_edit');
        $fit = Fit::find($id);
        $categories = new Categories('fit_categories');
        $array_categories = $categories->data_select_categories();
        $array_instock_status = config('app.instock_status');
        $option = json_decode(base64_decode($fit->option));
        $form = new MyForm();
        
        $data_form[] = $form->select('category_id',$fit->category_id,1,'Danh mục',$array_categories);
        $data_form[] = $form->text('name',$fit->name,1,'Tên','',1,'slug');
        $data_form[] = $form->slug('slug',$fit->slug);
        $data_form[] = $form->image('image',$fit->image,0);
        $data_form[] = $form->slide('slides',explode(',',$fit->slides),0);
        $data_form[] = $form->textarea('info',$fit->info,0,'Thông tin cơ bản','Xuống dòng với mỗi thông tin');
        $data_form[] = $form->text('package',$fit->package,0,'Đóng gói sản phẩm','VD: Hộp, cáp, sạc, tai nghe, cây lấy sim, sách hướng dẫn');
        $data_form[] = $form->text('price_old',$fit->price,0,'Giá cũ');
        $data_form[] = $form->text('price',$fit->price,0,'Giá');
        $data_form[] = $form->custom('admin.layouts.fit.option');
        $data_form[] = $form->text('warranty',$fit->warranty,0,'Bảo hành','VD: 6 tháng');
        $data_form[] = $form->select('instock_status',$fit->instock_status,0,'Trạng thái hàng',$array_instock_status);
        $data_form[] = $form->checkbox('internal_link',0,1,'Link nội bộ');
        $data_form[] = $form->editor('detail',$fit->detail,0,'Nội dung');
        $data_form[] = $form->tags('tags',$fit->tags,0,'Tags');
        $data_form[] = $form->checkbox('status',1,1,'Kích hoạt');
        $data_form[] = $form->action('edit',route('web.fits.show',$fit->slug));
        return view('admin.layouts.edit',compact('data_form','id','option'));
    }
    public function update(Request $request, $id)
    {
        //
        $this->checkRole($this->table_name.'_edit');
        $data_edit = DB::table($this->table_name)->where('id',$id)->first();

        $this->validate_form($request,'name',1,'Bạn chưa nhập tiêu đề');
        $this->validate_form($request,'slug',1,'Đường dẫn không được để trống');
        $this->validate_form($request,'category_id',1,'Bạn chưa chọn danh mục');
        $updated_at = $created_at = date("Y-m-d H:i:s");
        $data_form = $request->all();
        $status = 2;

        extract($data_form,EXTR_OVERWRITE);
        if (isset($slides) && $slides != '') {
            $slides = implode(',', $slides);
        }else {
            $slides = '';
        }
        if(isset($internal_link)){
            $detail = insert_internal_link($detail);
        }
        if(isset($data_form["option"]) && $data_form["option"] != null){
            $option = base64_encode(json_encode($data_form["option"]));    
        }else{
            $option = "";
        }
        $data_update = compact('category_id','name','slug','image','slides','price_old','price','warranty','option','info','instock_status','package','detail','tags','status','created_at','updated_at');
        DB::table($this->table_name)->where('id',$id)->update($data_update);
        $this->metaSeo($id,$seo_title,$seo_description,$seo_robots);
       
        $this->googleShopping($id,$google_shopping_brand,$google_shopping_category,$google_shopping_instock,$google_shopping_itemcondition);
       
        $old = [
            'category_id'=>$data_edit->category_id,
            'name'=>$data_edit->name,
            'slug'=>$data_edit->slug,
            'image'=>$data_edit->image,
            'detail'=>$data_edit->detail,
            'status'=>$data_edit->status,
            'updated_at'=>$data_edit->updated_at
        ];
        
        $this->systemLogs('Sửa '.$this->module_name,'update',$this->table_name,$id,['old'=>$old,'new'=>$data_update]);
        return redirect(route($this->table_name.'.'.$redirect,$id))->with(['flash_level'=>'success','flash_message'=>'Cập nhật dữ liệu thành công!']);
    }
    public function destroy($id)
    {
        //
        if($this->hasRole($this->table_name.'_delete')) {
            DB::table($this->table_name)->where('id',$id)->update([
                'status'=>4,
                'name' => DB::raw("CONCAT(name, '--delete--".time()."')"),
                'slug' => DB::raw("CONCAT(slug, '--delete--".time()."')")
            ]);
            return response()->json(['status'=>1,'message'=>'Xóa thành công']);
        }else {
            return response()->json(['status'=>0,'message'=>'Xóa không thành công']);
        }
    }
}
