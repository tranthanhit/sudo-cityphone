<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\MyClass\MyForm;
use App\MyClass\ListData;
use App\MyClass\Categories;
use DB;
use App\Service;
class ServiceController extends Controller
{
    function __construct()
    {
        $this->module_name = 'sửa chữa';
        $this->table_name = 'services';
        $this->has_google_shopping = true;
        parent::__construct();
    }
    public function index(Request $request)
    {
        $this->checkRole($this->table_name.'_access');

        $categories = new Categories('service_categories');
        $array_categories = $categories->data_select_categories();

        $class = new Categories('classes');
        $array_class = $class->data_select_categories();

        $listdata = new ListData($request,$this->table_name);
        $listdata->add('category_id','Danh mục','array',1,$array_categories);
        $listdata->add('category_id','Loại','array',1,$array_class);
        // $listdata->add('image','Ảnh đại diện','string');
        $listdata->add('name','Dịch vụ','string',1);
        $listdata->add('price','Giá','string');
        $listdata->add('warranty','Bảo hành','string');
        $listdata->add('','Thông tin');
        $listdata->add('highlight','Ghim Nổi bật','pins');
        $listdata->add('status','Trạng thái','status',1,[1=>'Hoạt động',2=>'Không hoạt động',3=>'Thùng rác']);
        $listdata->add('','Sửa','edit');
        $listdata->add('','Xóa','delete');

        $data = $listdata->data();

        $t_t = $data['show_data'];

        $system_logs_collect =collect( DB::table('system_logs')->where('table','services')->whereIn('table_id',$t_t->pluck('id')->toArray())->orderBy('id','DESC')->get() );
        $user_collect = collect(  DB::table('admin_users')->whereIn('id',$system_logs_collect->pluck('admin_id','admin_id')->toArray())->get() );


        return view('admin.layouts.list',compact('data','array_categories','system_logs_collect','user_collect','array_class'));        
    }
    public function create()
    {
        //
        $this->checkRole($this->table_name.'_create');

        $categories = new Categories('service_categories');
        $array_categories = $categories->data_select_categories();

        
        $classes = new Categories('classes');
        $array_classes = $classes->data_select_categories();

        $array_instock_status = config('app.instock_status');
        $form = new MyForm();
     
        $data_form[] = $form->text('name','',1,'Tên','',1,'slug');
        $data_form[] = $form->slug('slug','');
        $data_form[] = $form->select('category_id',0,1,'Danh mục hãng',$array_categories);
        $data_form[] = $form->select('class_id',0,1,'Danh mục loại',$array_classes);
        $data_form[] = $form->image('image','',0);
        $data_form[] = $form->slide('slides','',0);
        $data_form[] = $form->select('instock_status','',0,'Trạng thái hàng',$array_instock_status);
        $data_form[] = $form->text('warranty','',0,'Bảo hành','VD: 6 tháng');
        
        $data_form[] = $form->text('intend_time','',0,'Thời gian dự kiến','VD: 1 tiếng');
        $data_form[] = $form->textarea('promotion','',0,'Khuyến mãi','Xuống dòng với mỗi khuyến mãi');
      
        $data_form[] = $form->text('price','',0,'Giá dịch vụ');
        $data_form[] = $form->custom('admin.customs.regional_prices');
        $data_form[] = $form->editor('price_table','<table style="width: 100%;" id="price_list_home">
        <tbody >
        <tr>
        <td style="width: 153px;"><strong>STT</strong></td>
        <td style="width: 186px;"><strong>TÊN MODEL</strong></td>
        <td style="width: 169px;"><strong>GIÁ</strong></td>
        </tr>
        <tr>
        <td style="width: 153px;"> </td>
        <td style="width: 186px;"> </td>
        <td style="width: 169px;"> </td>
        </tr>
        <tr>
        <td style="width: 153px;"> </td>
        <td style="width: 186px;"> </td>
        <td style="width: 169px;"> </td>
        </tr>
        </tbody>
        </table>',0,'Bảng giá dịch vụ');
       
        $data_form[] = $form->checkbox('internal_link',0,1,'Link nội bộ');
     
        $data_form[] = $form->textarea('infor','',0,'Mô tả ');
        $data_form[] = $form->editor('detail','',0,'Nội dung');
        $data_form[] = $form->checkbox('status',1,1,'Kích hoạt');
        $data_form[] = $form->action('add');

        return view('admin.layouts.create',compact('data_form'));
    }
    public function store(Request $request)
    {
        //
        $this->checkRole($this->table_name.'_create');

        $this->validate_form($request,'name',1,'Bạn chưa nhập tiêu đề');
        $this->validate_form($request,'slug',1,'Đường dẫn không được để trống',1,'Đường dẫn bị trùng');
        $created_at = $updated_at = date("Y-m-d H:i:s");
        $data_form = $request->all();

        dd($data_form);

        $status = 2;
        extract($data_form,EXTR_OVERWRITE);
        if (isset($slides) && $slides != '') {
            $slides = implode(',', $slides);
        }else {
            $slides = '';
        }
       
       
        if(isset($internal_link)){
            $detail = insert_internal_link($detail);
        }
        $data_insert = compact('category_id','class_id','name','slug','image','slides','price','price_table','warranty','intend_time','promotion','instock_status','infor','detail','status','created_at','updated_at');
        $id_insert = DB::table($this->table_name)->insertGetId($data_insert);

        if(isset($localtion)){
            foreach($localtion as $key=>$value){
                if($value != 0){
                    DB::table('regional_prices')->insert([
                        'service_id'=>$id_insert,
                        'location_id'=>$value,
                        'price'=>$price_regional[$key],
                    ]);
                }
            }
        }

        $this->metaSeo($id_insert,$seo_title,$seo_description,$seo_robots);
        $this->googleShopping($id_insert,$google_shopping_brand,$google_shopping_category,$google_shopping_instock,$google_shopping_itemcondition);

        $this->systemLogs('Thêm mới '.$this->module_name,'store',$this->table_name,$id_insert,$data_insert);
        return redirect(route($this->table_name.'.'.$redirect,$id_insert))->with(['flash_level'=>'success','flash_message'=>'Thêm mới thành công!']);
    }
    public function show($id)
    {
        //
    }
    public function edit($id)
    {
        $this->checkRole($this->table_name.'_edit');

        $data_edit = DB::table($this->table_name)->where('id',$id)->first();

        $categories = new Categories('service_categories');
        $array_categories = $categories->data_select_categories();

        $classes = new Categories('classes');
        $array_classes = $classes->data_select_categories();

        $array_instock_status = config('app.instock_status');

        $form = new MyForm();
        $data_form[] = $form->text('name',$data_edit->name,1,'Tiêu đề tin','',1,'slug');
        $data_form[] = $form->slug('slug',$data_edit->slug);
        $data_form[] = $form->select('category_id',$data_edit->category_id,1,'Danh mục',$array_categories);
        $data_form[] = $form->select('class_id',$data_edit->class_id,0,'Danh mục loại',$array_classes);
        $data_form[] = $form->image('image',$data_edit->image,0);
        $data_form[] = $form->slide('slides',explode(',',$data_edit->slides),0);
        $data_form[] = $form->select('instock_status','',0,'Trạng thái hàng',$array_instock_status);
        $data_form[] = $form->text('warranty',$data_edit->warranty,0,'Bảo hành','VD: 6 tháng');
        $data_form[] = $form->text('intend_time',$data_edit->intend_time,0,'Thời gian dự kiến','VD: 1 tiếng');
        $data_form[] = $form->textarea('promotion',$data_edit->promotion,0,'Khuyến mại');
        $data_form[] = $form->text('price',$data_edit->price,0,'Giá dịch vụ');
        $data_form[] = $form->custom('admin.customs.regional_prices',$data_edit);
         $data_form[] = $form->editor('price_table',$data_edit->price_table ?? '<table style="width: 100%;" id="price_list_home">
         <tbody  >
         <tr>
         <td style="width: 153px;"><strong>STT</strong></td>
         <td style="width: 186px;"><strong>TÊN MODEL</strong></td>
         <td style="width: 169px;"><strong>GIÁ</strong></td>
         </tr>
         <tr>
         <td style="width: 153px;"> </td>
         <td style="width: 186px;"> </td>
         <td style="width: 169px;"> </td>
         </tr>
         <tr>
         <td style="width: 153px;"> </td>
         <td style="width: 186px;"> </td>
         <td style="width: 169px;"> </td>
         </tr>
         </tbody>
         </table>',0,'Bảng giá dịch vụ');
        $data_form[] = $form->textarea('infor',$data_edit->infor,0,'Mô tả ');
        $data_form[] = $form->editor('detail',$data_edit->detail,0,'Nội dung');

     
        $data_form[] = $form->checkbox('status',$data_edit->status,1,'Kích hoạt');
        $data_form[] = $form->action('edit',route('web.service.show',$data_edit->slug));
        return view('admin.layouts.edit',compact('data_form','id'));
    }
    public function update(Request $request, $id)
    {
        //
        $this->checkRole($this->table_name.'_edit');
        $data_edit = DB::table($this->table_name)->where('id',$id)->first();

        $this->validate_form($request,'name',1,'Bạn chưa nhập tiêu đề');
        $this->validate_form($request,'slug',1,'Đường dẫn không được để trống');
        $this->validate_form($request,'category_id',1,'Bạn chưa chọn danh mục');
        $created_at = $updated_at = date("Y-m-d H:i:s");
        $data_form = $request->all();
        extract($data_form,EXTR_OVERWRITE);
        if (isset($slides) && $slides != '') {
            $slides = implode(',', $slides);
        }else {
            $slides = '';
        }
     
        if(isset($internal_link)){
            $detail = insert_internal_link($detail);
        }
        if (isset($status)) {
            $status = 1;
        } else {
            $status = 2;
        }
        $data_update = compact('category_id','class_id','name','slug','image','slides','price','price_table','warranty','intend_time','instock_status','promotion','infor','detail','status','created_at','updated_at');
        DB::table($this->table_name)->where('id',$id)->update($data_update);

        
        if(isset($localtion)){
            DB::table('regional_prices')->where('service_id', $id)->delete();
            foreach($localtion as $key=>$value){
                if($value != 0){
                    DB::table('regional_prices')->insert([
                        'service_id'=>$id,
                        'location_id'=>$value,
                        'price'=>$price_regional[$key],
                    ]);
                }
            }
        }
     
        $old = [
            'category_id'=>$data_edit->category_id,
            'name'=>$data_edit->name,
            'slug'=>$data_edit->slug,
            'image'=>$data_edit->image,
            'detail'=>$data_edit->detail,
            'status'=>$data_edit->status,
            'updated_at'=>$data_edit->updated_at
        ];
        $this->metaSeo($id,$seo_title,$seo_description,$seo_robots);
        $this->googleShopping($id,$google_shopping_brand,$google_shopping_category,$google_shopping_instock,$google_shopping_itemcondition);

        $this->systemLogs('Sửa '.$this->module_name,'update',$this->table_name,$id,['old'=>$old,'new'=>$data_update]);
        return redirect(route($this->table_name.'.'.$redirect,$id))->with(['flash_level'=>'success','flash_message'=>'Cập nhật dữ liệu thành công!']);
    }
    public function destroy($id)
    {
        //
        if($this->hasRole($this->table_name.'_delete')) {
            DB::table($this->table_name)->where('id',$id)->update([
                'status'=>4,
                'name' => DB::raw("CONCAT(name, '--delete--".time()."')"),
                'slug' => DB::raw("CONCAT(slug, '--delete--".time()."')")
            ]);
            return response()->json(['status'=>1,'message'=>'Xóa thành công']);
        }else {
            return response()->json(['status'=>0,'message'=>'Xóa không thành công']);
        }
    }
}
