<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\MyClass\MyForm;
use App\MyClass\ListData;
use App\MyClass\Categories;
use DB;
use Illuminate\Support\Facades\Auth;
use App\Phone;
use App\Fit;
use App\Tablet;
use App\Service;
use App\Comment;

use App\Exports\CommentExport;
class CommentController extends Controller
{
    function __construct()
    {
        $this->module_name = 'Đánh giá';
        $this->table_name = 'comment';
        parent::__construct();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $arr_stt = [1=>'Đã duyệt',2=>'Chờ duyệt',3=>'Hủy'];
        $this->checkRole($this->table_name.'_access');

        $listdata = new ListData($request,$this->table_name);
        $listdata->add('status','Trạng thái','array',1,$arr_stt);
        $listdata->add('content','Nội dung bình luận','string',0);
        $listdata->add('','Thông tin khách');
        $listdata->add('created_at','Thông tin','range',1);
        $listdata->add('','Trả lời');
        $listdata->add('','Xóa','delete');

        $data = $listdata->data();

        $comment_data = $data['show_data'];
        $comment_phones = []; $comment_fittings = []; $comment_tablets = []; $comment_services = [];
        $comment_adm = $comment_data->pluck('id');
        foreach ($comment_data as $value) {
            switch ($value->type) {
                case 'phones':
                    $comment_phones[] = $value->type_id;
                break;
                case 'tablets':
                    $comment_tablets[] = $value->type_id;
                break;
                case 'fits':
                    $comment_fittings[] = $value->type_id;
                break;
                case 'services':
                    $comment_services[] = $value->type_id;
                break;
            }
        }
        $fittings = Fit::whereIn('id',$comment_fittings)->get();
        $services = Service::whereIn('id',$comment_services)->get();
        $comment_admins = Comment::whereIn('parent_id',$comment_adm)
            ->leftJoin('admin_users','admin_users.id','comment.admin_id')
            ->where('comment.status','!=',-1)
            ->select('comment.*','admin_users.fullname')
            ->get();
        
        return view('admin.layouts.list',compact('data','arr_stt','fittings','services','comment_admins'));
    }

    public function export(Request $request) {

        $status = $request->status;
        $start = $request->created_at_start;
        $end = $request->created_at_end;
        $product_name = $request->product_name;
        $type = $request->type;
        $comment_users = $request->comment_users;
        $comment_phones = $request->comment_phones;

        $data = DB::table($this->table_name)->where('status','!=',4);
        if (isset($type)) {
            if ($type == '' || $type == null) { 
                $data = $data;
            } if ($type == 'news') {
                $data = $data->wherein('type',['news','pages']);
            } else {
                $data = $data->where('type',$type);
            }
        }
        if($start != '' && $end !== '') {
            $data = $data->where('created_at','>',$start);
            $data = $data->where('created_at','<',$end);
        }
        if ($product_name != null && trim($product_name) != '') {
            if ($type != null) {
                $product = DB::table($type)->where('name','LIKE','%'.$product_name.'%')->get();
                $data = $data->whereIn('type_id',$product->pluck('id'));
            }
        }
        if ($comment_users != null && trim($comment_users) != '') {
            $admin = DB::table('admin_users')->where('fullname','LIKE','%'.$comment_users.'%')->get();
            $comment_admin = DB::table($this->table_name)->whereIn('admin_id',$admin->pluck('id'))->get();
            $data = $data->whereIn('id',$comment_admin->pluck('parent_id'));
        }
        if ($comment_phones != null && trim($comment_phones) != '') {
            $data = $data->where('phone','LIKE','%'.$comment_phones.'%');
        }       
        $count = $data->count();
        if($count <= 1000) {
            $time = date("H-i_d-m-Y");
            return (new CommentExport($status,$start,$end,$product_name,$type,$comment_users,$comment_phones))->download('comments_'.$time.'.xlsx');
        }else {
            die('Số lượng xuất quá lớn, vui lòng thu nhỏ bộ lọc');
        }
        return $request->all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $this->checkRole($this->table_name.'_edit');

        $data_edit = DB::table($this->table_name)->where('id',$id)->first();
        $form = new MyForm();
        $data_form[] = $form->text('name',$data_edit->name,1,'Họ tên','');
        $data_form[] = $form->text('email',$data_edit->email,0,'Email','');
        $data_form[] = $form->textarea('content',$data_edit->content,1,'Nội dung','');
        $data_form[] = $form->checkbox('status',$data_edit->status,1,'Duyệt');
        $data_form[] = $form->action('edit');
        return view('admin.layouts.edit',compact('data_form','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->checkRole($this->table_name.'_edit');

        $this->validate_form($request,'name',1,'Bạn chưa nhập tên');
        $this->validate_form($request,'content',1,'Bạn chưa nhập nội dung');

        $updated_at = date("Y-m-d H:i:s");
        $data_form = $request->all();
        $status = 2;
        extract($data_form,EXTR_OVERWRITE);

        $data_update = compact('name','email','content','status','updated_at');

        DB::table($this->table_name)->where('id',$id)->update($data_update);

        return redirect(route($this->table_name.'.'.$redirect,$id))->with(['flash_level'=>'success','flash_message'=>'Cập nhật dữ liệu thành công!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if($this->hasRole($this->table_name.'_delete')) {
            DB::table($this->table_name)->where('id',$id)->update(['status'=>4]);
            return response()->json(['status'=>1,'message'=>'Xóa thành công']);
        }else {
            return response()->json(['status'=>0,'message'=>'Xóa không thành công']);
        }
    }
    public function reply($id) {
        $this->checkRole($this->table_name.'_create');
        $data_edit = DB::table($this->table_name)->where('id',$id)->first();
        if($data_edit->parent_id == 0) {
            $data_parent = $data_edit;
        }else {
            $data_parent = DB::table($this->table_name)->where('id',$data_edit->parent_id)->first();
        }
        $child = DB::table($this->table_name)->where('parent_id',$data_parent->id)->get();

        switch ($data_parent->type) {
            case 'phones': $products = Phone::find($data_parent->type_id); break;
            case 'tablets': $products = Tablet::find($data_parent->type_id); break;
            case 'fits': $products = Fit::find($data_parent->type_id); break;
            case 'services': $products = Service::find($data_parent->type_id); break;
        }
        $data = [
            'comment' => $data_parent,
            'child' => $child
        ];
        return view('admin.comment.reply',compact('data','id','products'));
    }
    public function postReply(Request $request, $id) {
        $this->checkRole($this->table_name.'_create');

        $this->validate_form($request,'name',1,'Bạn chưa nhập tên');
        $this->validate_form($request,'content',1,'Bạn chưa nhập nội dung');

        $parent_id = $id;
        $data_edit = DB::table($this->table_name)->where('id',$id)->first();
        $url = $data_edit->url;
        $adm = Auth::guard('admin')->user();
        $admin_id = $adm->id;
        $name = $request->get('name');
        $content = $request->get('content');
        $created_at = $updated_at = date('Y-m-d H:i:s');

        $insert_id = DB::table('comment')->insertGetId(compact('parent_id','url','name','admin_id','content','created_at','updated_at'));
        
        return redirect()->back();
        // return redirect(route($this->table_name.'.edit',$insert_id))->with(['flash_level'=>'success','flash_message'=>'Thêm trả lời thành công!']);
    }

    public function quick_comment(Request $request) {
        $id = $request->id;
        $name = $request->name;
        $content = $request->content;
        $created_at = $updated_at = date('Y-m-d H:i:s');
        $data = [
            'name'          => $name,
            'type'          => null,
            'type_id'       => null,
            'parent_id'     => $id,
            'admin_id'      => Auth::guard('admin')->user()->id,
            'content'       => $content,
            'email'         => null,
            'phone'         => null,
            'like'          => 0,
            'status'        => 1,
            'gender'        => 1,
            'rank'          => 5,
            'created_at'    => $created_at,
            'updated_at'    => $updated_at,
        ];
        $insert_id = DB::table('comment')->insertGetId($data);
        return response()->json(['name'=>Auth::guard('admin')->user()->fullname??'Cityphone','content'=>$content,'parent_id'=>$id]);
    }
}
