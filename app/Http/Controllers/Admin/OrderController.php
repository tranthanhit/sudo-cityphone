<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\MyClass\MyForm;
use App\MyClass\ListData;
use DB;
use App\Exports\OrdersExport;
use Maatwebsite\Excel\Facades\Excel;
class OrderController extends Controller
{
    function __construct()
    {
        $this->module_name = 'Đặt hàng';
        $this->table_name = 'orders';
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $arr_stt = [0=>'Đơn hàng mới',1=>'Đã tiếp nhận',2=>'Hoàn thành',3=>'Hủy'];
        $arr_type = [
            'call_me' => "Gọi ngay",
            'services' => "Đặt lịch sửa chữa",
            'fits' => "Mua phụ kiện",
            'contact' => 'Liên hệ',
            'promotion_notice' => 'Thông báo khuyến mãi',
        ];
        $this->checkRole($this->table_name.'_access');

        $location_array = DB::table('location')->where('status',1)->orderBy('id','asc')->pluck('name','id')->toArray();
     

        $listdata = new ListData($request,$this->table_name);
        $listdata->add('status','Trạng thái','array',1,$arr_stt);
        $listdata->add('name','Họ tên','string',0);
        $listdata->add('phone','Số điện thoại','string',0);
        $listdata->add('email','Email','string',0);
        $listdata->add('address','Thông tin khác','string');
        $listdata->add('location_id','Địa điểm','array',1,$location_array);
        $listdata->add('type','Kiểu','array',1,$arr_type);
        $listdata->add('price','Giá tiền','string');
        $listdata->add('note','Ghi chú','string');
        $listdata->add('created_at','Thời gian','range',1);
        $listdata->add('','Chi tiết');
        // $listdata->add('status','Trạng thái','status',1,[1=>'Đơn hàng mới',2=>'Đã tiếp nhận',3=>'Hoàn thành']);
        // $listdata->add('','Sửa','edit');
        $listdata->add('','Xóa','delete');

        $data = $listdata->data();

        return view('admin.layouts.list',compact('data','arr_stt','arr_type'));
    }

    public function export(Request $request) {
        $status = $request->get('status',-1);
        $location = $request->get('location_id',-1);
        $type = $request->get('type',-1);
        $start = $request->get('created_at_start','');
        $end = $request->get('created_at_end','');

        $data = DB::table('orders');
        if($status != -1) {
            $data = $data->where('status',$status);
        }
        if($location != -1) {
            $data = $data->where('location_id',$location);
        }
        if($type != -1) {
            $data = $data->where('type',$type);
        }
        if($start != '' && $end !== '') {
            $data = $data->where('created_at','>',$start);
            $data = $data->where('created_at','<',$end);
        }
        $count = $data->count();
        if($count < 5000) {
            $time = date("H-i_d-m-Y");
            return (new OrdersExport($status,$location,$type,$start,$end))->download('donhang_'.$time.'.xlsx');
        }else {
            die('Số lượng xuất quá lớn, vui lòng thu nhỏ bộ lọc');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $order = DB::table('orders')->where('id',$id)->first();
        $order_detail = DB::table('order_detail')->where('order_id',$order->id)->first();
        $type = $order_detail->type;
        $product_id = $order_detail->product_id;
        switch ($type) {
            case 'phones':
                $type = "Đặt mua điện thoại";
                $product = DB::table('phones')->where('status',1)->where('id',$product_id)->first();
                $cate = DB::table('phone_categories')->where('status',1)->where('id',$product->category_id)->first();
                $link = route('web.phone.show',['category'=>$cate->slug,'slug'=>$product->slug]);
                break;
            case 'phoneiment':
                $type = "Trả góp điện thoại";
                $product = DB::table('phones')->where('status',1)->where('id',$product_id)->first();
                $link = route('web.phone.installment',$product->slug);
                break;
            case 'tablets':
                $type = "Đặt mua máy tính bảng";
                $product = DB::table('tablets')->where('status',1)->where('id',$product_id)->first();
                $link = route('web.tablet.show',$product->slug);
                break;
            case 'tabletiment':
                $type = "Trả góp máy tính bảng";
                $product = DB::table('tablets')->where('status',1)->where('id',$product_id)->first();
                $link = "";
                break;
            case 'services':
                $type = "Đặt lịch sửa chữa";
                $product = DB::table('services')->where("status",1)->where('id',$product_id)->first();
                $link = route('web.service.show',$product->slug);
                break;
            case 'servicesiment':
                $type = "Mua linh kiện";
                $product = DB::table('services')->where("status",1)->where('id',$product_id)->first();
                $link = route('web.service.show',$product->slug);
                break;
            case 'fits':
                $type = "Đặt mua phụ kiện";
                $product = DB::table('fits')->where('status',1)->where('id',$product_id)->first();
                $link = route('web.fits.show',$product->slug);
                break;
            default:
                $type = "";
                $link = "";
                break;
        }
        return view('admin.orders.order_detail',compact('order','order_detail','type','link'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if($this->hasRole($this->table_name.'_delete')) {
            DB::table($this->table_name)->where('id',$id)->update([
                'status'=>4,
                'name' => DB::raw("CONCAT(name, '--delete--".time()."')"),
            ]);
            $this->systemLogs('Xóa '.$this->module_name,'delete',$this->table_name,$id);
            return response()->json(['status'=>1,'message'=>'Xóa thành công']);
        }else {
            return response()->json(['status'=>0,'message'=>'Xóa không thành công']);
        }
    }
    
}
