<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\MyClass\MyForm;
use App\MyClass\ListData;
use DB;
class SlideController extends Controller
{
    function __construct()
    {
        $this->module_name = 'Slide';
        $this->table_name = 'slides';
        parent::__construct();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $array_slides = config('app.slides');
        $this->checkRole($this->table_name.'_access');
        $listdata = new ListData($request,$this->table_name);
        $listdata->add('name','Tên','string',1);
        $listdata->add('image','Ảnh đại diện desktop','string');
        $listdata->add('image_mobile','Ảnh đại diện mobile','string');
        $listdata->add('link','Link','string');
        $listdata->add('type','Loại');
        $listdata->add('order','Sắp xếp','int');
        $listdata->add('','Thông tin');
        $listdata->add('status','Trạng thái','status',1,[1=>'Hoạt động',2=>'Không hoạt động',3=>'Thùng rác']);
        $listdata->add('','Sửa','edit');
        $listdata->add('','Xóa','delete');
        $data = $listdata->data();
        return view('admin.layouts.list',compact('data','array_slides'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $array_slides = config('app.slides');
        $form = new MyForm();
        $data_form[] = $form->select('type','',1,'Loại',$array_slides);
        $data_form[] = $form->text('name','',1,'Tiêu đề');
        $data_form[] = $form->image('image','',0,'Ảnh đại diện desktop','Chọn làm ảnh đại diện','Lưu ý: Ảnh slide trang chủ kích thước 792px*320px, vui lòng tạo ảnh đúng kích thước và nén ảnh trước khi tải lên để tối ưu website');
        $data_form[] = $form->image('image_mobile','',0,'Ảnh đại diện mobile','Chọn làm ảnh đại diện','Lưu ý: Ảnh slide trang chủ kích thước 400px*180px, vui lòng tạo ảnh đúng kích thước và nén ảnh trước khi tải lên để tối ưu website');
        $data_form[] = $form->text('link','',0,'Link');
        $data_form[] = $form->checkbox('status',1,1,'Kích hoạt');
        $data_form[] = $form->action('add');

        return view('admin.layouts.create',compact('data_form'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->checkRole($this->table_name.'_create');
        $this->validate_form($request,'name',1,'Bạn chưa nhập tiêu đề');
        $created_at = $updated_at = date("Y-m-d H:i:s");
        $data_form = $request->all();
        extract($data_form,EXTR_OVERWRITE);
        $data_insert = compact('name','type','image','image_mobile','link','created_at','updated_at');
        $id_insert = DB::table($this->table_name)->insertGetId($data_insert);
        $this->metaSeo($id_insert,$seo_title,$seo_description,$seo_robots);
        $this->systemLogs('Thêm mới '.$this->module_name,'store',$this->table_name,$id_insert,$data_insert);
        return redirect(route($this->table_name.'.'.$redirect,$id_insert))->with(['flash_level'=>'success','flash_message'=>'Thêm mới thành công!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $array_slides = config('app.slides');
        $this->checkRole($this->table_name.'_edit');
        $slide = DB::table('slides')->find($id);
        $form = new MyForm();
        $data_form[] = $form->select('type',$slide->type,1,'Loại',$array_slides);
        $data_form[] = $form->text('name',$slide->name,1,'Tiêu đề');
        $data_form[] = $form->image('image',$slide->image,0,'Ảnh đại diện','Chọn làm ảnh đại diện','Lưu ý: Ảnh slide trang chủ kích thước 792px*320px, vui lòng tạo ảnh đúng kích thước và nén ảnh trước khi tải lên để tối ưu website');
        $data_form[] = $form->image('image_mobile',$slide->image_mobile,0,'Ảnh đại diện mobile','Chọn làm ảnh đại diện','Lưu ý: Ảnh slide trang chủ kích thước 400px*180px, vui lòng tạo ảnh đúng kích thước và nén ảnh trước khi tải lên để tối ưu website');
        $data_form[] = $form->text('link',$slide->link,0,'Link');
        $data_form[] = $form->action('edit');
        return view('admin.layouts.edit',compact('data_form','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->checkRole($this->table_name.'_edit');
        $data_edit = DB::table($this->table_name)->where('id',$id)->first();

        $this->validate_form($request,'name',1,'Bạn chưa nhập tiêu đề');
        $updated_at = date("Y-m-d H:i:s");
        $data_form = $request->all();
        $status = 1;
        extract($data_form,EXTR_OVERWRITE);
        $data_update = compact('name','type','image','image_mobile','link','status','updated_at');
        DB::table($this->table_name)->where('id',$id)->update($data_update);
        $this->metaSeo($id,$seo_title,$seo_description,$seo_robots);
        $old = [
            'name'=>$data_edit->name,
            'image'=>$data_edit->image,
            'image_mobile'=>$data_edit->image_mobile,
            'type' => $data_edit->type,
            'link'=>$data_edit->link,
            'status'=>$data_edit->status,
            'updated_at'=>$data_edit->updated_at
        ];
        $this->systemLogs('Sửa '.$this->module_name,'update',$this->table_name,$id,['old'=>$old,'new'=>$data_update]);
        return redirect(route($this->table_name.'.'.$redirect,$id))->with(['flash_level'=>'success','flash_message'=>'Cập nhật dữ liệu thành công!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if($this->hasRole($this->table_name.'_delete')) {
            DB::table($this->table_name)->where('id',$id)->update([
                'status'=>4,
                'name' => DB::raw("CONCAT(name, '--delete--".time()."')"),
            ]);
            $this->systemLogs('Xóa '.$this->module_name,'delete',$this->table_name,$id);
            return response()->json(['status'=>1,'message'=>'Xóa thành công']);
        }else {
            return response()->json(['status'=>0,'message'=>'Xóa không thành công']);
        }
    }
}
