<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use Illuminate\Support\Facades\Cache;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Request;
use DB;
use App\Phone;
use App\Tablet;
use App\Comment;
use App\Exports\OrdersExport;
use Maatwebsite\Excel\Facades\Excel;

class AjaxController extends Controller
{
    public function updateStatus(){
    	if(Request::ajax()){
			$id=Request::get('id');
			$table=Request::get('table');
			$status=Request::get('status');
		}
		DB::table($table)->where('id',$id)->update(['status'=>$status]);
    }
    public function deleteOne(){
    	if(Request::ajax()){
			$id=Request::get('id');
			$table=Request::get('table');
		}
        if($table=='news'){
            DB::table($table)->where('id',$id)->update(['status'=>4]);
            DB::table('tags_map')->where('id_news',$id)->delete();
        }else{
		  DB::table($table)->where('id',$id)->update(['status'=>4]);
        }
    }
    public function getSlug(){
    	if(Request::ajax()){
    		$title=Request::get('title');
    	}
    	$slug=slugTitle($title);
    	echo $slug;
    }
    public function quickEditCate(){
        if(Request::ajax()){
            $id=Request::get('id');
            $cate_name=Request::get('cate_name');
            $cate_slug=Request::get('cate_slug');
            $cate_order=Request::get('cate_order');
        }
        DB::table('categories')->where('id',$id)->update(['name' => $cate_name, 'slug'=>$cate_slug,'order'=>$cate_order]);
    }
    public function quickEditTags(){
        if(Request::ajax()){
            $id=Request::get('id');
            $tag_name=Request::get('tag_name');
            $tag_slug=Request::get('tag_slug');
        }
        DB::table('tags')->where('id',$id)->update(['name' => $tag_name, 'slug'=>$tag_slug]);
    }
    public function quickEditProduct(){
        if(Request::ajax()){
            $id=Request::get('id');
            $pro_name=Request::get('pro_name');
            $pro_slug=Request::get('pro_slug');
            $pro_price=Request::get('pro_price');
            $pro_code=Request::get('pro_code');
        }
        $pro_price=str_replace(',','',$pro_price);
        if(empty($pro_price)){
            $pro_price=0;
        }
        DB::table('products')->where('id',$id)->update(['name' => $pro_name, 'slug'=>$pro_slug,'price'=>$pro_price,'code'=>$pro_code]);
    }
    public function quickEditSlier(){
        if(Request::ajax()){
            $id=Request::get('id');
            $sli_name=Request::get('sli_name');
            $sli_link=Request::get('sli_link');
            $sli_order=Request::get('sli_order');
        }
        DB::table('sliders')->where('id',$id)->update(['name' => $sli_name, 'order'=>$sli_order,'link'=>$sli_link]);
    }
    public function duplicate(){
        if(Request::ajax()){
            $id=Request::get('id');
            $type=Request::get('type');
        }
        if($type == "phones"){
            $product = Phone::where('status',1)->where('id',$id)->first();
            $newProduct = $product->replicate();
            $newProduct->save();
            $newProduct->name = $product->name."[copy]";
            $newProduct->slug = $product->slug."-copy";
            $newProduct->save();
        }
        if($type == "tablets"){
            $product = Tablet::where('status',1)->where('id',$id)->first();
            $newProduct = $product->replicate();
            $newProduct->save();
            $newProduct->name = $product->name."[copy]";
            $newProduct->slug = $product->slug."-copy";
            $newProduct->save();
        }
        return response(json_encode(compact('type')));
    }
    public function check_new(){
        if(Request::ajax()){
            $id=Request::get('id');
            $type=Request::get('type');
            $val=Request::get('val');
        }
        if($type == "phones"){
            $product = Phone::where('status',1)->where('id',$id)->first();
            $product->new = $val;
            $product->save();
        }
        if($type == "tablets"){
            $product = Tablet::where('status',1)->where('id',$id)->first();
            $product->new = $val;
            $product->save();
        }
        return response(json_encode(compact('val')));
    }
    public function check_hot(){
        if(Request::ajax()){
            $id=Request::get('id');
            $type=Request::get('type');
            $val=Request::get('val');
        }
        if($type == "phones"){
            $product = Phone::where('status',1)->where('id',$id)->first();
            $product->hot = $val;
            $product->save();
        }
        if($type == "tablets"){
            $product = Tablet::where('status',1)->where('id',$id)->first();
            $product->hot = $val;
            $product->save();
        }
        return response(json_encode(compact('val')));
    }
    public function comment_success(){
        if(Request::ajax()){
            $id=Request::get('id');
            $val=Request::get('val');
        }
        $comment = Comment::where('status','<>',4)->where('id',$id)->first();
        $comment->status = $val;
        $comment->save();
        return response(json_encode(compact('val')));
    }
    public function export_order(){
        // $id = Request::get('id');
        // (new InvoicesExport)->store('invoices.xlsx');
        // return Excel::download(new OrdersExport, 'orders.xlsx');
        // return Excel::store(new OrdersExport(2019), 'duonganh.xlsx', 'local');
        // response()->download('uploads\duong.xlsx');

        // (new OrdersExport)->store('danh-sach-dat-hang.xlsx');
        // return (new OrdersExport)->download('invoices.xlsx');
        // return response(json_encode(compact('id')));
        // dump("zz");
        
        // return (new OrdersExport)->download('invoices.xlsx');
    }

    public function has_call(Request $request) {
        if(Request::ajax()){
            $id=Request::get('id');
            $type=Request::get('type');
            $val=Request::get('val');
        }
        if ($type == 'comment') {
            $comment = Comment::where('id',$id)->first();
            $comment->has_call = $val;
            $comment->save();
        }
        return 'Đã thay đổi';
    }
    public function quick_edit(Request $request) {
        if(Request::ajax()){
            $id=Request::get('id');
            $name=Request::get('name');
            $val=Request::get('val');
            $table=Request::get('table');
        }
        DB::table($table)->where('id',$id)->update([
            $name => $val,
        ]);
        return 'Cập nhật thành công!';
    }

    public function DeleteCache(Request $result)
    {
        Cache::flush();
        return 1;
    }
    
}
