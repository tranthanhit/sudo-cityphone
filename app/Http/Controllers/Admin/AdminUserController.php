<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\MyClass\MyForm;
use App\MyClass\ListData;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
class AdminUserController extends Controller
{
    function __construct()
    {
        $this->module_name = 'tài khoản quản trị';
        $this->table_name = 'admin_users';
        parent::__construct();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->checkRole($this->table_name.'_access');

        $listdata = new ListData($request,$this->table_name);
        $listdata->add('name','Tên đăng nhập','string',1);
        $listdata->add('email','Email','string',1);
        $listdata->add('','Phân quyền');
        $listdata->add('status','Trạng thái','status',1,[1=>'Hoạt động',2=>'Không hoạt động',3=>'Bị cấm']);
        $listdata->add('','Sửa','edit');
        $listdata->add('','Xóa','delete');

        $data = $listdata->data();
        foreach ($data['show_data'] as $key => $value) {
            $role_user = db::table('authorizations')->where('admin_id',$value->id)->first();
            if($role_user) {
                $role[$value->id] = json_decode($role_user->capabilities);
            }else {
                $role[$value->id] = [];
            }
        }
        return view('admin.layouts.list',compact('data','role'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->checkRole($this->table_name.'_create');

        $form = new MyForm();
        $data_form[] = $form->text('name','',1,'Tên đăng nhập');
        $data_form[] = $form->text('fullname','',0,'Tên đầy đủ');
        $data_form[] = $form->text('email','',1,'Email');
        $data_form[] = $form->text('password','',1,'Mật khẩu');
        $data_form[] = $form->text('comfirm_password','',1,'Nhập lại mật khẩu');
        $data_form[] = $form->checkbox('status',1,1,'Kích hoạt');
        $data_form[] = $form->custom('admin.admin_users.role');
        $data_form[] = $form->action('add');
        return view('admin.layouts.create',compact('data_form'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->checkRole($this->table_name.'_create');
        $this->validate_form($request,'name',1,'Bạn chưa nhập tên',1,'Tên đã được sử dụng');
        $this->validate_form($request,'email',1,'Bạn chưa nhập email',1,'Email đã được sử dụng');
        $this->validate_form($request,'password',1,'Bạn chưa nhập mật khẩu');
        $this->validate_form($request,'comfirm_password',1,'Bạn chưa nhập lại mật khẩu');
        $created_at = $updated_at = date("Y-m-d H:i:s");
        $data_form = $request->all();
        $status = 2;
        extract($data_form,EXTR_OVERWRITE);
        if($password != $comfirm_password){
            return redirect()->back()->with(['flash_level'=>'danger','flash_message'=>'Mật khẩu không trùng khớp!']);
        }else{
            $password = bcrypt($password);
            $data_insert = compact('name','fullname','email','password','status','created_at','updated_at');
            $id_insert = DB::table($this->table_name)->insertGetId($data_insert);

            if(isset($role)){
                $role=json_encode(array_keys($role));
            }else{
                $role='';
            }
            $db_insert_role = ['admin_id'=>$id_insert,'capabilities'=>$role];
            $id_insert_role = DB::table('authorizations')->insert($db_insert_role);
            $this->systemLogs('Thêm mới '.$this->module_name,'store',$this->table_name,$id_insert,$data_insert);
            return redirect(route($this->table_name.'.'.$redirect,$id_insert))->with(['flash_level'=>'success','flash_message'=>'Thêm thành công!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->checkRole($this->table_name.'_edit');

        $data_edit = DB::table($this->table_name)->where('id',$id)->first();

        $form = new MyForm();
        $data_form[] = $form->text('name',$data_edit->name,1,'Tên đăng nhập','',0,'','disabled');
        $data_form[] = $form->text('email',$data_edit->email,1,'Email','',0,'','disabled');
        $data_form[] = $form->text('fullname',$data_edit->fullname,0,'Tên đầy đủ');
        $data_form[] = $form->checkbox('status',$data_edit->status,1,'Kích hoạt');
        $data_form[] = $form->custom('admin.admin_users.role');
        $data_form[] = $form->action('edit');

        $data_role = DB::table('authorizations')->select('capabilities')->where('admin_id',$id)->first();
        if(!empty($data_role)){
            $role = json_decode($data_role->capabilities);
        }else{
            $role[] = '';
        }

        return view('admin.layouts.edit',compact('data_form','id','role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->checkRole($this->table_name.'_edit');
        $updated_at = date("Y-m-d H:i:s");

        $data_form = $request->all();
        $status = 2;
        extract($data_form,EXTR_OVERWRITE);

        $data_update = compact('fullname','status','updated_at');

        DB::table($this->table_name)->where('id',$id)->update($data_update);
        $role = json_encode(array_keys($role));
        DB::table('authorizations')->where('admin_id',$id)->update(['capabilities'=>$role]);
        $this->systemLogs('Sửa '.$this->module_name,'update',$this->table_name,$id,$data_update);
        return redirect(route($this->table_name.'.'.$redirect,$id))->with(['flash_level'=>'success','flash_message'=>'Cập nhật dữ liệu thành công!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($this->hasRole($this->table_name.'_delete')) {
            DB::table($this->table_name)->where('id',$id)->update(['status'=>4]);
            $this->systemLogs('Xóa '.$this->module_name,'delete',$this->table_name,$id);
            return response()->json(['status'=>1,'message'=>'Xóa thành công']);
        }else {
            return response()->json(['status'=>0,'message'=>'Xóa không thành công']);
        }
    }
    public function changePassword()
    {
        $form = new MyForm();
        $data_form=[];
        $data_form[]=$form->password('password','',1,'Mật khẩu cũ');
        $data_form[]=$form->password('password_new','',1,'Mật khẩu mới');
        $data_form[]=$form->password('repassword_new','',1,'Nhập lại mật khẩu mới');
        $data_form[]=$form->action('edit');
        dump($data_form);
        return view('admin.auth.change_password',compact('data_form'));
    }
    public function postChangePassword(Request $request){
        $password=$request->password;
        $password_new=$request->password_new;
        $repassword_new=$request->repassword_new;
        if (!Hash::check($password, Auth::guard('admin')->user()->password)) {
            return redirect()->route('admin.admin_user.changePassword')->with(['flash_level'=>'danger','flash_message'=>'Nhập mật khẩu cũ không đúng !']);
        }
        elseif($password_new!=$repassword_new){
            return redirect()->route('admin.admin_user.changePassword')->with(['flash_level'=>'danger','flash_message'=>'Nhập mật khẩu mới không trùng khớp!']);
        }else{
            DB::table('admin_users')->where('id', Auth::guard('admin')->user()->id)->update(['password' => bcrypt($password_new)]);
            return redirect()->route('admin.admin_user.changePassword')->with(['flash_level'=>'success','flash_message'=>'Cập nhật mật khẩu thành công !']);
        }
    }
}
