<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use DB;
use App\MyClass\MyForm;
use App\MyClass\ListData;
use App\MyClass\Categories;
use Cache;
/**
 * Class SettingController
 * @package App\Http\Controllers\Admin
 *
 * Phần cấu hình của core chúng ta chia thành 2 phần chính để tiện cho tối ưu query ngoài frontend:
 * - Cấu hình trang chủ: Seo (title, description, ...) của trang chủ, các nội dung chỉ hiện thị ở trang chủ như banner, các nội dung cố định ...
 * - Cấu hình chung: cho các nội dung chung trên toàn bộ các trang như: html head, hotline, link ở header, các thông tin ở footer ...
 *
 */

class SettingController extends Controller
{
    private function postData($setting_name, $data){
        Cache::forget('options_'.$setting_name);

        unset($data['_token']);
        unset($data['submit']);
     
        if (isset($data['key'])) {
            if ($data['key'] == 'short_code') {
                if (isset($data['short_code_key'])) {
                    
                    $short_code_key = $data['short_code_key'];
                    $short_code_content = $data['short_code_content'];

                    unset($data['key']);
                    unset($data['short_code_key']);
                    unset($data['short_code_content']);

                    $data = [
                        'short_code_key' => $short_code_key,
                        'short_code_content' => $short_code_content,
                    ];
                }
                // var_dump(json_encode($data));
            }
        }
        
        $data = base64_encode(json_encode($data));
        if(DB::table('options')->where('name',$setting_name)->exists()){
            DB::table('options')->where('name',$setting_name)->update(['value'=>$data]);
        }else{
            DB::table('options')->insert(['name'=>$setting_name,'value'=>$data]);
        }
    }
    public function home(Request $request) {
        $setting_name = 'home';
        $title = "Cấu hình trang chủ";

        $this->checkRole('settings_'.$setting_name);
        $data = $request->all();

        if(isset($data['submit'])){
            $this->postData($setting_name, $data);
        }

        $option = DB::table('options')->select('value')->where('name',$setting_name)->first();
        if(empty($option)){
            $data = [];
        }else{
            $data = json_decode(base64_decode($option->value),true);
        }

        
        $form = new MyForm();
        $data_form[] = $form->text('meta_title',isset($data['meta_title']) ? $data['meta_title'] : '',0,'Meta title (Tiêu đề)');
        $data_form[] = $form->textarea('meta_description',isset($data['meta_description']) ? $data['meta_description'] : '',0,'Meta description (Mô tả)','');
        $data_form[] = $form->text('hotline',isset($data['hotline']) ? $data['hotline'] : '',0,'Hotline');
        $data_form[] = $form->custom('admin.layouts.setting.banner');


        $data_form[] = $form->action('editconfig');
        return view('admin.layouts.setting', compact('data_form','title'));

    }
    public function general(Request $request) {
        $setting_name = 'general';
        $title = "Cấu hình chung";

        $this->checkRole('settings_'.$setting_name);
        $data = $request->all();

        // post data
        if(isset($data['submit'])){
            $this->postData($setting_name, $data);
        }
        //
        $option = DB::table('options')->select('value')->where('name',$setting_name)->first();
        if(empty($option)){
            $data = [];
        }else{
            $data = json_decode(base64_decode($option->value),true);
        }
        $form = new MyForm();

        $data_form[] = $form->title('Thông tin hiển thị header và footer');
        $data_form[] = $form->image('image_favicon',isset($data['image_favicon']) ? $data['image_favicon'] : '',0,'ảnh favicon','');
       
        $data_form[] = $form->image('logo_header',isset($data['logo_header']) ? $data['logo_header'] : '',0,'Logo hiển thị header','');

    

        
        $data_form[] = $form->title('Cấu hình chân trang web');
        $data_form[] = $form->textarea('text_footer',isset($data['text_footer']) ? $data['text_footer'] : '',0,'Hiển thị chân trang web','');
        
         
        $data_form[] = $form->title('Cấu hình Url');
        $data_form[] = $form->text('link_vong_quay_m_m',isset($data['link_vong_quay_m_m']) ? $data['link_vong_quay_m_m'] : '',0,'Url vòng quay may mắn hiển thị trang chủ','');

        $data_form[] = $form->text('link_facebook_desktop',isset($data['link_facebook_desktop']) ? $data['link_facebook_desktop'] : '',0,'Url Facebook fanpage  hiển thị phần chân trang','');
        $data_form[] = $form->text('link_youtube_desktop',isset($data['link_youtube_desktop']) ? $data['link_youtube_desktop'] : '',0,'Url Youtube hiển thị phần Chân trang','');
        
        $data_form[] = $form->text('link_thong_bao_bct',isset($data['link_thong_bao_bct']) ? $data['link_thong_bao_bct'] : '',0,'Url Thông báo bộ công thương hiển thị phần chân trang','');
        
        $data_form[] = $form->text('link_facebook',isset($data['link_facebook']) ? $data['link_facebook'] : '',0,'Url chat facebook hiển thị bản mobile chân trang ','');
      
        $data_form[] = $form->text('messaging_via_facebook',isset($data['messaging_via_facebook']) ? $data['messaging_via_facebook'] : '',0,'Url nhắn tin qua facebook ở trang chi tiết phụ kiện , dịch vụ','');

        $data_form[] = $form->text('terms_of_use',isset($data['terms_of_use']) ? $data['terms_of_use'] : '',0,'Url điều khoản sử dụng ở trân trang','');
        $data_form[] = $form->text('privacy_policy',isset($data['privacy_policy']) ? $data['privacy_policy'] : '',0,'Url chính sách bảo mật ở trân trang','');
        


        $data_form[] = $form->text('content_regulations',isset($data['content_regulations']) ? $data['content_regulations'] : '',0,'Url quy định nội dung của cityphone','');

         
        $data_form[] = $form->title('Cấu hình Số điện thoại');
        $data_form[] = $form->text('phone_mobile_1',isset($data['phone_mobile_1']) ? $data['phone_mobile_1'] : '',0,'Số điện thoại tư vấn hiển thị chân trang','');
        $data_form[] = $form->text('phone_mobile_2',isset($data['phone_mobile_2']) ? $data['phone_mobile_2'] : '',0,'Số điện thoại góp ý, phản ánh hiển thị chân trang','');
        
        $data_form[] = $form->text('phone_destop_tu_van',isset($data['phone_destop_tu_van']) ? $data['phone_destop_tu_van'] : '',0,'Số điện thoại tư vấn chi tiết dịch vụ, phụ kiện','');
      
        $data_form[] = $form->text('hotline_head',isset($data["hotline_head"]) ? $data["hotline_head"] : '',0,'Hotline đầu trang','VD: 0969.120.120 - 097.120.6688 - 0965.39.79.661');
        $data_form[] = $form->text('email_order', isset($data["email_order"]) ? $data["email_order"] : '',0,'Email nhận liên hệ, đặt hàng');
      
        $data_form[] = $form->title('Cấu hình code chat');
        $code_chat_all = isset($data["code_chat_all"])?$data["code_chat_all"]:[];

         $location = DB::table('location')->where('status',1)->get();
        $data_form[] = $form->custom('admin.layouts.setting.code_chat_all');

        $data_form[] = $form->custom('admin.settings.home');

        // $data_form[] = $form->customMenu('menu_footer', isset($data['menu_footer']) ? $data['menu_footer'] : '', 'Cấu hình menu footer');
        $data_form[] = $form->customMenu('menu_primary', isset($data['menu_primary']) ? $data['menu_primary'] : '','Cấu hình menu footer');

     

        $data_form[] = $form->action('editconfig');
        return view('admin.layouts.setting', compact('data_form','title','location','data'));
    }
    public function promotion(Request $request){
        $setting_name = 'promotion';
        $title = "Cấu hình Khuyến mãi";
        $this->checkRole('settings_'.$setting_name);
        $data = $request->all();
        if(isset($data['submit'])){
            $this->postData($setting_name, $data);
        }
        $option = DB::table('options')->select('value')->where('name',$setting_name)->first();
        if(empty($option)){
            $data = [];
        }else{
            $data = json_decode(base64_decode($option->value),true);
        }
        $form = new MyForm();
        $data_form[] = $form->textarea('promotion_default_service',isset($data["promotion_default_service"]) ? $data["promotion_default_service"]: '' ,0,"Ưu đãi khuyến mãi mặc định cho sửa chữa");
        $data_form[] = $form->textarea('promotion_default_fit',isset($data["promotion_default_fit"]) ? $data["promotion_default_fit"]: '' ,0,"Ưu đãi khuyến mãi mặc định cho Phụ kiện");
        $data_form[] = $form->action('editconfig');
        return view('admin.layouts.setting', compact('data_form','title'));

    }
   
    public function meta_code(Request $request){
        $setting_name = 'meta_code';
        $title = "Mã chèn vào website";
        $this->checkRole('settings_'.$setting_name);
        $data = $request->all();
        if(isset($data['submit'])){
            $this->postData($setting_name, $data);
        }
        $option = DB::table('options')->select('value')->where('name',$setting_name)->first();
        if(empty($option)){
            $data = [];
        }else{
            $data = json_decode(base64_decode($option->value),true);
        }
        $form = new MyForm();
        $data_form[] = $form->textarea('insert_head',isset($data["insert_head"]) ? $data["insert_head"]: '' ,0,"Mã chèn vào trong thẻ Head");
        $data_form[] = $form->textarea('insert_open_body',isset($data["insert_open_body"]) ? $data["insert_open_body"]: '' ,0,"Mã chèn vào ngay sau thẻ mở body");
        $data_form[] = $form->textarea('insert_close_body',isset($data["insert_close_body"]) ? $data["insert_close_body"]: '' ,0,"Mã chèn vào ngay trước thẻ đóng body");
        
        $data_form[] = $form->action('editconfig');
        return view('admin.layouts.setting', compact('data_form','title'));
    }
    public function seo_category(Request $request){
        $setting_name = 'seo_category';
        $title = "Cấu hình Nội dung Seo cho danh mục";
        $this->checkRole('settings_'.$setting_name);
        $data = $request->all();
        if(isset($data['submit'])){
            $this->postData($setting_name, $data);
        }
        $option = DB::table('options')->select('value')->where('name',$setting_name)->first();
        if(empty($option)){
            $data = [];
        }else{
            $data = json_decode(base64_decode($option->value),true);
        }
       
        $detail_fit = isset($data["detail_fit"])?$data["detail_fit"]:"";
        $title_fit = isset($data["title_fit"])?$data["title_fit"]:"";
        $desc_fit = isset($data["desc_fit"])?$data["desc_fit"]:"";
        $detail_service = isset($data["detail_service"])?$data["detail_service"]:"";
        $title_service = isset($data["title_service"])?$data["title_service"]:"";
        $desc_service = isset($data["desc_service"])?$data["desc_service"]:"";
        $detail_news = isset($data["detail_news"])?$data["detail_news"]:"";
        $title_news = isset($data["title_news"])?$data["title_news"]:"";
        $desc_news = isset($data["desc_news"])?$data["desc_news"]:"";
        $list_seo = compact('detail_fit','title_fit','desc_fit','detail_service','title_service','desc_service','detail_news','title_news','desc_news');
        $form = new MyForm();
        $data_form[] = $form->custom('admin.layouts.setting.seo');
        
        $data_form[] = $form->action('editconfig');
        return view('admin.layouts.setting', compact('data_form','title','list_seo'));
 
    }
    /**
     * cấu hình menu header
     */
    public function menuHeader(Request $request) {
        $setting_name = 'menu_header';
        $title = "Cấu hình Menu header";
        $this->checkRole('settings_'.$setting_name);
        $data = $request->all();
       
        if(isset($data['submit'])){
            $this->postData($setting_name, $data);
        }
        $option = DB::table('options')->select('value')->where('name',$setting_name)->first();
        if(empty($option)){
            $data = [];
        }else{
            $data = json_decode(base64_decode($option->value),true);
        }
        $form = new MyForm();
        $data_form[] = $form->custom('admin.layouts.setting.menu_header',$data);
        $data_form[] = $form->action('editconfig');
        return view('admin.layouts.setting', compact('data_form','title'));
    }
    /**
     * cấu hình google shopping
     */
    
    public function google_shopping(Request $request) {
        $setting_name = 'google_shopping';
        $title = "Cấu hình google_shopping";

        if(checkRole('settings_google_shopping')){
            $data = $request->all();

            // post data
            if(isset($data['submit'])){
                $this->postData($setting_name, $data);
            }
            //
            $option = DB::table('options')->select('value')->where('name',$setting_name)->first();
            if(empty($option)){
                $data = [];
            }else{
                $data = json_decode(base64_decode($option->value),true);
            }
            $form = new MyForm();

            $data_form[] = $form->custom('admin.layouts.google_shopping_info');
            
            $data_form[] = $form->text('brand',isset($data['brand']) ? $data['brand'] : '',0,'Thương hiệu mặc định','');
            $data_form[] = $form->text('category',isset($data['category']) ? $data['category'] : '',0,'Danh mục Google shopping mặc định','');
            $data_form[] = $form->select('instock',isset($data['instock']) ? $data['instock'] : '',0,'Tình trạng kho hàng mặc định',[''=>'Mặc định','còn hàng'=>'còn hàng','hết hàng'=>'hết hàng']);
            $data_form[] = $form->select('itemcondition',isset($data['itemcondition']) ? $data['itemcondition'] : '',0,'Tình trạng sản phẩm mặc định',[''=>'Mặc định','mới'=>'mới','đã qua sử dụng'=>'đã qua sử dụng']);

            $data_form[] = $form->action('editconfig');
            return view('admin.layouts.setting', compact('data_form','title'));
        }else{

        }
    }
}
