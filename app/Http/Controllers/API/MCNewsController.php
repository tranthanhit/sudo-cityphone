<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Web\Controller;
use DB;
use Cache;
use App\News;
use App\NewsCategory;

class MCNewsController extends Controller
{
    function replace_detail($content) {
        //đổi các link ảnh sv cũ sang object storage
        $old_domain = ['','',''];
        $new_domain = config('filesystems.disks.do.domain').'/'.config('filesystems.disks.do.folder').'/images';
        $content = str_replace($old_domain,$new_domain,$content);

        // short_code
        $config_short_code = DB::table('options')->select('value')->where('name','short_code')->first();
        if($config_short_code){
            $short_code = json_decode(base64_decode($config_short_code->value),true);
        }else{
            $short_code = null;
        }
        $content = str_replace($short_code['short_code_key']??[], $short_code['short_code_content']??[], $content);

        $content = http_to_https($content);

        return $content;
    }

    public function news_hots() {
    	$news_hot = News::where('status',1)->orderBy('type_id','desc')
            ->leftJoin('pins', 'news.id', '=', 'pins.type_id')->where('type','news')->where('place','hot')
            ->limit(5)->get();
        $news_hot_category = collect(NewsCategory::whereIn('id',$news_hot->pluck('category_id')->toArray())->get());
        $data = [];
        foreach ($news_hot as $value) {
        	$data[] = [
        		'name' => $value->name,
        		'image' => $value->getImage('large'),
        		'slug' => $value->slug,
        		'time' => $value->created_at->format('d/m/Y'),
        		'author' => $value->getAdminName(),
        		'category' => [
        			'id' => $news_hot_category->where('id',$value->category_id)->first()->id,
        			'name' => $news_hot_category->where('id',$value->category_id)->first()->name,
        			'slug' => $news_hot_category->where('id',$value->category_id)->first()->slug,
        		],
        	];
        }

        if (count($news_hot) < 5) {
        	$news = News::whereNotIn('id',$news_hot->pluck('id')->toArray())->where('status',1)->orderBy('created_at','desc')->take(5-count($news_hot))->get();
        	$news_category = collect(NewsCategory::whereIn('id',$news->pluck('category_id')->toArray())->get());
        	foreach ($news as $value) {
	        	$data[] = [
	        		'name' => $value->name,
	        		'image' => $value->getImage(),
	        		'slug' => $value->slug,
	        		'time' => $value->created_at->format('d/m/Y'),
	        		'author' => $value->getAdminName(),
	        		'category' => [
	        			'id' => $news_category->where('id',$value->category_id)->first()->id,
	        			'name' => $news_category->where('id',$value->category_id)->first()->name,
	        			'slug' => $news_category->where('id',$value->category_id)->first()->slug,
	        		]
	        	];
	        }
        }

       return response()->json($data);
    }

    public function getNews($slug) {
        // thông tin bài viết
        $news = News::where('slug',$slug)->first();
        $news_category = NewsCategory::where('id',$news->category_id)->first();
        $data = [
            'name' => $news->name,
            'image' => $news->getImage(),
            'slug' => $news->slug,
            'time' => $news->created_at->format('d/m/Y'),
            'author' => $news->getAdminName(),
            'detail' => $this->replace_detail($news->detail),
            'category' => [
                'id' => $news_category->id,
                'name' => $news_category->name,
                'slug' => $news_category->slug,
            ]
        ];
        // Tin liên quan
        $related_news = News::whereNotIn('id',[$news->id])->where('category_id',$news_category->id)->where('status',1)->orderBy('created_at','DESC')->take(6)->get();
        $related = [];
        foreach ($related_news as $value) {
            $related[] = [
                'name' => $value->name,
                'image' => $value->getImage(),
                'slug' => $value->slug,
                'time' => $news->created_at->format('d/m/Y'),
            ];
        }
        $data['related'] = $related;

        $meta_seo = $this->meta_seo('news',$news->id,[
            'title' => $news->name.' - Cityphone',
            'description'=>($news->detail != null)?cutString(removeHTML($news->detail),150):$news->name ,
            'url' => route('web.news.show',$news->slug),
            'image'=> $news->getImage(),
        ]);

        $data['meta_seo'] = $meta_seo;

        return response()->json($data);
    }

    function categories() {
        $categories = NewsCategory::where('status',1)->orderBy('order','ASC')->get();
        $data = [];
        foreach ($categories as $key => $value) {
            $cate_id = $value->id;
            $total_news = Cache::remember('total_news_in_cate_' . $value->id, 60, function() use($cate_id) {
                return News::where('category_id',$cate_id)->where('status',1)->count();
            });
            $data[] = [
                'name' => $value->name,
                'slug' => $value->slug,
                'total_news' => $total_news,
            ];
        }
        return response()->json($data);
    }

    public function news_categories($slug) {
        if ($slug == 'tin-tuc') {
            $categories = collect(NewsCategory::all());
            $news = News::where('status',1)->orderBy('id', 'desc')->paginate(15);
            $data = [
                'total' => $news->total(),
                'lastPage' => $news->lastPage(),
                'currentPage' => $news->currentPage(),
                'perPage' => $news->perPage(),
                'item' => []
            ];
            foreach ($news as $key => $value) {
                $data['item'][] = [
                    'name' => $value->name,
                    'slug' => $value->slug,
                    'image' => $value->getImage(),
                    'description' => ($value->detail != null)?cutString(removeHTML($value->detail),300):$value->name,
                    'time' => $value->created_at->format('d/m/Y'),
                    'category' => [
                        'name' => $categories->where('id', $value->category_id)->first()->name,
                        'slug' => $categories->where('id', $value->category_id)->first()->slug,
                    ],
                ];
            }
            $category = [
                'name' => "Tin mới nhất",
            ];

            $config_seo = DB::table('options')->select('value')->where('name','seo_category')->first();
            if($config_seo){
                $config_seo = json_decode(base64_decode($config_seo->value),true);
            }
            $meta_seo = $this->meta_seo('',0,[
                'title' => ($config_seo["title_news"] != null)?$config_seo["title_news"]:"Tin tức - Cityphone",
                'description'=> ($config_seo["desc_news"] != null)?$config_seo["desc_news"]:"Tin tức - Cityphone",
                'url' => route('web.news_categories.showall')
            ]);
        } else {
            $category = NewsCategory::where('slug',$slug)->firstOrFail();
            $meta_seo = $this->meta_seo('news_categories',$category->id,[
                'title' => $category->name.' - Cityphone',
                'description'=>($category->detail != null)?removeHTML(cutString($category->detail,150)):$category->name ,
            ]);

            $news = News::where('category_id',$category->id)->where('status',1)
                    ->select('*', DB::raw("DATE_FORMAT(news.created_at, '%d-%m-%Y') as created_time, DATE_FORMAT(news.updated_at, '%d-%m-%Y') as updated_time"))
                    ->orderBy('id', 'desc')
                    ->paginate(15);
            $data = [
                'total' => $news->total(),
                'lastPage' => $news->lastPage(),
                'currentPage' => $news->currentPage(),
                'perPage' => $news->perPage(),
                'item' => []
            ];
            foreach ($news as $key => $value) {
                $data['item'][] = [
                    'name' => $value->name,
                    'slug' => $value->slug,
                    'image' => $value->getImage(),
                    'description' => ($value->detail != null)?cutString(removeHTML($value->detail),300):$value->name,
                    'time' => $value->created_at->format('d/m/Y'),
                    'category' => [
                        'name' => $category->name,
                        'slug' => $category->slug,
                    ],
                ];
            }
        }
        return response()->json(['meta_seo' => $meta_seo, 'news' => $data, 'category' => $category]);
    }
}
