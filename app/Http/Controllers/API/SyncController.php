<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use App\Phone;
use App\PhoneCategory;
use App\Tablet;
use App\Service;
use App\Fit;
use App\News;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class SyncController extends Controller
{
    public function phoneToSoftware() {
    	$phones = Phone::select('phones.id','phones.system_id','phones.name','phones.price','phones.warranty','color','storage','ram','aspect','country')
        ->leftJoin('product_attribute','phones.id','=','type_id')
        ->where('phones.status',1)
        ->get();
        $data = [];
        foreach ($phones as $value) {
            $temp = [];
            $temp['id'] = $value->id;
            $temp['system_id'] = $value->system_id;
            $temp['name'] = $value->name;
            $temp['price'] = $value->price;
            $temp['warranty'] = $value->warranty;
            $default = json_decode(base64_decode(json_encode([])));
            $color = json_decode(base64_decode($value->color??$default),true);
            $ram = json_decode(base64_decode($value->ram??$default),true);
            $storage = json_decode(base64_decode($value->storage??$default),true);
            $aspect = json_decode(base64_decode($value->aspect??$default),true);
            $country = json_decode(base64_decode($value->country??$default),true);
            $temp['color'] = $color;
            $temp['ram'] = $ram;
            $temp['storage'] = $storage;
            $temp['aspect'] = $aspect;
            $temp['country'] = $country;
            $data[] = $temp;
        }

        $token = 'KMXyehDlqUeW6dQtz1AYiHNSIXVwQVMPzxCR4jRw0le30KRkxoaoVCFsBJ6C';
        $client = new Client();
        try {
            $response = $client->request(
                'POST',
                config('app.domain_system').'/api/sync-phone-from-web',
                [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $token
                    ],
                    'form_params' => [
                        'phones' => json_encode($data)
                    ]
                ]
            );
            if ($response->getStatusCode() == 200) {
                $response_ids = json_decode($response->getBody(), true);
                foreach ($response_ids as $value) {
                	Phone::where('id',$value)->update(['system_id'=>$value['system_id']]);
                }
                echo 'Đã đồng bộ '.count($response_ids).' điện thoại lên phần mềm';
            }
        } catch (BadResponseException $e){
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            dump($responseBodyAsString);
        }
    }

    public function fittingToSoftware() {
    	$fittings = Fit::select('id','system_id','name','price','warranty')
        ->where('status',1)
        ->get();
        $data = [];
        foreach ($fittings as $value) {
            $temp = [];
            $temp['id'] = $value->id;
            $temp['system_id'] = $value->system_id;
            $temp['name'] = $value->name;
            $temp['price'] = $value->price;
            $temp['warranty'] = $value->warranty;
            $data[] = $temp;
        }

        $token = 'KMXyehDlqUeW6dQtz1AYiHNSIXVwQVMPzxCR4jRw0le30KRkxoaoVCFsBJ6C';
        $client = new Client();
        try {
            $response = $client->request(
                'POST',
                config('app.domain_system').'/api/sync-fitting-from-web',
                [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $token
                    ],
                    'form_params' => [
                        'fittings' => json_encode($data)
                    ]
                ]
            );
            if ($response->getStatusCode() == 200) {
                $response_ids = json_decode($response->getBody(), true);
                //dump($response_ids);die;
                foreach ($response_ids as $value) {
                	Fit::where('id',$value)->update(['system_id'=>$value['system_id']]);
                }
                echo 'Đã đồng bộ '.count($response_ids).' phụ kiện lên phần mềm';
            }
        } catch (BadResponseException $e){
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            dump($responseBodyAsString);
        }
    }

    public function findPhone($id) {
    	$phone = Phone::where('system_id',$id)->first();
    	if($phone) {
    		return redirect($phone->getUrl());
    	}else {
    		abort(404);
    	}
    }

    public function findFitting($id) {
    	$fit = Fit::where('system_id',$id)->first();
    	if($fit) {
    		return redirect($fit->getUrl());
    	}else {
    		abort(404);
    	}
    }
}
