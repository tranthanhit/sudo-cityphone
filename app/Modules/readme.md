**Cấu trúc File của 1 Module**

	ModuleName
		|- Configs
		|- Controllers
		|- Models
		|- Views
		|- Helpers
			|- functions.php
		|- Langguages
			|-en
				|-admin.php
			|-vi
				|-admin.php
		|- Migrations
		|- Repositories
			|- Contracts
			|- Eloquents
		|- routes.php

**Cấu trúc thư mục trên có thể tham khảo Module Demo**

**Configs**
- Chưa các File configs dùng để set các giá trị cấu hình cho từng Module
- Đăng ký Configs tại PlatformServiceProvider

**Controllers**
- Chưa các file xử lý chung của Module đó

**Models**
- Chưa các File Models

**Views**
- Chưa giao diện của module

**Helpers**
- Chứa các functions dùng chung trên toàn bộ Module

**Langguages**
- Chứa các file cấu hình ngôn ngữ cho Modules
- Từng ngôn ngữ sẽ chia ra từng file cụ thể và có admin.php làm nơi đặt text phiên dịch

**Migrations**
- Chưa các file migration dùng để khởi tạo Database
- Để tạo migration tại Module ta sử cấu lệnh
	
	php artisan make:migration create_{demo?}_table --path=App/Platform/{Module?}/Migrations

**Repositories**
- Nơi chưa code Logic truy cập dữ liệu
- Truy cập đường dẫn dưới đây để hiểu rõ cách sử dụng

	https://viblo.asia/p/laravel-design-patterns-series-repository-pattern-part-3-ogBG2l1ZRxnL

**routes.php**
- Nơi chưa route của Module

**Lưu ý: Cấu trúc thư mục trên KHÔNG nhất thiết phải có để Module có thể hoạt động**