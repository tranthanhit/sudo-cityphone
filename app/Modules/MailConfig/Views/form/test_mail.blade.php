<div class="form-group">
    <label class="control-label col-md-2 col-sm-2 col-xs-12">Gửi thử Email</label>
    <div class="controls col-md-7 col-sm-8 col-xs-10" style="padding-right: 0;">
    	<input type="text" class="form-control" id="check_mail" placeholder="Email của bạn" value="">
    	<p style="margin-top: 5px;" id="check_mail_notificate"></p>
    </div>
    <div class="controls col-md-2 col-sm-2 col-xs-2" style="padding-left: 0;">
    	<button type="button" class="btn btn-primary" id="check_mail_btn" style="width: 100%;">Kiểm tra</button>
    </div>
</div>
<script>
	$(document).ready(function() {
		$('body').on('click', '#check_mail_btn', function() {
			email 			= $('#check_mail').val();
			driver 			= $('#driver').val();
			host 			= $('#host').val();
			encryption 		= $('#encryption').val();
			port 			= $('#port').val();
			username 		= $('#username').val();
			password 		= $('#password').val();
			from_address 	= $('#from_address').val();
			from_name 		= $('#from_name').val();
			sendmail 		= $('#sendmail').val();
			if (email == '') {
				alert('Email thử không được để trống')
			} else {
				data = {
					email 			: email,
					driver 			: driver,
					host 			: host,
					encryption		: encryption,
					port 			: port,
					username 		: username,
					password 		: password,
					from_address	: from_address,
					from_name 		: from_name,
					sendmail 		: sendmail,
				};
				$.ajax({
			        headers: {
			            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			        },
			        type: 'POST',
			        url: '{{route('admin.setting.test_mail')}}',
			        data: data,
			        beforeSend: function(){
			            $('#check_mail_notificate').html('Đang kiểm tra! Vui lòng chờ giây lát!');
			        },
			        success:function(result){
			            $('#check_mail_notificate').html(result.message);
			        },
			        error: function (error) {
			            $('#check_mail_notificate').html('Có lỗi xảy ra! Lỗi: <br>'+error.responseJSON.message);
			        }
			    });
			}
		});
	});
</script>