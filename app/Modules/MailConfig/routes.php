<?php
$namespace = 'App\Modules\MailConfig\Controllers';

Route::namespace($namespace)->prefix('admin')->middleware(['web', 'auth-admin'])->group(function() {
	//route cho settings
    Route::group(['prefix' => 'settings'], function() {
		// admin/settings/mail_configs
        Route::get('/mail_configs','MailConfigController@mailConfigs')->name('admin.setting.mail_configs');
        Route::post('/mail_configs','MailConfigController@mailConfigs');
		// admin/settings/mail_configs/test_mail
		Route::post('/mail_configs/test_mail','MailConfigController@testMail')->name('admin.setting.test_mail');
    });
});