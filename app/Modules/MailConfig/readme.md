**Copy Folder Modules vào thư mục app**

**configs/app.php**

	'providers' => [
		// Illuminate\Mail\MailServiceProvider::class,
		...,
		App\Modules\ModuleServiceProvider::class,
		App\Modules\MailConfig\Providers\CustomMailServiceProvider::class,
	]

**configs/modules.php**

	'module' => [
		...,
		'settings' => [
			...,
			'mail_configs',
		],
	],
	'name' => [
		...,
		'mail_configs' => 'Cấu hình Mail',
	]

**Thêm hàm verify_email_org tại app/includes/functions.php nếu chưa có (Không cần hàm này vì verify-email nó cùi và đang bị lỗi)**

	/**
	 * Verify email qua verify-email.org
	 */
	function verify_email_org($email) {
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL,"https://app.verify-email.org/api/v1/RxmBjTr3s8j5l8pvy4uRBxrKbV60ZR7fJ4AB1TtkpYVCjo7gTP/verify/$email");
	    curl_setopt($ch, CURLOPT_HTTPHEADER, [
	        'Accept: application/json',
	        'Content-Type: application/json'
	    ]);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	    $data = curl_exec($ch);
	    $data = json_decode($data,true);
	    //$http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	    curl_close($ch);
	    if($data['status'] == 1) {
	    	return true;
	    } else {
	    	return false;
	    }
	}

**Route được sinh ra tại Modules sẽ chạy sau router/web.php nên hãy chú ý set lại đường dẫn Route tại (App/Modules/MailConfig/routes.php) nếu lỗi**