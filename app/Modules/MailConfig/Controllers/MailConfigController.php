<?php

namespace App\Modules\MailConfig\Controllers;
use App\Http\Controllers\Admin\Controller as BaseController;

use Illuminate\Http\Request;
use DB;
use App\MyClass\MyForm;
use App\MyClass\ListData;
use App\MyClass\Categories;

use Barryvdh\Debugbar\Facade as Debugbar;

/**
 * Class SettingController
 * @package App\Http\Controllers\Admin
 *
 * Phần cấu hình của core chúng ta chia thành 2 phần chính để tiện cho tối ưu query ngoài frontend:
 * - Cấu hình trang chủ: Seo (title, description, ...) của trang chủ, các nội dung chỉ hiện thị ở trang chủ như banner, các nội dung cố định ...
 * - Cấu hình chung: cho các nội dung chung trên toàn bộ các trang như: html head, hotline, link ở header, các thông tin ở footer ...
 *
 */

class MailConfigController extends BaseController
{
	private function postData($setting_name, $data){
        unset($data['_token']);
        unset($data['submit']);
        $data = base64_encode(json_encode($data));

        \Cache::pull('setting_'.$setting_name);

        if(DB::table('options')->where('name',$setting_name)->exists()){
            DB::table('options')->where('name',$setting_name)->update(['value'=>$data]);
        }else{
            DB::table('options')->insert(['name'=>$setting_name,'value'=>$data]);
        }
    }

    public function mailConfigs(Request $request) {
    	$setting_name = 'mail_configs';
        $title = "Cấu hình Mail";

        $this->checkRole('settings_'.$setting_name);
        $data = $request->all();

        if(isset($data['submit'])){
            $this->postData($setting_name, $data);
        }

        $option = DB::table('options')->select('value')->where('name',$setting_name)->first();
        if(empty($option)){
            $data = [];
        }else{
            $data = json_decode(base64_decode($option->value),true);
        }
        $driver = [
            'smtp' => 'SMTP',
            'ses' => 'SES',
            'sendmail' => 'Senmail',
            'mailgun' => 'Mailgun',
            'mandrill' => 'Mandrill',
            'sparkpost' => 'Sparkpost',
            'log' => 'Log',
            'array' => 'Array',
        ];
        $encryption = [
            'tls' => 'TLS',
            'ssl' => 'SSL',
        ];
        
        $form = new MyForm();

        $data_form[] = $form->select('driver', $data['driver']??config('mail.driver')??'', 0, 'Giao thức (MAIL_DRIVER)', $driver);
        $data_form[] = $form->text('host', $data['host']??config('mail.host')??'', 0, 'Máy chủ SMTP (MAIL_HOST)', 'smtp.gmail.com');
        $data_form[] = $form->select('encryption', $data['encryption']??config('mail.encryption')??'', 0, 'Mã hóa Email (MAIL_ENCRYPTION)', $encryption);
        $data_form[] = $form->text('port', $data['port']??config('mail.port')??'', 0, 'Cổng (MAIL_HOST)', '587');
        $data_form[] = $form->text('sendmail', $data['sendmail']??config('mail.sendmail')??'', 0, 'Sendmail', '/usr/sbin/sendmail -bs');
        
        $data_form[] = $form->text('username', $data['username']??config('mail.username')??'', 0, 'Tên người dùng SMTP (MAIL_USERNAME)', 'no-reply@sudo.vn');
        $data_form[] = $form->password('password', $data['password']??config('mail.password')??'', 0, 'Mật khẩu (MAIL_PASSWORD)', 'Mật khẩu');
        $data_form[] = $form->text('from_address', $data['from_address']??config('mail.from.address')??'', 0, 'Email Gửi đi (MAIL_FROM_ADDRESS)', 'no-reply@sudo.vn');
        $data_form[] = $form->text('from_name', $data['from_name']??config('mail.from.name')??'', 0, 'Tên đại diện (MAIL_FROM_NAME)', 'no-reply@sudo.vn');

        $data_form[] = $form->custom('MailConfig::form.test_mail');
        $data_form[] = $form->action('editconfig');

        return view('admin.layouts.setting', compact('data_form','title'));
    }

    public function testMail(Request $request) {
        extract($request->all(),EXTR_OVERWRITE);// đưa mảng về các biến có tên là các key của mảng

        Debugbar::enable();

        config([
            'mail.driver'           => $driver,
            'mail.host'             => $host,
            'mail.port'             => $port,
            'mail.from.address'     => $from_address,
            'mail.from.name'        => $from_name,
            'mail.encryption'       => $encryption,
            'mail.username'         => $username,
            'mail.password'         => $password,
            'mail.sendmail'         => $sendmail
        ]);

        $app = \App::getInstance();
        $app->singleton('swift.transport', function ($app) {
            return new \Illuminate\Mail\TransportManager($app);
        });
        $mailer = new \Swift_Mailer($app['swift.transport']->driver());
        \Mail::setSwiftMailer($mailer);

        try {
            \Mail::to($email)->send(new \App\Modules\MailConfig\Mail\TestEmail);
            return [
                'status' => 1,
                'message' => 'Gửi thành công',
            ];
            // if (verify_email_org($email)) {
            // } return [
            //     'status' => 2,
            //     'message' => 'Email không tồn tại'
            // ];
        } catch (Exception $e) {
            return [
                'status' => 2,
                'message' => 'Gửi thất bại',
                'error' => $e->message(),
            ];
        }
    }
}