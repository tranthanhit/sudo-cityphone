<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FitCategory extends Model
{
    //
    protected $table = "fit_categories";

    public function getUrl() {
        return route('web.fits_categories.show',['slug' => $this->slug]);
    }
}
