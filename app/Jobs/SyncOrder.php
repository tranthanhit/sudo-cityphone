<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use GuzzleHttp\Client;
use DB;
class SyncOrder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data_order;
    protected $order_id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data_order,$order_id)
    {
        $this->data_order = $data_order;
        $this->order_id = $order_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $request = $this->data_order;
        $order_id = $this->order_id;

        $request = json_decode(json_encode($request));

        if ($request->type != 2) {
            $client = new Client();
            $data = [
                'cus_name'=>$request->name,
                'cus_gender'=>$request->gender,
                'cus_phone'=>$request->phone,
                'cus_email'=>$request->email,
                'cus_address'=>$request->address,
                'location_id'=>$request->location_id,
                'type'=>$request->type,
                'info'=>$request->info,
                'product_name'=>$request->product_name,
                'product_price'=>$request->price,
                'product_url'=>$request->link,
            ];
            try {
                $response = $client->request(
                    'POST',
                    'http://sales.thanhphodienthoai.vn/api/syncOrder',
                    [
                        'headers' => [
                            'Authorization' => 'Bearer ' . getToken()
                        ],
                        'form_params' => [
                            "data" => json_encode($data),
                        ]
                    ]
                );
                if ($response->getStatusCode() == 200) {
                    $data = json_decode($response->getBody());
                } else {
                    $admin_id = Auth::guard('admin')->user()->id;
                    $ip = get_client_ip();
                    $time = date("Y-m-d H:i:s");
                    $title = "Lỗi Đồng bộ đơn hàng";
                    $type = 'sync_order_fail';
                    $table = "orders";
                    $table_id = $order_id;
                    $detail = '';
                    $status=1;
                    if($detail != '') {
                        $detail = encode_compress_string(json_encode($detail));
                    }
                    DB::table('system_logs')->insert(compact('admin_id','ip','time','title','type','table','table_id','detail','status'));
                }
            } catch (BadResponseException $e){
                $response = $e->getResponse();
                $responseBodyAsString = $response->getBody()->getContents();
                dump($responseBodyAsString);
            }
        }
    }
}
