<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class News extends Model
{
    protected $fillable = [
        'category_id',
        'name',
        'slug',
        'image',
        'detail',
        'tags',
        'status',
        'user_id',
    ];

    public function category()
    {
        return $this->belongsTo('App\NewsCategory', 'category_id', 'id');
    }

    public function getUrl()
    {
        return route('web.news.show', $this->slug);
    }

    public function getTime()
    {
        return $this->created_at->format('H:i d/m/Y');
    }

    public function getImage($size = '')
    {
        if (!isset($this->image) || $this->image == null) {
            $image = asset('public/template-admin/images/no-image.png');
            return $image;
        }
        return image_by_link($this->image, $size);
    }

    public function getRatings()
    {
        return \Cache::remember('rating_news_' . $this->id, 1440, function () {
            return Comment::selectRaw("COUNT(id) as count, SUM(rank) as sum, AVG(rank) as avg")->where('parent_id', 0)->where('status', 1)->where('type', 'news')->where('type_id', $this->id)->first();
        });
    }

    public static function getRelated($id_related, $item_number, $category_id = 0)
    {
        if (count($id_related) == 0 || $id_related[0] == "") {
            $related = News::where('status', 1)->orderBy('id', 'DESC');
            if ($category_id != 0) {
                $related = $related->where('category_id', $category_id);
            }
        } else {
            $related = News::join('news_categories', 'news_categories.id', 'news.category_id')
                ->where('news_categories.status', 1)
                ->where('news.status', 1)
                ->whereIn('news.id', $id_related)
                ->select('news.*');
        }
        $related = $related->take($item_number)->get();
        return $related;
    }

    public function getAdminName()
    {
        if($this->user_id == 0) {
            $admin_name = 'Cityphone';
        }else {
            $user = DB::table('admin_users')->where('id', $this->user_id)->first();
            $admin_name = $user->fullname ?? $user->name ?? 'Không xác định';
        }
        return $admin_name;
    }

    public function getAdminId()
    {
        $admin_id = 0;
        $system_logs = DB::table('system_logs')->where('table', 'news')->where('table_id', $this->id)->orderBy('id', 'asc')->first();
        if (isset($system_logs)) {
            $user = DB::table('admin_users')->where('id', $system_logs->admin_id)->first();
            $admin_id = $user->id;
        }
        return $admin_id;
    }
}
