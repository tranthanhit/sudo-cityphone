<?php

namespace App\Exports;

use App\Order;
use DB;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class CommentExport implements FromQuery, WithHeadings, WithMapping
{
	use Exportable;
    
    public function __construct($status,$start,$end,$product_name,$type,$comment_users,$comment_phones)
    {
        $this->status = $status;
        $this->start = $start;
        $this->end = $end;
        $this->product_name = $product_name;
        $this->type = $type;
        $this->comment_users = $comment_users;
        $this->comment_phones = $comment_phones;
        $this->stt = 0;
    }

    public function query()
    {
    	$data = DB::table('comment')->where('parent_id',0)->where('status','!=',4);
        if (isset($this->type)) {
            if ($this->type == '' || $this->type == null) { $data = $data;
            } if ($this->type == 'news') {
                $data = $data->wherein('type',['news','pages']);
            } else {
                $data = $data->where('type',$this->type);
            }
        }
        if($this->start != '' && $this->end !== '') {
            $data = $data->where('created_at','>',$this->start);
            $data = $data->where('created_at','<',$this->end);
        }
        $product_name = $this->product_name;
        if ($product_name != null && trim($product_name) != '') {
            if ($this->type != null) {
                $product = DB::table($this->type)->where('name','LIKE','%'.$product_name.'%')->get();
                $data = $data->whereIn('type_id',$product->pluck('id'));
            }
        }
        $comment_users = $this->comment_users;
        if ($comment_users != null && trim($comment_users) != '') {
            $admin = DB::table('admin_users')->where('fullname','LIKE','%'.$comment_users.'%')->get();
            $comment_admin = DB::table('comment')->whereIn('admin_id',$admin->pluck('id'))->get();
            $data = $data->whereIn('id',$comment_admin->pluck('parent_id'));
        }
        $comment_phones = $this->comment_phones;
        if ($comment_phones != null && trim($comment_phones) != '') {
            $data = $data->where('phone','LIKE','%'.$comment_phones.'%');
        }    	
    	$data = $data->orderBy("id","DESC")->limit(1000);
        return $data;
    }

    public function headings(): array
    {
        return [
            'STT',
            'Tên KH',
            'SĐT',
            'Email',
            'Sản phẩm',
            'Nội dung bình luận',
            'Trạng thái',
            'Đã gọi',
            'Like',
            'Rank',
            'Ngày bình luận',
        ];
    }

    public function map($comment): array
    {
        $this->stt++;
    	$arr_stt = [1=>'Đã duyệt',2=>'Chờ duyệt',3=>'Hủy'];
        $has_call_arr = [0 => '',1=>'Đã gọi'];
        switch ($comment->type) {
            case 'phones':
                $comment_product_name = DB::table('phones')->find($comment->type_id);
            break;
            case 'tablets':
                $comment_product_name = DB::table('tablets')->find($comment->type_id);
            break;
            case 'fits':
                $comment_product_name = DB::table('fits')->find($comment->type_id);
            break;
            case 'services':
                $comment_product_name = DB::table('services')->find($comment->type_id);
            break;
        }
        return [
            $this->stt,
            $comment->name??'',
            $comment->phone??'',
            $comment->email??'',
            $comment_product_name->name??'',
            $comment->content??'',
            $arr_stt[$comment->status]??'Không xác định',
            $has_call_arr[$comment->has_call]??'',
            $comment->like??0,
            $comment->rank??0,
            $comment->created_at??'',
        ];
    }
}
