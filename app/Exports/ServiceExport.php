<?php

namespace App\Exports;

use App\Service;
use App\ServiceCategory;
use DB;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ServiceExport implements FromQuery, WithHeadings, WithMapping
{
	use Exportable;
    
    public function __construct()
    {
        $this->stt = 0;
    }

    public function query()
    {
    	$data = Service::where('status',1)->orderBy("category_id")->select('id','category_id','name','slug','created_at')->limit(4000);
        return $data;
    }

    public function headings(): array
    {
        return [
            'STT',
            'Danh mục',
            'Tiêu đề',
            'Url',
            'Ngày thêm',
        ];
    }

    public function map($service): array
    {
        $category = ServiceCategory::pluck('name','id')->toArray();
        $this->stt++;
        return [
            $this->stt,
            $category[$service->category_id]??'',
            $service->name??'',
            $service->getUrl()??'',
            $service->created_at->format('d/m/Y')??'',
        ];
    }
}
