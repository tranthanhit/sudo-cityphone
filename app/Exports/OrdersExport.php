<?php

namespace App\Exports;

use App\Order;
use DB;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class OrdersExport implements FromQuery, WithHeadings, WithMapping
{
	use Exportable;
    
    public function __construct($status,$location,$type,$start,$end)
    {
        $this->status = $status;
        $this->location = $location;
        $this->type = $type;
        $this->start = $start;
        $this->end = $end;
    }

    public function query()
    {
    	$data = DB::table('orders');
    	if($this->status != -1) {
    		$data = $data->where('status',$this->status);
    	}
    	if($this->location != -1) {
    		$data = $data->where('location_id',$this->location);
    	}
    	if($this->type != -1) {
    		$data = $data->where('type',$this->type);
    	}
    	if($this->start != '' && $this->end !== '') {
    		$data = $data->where('created_at','>',$this->start);
    		$data = $data->where('created_at','<',$this->end);
    	}
    	$data = $data->select('id','name','phone','email','address','note','status','created_at','location_id','type','info','price');
    	$data = $data->orderBy("id","DESC")->limit(1000);

        return $data;
    }

    public function headings(): array
    {
        return [
            'ID',
            'Tên KH',
            'SĐT',
            'Email',
            'Địa chỉ',
            'Ghi chú',
            'Trạng thái',
            'Thời điểm',
            'Địa điểm',
            'Loại',
            'Thông tin',
            'Giá',
        ];
    }

    public function map($order): array
    {
    	$arr_stt = [0=>'Đơn hàng mới',1=>'Đã tiếp nhận',2=>'Hoàn thành',3=>'Hủy'];
    	$arr_type = [
            'phones' => "Đặt mua điện thoại",
            'phoneiment' => "Trả góp điện thoại",
            'tablets' => "Đặt mua máy tính bảng",
            'tabletiment' => "Trả góp máy tính bảng",
            'services' => "Đặt lịch sửa chữa",
            'servicesiment' => "Mua linh kiện",
            'fits' => "Mua phụ kiện"
        ];
    	$locations = DB::table('location')->pluck('name','id')->toArray();
        return [
            $order->id,
            $order->name,
            $order->phone,
            $order->email,
            $order->address,
            $order->note,
            isset($arr_stt[$order->status]) ? $arr_stt[$order->status] : 'Không xác định',
            $order->created_at,
            isset($locations[$order->location_id]) ? $locations[$order->location_id] : 'Không xác định',
            isset($arr_type[$order->type]) ? $arr_type[$order->type] : 'Không xác định',
            $order->info,
            format_price($order->price),
        ];
    }
}
