<?php

namespace App\Exports;

use App\Order;
use DB;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class FittingExport implements FromQuery, WithHeadings, WithMapping
{
	use Exportable;
    
    public function __construct($status,$category_id,$name)
    {
        $this->status = $status;
        $this->category_id = $category_id;
        $this->name = $name;
    }

    public function query()
    {
    	$data = DB::table('fits');

        if($this->status != -1) {
            $data = $data->where('status',$this->status);
        }
        if($this->category_id != -1) {
            $data = $data->where('category_id',$this->category_id);
        }
        if($this->name != '') {
            $data = $data->where('name','LIKE','%'.$this->name.'%');
        }

        $data = $data->where('price','<>',0);
        $data = $data->where('price','<>',68000000);

    	$data = $data->select('id','name','price');
    	$data = $data->orderBy("name","asc")->limit(1000);

        return $data;
    }

    public function headings(): array
    {
        return [
            'ID',
            'Tên phụ kiện',
            'Giá ',
            'Giá format',
        ];
    }

    public function map($order): array
    {
    	
        return [
            $order->id,
            $order->name,
            $order->price,
            format_price($order->price),
        ];
    }
}
