<?php

namespace App\Exports;

use App\Order;
use DB;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class PhoneExport implements FromQuery, WithHeadings, WithMapping
{
	use Exportable;
    
    public function __construct($status,$category_id,$name)
    {
        $this->status = $status;
        $this->category_id = $category_id;
        $this->name = $name;
    }

    public function query()
    {
    	$data = DB::table('phones');

        if($this->status != -1) {
            $data = $data->where('status',$this->status);
        }
        if($this->category_id != -1) {
            $data = $data->where('category_id',$this->category_id);
        }
        if($this->name != '') {
            $data = $data->where('name','LIKE','%'.$this->name.'%');
        }

        // $data = $data->where('price','<>',0);
        // $data = $data->where('price','<>',68000000);

    	$data = $data->select('id','name','price');
    	$data = $data->orderBy("name","asc")->limit(1000);

        return $data;
    }

    public function headings(): array
    {
        return [
            'ID',
            'Tên phụ kiện',
            'Ram',
            'Dung lượng',
            'Màu',
            'Tình trạng',
            'Xuất xứ',
            'Giá ',
            'Giá format',
        ];
    }

    public function map($phone): array
    {
    	$phone_attribute = DB::table('product_attribute')->where('type','phones')->where('type_id',$phone->id)->first();
        $default = json_decode(base64_decode(json_encode([])));
        $color = json_decode(base64_decode($phone_attribute->color??$default),true);
        $ram = json_decode(base64_decode($phone_attribute->ram??$default),true);
        $storage = json_decode(base64_decode($phone_attribute->storage??$default),true);
        $aspect = json_decode(base64_decode($phone_attribute->aspect??$default),true);
        $country = json_decode(base64_decode($phone_attribute->country??$default),true);


        // Trộn biến thể
        $data_attribute = [];
        if($color != null){
            $data_attribute = $color;
        }
        if($storage != null){
            $test1 = [];
            foreach ($storage as $key => $value) {
                array_push($test1, [$value,$data_attribute]);
            }
            $all1 = []; 
            foreach($test1 as $key => $value){
                foreach ($value[1] as $k => $v) {
                    $new_price = intval($value[0]["price"]) + intval($v["price"]);
                    $new_item = array_merge((array)$value[0],(array)$v);
                    $new_item["price"] = $new_price;
                    array_push($all1, $new_item);
                }
            }
            $data_attribute = $all1;

        }
        if($aspect != null){
            $test2 = [];
            foreach ($aspect as $key => $value) {
                array_push($test2, [$value,$data_attribute]);
            }
            $all2 = []; 
            foreach($test2 as $key => $value){
                foreach ($value[1] as $k => $v) {
                    $new_price = intval($value[0]["price"]) + intval($v["price"]);
                    $new_item = array_merge((array)$value[0],(array)$v);
                    $new_item["price"] = $new_price;
                    array_push($all2, $new_item);
                }
            }
            $data_attribute = $all2;

        }

        if($ram != null){
            $test3 = [];
            foreach ($ram as $key => $value) {
                array_push($test3, [$value,$data_attribute]);
            }

            $all3 = []; 
            foreach($test3 as $key => $value){
                foreach ($value[1] as $k => $v) {
                    $new_price = intval($value[0]["price"]) + intval($v["price"]);
                    $new_item = array_merge((array)$value[0],(array)$v);
                    $new_item["price"] = $new_price;
                    array_push($all3, $new_item);
                }
            }
            $data_attribute = $all3;

        }

        if($country != null){
            $test4 = [];
            foreach ($country as $key => $value) {
                array_push($test4, [$value,$data_attribute]);
            }

            $all4 = []; 
            foreach($test4 as $key => $value){
                foreach ($value[1] as $k => $v) {
                    $new_price = intval($value[0]["price"]) + intval($v["price"]);
                    $new_item = array_merge((array)$value[0],(array)$v);
                    $new_item["price"] = $new_price;
                    array_push($all4, $new_item);
                }
            }
            $data_attribute = $all4;
        }
        
        $data = [];
        foreach ($data_attribute as $key => $value) {
            $data[] = [
                $phone->id,
                $phone->name,
                $value['ram_name']??[],
                $value['storage_name']??[],
                $value['color_name']??[],
                $value['tt_name']??[],
                $value['country_name']??[],
                $phone->price,
                format_price($phone->price),
            ];
        }
        return $data;
    }
}