<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client;
use App\OrderDetail;

use App\Jobs\SyncOrder;

class Order extends Model
{
    //
    protected $table = "orders";

    protected $fillable = [
        'name', 'gender', 'phone', 'email', 'address', 'note', 'status','location_id','type','info','link','price'
    ];

    public static function addOrder($request)
    {
        switch ($request->name) {
            case 'phones':
                $info = $request->product_name." - Màu: ".$request->info[0]['color_name']." - DL: ".$request->info[0]['storage']." - Tình Trạng: ".$request->info[0]['aspect']." - Bảo hành: ".$request->warranty;

                $type = 1;
                $info_api = "Màu: ".$request->info[0]['color_name']." - DL: ".$request->info[0]['storage']." - Tình Trạng: ".$request->info[0]['aspect']." - Bảo hành: ".$request->warranty;
            break;
            case 'phoneiment':
                $info = $request->product_name." - Màu: ".$request->info[0]['color_name']." - DL: ".$request->info[0]['storage']." - Tình Trạng: ".$request->info[0]['aspect']." - Bảo hành: ".$request->warranty." - ".$request->pay_before." - ".$request->pay_month;

                $type = 1;
                $info_api = "Màu: ".$request->info[0]['color_name']." - DL: ".$request->info[0]['storage']." - Tình Trạng: ".$request->info[0]['aspect']." - Bảo hành: ".$request->warranty." - ".$request->pay_before." - ".$request->pay_month;
            break;
            case 'fits':
                $info = $request->product_name." - Bảo hành: ".$request->warranty;

                $type = 1;
                $info_api = $request->product_name." - Bảo hành: ".$request->warranty;
            break;
            case 'tablets':
                $info = $request->product_name." - Màu: ".$request->info[0]['color_name']." - DL: ".$request->info[0]['storage']." - Tình Trạng: ".$request->info[0]['aspect']." - Bảo hành: ".$request->warranty;

                $type = 1;
                $info_api = "Màu: ".$request->info[0]['color_name']." - DL: ".$request->info[0]['storage']." - Tình Trạng: ".$request->info[0]['aspect']." - Bảo hành: ".$request->warranty;
            break;
            case 'tabletiment':
                $info = $request->product_name." - Màu: ".$request->info[0]['color_name']." - DL: ".$request->info[0]['storage']." - Tình Trạng: ".$request->info[0]['aspect']." - Bảo hành: ".$request->warranty." - ".$request->pay_before." - ".$request->pay_month;

                $type = 1;
                $info_api = "Màu: ".$request->info[0]['color_name']." - DL: ".$request->info[0]['storage']." - Tình Trạng: ".$request->info[0]['aspect']." - Bảo hành: ".$request->warranty." - ".$request->pay_before." - ".$request->pay_month;
            break;
            case 'services':
                $info = "Dịch vụ: ".$request->product_name." - Linh kiện: ".$request->service_name." - Giá: ".$request->price_serice;

                $type = 2;
                $info_api = "Dịch vụ: ".$request->product_name." - Linh kiện: ".$request->service_name." - Giá: ".$request->price_serice;
            break;
            case 'servicesiment':
                $info = "Dịch vụ: ".$request->product_name." - Linh kiện: ".$request->service_name." - Giá: ".$request->price_serice;

                $type = 2;
                $info_api = "Dịch vụ: ".$request->product_name." - Linh kiện: ".$request->service_name." - Giá: ".$request->price_serice;
            break;
        }

        $data = [
            'name' => $request->cname,
            'gender' => $request->gender,
            'phone' => $request->phone,
            'email' => $request->email,
            'address' => $request->address,
            'note' => $request->note,
            'status' => 0,
            'location_id' => intval($request->cuslocation),
            'type' => $request->name,
            'info' => $info,
            'link' => $request->link,
            'price' => $request->price,
        ];
        $data_order = [
            'type'=> $type,
            'name' => $request->cname,
            'gender' => $request->gender,
            'phone' => $request->phone,
            'email' => $request->email,
            'address' => $request->address,
            'location_id' => intval($request->cuslocation),
            'info' => $info_api,
            'product_name' => $request->product_name,
            'link' => $request->link,
            'price' => $request->price,
        ];


        $order_id = Order::create($data)->id;
        dispatch(new SyncOrder($data_order,$order_id));

        OrderDetail::addOrderDetail($order_id,$request);
        
        
        // Order::syncOrder($request);
    }

    public function showStatus()
    {
        switch ($this->status) {
            case '0': $status = "Đơn hàng mới"; break;
            case '1': $status = "Đã tiếp nhận"; break;
            case '2': $status = "Hoàn thành"; break;
            case '3': $status = "Đã Hủy"; break;
            default : $status = "Không xác định"; break;
        }
        return $status;
    }

    public static function syncOrder($request) {
        $client = new Client();
        switch ($request->name) {
            case 'phones':
                $type = 1;
                $info = "Màu: ".$request->info[0]['color_name']." - DL: ".$request->info[0]['storage']." - Tình Trạng: ".$request->info[0]['aspect']." - Bảo hành: ".$request->warranty;
            break;
            case 'phoneiment':
                $type = 1;
                $info = "Màu: ".$request->info[0]['color_name']." - DL: ".$request->info[0]['storage']." - Tình Trạng: ".$request->info[0]['aspect']." - Bảo hành: ".$request->warranty." - ".$request->pay_before." - ".$request->pay_month;
            break;
            case 'fits':
                $type = 1;
                $info = $request->product_name." - Bảo hành: ".$request->warranty;
            break;
            case 'tablets':
                $type = 1;
                $info = "Màu: ".$request->info[0]['color_name']." - DL: ".$request->info[0]['storage']." - Tình Trạng: ".$request->info[0]['aspect']." - Bảo hành: ".$request->warranty;
            break;
            case 'tabletiment':
                $type = 1;
                $info = "Màu: ".$request->info[0]['color_name']." - DL: ".$request->info[0]['storage']." - Tình Trạng: ".$request->info[0]['aspect']." - Bảo hành: ".$request->warranty." - ".$request->pay_before." - ".$request->pay_month;
            break;
            case 'services':
                $type = 2;
                $info = "Dịch vụ: ".$request->product_name." - Linh kiện: ".$request->service_name." - Giá: ".$request->price_serice;
            break;
            case 'servicesiment':
                $type = 2;
                $info = "Dịch vụ: ".$request->product_name." - Linh kiện: ".$request->service_name." - Giá: ".$request->price_serice;
            break;
        }

        $data = [
            'type'=>$type,
            'cus_gender'=>$request->gender,
            'cus_name'=>$request->cname,
            'cus_phone'=>$request->phone,
            'cus_email'=>$request->email,
            'cus_address'=>$request->address,
            'location_id'=>$request->cuslocation,
            'info'=>$info,
            'product_name'=>$request->product_name,
            'product_price'=>$request->price,
            'product_url'=>$request->link,
        ];
        try {
            $response = $client->request(
                'POST',
                'https://dathang.thanhphodienthoai.vn/api/syncOrder',
                [
                    'headers' => [
                        'Authorization' => 'Bearer ' . getToken()
                    ],
                    'form_params' => [
                        "data" => json_encode($data),
                    ]
                ]
            );
            if ($response->getStatusCode() == 200) {
                $data = json_decode($response->getBody());
            }
        } catch (BadResponseException $e){
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            dump($responseBodyAsString);
        }
    }
}
