<?php
/**
 * Toàn bộ các function phần admin
 */
function checkRole($role){
    $admin_id=Auth::guard('admin')->user()->id;
    if($admin_id==1){
        return true;
    }else{
        $data=DB::table('authorizations')->where('admin_id',$admin_id)->first();
        $array_role=json_decode($data->capabilities);
        if(in_array($role, $array_role)){
            return true;
        }
        else{
            return false;
        }
    }
}
function get_slug($slug = '',$title,$table,$id_field,$slug_field,$record_id_edit = 0) {
    if($slug == '') {
        $slug = slugTitle($title);
    }else {
        $slug = slugTitle($slug);
    }
    $data=DB::table($table)->select($id_field)->where($slug_field,$slug)->first();
    if(!empty($data)) {
        if($data->$id_field == $record_id_edit) {
            return $slug;
        }else {
            $i = 1;
            while($i > 0) {
                $slug_new = $slug.'-'.$i;
                $data1=DB::table($table)->select($id_field)->where($slug_field,$slug_new)->first();
                if(!empty($data1)) {
                    $i++;
                }
                else {
                    return $slug_new;
                }
            }
        }

    }
    else {
        return $slug;
    }
}
function admin_paging($page,$page_size,$total,$prefix_link,$suffix_link) {
    if($total%($page_size) == 0) {
        $total_count = $total/($page_size);
    }else {
        $total_count = (int)($total/($page_size)) + 1;
    }
    if ($total_count <= 1) return '';
    $html = '<ul class="pagination">';
    $html .= '<li class="paginate_button"><a class="pagingCount" href="#">Trang '.$page.'/'.$total_count.'</a></li>';
    if($page > 1) {
        $html .= '<li class="paginate_button"><a class="pagingFirst" href="'.$prefix_link.'1'.$suffix_link.'">&lt;&lt;</a></li>';
    }
    // Tổng số trang nhỏ hơn 5
    if($total_count <= 5) {
        // Trang hiện tại lớn hơn 1 thì hiện nút previous
        if($page > 1) {
            $html .= '<li class="paginate_button"><a class="pagingPrevious" href="'.$prefix_link.($page - 1).$suffix_link.'">&lt;</a></li>';
        }
        for($i = 1; $i <= $total_count; $i++) {
            $html .= '<li class="paginate_button'.(($page == $i) ? " active" : "").'"><a class="pagingNumber" href="'.$prefix_link.$i.$suffix_link.'">'.$i.'</a></li>';
        }
        // Trang hiện tại nhỏ hơn tổng số trang thì hiện nút next
        if($page < $total_count) {
            $html .= '<li class="paginate_button"><a class="pagingNext" href="'.$prefix_link.($page + 1).$suffix_link.'">&gt;</a></li>';
        }
    }
    // Trang hiện tại nhỏ hơn 3 và tổng số trang lớn hơn 5
    if($page <= 3 && $total_count > 5) {
        // Trang hiện tại lớn hơn 1 thì hiện nút previous
        if($page > 1) {
            $html .= '<li class="paginate_button"><a class="pagingPrevious" href="'.$prefix_link.($page - 1).$suffix_link.'">&lt;</a></li>';
        }
        for($i = 1; $i < 5; $i++) {
            $html .= '<li class="paginate_button'.(($page == $i) ? " active" : "").'"><a class="pagingNumber" href="'.$prefix_link.$i.$suffix_link.'">'.$i.'</a></li>';
        }
        $html .= '<li class="paginate_button"><a class="pagingDot" href="#">...</a></li>';
        // Trang hiện tại nhỏ hơn tổng số trang thì hiện nút next
        if($page < $total_count) {
            $html .= '<li class="paginate_button"><a class="pagingNext" href="'.$prefix_link.($page + 1).$suffix_link.'">&gt;</a></li>';
        }
    }
    // Trang hiện tại lớn hơn 3, tổng số trang lớn hơn 5 và sau trang hiện tại 2 trang vẫn nhỏ hơn tổng số trang
    if($page > 3 && $total_count > 5 && $page + 2 <= $total_count) {
        $html .= '<li class="paginate_button"><a class="pagingPrevious" href="'.$prefix_link.($page - 1).$suffix_link.'">&lt;</a></li>
                <li class="paginate_button"><a class="pagingDot" href="#">...</a></li>
                <li class="paginate_button"><a class="pagingNumber" href="'.$prefix_link.($page - 2).$suffix_link.'">'.($page-2).'</a></li>
                <li class="paginate_button"><a class="pagingNumber" href="'.$prefix_link.($page - 1).$suffix_link.'">'.($page-1).'</a></li>
                <li class="paginate_button active"><a class="pagingNumber" href="'.$prefix_link.($page).$suffix_link.'">'.($page).'</a></li>
                <li class="paginate_button"><a class="pagingNumber" href="'.$prefix_link.($page + 1).$suffix_link.'">'.($page+1).'</a></li>
                <li class="paginate_button"><a class="pagingNumber" href="'.$prefix_link.($page + 2).$suffix_link.'">'.($page+2).'</a></li>';
        $html .= '<li class="paginate_button"><a class="pagingDot" href="#">...</a></li>';
        // Trang hiện tại nhỏ hơn tổng số trang thì hiện nút next
        if($page < $total_count) {
            $html .= '<li class="paginate_button"><a class="pagingNext" href="'.$prefix_link.($page + 1).$suffix_link.'">&gt;</a></li>';
        }
    }
    // Trang hiện tại lớn hơn 3, tổng số trang lớn hơn 5 và sau trang hiện tại 2 trang lớn hơn tổng số trang
    if($page > 3 && $total_count > 5 && $page + 2 > $total_count) {
        $html .= '<li class="paginate_button"><a class="pagingPrevious" href="'.$prefix_link.($page - 1).$suffix_link.'">&lt;</a></li>
                <li class="paginate_button"><a class="pagingDot" href="#">...</a></li>
                <li class="paginate_button"><a class="pagingNumber" href="'.$prefix_link.($page - 2).$suffix_link.'">'.($page-2).'</a></li>
                <li class="paginate_button"><a class="pagingNumber" href="'.$prefix_link.($page - 1).$suffix_link.'">'.($page-1).'</a></li>
                <li class="paginate_button active"><a class="pagingNumber" href="'.$prefix_link.($page).$suffix_link.'">'.($page).'</a></li>';
        for($i = $page+1; $i <= $total_count; $i++) {
            $html .= '<li class="paginate_button"><a class="pagingNumber" href="'.$prefix_link.$i.$suffix_link.'">'.$i.'</a></li>';
        }
        // Trang hiện tại nhỏ hơn tổng số trang thì hiện nút next
        if($page < $total_count) {
            $html .= '<li class="paginate_button"><a class="pagingNext" href="'.$prefix_link.($page + 1).$suffix_link.'">&gt;</a></li>';
        }
    }
    if($page < $total_count) {
        $html .= '<li class="paginate_button"><a class="pagingLast" href="'.$prefix_link.$total_count.$suffix_link.'">&gt;&gt;</a></li>';
    }
    $html .= '</ul>';
    return $html;

}
function list_timezone() {
    $zones_array = array();
    $timestamp = time();
    foreach(timezone_identifiers_list() as $key => $zone) {
        date_default_timezone_set($zone);
        $zones_array[$key]['zone'] = $zone;
        $zones_array[$key]['diff_from_GMT'] = 'UTC/GMT ' . date('P', $timestamp);
    }
    return $zones_array;
}
function change_time_to_text($time,$type = null){
    if(!is_numeric($time)){
        $time = strtotime($time);
    }
    if($type == 'update'){
        $text = 'Cập nhật khoảng ';
    }else{
        $text = '';
    }
    if((time() - $time) < 60){
        return $text.round(time()-$time).' giây trước';
    }elseif((time() - $time) < 60*60){
        return $text.round((time() - $time)/60).' phút trước';
    }elseif ((time() - $time) < 60*60*24){
        return $text.round(((time()-$time)/60)/60).' giờ trước';
    }else{
        return date('H:i:s A d/m/Y',$time);
    }
}
function show_editor_sudo_media($name_textare,$height,$default = null){
    $option = '<script>
    document.addEventListener("DOMContentLoaded", function(event) { 
        tinymce.init({
            path_absolute : "/",
            selector:\'textarea[id="'.$name_textare.'"]\',
            branding: false,
            hidden_input: false,
            relative_urls: false,
            convert_urls: false,
            height : '.$height.',
            autosave_ask_before_unload:true,
            autosave_interval:\'10s\',
            autosave_restore_when_empty:true,
            entity_encoding : "raw",
            fontsize_formats: "8pt 9pt 10pt 11pt 12pt 13pt 14pt 15pt 16pt 17pt 18pt 19pt 20pt 22pt 24pt 26pt 28pt 30pt 32pt 36pt 40pt 46pt 52pt 60pt",
            plugins: [
                "textcolor",
                "advlist autolink lists link image imagetools charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table autosave contextmenu paste wordcount"
            ],
            wordcount_countregex: /[\w\u2019\x27\-\u00C0-\u1FFF]+/g,
            language: "vi_VN",
            autosave_retention:"30m",
            autosave_prefix: "tinymce-autosave-{path}{query}-{id}-",
            wordcount_cleanregex: /[0-9.(),;:!?%#$?\x27\x22_+=\\\/\-]*/g,
            toolbar: "insertfile undo redo table sudomedia charmap | styleselect | sizeselect | bold italic | fontselect |  fontsizeselect | forecolor " +
            "backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent " +
            "indent | link unlink fullscreen restoredraft filemanager",
            setup: function (editor) {
                editor.addButton(\'sudomedia\', {
                  text: \'Tải ảnh\',
                  icon: \'image\',
                  label:\'Nhúng ảnh vào nội dung\',
                  onclick: function () {
                    media_popup("add","tinymce","'.$name_textare.'","Chèn ảnh vào bài viết");
                  }
                });
              },
            file_picker_callback: function() {
                    media_popup("add","tinymce","'.$name_textare.'","Chèn ảnh vào bài viết");
            }
        });
    });
    </script>';
    $option .= '<textarea id="'.$name_textare.'" name="'.$name_textare.'">'.$default.'</textarea>';
    return $option;
}
function seoMeta($table_name = '',$post_id = 0){
    $form = '<div class="wrap_seometa">
                     <h2>Nội dung cho Seo</h2>';
    //Nếu là file add để rỗng trường post_id
    if($table_name != '' && $post_id == 0 ) {
        $form .= '<div class="preview_snippet">
                        <h4>Xem trước hiển thị tìm kiếm trên Google</h4>
                        <div class="preview_snippet_main">
                           <h3 class="preview_snippet_title"></h3>
                           <p class="preview_snippet_link">http://'.$_SERVER['HTTP_HOST'].'/<span>bai-viet-so-1</span>.html &nbsp;&nbsp;&nbsp; <a class="btn btn-xs btn-warning" href="#slug">Sửa đường dẫn</a></p>
                           <p class="preview_snippet_des"></p>
                        </div>
                     </div>';
        $form .= '<div class="form-group">
                        <label for="'.$table_name.'_title" class="control-label col-md-2 col-sm-2 col-xs-12"><span class="form-asterick"></span>Tiêu đề cho thẻ meta title (SEO)</label>
                        <div class="controls  col-sm-9">
                            <input type="text" placeholder="" class="form-control col-sm-9 in_title" id="'.$table_name.'_title" value="" name="seo_title">
                            <span class="in_title_count">0</span>Ký tự. Tiêu đề (title) tốt nhất khoảng 60 - 70 ký tự
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="'.$table_name.'_description" class="control-label col-md-2 col-sm-2 col-xs-12"><span class="form-asterick"></span>Đoạn mô tả cho thẻ meta description (SEO)</label>
                        <div class="controls col-sm-9">
                            <textarea placeholder="" class="span6 in_des" id="'.$table_name.'_description" value="" name="seo_description" style="width:100%;height:60px"></textarea>
                            <span class="in_des_count">0</span> Ký tự. Mô tả (description) tốt nhất khoảng 120 - 160 ký tự
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="'.$table_name.'_robots" class="control-label col-md-2 col-sm-2 col-xs-12"><span class="form-asterick"></span>Điều hướng Robots</label>
                        <div class="controls col-sm-3">
                           <select id="'.$table_name.'_robots" name="seo_robots" class="form-control">
                              <option value="Index,Follow">Index,Follow</option>
                              <option value="Noindex,Nofollow">Noindex,Nofollow</option>
                              <option value="Index,Nofollow">Index,Nofollow</option>
                              <option value="Noindex,Follow">Noindex,Follow</option>
                           </select>
                        </div>
                    </div>';
    }
    //Nếu là file edit truyền đủ tên bảng và post_id (id bài viết)
    if($table_name != '' && $post_id != 0 ) {

        $meta_seo = DB::table('meta_seo')->where('type',$table_name)->where('type_id',$post_id)->first();
        if(isset($meta_seo)){
            $form .= '<div class="preview_snippet">
                        <h4>Xem trước hiển thị tìm kiếm trên Google</h4>
                        <div class="preview_snippet_main">
                           <h3 class="preview_snippet_title">'.$meta_seo->title.'</h3>
                           <p class="preview_snippet_link">http://'.$_SERVER['HTTP_HOST'].'/<span>bai-viet-so-1</span>.html &nbsp;&nbsp;&nbsp; <a class="btn btn-xs btn-warning" href="#slug">Sửa đường dẫn</a></p>
                           <p class="preview_snippet_des">'.$meta_seo->description.'</p>
                        </div>
                     </div>';
            $form .= '<div class="form-group">
                        <label for="'.$table_name.'_title" class="control-label col-md-2 col-sm-2 col-xs-12"><span class="form-asterick"></span>Tiêu đề cho thẻ meta title (SEO)</label>
                        <div class="controls col-sm-9">
                            <input type="text" placeholder="Tiêu đề tốt nhất 60 - 70 ký tự" class="form-control col-sm-9 in_title" id="'.$table_name.'_title" value=\''.$meta_seo->title.'\' name="seo_title">
                            <span class="in_title_count">'.strlen(utf8_decode($meta_seo->title)).'</span> ký tự. Tiêu đề (title) tốt nhất khoảng 60 - 70 ký tự
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="'.$table_name.'_description" class="control-label col-md-2 col-sm-2 col-xs-12"><span class="form-asterick"></span>Đoạn mô tả cho thẻ meta description (SEO)</label>
                        <div class="controls col-sm-9">
                            <textarea placeholder="Mô tả khoảng 160 ký tự" class="span6 in_des" id="'.$table_name.'_description" name="seo_description" style="width:100%;height:60px">'.$meta_seo->description.'</textarea>
                            <span class="in_des_count">'.strlen(utf8_decode($meta_seo->description)).'</span> ký tự. Mô tả (description) tốt nhất khoảng 120 - 160 ký tự
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="'.$table_name.'_robots" class="control-label col-md-2 col-sm-2 col-xs-12"><span class="form-asterick"></span>Điều hướng Robots</label>
                        <div class="controls col-sm-3">
                           <select id="'.$table_name.'_robots" name="seo_robots" class="form-control">
                              <option value="Index,Follow" '.($meta_seo->robots == 'Index,Follow' ? ' selected="selected"' : '').'>Index,Follow</option>
                              <option value="Noindex,Nofollow" '.($meta_seo->robots == 'Noindex,Nofollow' ? ' selected="selected"' : '').'>Noindex,Nofollow</option>
                              <option value="Index,Nofollow" '.($meta_seo->robots == 'Index,Nofollow' ? ' selected="selected"' : '').'>Index,Nofollow</option>
                              <option value="Noindex,Follow" '.($meta_seo->robots == 'Noindex,Follow' ? ' selected="selected"' : '').'>Noindex,Follow</option>
                           </select>
                        </div>
                    </div>';
        }else{
            $form .= '<div class="preview_snippet">
                        <h4>Xem trước hiển thị tìm kiếm trên Google</h4>
                        <div class="preview_snippet_main">
                           <h3 class="preview_snippet_title"></h3>
                           <p class="preview_snippet_link">http://'.$_SERVER['HTTP_HOST'].'/<span>bai-viet-so-1</span>.html &nbsp;&nbsp;&nbsp; <a class="btn btn-xs btn-warning" href="#slug">Sửa đường dẫn</a></p>
                           <p class="preview_snippet_des"></p>
                        </div>
                     </div>';
            $form .= '<div class="form-group">
                        <label for="'.$table_name.'_title" class="control-label col-md-2 col-sm-2 col-xs-12"><span class="form-asterick"></span>Tiêu đề cho thẻ meta title (SEO)</label>
                        <div class="controls  col-sm-9">
                            <input type="text" placeholder="Tiêu đề tốt nhất 60 - 70 ký tự" class="form-control col-sm-9 in_title" id="'.$table_name.'_title" value="" name="seo_title">
                            <span class="in_title_count">0</span> ký tự. Tiêu đề (title) tốt nhất khoảng 60 - 70 ký tự
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="'.$table_name.'_description" class="control-label col-md-2 col-sm-2 col-xs-12"><span class="form-asterick"></span>Đoạn mô tả cho thẻ meta description (SEO)</label>
                        <div class="controls col-sm-9">
                            <textarea placeholder="Mô tả khoảng 160 ký tự" class="span6 in_des" id="'.$table_name.'_description" value="" name="seo_description" style="width:100%;height:60px"></textarea>
                            <span class="in_des_count">0</span> ký tự. Mô tả (description) tốt nhất khoảng 120 - 160 ký tự
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="'.$table_name.'_robots" class="control-label col-md-2 col-sm-2 col-xs-12"><span class="form-asterick"></span>Điều hướng Robots</label>
                        <div class="controls col-sm-3">
                           <select id="'.$table_name.'_robots" name="seo_robots" class="form-control">
                              <option value="Index,Follow">Index,Follow</option>
                              <option value="Noindex,Nofollow">Noindex,Nofollow</option>
                              <option value="Index,Nofollow">Index,Nofollow</option>
                              <option value="Noindex,Follow">Noindex,Follow</option>
                           </select>
                        </div>
                    </div>';
        }
    }
    // $form .= '';
    $form .= '</div>';

    return $form;
}

function recursive_setting_menu($data,$count = 0,$name,$is_first=false) {
    $html = '';
    if(!empty($data)) {
        if($is_first) {
            $html .= '<ol class="dd-list" id="ol-first">';
        }else {
            $html .= '<ol class="dd-list">';
        }
        foreach($data as $key => $value) {
            if(!empty($value['table'])){
                $getData = DB::table($value['table'])->select('slug')->where('id', $value['id'])->first();
            }
            if(isset($getData) && !empty($getData)){
                $slug = route('web.'.$value['table'].'.show', $getData->slug);
            }else{
                $slug = $value['link'];
            }
            $html .= '<li class="dd-item" id="li-'.$count.'-'.$key.'" data-name="'.$value['name'].'" data-link="'.$slug.'" data-table="'.$value['table'].'" data-id="'.$value['id'].'" data-taget="'.$value['taget'].'" data-rel="'.$value['rel'].'">';
            $html .= '<div class="dd-handle handle-'.$count.'-'.$key.'">'.$value['name'];
            $html .= '</div>';
            $html .= '<p class="p-action">';
            if($value['table'] == ''){
                $html .= '<a class="a1" href="javascript:;" onclick="show_edit_menu(\''.$name.'\',\''.$count.'-'.$key.'\');">Sửa</a>';
            }else{
                $html .= '<a class="a1" href="javascript:;" onclick="show_edit_menu1(\''.$name.'\',\''.$count.'-'.$key.'\', \''.$value['table'].'\', '.$value['id'].');">Sửa</a>';
            }
            $html .= '<a class="a2" href="javascript:;" onclick="remove_menu(\''.$name.'\',\''.$count.'-'.$key.'\');">Xóa</a>';
            $html .= '</p>';
            if(isset($value['children'])){
                $data_child = $value['children'];
            }else{
                $data_child = [];
            }
            $count +=1;
            $html .= recursive_setting_menu($data_child,$count,$name);
        }
        $html .= '</ol>';
    }
    return $html;
}
function api_custom_price($content,$fomat_price = false,$host = 'http://banggia.thanhphodienthoai.vn') {
    if($content == '') return 'Liên hệ';
    $content = trim($content);
    if(strpos($content,'{price}') !== false) {
        $id = str_replace('{price}','',$content);
        $link = $host.'/price/'.$id.'/';
        $value = file_get_contents($link);
        return $value;
    }
    if(strpos($content,'{name}') !== false) {
        $id = str_replace('{name}','',$content);
        $link = $host.'/name/'.$id.'/';
        $value = file_get_contents($link);
        return $value;
    }
    if(strpos($content,'{warranty}') !== false) {
        $id = str_replace('{warranty}','',$content);
        $link = $host.'/warranty/'.$id.'/';
        $value = file_get_contents($link);
        return $value;
    }
    if($fomat_price) {
        return format_price($content);
    }
    return $content;
}
function curl_price($link) {
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL,$link);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Connection: Keep-Alive", "Content-type: application/x-www-form-urlencoded;charset=UTF-8"));
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $response = curl_exec($ch);
    if (curl_errno($ch)) {
        return 'Liên hệ';
    } else {
        return $response;
    }
}
function api_custom_price_filter( $content ) {
    $host = 'http://banggia.thanhphodienthoai.vn';
    
    $check_name = preg_match_all("@{name}([0-9]+)@" , $content, $regs);
    if ($check_name > 0){
        $list_name = $regs[1];
        foreach($list_name as $id) {
            $link = $host.'/name/'.$id.'/';
            $value = curl_price($link);
            $content = str_replace('{name}'.$id,$value,$content);
        }
    }
    
    $check_price = preg_match_all("@{price}([0-9]+)@" , $content, $regs);
    if ($check_price > 0){
        $list_price = $regs[1];
        foreach($list_price as $id) {
            $link = $host.'/price/'.$id.'/';
            $value = curl_price($link);
            $content = str_replace('{price}'.$id,$value,$content);
        }
    }
    
    $check_warranty = preg_match_all("@{warranty}([0-9]+)@" , $content, $regs);
    if ($check_warranty > 0){
        $list_warranty = $regs[1];
        foreach($list_warranty as $id) {
            $link = $host.'/warranty/'.$id.'/';
            $value = curl_price($link);
            $content = str_replace('{warranty}'.$id,$value,$content);
        }
    }
    
    return $content;
}
function getImage($size='',$image){
        // replace https://image.Cityphone.com// = config('filesystem.sop.container_url')
    if ($image) {
        $remove_characters = str_replace('//','/', $image);
        $link_img_old = [
            'http://images.Cityphone.vn/media/images',
            'https://images.Cityphone.vn/media/images',
            'https://object-storage.tyo1.cloud.z.com/v1/zc_6c13a3172446411fab7837a8a5479710/Cityphonevn/images',
        ];
        $link_img_new = config('filesystems.disks.do.domain').'/'.config('filesystems.disks.do.folder').'/images';
        $image_replace = str_replace($link_img_old, $link_img_new, $remove_characters);
        $explore = explode('/', $image_replace);
        // lấy tháng năm
        $count_explore = count($explore);
        $image_year = $explore[$count_explore-3];
        $image_month = $explore[$count_explore-2];
        // tách lấy tên và đuôi file
        $image_full_path = explode('.',array_pop($explore));
        $image_name = $image_full_path[0];
        $image_extention = array_pop($image_full_path);
        // nối size
        if ($size == "") {
            $image = $link_img_new.'/'.$image_year.'/'.$image_month.'/'.$image_name.'.'.$image_extention;
        } else {
            $image = $link_img_new.'/'.$image_year.'/'.$image_month.'/'.$image_name.'-'.$size.'.'.$image_extention;
        }
    } else {
        $image = getImageDefault('');
    }
    
    return $image;
}
function internal_link($content,$anchor,$title,$link) {
    $occurrences = substr_count(strtoupper($content),strtoupper($anchor));//số lần xuất hiện
    if ($occurrences == 0)//nếu anchor không có trong content trả về false
        return false;
    $rand_position = rand(1,$occurrences);//chọn ngẫu nhiên 1 vị trí
    $anchor_length = strlen($anchor);//độ dài của anchor
    $str_first_temp = '';
    $str_last_temp = $content;
    $anchor_i = $anchor;
    for($i=0;$i<$rand_position;$i++) {
        $length_in = stripos($str_last_temp,$anchor);
        $length_out = $length_in + $anchor_length;
        if($i == ($rand_position - 1)) {
            $anchor_i = substr($str_last_temp,$length_in,$anchor_length);//lấy anchor ở vị trí đã chọn (đề phòng hoa thường)
        }
        $str_first_temp .= substr($str_last_temp,0,$length_out);
        $str_last_temp = substr($str_last_temp,$length_out);
    }
    $str_first_temp = substr($str_first_temp,0,-$anchor_length);
    $result = $str_first_temp.'<a class="sudo-internal-link" href="'.$link.'" title="'.$title.'" target="_blank">'.$anchor_i.'</a>'.$str_last_temp;
    return $result;
}
function remove_link_content($subject) {
    return preg_replace('#<a.*?>([^>]*)</a>#i', '$1', $subject);
}
function insert_internal_link($content){
    $list_internal_link = DB::table('internal_link')->where('status',1)->orderBy('created_at','asc')->limit(200)->get();
    $string_duplicate = '';
    
    if(count($list_internal_link)) {
        $content = $content;
        $c = 0;
        foreach ($list_internal_link as $value) {
            if(stripos($string_duplicate,$value->anchor) === false) {
                $il_content = internal_link($content,$value->anchor,$value->title,$value->link);
                if ($il_content !== false) {
                    $c++;
                    $string_duplicate .= $value->anchor.' '.$value->title.' '.$value->link.' ';
                    $content = $il_content;
                }
            }
            if ($c == 10)
            break;
        }
    }
    return $content;
}