<?php
/**
 * all functions
 * Các function chung nhất dùng cho core
 */

function removeSign($str){
    $coDau=array("à","á","ạ","ả","ã","â","ầ","ấ","ậ","ẩ","ẫ","ă",
        "ằ","ắ","ặ","ẳ","ẵ",
        "è","é","ẹ","ẻ","ẽ","ê","ề" ,"ế","ệ","ể","ễ",
        "ì","í","ị","ỉ","ĩ",
        "ò","ó","ọ","ỏ","õ","ô","ồ","ố","ộ","ổ","ỗ","ơ"
    ,"ờ","ớ","ợ","ở","ỡ",
        "ù","ú","ụ","ủ","ũ","ư","ừ","ứ","ự","ử","ữ",
        "ỳ","ý","ỵ","ỷ","ỹ",
        "đ",
        "À","Á","Ạ","Ả","Ã","Â","Ầ","Ấ","Ậ","Ẩ","Ẫ","Ă"
    ,"Ằ","Ắ","Ặ","Ẳ","Ẵ",
        "È","É","Ẹ","Ẻ","Ẽ","Ê","Ề","Ế","Ệ","Ể","Ễ",
        "Ì","Í","Ị","Ỉ","Ĩ",
        "Ò","Ó","Ọ","Ỏ","Õ","Ô","Ồ","Ố","Ộ","Ổ","Ỗ","Ơ"
    ,"Ờ","Ớ","Ợ","Ở","Ỡ",
        "Ù","Ú","Ụ","Ủ","Ũ","Ư","Ừ","Ứ","Ự","Ử","Ữ",
        "Ỳ","Ý","Ỵ","Ỷ","Ỹ",
        "Đ","ê","ù","à");

    $khongDau=array("a","a","a","a","a","a","a","a","a","a","a"
    ,"a","a","a","a","a","a",
        "e","e","e","e","e","e","e","e","e","e","e",
        "i","i","i","i","i",
        "o","o","o","o","o","o","o","o","o","o","o","o"
    ,"o","o","o","o","o",
        "u","u","u","u","u","u","u","u","u","u","u",
        "y","y","y","y","y",
        "d",
        "A","A","A","A","A","A","A","A","A","A","A","A"
    ,"A","A","A","A","A",
        "E","E","E","E","E","E","E","E","E","E","E",
        "I","I","I","I","I",
        "O","O","O","O","O","O","O","O","O","O","O","O"
    ,"O","O","O","O","O",
        "U","U","U","U","U","U","U","U","U","U","U",
        "Y","Y","Y","Y","Y",
        "D","e","u","a");
    return str_replace($coDau,$khongDau,$str);
}
function removeHTML($string){
    $string = preg_replace ('/<script.*?\>.*?<\/script>/si', ' ', $string);
    $string = preg_replace ('/<style.*?\>.*?<\/style>/si', ' ', $string);
    $string = preg_replace ('/<.*?\>/si', ' ', $string);
    $string = str_replace ('&nbsp;', ' ', $string);
    return $string;
}
function removeScript($string){
    $string = preg_replace ('/<script.*?\>.*?<\/script>/si', '<br />', $string);
    $string = preg_replace ('/on([a-zA-Z]*)=".*?"/si', ' ', $string);
    $string = preg_replace ('/On([a-zA-Z]*)=".*?"/si', ' ', $string);
    $string = preg_replace ("/on([a-zA-Z]*)='.*?'/si", " ", $string);
    $string = preg_replace ("/On([a-zA-Z]*)='.*?'/si", " ", $string);
    return $string;
}
function removeXML($string){
    $string = preg_replace('/<xml>.*?<\/xml>/si','',$string);
    //$string = preg_replace('/<!--.*?-->/si','',$string);
    return $string;
}
function replaceMQ($text){
    $text	= str_replace("\'", "'", $text);
    $text	= str_replace("'", "''", $text);
    return $text;
}

function removeMagicQuote($str){
    $str = str_replace("\'", "'", $str);
    $str = str_replace("\&quot;", "&quot;", $str);
    $str = str_replace("\\\\", "\\", $str);
    return $str;
}
function removeUTF8BOM($text) {
    $bom = pack('H*','EFBBBF');
    $text = preg_replace("/^$bom/", '', $text);
    return $text;
}
function slugTitle($string,$keyReplace = "/"){
    $string = removeSign($string);
    $string  =  trim(preg_replace("/[^A-Za-z0-9]/i"," ",$string)); // khong dau
    $string  =  str_replace(" ","-",$string);
    $string = str_replace("--","-",$string);
    $string = str_replace($keyReplace,"-",$string);
    return strtolower($string);
}
function cutString($str, $length, $char=" ..."){
    //Nếu chuỗi cần cắt nhỏ hơn $length thì return luôn
    $strlen	= mb_strlen($str, "UTF-8");
    if($strlen <= $length) return $str;

    //Cắt chiều dài chuỗi $str tới đoạn cần lấy
    $substr	= mb_substr($str, 0, $length, "UTF-8");
    if(mb_substr($str, $length, 1, "UTF-8") == " ") return $substr . $char;

    //Xác định dấu " " cuối cùng trong chuỗi $substr vừa cắt
    $strPoint= mb_strrpos($substr, " ", "UTF-8");

    //Return string
    if($strPoint < $length - 20) return $substr . $char;
    else return mb_substr($substr, 0, $strPoint, "UTF-8") . $char;
}
function get_url(){
    if(isset($_SERVER['HTTPS'])){
        $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
    }
    else{
        $protocol = 'http';
    }
    return $protocol . "://" . $_SERVER['HTTP_HOST'];
}
function get_client_ip() {
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        return $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        return $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        return $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        return $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        return $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        return $_SERVER['REMOTE_ADDR'];
    else
        return 'UNKNOWN';
}

function http_to_https($string='') {
    $string = str_replace([
        'http://Cityphone.vn'
    ], 'https://Cityphone.vn', $string);
    return $string;
}

function image_by_link($link_img='',$size='') {
    if (strpos($link_img, '/template-admin/images/no-image.png')) {
        return $link_img;
    }
    // đổi ảnh lỗi link // trước tên
    // https://sudospaces.com/Cityphone-vn/images/2018/05//thay-mat-kinh-cam-ung-vivo-y83-medium.jpg
    $link_img = str_replace('//', '/', $link_img);

    $link_img_old = [
        'http:/images.Cityphone.vn/media/images',
        'https:/images.Cityphone.vn/media/images',
        'https:/object-storage.tyo1.cloud.z.com/v1/zc_6c13a3172446411fab7837a8a5479710/Cityphonevn/images',
        'https:/sudospaces.com/Cityphone-vn/images',
    ];
    $link_img_new = config('filesystems.disks.do.domain').'/'.config('filesystems.disks.do.folder').'/images';
    $link_img = str_replace($link_img_old, $link_img_new, $link_img);
    
    // $link_img = http://domain.com/uploads/2017/06/dan-cuong-luc-sony-large.jpg
    // $size: tiny 80, small 150, medium 300, large 600
    $array_size = array('tiny','small','medium','large');
    //$link_img = str_replace(['http://old.domain'],config('app.url'),$link_img);
    if(!in_array($size,$array_size)) {
        return $link_img;
    }

    $endS = strrpos($link_img,'.'); //vị trí của cái . cuối cùng
    $img_ext = substr($link_img,$endS+1); //jpg
    $img_path_name = substr($link_img,0,$endS); //http://domain.com/uploads/2017/06/dan-cuong-luc-sony-large
    $starG = strrpos($img_path_name,'-'); // vị trí dấu - cuối cùng
    $prefix = substr($img_path_name,$starG+1); //tiny,small,medium,large hoặc 1 text bất kỳ nếu link là ảnh mặc định
    if(in_array($prefix,$array_size)) {
        $img_path_name = str_replace($prefix,'',$img_path_name); //dan-cuong-luc-sony-
        return $img_path_name.$size.'.'.$img_ext;
    }else {
        return $img_path_name.'-'.$size.'.'.$img_ext;
    }
}

/**
 * Nén string và encode cho các trường hợp xuất dữ liệu
 * @param  string $string
 */
function encode_compress_string($string) {
    return base64_encode(gzcompress($string, 9));
}

/**
 * Giải nén string và decode cho các string qua func encode_compress_string
 * @param  string $string
 */
function decode_compress_string($string) {
    return gzuncompress(base64_decode($string));
}
function strip_param_from_url( $url, $param )
{
    $base_url = strtok($url, '?');              // Get the base url
    $parsed_url = parse_url($url);              // Parse it 
    $query = $parsed_url['query'];              // Get the query string
    parse_str( $query, $parameters );           // Convert Parameters into array
    unset( $parameters[$param] );               // Delete the one you want
    $new_query = http_build_query($parameters); // Rebuilt query string
    return $base_url.'?'.$new_query;            // Finally url is ready
}
function append_query_string_to_url($url,$query_string = '', $query_value = '') {
    $parsed_url = parse_url($url);
    $scheme   = isset($parsed_url['scheme']) ? $parsed_url['scheme'] . '://' : '';
    $host     = isset($parsed_url['host']) ? $parsed_url['host'] : '';
    $port     = isset($parsed_url['port']) ? ':' . $parsed_url['port'] : '';
    $user     = isset($parsed_url['user']) ? $parsed_url['user'] : '';
    $pass     = isset($parsed_url['pass']) ? ':' . $parsed_url['pass']  : '';
    $pass     = ($user || $pass) ? "$pass@" : '';
    $path     = isset($parsed_url['path']) ? $parsed_url['path'] : '';
    //$query    = isset($parsed_url['query']) ? '?' . ($parsed_url['query'].'&'.$query_string) : ('?'.$query_string);
    if(isset($parsed_url['query'])) {
        $new_query = '';
        $arr_query = explode('&',$parsed_url['query']);
        $no_conflict = true;
        $page_is_first = false;
        foreach($arr_query as $k=>$v) {
            if($k == 0) {
                $new_query .= '?';
            }else {
                $new_query .= '&';
            }
            $ev = explode('=',$v);
            if($ev[0] == 'page') {
             if($k == 0)
               $page_is_first = true;
             $new_query = substr($new_query, 0, -1);//bỏ dấu & vừa thêm trong vòng này
            }elseif($ev[0] == $query_string) {
                $new_query .= $query_string.'='.$query_value;
                $no_conflict = false;
            }else {
                $new_query .=  $v;
            }
        }
        if($no_conflict) {
           if($page_is_first) 
             $new_query .= '?'.$query_string.'='.$query_value;
           else
            $new_query .= '&'.$query_string.'='.$query_value;
        }
    }else {
        $new_query = '?'.$query_string.'='.$query_value;
    }
    $fragment = isset($parsed_url['fragment']) ? '#' . $parsed_url['fragment'] : '';
    return "$scheme$user$pass$host$port$path$new_query$fragment";
}

function cutPhonenumber($phone) {
    $phoneCut = substr($phone, 0, -2).'xx';
    return $phoneCut;
}

function format_price($price){
    if($price != 68000000 && $price != 0 && $price != '' && is_numeric($price))
        return number_format($price, 0, ',', '.') . ' ₫';
    else
        return 'Liên hệ';
}

function get_price_data_str($price=0) {
    if ($price == 68000000) {
        return 0;
    } else {
        return $price;
    }
}

function getImageDefault($name) {
    if ($name == "load") {
        return asset('/assets/img/load_video.svg');
    } else {
        return asset('/template-admin/images/no-image.png');
    }
}

function getAlt($link='') {
    if ($link=='') {
        return 'no-image';
    } else {
        $link_explore = explode('/', $link);
        $img = array_pop($link_explore);
        $alt = explode('.', $img)[0];
        $alt = str_replace(['-tiny','-small','-medium','-large'], '', $alt);
        return $alt;
    }
    
}
function time_to_text($time,$type = null) {
    if(!is_numeric($time)){
        $time = strtotime($time);
    }
    if($type == 'update'){
        $text = 'Cập nhật khoảng ';
    }else{
        $text = '';
    }
    if((time() - $time) < 60){
        return $text.round(time()-$time).' giây trước';
    }elseif((time() - $time) < 60*60){
        return $text.round((time() - $time)/60).' phút trước';
    }elseif ((time() - $time) < 60*60*24){
        return $text.round(((time()-$time)/60)/60).' giờ trước';
    }else{
        return date('H:i d/m/Y',$time);
    }
}

function replace_detail($content) {
    //đổi các link ảnh sv cũ sang object storage
    $old_domain = ['http://images.Cityphone.vn/media/images','https://images.Cityphone.vn/media/images','https://object-storage.tyo1.cloud.z.com/v1/zc_6c13a3172446411fab7837a8a5479710/Cityphonevn/images'];
    $new_domain = config('filesystems.disks.do.domain').'/'.config('filesystems.disks.do.folder').'/images';
    $content = str_replace($old_domain,$new_domain,$content);

    //thay đổi img để chạy lazyload
    //đổi src thành data-original
    $content = preg_replace("/<img(.*?)(src=|srcset=)(.*?)>/i", '<img$1src="'.getImageDefault('load').'" data-original=$3>', $content);
    //thêm class lazy vào các thẻ đã có sẵn class
    $content = preg_replace('/<img(.*?)class=\"(.*?)\"(.*?)>/i', '<img$1class="$2 lazy"$3>', $content);
    //thêm class lazy vào các thẻ chưa có class
    $content = preg_replace('/<img((?:(?!class=).)*?)>/i', '<img class="lazy"$1>', $content);

    // short_code
    $config_short_code = DB::table('options')->select('value')->where('name','short_code')->first();
    if($config_short_code){
        $short_code = json_decode(base64_decode($config_short_code->value),true);
    }else{
        $short_code = null;
    }
    $content = str_replace($short_code['short_code_key']??[], $short_code['short_code_content']??[], $content);

    $content = http_to_https($content);

    return $content;
}

function filter_code_chat($content) {
    return str_replace('<script>','<script defer>',$content);
}
function number_in_string($string=0) {
    return (int)preg_replace('/[^0-9]/', '', $string);
}
function getAvatarText($name) {
    return substr($name,0,1);
}
function getToken() {
    $token = '9vBt8PTEsR9DTGaGO1G5NXfL1C5ipL4vd6Um3CZbkjXFAgn47tHRJdsNWjef';
    return $token;
}
function getSimpleTextTime($time){
    if(!is_numeric($time)){
        $time = strtotime($time);
    }
    $offset = time()-$time;
    if($offset <= 60){
        return $offset.' giây trước';
    }elseif ($offset > 60 && $offset <= 60*60) {
        return intval($offset/60) . ' phút trước';
    }elseif ($offset > 60*60 && $offset <= 24*60*60) {
        return intval($offset/(60*60)) . ' giờ trước';
    }elseif ($offset > 24*60*60 && $offset <= 30*24*60*60) {
        return intval($offset/(24*60*60)) . ' ngày trước';
    }else {
        return ''.date('H:i d/m/Y',$time);
    }
}

function getCharacterAvatar($name) {
    $arr = explode(' ',$name);
    array_filter($arr);
    $last = end($arr);
    return substr($last,0,1);
}
function isJson($string) {
    json_decode($string);
    return (json_last_error() == JSON_ERROR_NONE);
}