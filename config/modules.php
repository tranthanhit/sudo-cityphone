<?php

return [
    'module' => [
        'classes' => ['access','create','edit','delete'],
        'fit_categories' => ['access','create','edit','delete'],
        'fits' => ['access','create','edit','delete'],
        'service_categories' => ['access','create','edit','delete'],
        'services' => ['access','create','edit','delete'],
        'orders' => ['access','create','edit','delete'],
        'comment' => ['access','create','edit','delete'],
        'news_categories' => ['access','create','edit','delete'],
        'news' => ['access','create','edit','delete'],
        'address' => ['access','create','edit','delete'],
        'location' => ['access','create','edit','delete'],
        'pages' => ['access','create','edit','delete'],
        'prices' => ['access','create','edit','delete'],
        'slides' => ['access','create','edit','delete'],
        'sync' => ['access','create','edit','delete'],
        'internal_link' => ['access','create','edit','delete'],
        'admin_users' => ['access','create','edit','delete'],
        'settings' => [
            'access',
            'general',
            'home',
            'menu_header',
            'promotion',
            'meta_code',
            'seo_category',
            'google_shopping',
            'mail_configs',
        ],
        'system_logs' => ['access','create','edit','delete'],
    ],

    'name' => [
        'fit_categories' => 'Danh mục Phụ kiện',
        'fits' => 'Phụ kiện',
        'service_categories' => 'Danh mục Dịch vụ',
        'services' => 'Dịch vụ',
        'classes'=>'Danh mục theo loại',
        'orders' => 'Hỗ trợ',
        'comment' => 'Đánh giá',
        'news_categories'=>'Danh mục tin tức',
        'news'=>'Tin tức',
        'address'=>'Địa chỉ',
        'location'=>'Tỉnh Thành',
        'pages'=>'Trang',
        'prices'=>'Bảng giá',
        'slides'=>'Slide',
        'sync'=>'Link đồng bộ',
        'internal_link'=>'Link nội bộ',
        'admin_users'=>'Tài khoản quản trị',
        'settings'=>'Cấu hình',
        'system_logs'=>'Logs hệ thống',

        'access'=>'Truy cập',
        'create'=>'Thêm',
        'edit'=>'Sửa',
        'delete'=>'Xóa',

        'general'=>'Cấu hình chung',
        'home'=>'Cấu hình trang chủ',
        'promotion'=>'Khuyến Mãi',
        'menu_header'=>'Cấu hình menu header',
        'meta_code'=>'Mã chèn vào website',
        'seo_category'=>'Seo danh mục',
        'google_shopping'=>'Cấu hình Google shopping',
        'mail_configs' => 'Cấu hình Mail',
    ],

    'icon' => [
        'fit_categories' => 'fa-bars',
        'fits' => 'fa-battery-quarter',
        'service_categories' => 'fa-bars',
        'services' => 'fa-wrench',
        'classes'=>'fa-bars',
        'orders' => 'fa-bar-chart',
        'comment' => 'fa-comment',
        'news_categories'=>'fa-bars',
        'news'=>'fa-newspaper-o',
        'address'=>'fa-map-marker',
        'pages'=>'fa-file-text',
        'prices'=>'fa-table',
        'slides'=>'fa-picture-o',
        'sync'=>'fa-refresh',
        'internal_link'=>'fa-link',
        'location'=>'fa-picture-o',
        'admin_users'=>'fa-users',
        'settings'=>'fa-cogs',
        'system_logs'=>'fa-repeat',
    ]
];
