<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
    <h1>Xin chào <b>{{$data['name']}}</b>!</h1>
    <table style="width: 100%;">
        <tbody>
            <tr>
                <td style="width: 153px;"><strong>Tên </strong></td>
                <td style="width: 186px;"><strong>Số điện thoại</strong></td>
                <td style="width: 169px;"><strong>Email</strong></td>
                <td style="width: 169px;"><strong>Ghi chú</strong></td>
                <td style="width: 169px;"><strong>payment</strong></td>
                <td style="width: 169px;"><strong>Địa chỉ</strong></td>
                <td style="width: 169px;"><strong>Loại</strong></td>
                <td style="width: 169px;"><strong>Số lượng</strong></td>
                <td style="width: 169px;"><strong>Tồng tiền</strong></td>
            </tr>
            <tr>
                <td style="width: 153px;">{{ $data['name'] }}</td>
                <td style="width: 153px;">{{ $data['phone'] }}</td>
                <td style="width: 153px;">{{ $data['email'] }}</td>
                <td style="width: 153px;">{{ $data['note'] }}</td>
                <td style="width: 153px;">{{ $data['payment'] }}</td>
                <td style="width: 153px;">{{ $data['address'] }}</td>
                <td style="width: 153px;">{{ $data['type'] }}</td>
                <td style="width: 153px;">{{ $data['quantity'] }}</td>
                <td style="width: 153px;">{{ $data['total'] }}</td> 
            </tr> 
        </tbody>
    </table>
</body>
</html>
