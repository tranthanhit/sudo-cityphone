@extends('web.layouts.app')
@section('head')
<style>
    .modal{
        visibility: visible;
        display: block;
        opacity: 1;
    }
</style>
<script type="application/ld+json">
{
    "@graph": [{
            "@context": "http://schema.org",
            "@type": "Store",
            "image": "{{config('app.url')}}/assets/img/logo.png",
            "name": "{{config('app.name')}}",
            "address": {
                "@type": "PostalAddress",
                "streetAddress": "120 Thái Hà",
                "addressLocality": "Q. Đống Đa",
                "addressRegion": "Hà Nội",
                "postalCode": "100000",
                "addressCountry": "VN"
            },
            "priceRange": "$$",
            "geo": {
                "@type": "GeoCoordinates",
                "latitude": 21.011915,
                "longitude": 105.821283
            },
            "telephone": "0969.120.120 - 0433.120.120"

        },
        {
            "@context": "http://schema.org",
            "@type": "WebSite",
            "name": "{{config('app.name')}}",
            "url": "{{config('app.url')}}"
        }
    ]
}
</script>
@endsection
@section('content')



<div class="banner container default ">
    <div class="wrap">
        <div class="banner_left " data-aos="fade-right" data-aos-duration="1000">
            <div class="owl-carousel" data-slider-id="3">
                @foreach ($slides_home as $value_img)
                    <div class="mySlides"  @if($is_mobile) style="height: 180px;" @endif>
                        <img src=" @if(!$is_mobile) {{ image_by_link($value_img->image,'large') }} @else {{ image_by_link($value_img->image_mobile??$value_img->image,'large') }}  @endif" style="width:100%">
                    </div>
                @endforeach
            </div>
            <div class="banner_left_list owl-thumbs" data-slider-id="3">
                @foreach ($slides_home as $value_name)
                    <span class="owl-thumb-item">{{ $value_name->name }}</span>
                @endforeach
            </div>
        </div>
        <div class="banner_right" data-aos="fade-left" data-aos-duration="1000">
            <div class="banner_right_top">
                <div class="title">
                    <h4 id="title">Tin công nghệ</h4>
                    <div class="redline">
                        <div class="dot">
                            <div class="wave"></div>
                        </div>
                        <a href="{!! @$config_general['link_vong_quay_m_m'] !!}">Mua sim tặng dịch vụ MCA </a>
                    </div>
                </div>
                @if(isset($list_news_hot) && count($list_news_hot) > 0)
                    <div class="list">
                        @foreach($list_news_hot as $value)
                            <div class="item">
                                <a href="{{ $value->getUrl() }}">
                                    <div class="img">
                                        <img class="lazy" src="{{ $value->getImage('small') }}" data-original="{{$value->getImage('small')}}" alt="{{$value->slug}}">
                                    </div>
                                    <div class="infor">
                                        <h3 id="title">{{ $value->name }}</h3>
                                        <span>
                                            {{ date('d/m/Y',strtotime($value->created_at)) }} | {{ date('h:s A',strtotime($value->created_at)) }}
                                        </span>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                @endif
            </div>
            <div class="banner_right_banner">
                @if(isset($config_general['brands']))
                    @foreach($config_general['brands']['image'] as $key=>$value1)
                        <a href="{{ $config_general['brands']['text_1'][$key] }}"><img src="{!! $value1 !!}" alt=""></a>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</div>

<div class="content default product_top" id="outstanding_service" style="margin-top: 2px">
    <div class="wrap">
        <div class="content_title">
            <h4 id="title_content">Các dịch vụ nổi bật</h4>
            <ul>
                @if(isset($service_category) && count($service_category)> 0 )
                    @foreach ($service_category as $value)
                        <li><a href="{{ $value->getUrl() }}">{{ $value->name }}</a></li>
                    @endforeach
                @endif
            </ul>
            @if($is_mobile)
                @if(isset($service_category) && count($service_category)>0)
                    <select style="border: none;position: absolute;right: 0;background: #fff;" name="" onchange="service_cate_m()" class="service-cate-m">
                        <option value="">Dịch vụ nổi bật</option>
                        @foreach ($service_category as $item)
                            <option value="{{ $item->slug }}">{{ $item->name }}</option>
                        @endforeach 
                    </select>
                @endif
            @endif
            @if(!$is_mobile)
                <a href="/sua-chua" id="view_all">Xem tất cả <i class="fa fa-angle-double-right"></i></a>
            @endif  
        </div>
        @if(isset($list_services) && count($list_services)>0)
            <div class="content_list">
                @foreach ($list_services as $value)
                    @php
                        $promotion = $config_promotion["promotion_default_service"];
                        $promotion = preg_split('/\n|\r\n/',$promotion);
                    @endphp
                    @include('web.layouts.item_service',compact('value','promotion'))
                    
                @endforeach
            </div>
        @endif
    </div>
</div>

@if(isset($home_prices) && count($home_prices)>0 )
    <div class="content default product_top" id="price_list_home">
        <div class="wrap">
            <div class="content_title">
                <h4 id="title_content">Bảng giá dịch vụ sửa chữa điện thoại</h4>
            </div>
            <div class="content_price-list">
                @foreach ($home_prices as $value)
                    <a href="{{ $value->getUrl() }}" data-aos="fade-up" >
                        <div class="img">
                            <img class="lazy" src="{{ $value->getImage('medium') }}" data-original="{{$value->getImage('medium')}}" alt="{{$value->slug}}">
                        </div>
                    </a>
                @endforeach
            </div>
        </div>
    </div>
@endif


<div class="content default product_top" id="Repair_service_yet_iphone">
    <div class="wrap">
        <div class="content_title">
            <h4 id="title_content">Dịch vụ sữa chữa iphone</h4>
            <ul>
                @if(isset($list_s_cate_ip_child) && count($list_s_cate_ip_child)> 0)
                    @foreach ($list_s_cate_ip_child as $key=>$value)
                        @if($key<=4)
                            <li><a title="{{ $value->name }}" href="{{ $value->getUrl() }}">{{ cutString($value->name,20) }}</a></li>
                        @endif
                    @endforeach
                @endif
            </ul>

            @if($is_mobile)
                @if(isset($list_s_cate_ip_child) && count($list_s_cate_ip_child)>0)
                    <select style="border: none;position: absolute;right: 0;background: #fff;" name="" onchange="service_cate_iphone()" class="service-cate-iphone">
                        <option value="">Dịch vụ sữa chữa Iphone</option>
                        @foreach ($list_s_cate_ip_child as $item)
                            <option value="{{ $item->slug }}">{{ $item->name }}</option>
                        @endforeach 
                    </select>
                @endif
            @endif
            @if(!$is_mobile)
                <a href="/sua-chua-iphone" id="view_all">Xem tất cả <i class="fa fa-angle-double-right"></i></a>
            @endif    
        </div>
        @if(isset($list_s_ip_collect) && count($list_s_ip_collect)>0)
            <div class="content_list">
                @foreach ($list_s_ip_collect as $value)
                    @php
                        $promotion = $config_promotion["promotion_default_service"];
                        $promotion = preg_split('/\n|\r\n/',$promotion);
                    @endphp
                    @include('web.layouts.item_service',compact('value','promotion'))
                @endforeach
            </div>
        @endif
    </div>
</div>


<div class="content default product_top" id="parts_accessories">
    <div class="wrap">
        <div class="content_title">
            <h4 id="title_content">Linh kiện, phụ kiện điện thoại</h4>
            @if(isset($fit_category) && count($fit_category)>0)
                <ul>
                    @foreach ($fit_category as $item)
                        <li><a href="{{ $item->getUrl() }}">{{ $item->name }}</a></li>
                    @endforeach 
                </ul>
            @endif
            @if(!$is_mobile)
                <a href="{{ route('web.fits_categories.showall') }}" id="view_all">Xem tất cả <i class="fa fa-angle-double-right"></i></a>
            @endif
            @if($is_mobile)
                @if(isset($fit_category) && count($fit_category)>0)
                    <select style="border: none;position: absolute;right: 0;background: #fff;" name="" onchange="fit_category()" id="fit-category" class="fit-category">
                        <option value="">Phụ kiện nổi bật</option>
                        @foreach ($fit_category as $item)
                            <option value="{{ $item->slug }}">{{ $item->name }}</option>
                        @endforeach 
                    </select>
                @endif
            @endif
        </div>
        @if(isset($list_fits) && count($list_fits)>0)
            <div class="content_list">
                @foreach($list_fits as $value)
                    <div class="content_list_accessories" data-aos="fade-up" data-aos-duration="1000">
                        <a href="{{ $value->getUrl() }}">
                            <div class="img">
                                <img class="lazy" src="{{ $value->getImage('medium') }}" data-original="{{$value->getImage('medium')}}" alt="{{$value->slug}}">
                            </div>
                            <div class="infor">
                                <h3 id="title">{{ $value->name }}</h3>
                                <p id="price">
                                    <strong>{{ $value->getPrice() }}</strong>
                                </p>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        @endif
    </div>
</div>

{{--  @include('web.layouts.form')  --}}

@if ($is_mobile)
<footer id="footer_infor_mobile" >
    <div class="wrap">
        <div class="footer-support ui-grid-a" style="float: left; width: 100% !important;">
            <div class="footer-support-item ui-block-a">
                <p>Tư vấn (24/7)</p>
                <a href="tel:{!! @$config_general['phone_mobile_1'] !!}"><i class="fa fa-phone"></i> {!! @$config_general['phone_mobile_1'] !!}</a>
            </div>
            <div class="footer-support-item ui-block-b">
                <p>Góp ý, phản ánh</p>
                <a href="tel:{!! @$config_general['phone_mobile_2'] !!}"><i class="fa fa-phone"></i> {!! @$config_general['phone_mobile_2'] !!}</a>
            </div>
            <div class="footer-support-item ui-block-a">
               <a href="javascript:;" id="other_information" style="color: #288ad6 !important;font-size: 16px !important;">Thông tin khác <i class="fa fa-angle-down" aria-hidden="true"></i></a>
            </div>
            <div class="footer-support-item ui-block-a">
                <a href="javascript:;" id="store_address" style="color: #288ad6 !important;font-size: 16px !important;">Địa chỉ của hàng <i class="fa fa-angle-down" aria-hidden="true"></i></a>
            </div>
        </div>
        <div class="m_footer_2_mobile product_top" style="display: none" >
            @if(isset($menu_primary))
                @foreach($menu_primary as $value)
                    <div class="item">
                        <h4 id="title">{{ $value->name }}</h4>
                        <ul>
                            @foreach($value->children as $children)
                                <li><a href="{{ $children->link }}">{{ $children->name }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                @endforeach
            @endif
        </div>
        <div class="m_address_2_mobile product_top" style="display: none" >
            @foreach($locations as $location)
                <h3 id="title">{{$location->name}}</h3>
                @foreach($address as $locate)
                    @if($locate->location_id == $location->id)
                        <div class="item">
                            <p id="adrress"><i class="fa fa-map-marker"></i>{{ $locate->name }}</p>
                            <p id="phone">
                                <a href="tel:{{$locate->mobile}}"><i class="fa fa-phone"></i> {{$locate->mobile}}</a>
                                <a href="{{ $locate->map }}" target="_blank"><i class="fa fa-location-arrow"></i> Vị trí bản đồ
                                </a>
                            </p>
                        </div>
                    @endif
                @endforeach
            @endforeach 
        </div>
    </div>
</footer>
@endif

{{--  <div class="content default product_top" id="technology_news">
    <div class="wrap">
        <div class="content_title">
            <h4 id="title_content">tin tức công nghệ</h4>
            <a href="{{ route('web.news_categories.showall') }}" id="view_all">Xem tất cả <i class="fa fa-angle-double-right"></i></a>
        </div>
        <div class="news">
            @if(isset($list_news) && count($list_news) > 0)
                <div class="news_list">
                    @foreach ($list_news as $value)
                        @include('web.layouts.news.data_news') 
                    @endforeach 
                </div>
            @endif
        </div>

    </div>
</div>  --}}



@if(isset($session_modal) && $session_modal == 1)




@else




@endif
	
@endsection

