@extends('web.layouts.app')

@section('head')
<link rel="stylesheet" href="/assets/css/libs/jssocials.css">
<link rel="stylesheet" href="/assets/css/css-content.css">
<link rel="stylesheet" href="/assets/css/pages/news_detail.css">
@endsection
@section('content')
<div class="content default product_top">
    <div class="wrap">
        <div class="news" id="new_category">
            <div class="news_left">
				<h1 class="news-content-detail-title">{{$page->name}}</h1>
				{{-- <div class="time-and-category">
					<p class="time">{{$page->updated_at->format('H:i d/m/Y')}}</p>
				</div> --}}
				@if($page->detail)
				<div class="detail css-content">
					{!!replace_detail($page->detail)!!}
				</div>
				@endif
			</div>
			<div class="news_right">
                @include('web.layouts.detail_right_news')
            </div>
		</div>
	</div>
</div>

@if ($is_mobile)
    @include('web.layouts.customer_mobile') 
@endif

@endsection