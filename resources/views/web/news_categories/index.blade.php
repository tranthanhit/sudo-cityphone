@extends('web.layouts.app')
@section('head')
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "WebSite",
        "name": "{{config('app.name')}}",
        "url": "{{config('app.url')}}"
    }, {
        "@context": "http://schema.org",
        "@type": "Organization",
        "url": "{{config('app.url')}}",
        "@id": "{{config('app.url')}}/#organization",
        "name": "Cityphone điện thoại xách tay iPhone, Sony, LG, HTC, SamSung",
        "logo": "{{config('app.url')}}/assets/img/logo.png"
    }
</script>
@endsection
@section('content')
<div class="content default product_top">
    <div class="wrap">
        <div class="news" id="new_category">
            <div class="news_left">
                <div class="content_title">
                    <h4 id="title_content">{{$category->name}}</h4>
				</div>
				@if(isset($news) && count($news) > 0)
					<div class="news_left_hot">
						@foreach($news as $key=>$v1)
							@if($key == 0)
								<div class="list" data-aos="fade-up" data-aos-duration="1000">
									<a href="{{ $v1->getUrl() }}">
										<div class="img">
											<img class="lazy" src="{{ $v1->getImage('large') }}" data-original="{{$v1->getImage('large')}}" alt="{{$v1->slug}}">
										</div>
										<div class="infor">
											<p id="time">
												<span><i class="fa fa-calendar"></i></span>
												<span>
													{{ date('d/m/Y',strtotime($v1->created_at)) }} | {{ date('h:s A',strtotime($v1->created_at)) }}
												</span>
											</p>
											<h3 id="title">{{ $v1->name }}</h3>
											<p id="detail">
												{!!cutString(removeHTML($v1->detail), 300)!!}
											</p>
										</div>
									</a>
								</div>
							@endif
						@endforeach
						<div class="list">
							@foreach($news as $key=>$v2)
								@if($key != 0 && $key <=2)
									<div class="item" data-aos="fade-up" data-aos-duration="1000">
										<a href="{{ $v2->getUrl() }}">
											<div class="img">
												<img class="lazy" src="{{ $v2->getImage('medium') }}" data-original="{{$v2->getImage('medium')}}" alt="{{$v2->slug}}">
											</div>

											<h3 id="title">{{ $v2->name }}</h3>

										</a>
									</div>
								@endif
							@endforeach
						</div>
					</div>

					<div class="news_left_all product_top">
						@foreach($news as $key=>$v3)
							@if($key >= 3)
								<div class="item" data-aos="fade-up" data-aos-duration="1000">
									<a href="{{ $v3->getUrl() }}">
										<div class="img">
											<img class="lazy" src="{{ $v3->getImage('medium') }}" data-original="{{$v3->getImage('medium')}}" alt="{{$v3->slug}}">
										</div>
										<div class="infor">
											<p id="time">
												<span><i class="fa fa-calendar"></i></span>
												<span>
													{{ date('d/m/Y',strtotime($v3->created_at)) }} | {{ date('h:s A',strtotime($v3->created_at)) }}
												</span>
											</p>
											<h3 id="title">{{ $v3->name }}</h3>
											<p id="detail">
												{!!cutString(removeHTML($v3->detail), 300)!!}
											</p>
										</div>
									</a>
								</div>
							@endif
						@endforeach
						
					</div>
					
					<div class="pagination-box" id="pagination-box">
						{!! $news->links() !!}
					</div>
				@endif
            </div>
            <div class="news_right">
                @include('web.layouts.detail_right_news')
            </div>
        </div>

    </div>
</div>

@if ($is_mobile)
    @include('web.layouts.customer_mobile') 
@endif

@endsection