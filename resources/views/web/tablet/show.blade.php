@extends('web.layouts.app')
@section('head')
@if(strpos($_SERVER['HTTP_USER_AGENT'], 'coc_coc_browser') === false)
<script type="application/ld+json">
{
    "@graph": [{
            "@context": "http://schema.org/",
            "@type": "Product",
            "sku": "{{$that_tablet->id}}",
            "id": "{{$that_tablet->id}}",
            "mpn": "Cityphone",
            "name": "{{$that_tablet->name}}",
            "description": "{{$meta_seo['description'] == '' ? cutString(removeHTML($that_tablet->detail),150) : $meta_seo['description']}}",
            "image": "{{$that_tablet->getImage()}}",
            "brand": "{{$that_category->name}}",
            "aggregateRating": {
                "@type": "AggregateRating",
                "ratingValue": "{{$rank_peren}}",
                "reviewCount": "{{($rank_count==0)?1:$rank_count}}"
            },
            "review": {
				"@type": "Review",
				"author": "Tran Le Hai",
				"reviewRating": {
					"@type": "Rating",
					"bestRating": "5",
					"ratingValue": "1",
					"worstRating": "1"
				}
			},
            "offers": {
                "@type": "AggregateOffer",
                "priceCurrency": "VND",
                "offerCount": 10,
                "price": "{{(isset($that_tablet->price))?get_price_data_str($that_tablet->price):0}}",
        		"lowPrice":"{{(isset($that_tablet->price))?get_price_data_str($that_tablet->price):0}}",
				"highPrice":"{{(isset($that_tablet->price_old))?$that_tablet->price_old:0}}",
				"priceValidUntil": "2019-12-31",
                "availability": "http://schema.org/InStock",
                "warranty": {
                    "@type": "WarrantyPromise",
                    "durationOfWarranty": {
                        "@type": "QuantitativeValue",
                        "value": "6 tháng",
                        "unitCode": "ANN"
                    }
                },
                "itemCondition": "mới",
                "seller": {
                    "@type": "Organization",
                    "name": "{{config('app.name')}}"
                }
            }
        },
        {
            "@context": "http://schema.org",
            "@type": "WebSite",
            "name": "{{config('app.name')}}",
            "url": "{{config('app.url')}}"
        }
    ]
}
</script>
@endif
@endsection
@section('content')


<div class="service_detail default">
    <div class="wrap">
        <div class="title">

            <h1>{{$that_tablet->name}}</h1>

            <div class="show-rate">
                <span>5.0/5</span>
                <ul>
                    <li><i class="fa fa-star star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star star" aria-hidden="true"></i></li>
                </ul>
                <a href="javascript:;" class="scroll_cmt"><span>(1 đánh giá)</span></a>
            </div>
            <div class="share_facebook">

            </div>
        </div>
        <div class="content_service">
            <div class="service_detail_left">
                <div class="detail_slide">
                    <div class="slide">
                        <div class="owl-carousel" data-slider-id="3">
                            <div class="mySlides">
                                <img class="lazy" src="{{ $that_tablet->getImage('large') }}" data-original="{{$that_tablet->getImage('large')}}" alt="{{$that_tablet->slug}}">
                            </div>
                        </div>
                        <div class="slide_list owl-thumbs" data-slider-id="3">
                            <span class="owl-thumb-item"> <img class="lazy" src="{{ $that_tablet->getImage('large') }}" data-original="{{$that_tablet->getImage('large')}}" alt="{{$that_tablet->slug}}"></span>
                        </div>
                    </div>
                    <div class="detail">
                        <div id="price">
                            <strong>{{ $that_tablet->getPrice() }}</strong>
                            <del>{{ $that_tablet->price_old }}</del>
                        </div>
                        <p id="descript">{!! $that_tablet->info !!} </p>
                        <div class="promotion">
                            <p class="title">
                                <strong>Khuyến mãi độc quyền</strong>
                            </p>
                            <div class="info_promotion">
                                <ul>
                                    @foreach($promotion as $info)
										@if($info != "")
											<li><i class="fa fa-circle" aria-hidden="true"></i> &nbsp;{!! $info !!}
											</li>
										@endif
									@endforeach
                                </ul>
                            </div>
                        </div>
                        <p class="order">
                            <a href="javascript:;" data-id="{!! $that_tablet->id !!}" class="add_to_cart" data-type="tablets" data-token="{!! csrf_token() !!}">Đặt hàng</a>
                            <a href="">Liên hệ</a>
                        </p>
                        <a class="advisory" href="tel:19006388">
                            <img src="assets/images/icon/phone.png" alt="">
                            <span>Tư vấn: 19006388 - 19005521(7h - 22h30)</span>
                        </a>
                        <ul class="service">
                            <li><i class="fa fa-check-square-o"></i> Nguon gốc rõ ràng</li>
                            <li><i class="fa fa-check-square-o"></i> Nguon gốc rõ ràng</li>
                            <li><i class="fa fa-check-square-o"></i> Nguon gốc rõ ràng</li>
                            <li><i class="fa fa-check-square-o"></i> Nguon gốc rõ ràng</li>
                            <li><i class="fa fa-check-square-o"></i> Nguon gốc rõ ràng</li>
                            <li><i class="fa fa-check-square-o"></i> Nguon gốc rõ ràng</li>
                        </ul>
                    </div>
                </div>
				@if (isset($that_tablet->option) && $that_tablet->option!=null)
				@php
					$option = json_decode(base64_decode($that_tablet->option));
				@endphp
				<div class="price_list">
					<h4 id="title">Bảng giá {{ $that_tablet->name }}</h4>
					<table>
						<thead>
						<tr>
							<th>Tên</th>
							<th>Giá</th>
							<th>Bảo hành</th>
						</tr>
						</thead>
						<tbody>
							
							@if(isset($option))
								@foreach ($option as $value)
									<tr>
										<td>{{$value->name}}</td>
										<td>{{format_price(number_in_string(api_custom_price($value->price)))}}</td>
										<td>{{$value->warranty}}</td>
									</tr>
								@endforeach
							@endif
						</tbody>
					</table>
					<p>Rất nhiều máy chỉ bị hư mặt kính không cần thay thế màn hình. ĐRất nhiều máy chỉ bị hư mặt kính
						không cần thay thế màn hình. ĐRất nhiều máy chỉ bị hư mặt kính không cần thay thế màn hình.
						Đ</p>
				</div>
			@endif
                <div class="specifications">
                    <div class="content_title">
                        <h4 id="title_content">thông số kỹ thuật</h4>
                    </div>
                    <div class="css-content" id="specifications_detail">
                       {!! $that_tablet->detail !!}
                    </div>
                    <div class="product-detail-show">
                        <button class="product-detail-view product-detail-viewall">
                            Đọc thêm chi tiết <i class="fa fa-long-arrow-down"></i>
                        </button>
                        <button class="product-detail-view product-detail-viewdefault">
                            Ẩn bớt chi tiết <i class="fa fa-long-arrow-up"></i>
                        </button>
                    </div>
                </div>
                <div class="m_detail">
                    <div class="product">
                        <a href="">
                            <div class="img">
								<img class="lazy" src="{{ $that_tablet->getImage('medium') }}" data-original="{{$that_tablet->getImage('medium')}}" alt="{{$that_tablet->slug}}">
                            </div>
                            <div class="infor">
                                <h3 id="title">{{ $that_tablet->name }}</h3>
                                <div id="price">
                                    <strong>{{ $that_tablet->getPrice() }}</strong>
                                    <del>{{ $that_tablet->price_old }}</del>
                                </div>
                            </div>
                        </a>
                        <div class="order">
                            <p id="order">
                                <a href="javascript:;" data-id="{!! $that_tablet->id !!}" class="add_to_cart" data-type="tablets" data-token="{!! csrf_token() !!}">Đặt hàng</a>
                                <a href="">Liên hệ </a>
                            </p>
                            <a id="advisory" href="tel:19006388">
                                <img src="assets/images/icon/phone.png" alt="">
                                <span>Tư vấn: 19006388 - 19005521(7h - 22h30)</span>
                            </a>
                        </div>
                    </div>
                </div>
                {{-- <p id="p_admin"><i>Đăng bơi <b>Quản trị viên</b></i> lúc 04:5 06/07/2020</p> --}}
                @include('web.layouts.star')
                <div id="detail-comment-main">
                    @php
                        $type_id = $that_tablet->id;
                        $type = 'tablets';
                    @endphp
                    @include('web.comments.comments',compact('type_id','type'))
                    <div class="clear"></div>
                </div>
            </div>
            <div class="news_right">
               @include('web.layouts.detail_right')
            </div>
        </div>
    </div>
</div>
@include('web.layouts.form')

@endsection