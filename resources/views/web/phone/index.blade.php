@extends('web.layouts.app')
@section('head')
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "WebSite",
        "name": "{{config('app.name')}}",
        "url": "{{config('app.url')}}"
    }, {
        "@context": "http://schema.org",
        "@type": "Organization",
        "url": "{{config('app.url')}}",
        "@id": "{{config('app.url')}}/#organization",
        "name": "Cityphone điện thoại xách tay iPhone, Sony, LG, HTC, SamSung",
        "logo": "{{config('app.url')}}/assets/img/logo.png"
    }
</script>
@endsection
@section('content')

<div class="service default">
    <div class="wrap">
        <div class="service_left">
            <div class="menu_item">
                <h4 id="title">Danh mục điện thoại</h4>
                <ul>
					@if(isset($phone_categories) && count($phone_categories)>0)
						@foreach($phone_categories as $value)
							<li><a href="{{ $value->getUrl() }}">{{ $value->name }}</a></li>
						@endforeach
					@endif
                </ul>
            </div>
            <div class="menu_item product_top" >
                <h4 id="title">Tra theo hãng</h4>
                <ul>
                    <li><a href="">Apple</a></li>
                    <li><a href="">Apple</a></li>
                    <li><a href="">Apple</a></li>
                    <li><a href="">Apple</a></li>
                    <li><a href="">Apple</a></li>
                    <li><a href="">Apple</a></li>
                    <li><a href="">Apple</a></li>
                    <li><a href="">Apple</a></li>
                </ul>
            </div>
        </div>
        <div class="service_right">
                @if(isset($list_phones) && count($list_phones)>0)
				<div class="content_list">
					@foreach($list_phones as $value)
						<div class="content_list_accessories" id="list_product">
							<a href="{{ $value->getUrl() }}">
								<div class="img">
									<img class="lazy" src="{{ $value->getImage('medium') }}" data-original="{{$value->getImage('medium')}}" alt="{{$value->slug}}">
								</div>
								<div class="infor">
									<h3 id="title">{{ $value->name }}</h3>
									<p id="price">
										<strong>{{ $value->getPrice() }}</strong>
									</p>
								</div>
							</a>
						</div>
					@endforeach
				</div>
			@endif  
        </div>
    </div>
</div>



@endsection
