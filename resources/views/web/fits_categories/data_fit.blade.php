@foreach($fits as $value)
    <div class="content_list_accessories" id="list_product" data-aos="fade-up" data-aos-duration="1000">
        <a href="{{ $value->getUrl() }}">
            <div class="img">
                <img class="lazy" src="{{ $value->getImage('medium') }}" data-original="{{$value->getImage('medium')}}" alt="{{$value->slug}}">
            </div>
            <div class="infor">
                <h3 id="title">{{ $value->name }}</h3>
                <p id="price">
                    <strong>{{ $value->getPrice() }}</strong>
                </p>
            </div>
        </a>
    </div>
@endforeach