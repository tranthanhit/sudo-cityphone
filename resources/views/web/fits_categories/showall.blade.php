
@extends('web.layouts.app')
@section('head')
@endsection
@section('content')
<div class="service default product_top product_bottom">
    <div class="wrap">
        <div class="service_left">
            <div class="menu_item">
                <h4 id="title">Linh kiện & phụ kiện</h4>
                <ul>
					@if(isset($fit_categories) && count($fit_categories))
						@foreach($fit_categories as $value)
							<li><a href="{{ $value->getUrl() }}">{{ $value->name }}</a></li>
						@endforeach
					@endif
                </ul>
            </div>
        </div>
        <div class="service_right view-more-fit-service">

			<div class="service_right_filter">
                <h1>Phụ kiện</h1>
                <label class="price @if($filter_price == '200.000') active @endif "><a href="?filter_price=200.000">Dưới 200.000đ</a></label>
                <label class="price @if($filter_price == '200.000vs500.000') active @endif "><a href="?filter_price=200.000vs500.000">Từ 200.000đ - 500.000đ</a></label>
                <label class="price @if($filter_price == '500.000vs1000.000') active @endif"><a href="?filter_price=500.000vs1000.000">Từ 500.000đ - 1 triệu</a></label>
                <label class="price @if($filter_price == '1000.000') active @endif"><a href="?filter_price=1000.000">Trên 1 triệu</a></label>
                <div class="priceprice">
                    <span>Sắp xếp</span>
                    <div class="listprice">
                        <button type="button" class="closefilter"><i class="iconacc-close"></i></button>
                        <div class="title">Sắp xếp theo</div>
                        <label class="order @if($sort == 'default') check @endif "><a href="?sort=default">Mới nhất</a></label>
                        <label class="order @if($sort == 'asc') check @endif  "><a href="?sort=asc">Giá thấp đến cao</a></label>
                        <label class="order  @if($sort == 'desc') check @endif  "><a href="?sort=desc">Giá cao đến thấp</a></label>
                    </div>
                </div>
            </div>

			@if(isset($fits) && count($fits)>0)
				<div class="content_list">
					@include('web.fits_categories.data_fit')
                </div>
            @endif
            @if($fits->count()>10)
                <div class="clear"></div>
                <div class="viewmore" id="viewmore">
                    <button class="view-product-fits-service" href="javascript:;" data-filter_price="{{ @$_GET['filter_price'] }}" data-sort="{{ @$_GET['sort'] }}" data-cate_id="" data-type="fits"  data-count="{{$fits->count()}}">Xem thêm Phụ kiện</button>
                </div>
            @endif
            @if($config_seo_category['title_fit'] != '')
                <div class="seo_category" style="padding: 10px;">
                    <div class="">
                        <h1 id="" style="color: #333;font-weight: unset"> {!! @$config_seo_category['title_fit'] !!}</h1>
                    </div>
                    <div class="css-content">
                        {!! @$config_seo_category['detail_fit'] !!}
                    </div>
                    <a href="javascript:;" class="show-seo-category" data-status="hide" data-up="Thu gọn ▲">Xem thêm ▼</a>
                </div>
            @endif
        </div>
    </div>
</div>

@if ($is_mobile)
    @include('web.layouts.customer_mobile') 
@endif

@endsection