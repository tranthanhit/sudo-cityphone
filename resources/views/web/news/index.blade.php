@extends('web.layouts.app')
@section('head')
    <script type="application/ld+json">
    {
        "@graph": [{
                "@context": "http://schema.org",
                "@type": "NewsArticle",
                "mainEntityOfPage": {
                    "@type": "WebPage",
                    "@id": "{{$news->getUrl()}}"
                },
                "headline": "{{$news->name}}",
                "image": {
                    "@type": "ImageObject",
                    "url": "{{$news->getImage()}}",
                    "height": 800,
                    "width": 800
                },
                // "aggregateRating": {
                //     "@type": "AggregateRating",
	               //  "ratingValue": "{{$rank_peren}}",
	               //  "reviewCount": "{{$rank_count}}"
                // },
                "datePublished": "{{date('Y/m/d',strtotime($news->created_at))}}",
                "dateModified": "{{date('Y/m/d',strtotime($news->updated_at))}}",
                "author": {
                    "@type": "Person",
                    "name": "{{$admin_name??'Cityphone'}}"
                },
                "publisher": {
                    "@type": "Organization",
                    "name": "{{config('app.name')}}",
                    "logo": {
                        "@type": "ImageObject",
                        "url": "https://Cityphone.vn/themes/img/logo.png",
                        "width": 600,
                        "height": 60
                    }
                },
                "description": "{{$meta_seo['description'] == '' ? cutString(removeHTML($news->detail),150) : $meta_seo['description']}}"
            }, {
                "@context": "http://schema.org",
                "@type": "WebSite",
                "name": "{{config('app.name')}}",
                "url": "{{config('app.url')}}"
            },
            {
                "@context": "http://schema.org",
                "@type": "Organization",
                "url": "{{config('app.url')}}",
                "@id": "{{config('app.url')}}/#organization",
                "name": "Cityphone điện thoại xách tay iPhone, Sony, LG, HTC, SamSung",
                "logo": "{{config('app.url')}}/assets/img/logo.png"
            }
        ]
    }
    </script>
@endsection
@section('content')
<div class="content default product_top">
    <div class="wrap">
        <div class="news" id="new_category">
            <div class="news_left">
               <h1>{{$news->name}}</h1>
                <p style="margin: 18px 0;">
                    <span><i class="fa fa-calendar"></i></span>
                    <span>
                        {{ date('d/m/Y',strtotime($news->created_at)) }} | {{ date('h:s A',strtotime($news->created_at)) }}
                    </span>
                </p>
               <div class="css-content">
                    {!! replace_detail($news->detail) !!}
               </div>
            </div>
            <div class="news_right">
                @include('web.layouts.detail_right_news')
            </div>
        </div>

    </div>
</div>

<div class="content default product_top">
    <div class="wrap">
        <div class="content_title" style="border-bottom: 0">
            <h4 id="title_content">Bình luận</h4>
        </div>
        <div class="news">
            <div id="fb-root"></div>
            <script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v6.0&appId=535054533725628&autoLogAppEvents=1"></script>
            <div class="fb-comments" data-href="{{ $news->getUrl() }}" data-width="100%"  data-numposts="5"></div>
        </div>
    </div>
</div>

<div class="content default product_top">
    <div class="wrap">
        <div class="content_title" style="border-bottom: 0">
            <h4 id="title_content">tin tức liên quan</h4>
            <a href="{!! $new_category->getUrl() !!}" id="view_all">Xem tất cả <i class="fa fa-angle-double-right"></i></a>
        </div>
        <div class="news product_top">
            @if(isset($news_related) && count($news_related) > 0)
                <div class="news_list">
                    @foreach ($news_related as $value)
                        @include('web.layouts.news.data_news') 
                    @endforeach
                </div>
            @endif
        </div>
    </div>
</div>

@if ($is_mobile)
    @include('web.layouts.customer_mobile') 
@endif

@endsection