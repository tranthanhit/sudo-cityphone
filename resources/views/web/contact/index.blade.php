@extends('web.layouts.app')
@section('content')
    <div class="contact default product_top">
        <div class="wrap">
            <h1 id="title" style="margin-bottom: 20px">City phone</h1>
            <div class="contact_list  product_bottom">
                <div class="left" @if(!$is_mobile) style=" height: 320px;overflow-y: auto;" @endif>
                    @foreach($locations as $location)
                        <h3 id="title">{{$location->name}}</h3>
                        @foreach($address as $locate)
                            @if($locate->location_id == $location->id)
                                <div class="item">
                                    <p id="adrress"><i class="fa fa-map-marker"></i>{{ $locate->name }}</p>
                                    <p id="phone">
                                        <a href="tel:{{$locate->mobile}}"><i class="fa fa-phone"></i> {{$locate->mobile}}</a>
                                        <a href="{{ $locate->map }}" target="_blank"><i class="fa fa-location-arrow"></i> Vị trí bản đồ
                                        </a>
                                    </p>
                                </div>
                            @endif
                        @endforeach
                    @endforeach
                    
                </div>
                <div class="right">
                    <div class="form_contact">
                        <form id="form_contact" action="" method="POST" role="form">
                            {{ csrf_field() }} 
                            <div class="form-group">
                                <input name="title" type="text" class="form-control title_contact" id="name_contact"
                                       placeholder="Họ và tên (*)">
                            </div>
                            <div class="form-group">
                                <input name="address_contact" type="text" class="form-control address_contact" id="address_contact"
                                       placeholder="Địa chỉ" required="">
                            </div>
                            <div class="form-group" id="email_phone">
                                <input name="email" type="email" class="form-control email_contact" id="email_contact"
                                       placeholder="Email (*)" required="">
                                <input name="phone" type="number" class="form-control phone_contact" id="phone_contact"
                                       placeholder="Số điện thoại (*)" required="">
                            </div>
                            <div class="form-group">
                                <textarea name="infor_contact" id="infor_contact" placeholder="Nội dung" cols="30" rows="10"></textarea>
                            </div>
                            <button type="button" id="contact_buttom" class="contact_buttom" name="contact_buttom" value="contact_buttom"> Gửi thông tin</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="map_contact">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3723.868725715509!2d105.78807131476341!3d21.03793798599343!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab36485b34a1%3A0x78b520fad0e62565!2zOTkgTmd1eeG7hW4gUGhvbmcgU-G6r2MsIEThu4tjaCBW4buNbmcsIEPhuqd1IEdp4bqleSwgSMOgIE7hu5lpLCBWaeG7h3QgTmFt!5e0!3m2!1svi!2s!4v1584328678657!5m2!1svi!2s" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            </div>
        </div>
    </div>

    
@if ($is_mobile)
@include('web.layouts.customer_mobile') 
@endif
@endsection