@extends('web.layouts.app')
@section('head')
<link rel="stylesheet" href="/assets/css/css-content.css">

<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "WebSite",
        "name": "{{config('app.name')}}",
        "url": "{{config('app.url')}}"
    }, {
        "@context": "http://schema.org",
        "@type": "Organization",
        "url": "{{config('app.url')}}",
        "@id": "{{config('app.url')}}/#organization",
        "name": "Cityphone điện thoại xách tay iPhone, Sony, LG, HTC, SamSung",
        "logo": "{{config('app.url')}}/assets/img/logo.png"
    }
</script>
@endsection
@section('content')
<section class="container">
	<div id="rss_home" class="css-content">
        <h1 style="color:#c69a39;font-size: 24px;text-align: center;margin: 20px 0;">Cityphone và các kênh tin RSS Feeds</h1>
        <hr>
        <h2>Khái niệm RSS</h2>
        <p>RSS ( viết tắt từ Really Simple Syndication ) là một tiêu chuẩn định dạng tài liệu dựa trên XML nhằm giúp người sử dụng dễ dàng cập nhật và tra cứu thông tin một cách nhanh chóng và thuận tiện nhất bằng cách tóm lược thông tin vào trong một đoạn dữ liệu ngắn gọn, hợp chuẩn.
        Dữ liệu này được các chương trình đọc tin chuyên biệt ( gọi là News reader) phân tích và hiển thị trên máy tính của người sử dụng. Trên trình đọc tin này, người sử dụng có thể thấy những tin chính mới nhất, tiêu đề, tóm tắt và cả đường link để xem toàn bộ tin.</p>
        <h2>Thông tin và điều khoản sử dụng</h2>        
        <p>Cityphone hiện tại cung cấp các nguồn kênh dữ liệu dưới đây theo định dạng chuẩn mới nhất RSS 2.0. Các nguồn kênh này là miễn phí cho việc sử dụng dưới mục đích cá nhân và phi lợi nhuận. Bạn chỉ việc copy và dán các địa chỉ URL này vào những trang web hoặc phần mềm hỗ trợ đọc tin từ RSS Feeds hoặc kéo thả biểu tượng RSS dưới đây vào các phần mềm hỗ trợ RSS là được.</p>
        <h2>Danh mục tin RSS mà Cityphone cung cấp</h2>
        <ul>
            <li><a href="{{route('rss.phone')}}"><i class="fa fa-rss" aria-hidden="true"></i> Điện thoại</a></li>
            <li><a href="{{route('rss.tablet')}}"><i class="fa fa-rss" aria-hidden="true"></i> Máy tính bảng</a></li>
            <li><a href="{{route('rss.fit')}}"><i class="fa fa-rss" aria-hidden="true"></i> Phụ kiện</a></li>
            <li><a href="{{route('rss.service')}}"><i class="fa fa-rss" aria-hidden="true"></i> Sửa chữa</a></li>
            <li><a href="{{route('rss.news')}}"><i class="fa fa-rss" aria-hidden="true"></i> Tin tức</a></li>
        </ul>
    </div>
</section>
@endsection