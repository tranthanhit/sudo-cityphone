@extends('web.layouts.app')

@section('head')
<link href="/assets/css/comments.min.css" rel="stylesheet">

<link rel="stylesheet" href="/assets/libs/slick/slick.min.css">
<link rel="stylesheet" href="/assets/libs/slick/slick-theme.min.css">

<link rel="stylesheet" type="text/css" href="/assets/libs/fancybox/jquery.fancybox.min.css">
<script src="/assets/libs/slick/slick.min.js"></script>
@if(strpos($_SERVER['HTTP_USER_AGENT'], 'coc_coc_browser') === false)
<script type="application/ld+json">
{
    "@graph": [{
            "@context": "http://schema.org/",
            "@type": "Product",
            "sku": "{{$fit->id}}",
            "id": "{{$fit->id}}",
            "mpn": "Cityphone",
            "name": "{{$fit->name}}",
            "description": "{{$meta_seo['description'] == '' ? cutString(removeHTML($fit->detail),150) : $meta_seo['description']}}",
            "image": "{{$fit->getImage()}}",
            "brand": "{{$fit->name}}",
            "aggregateRating": {
                "@type": "AggregateRating",
                "ratingValue": "{{$rank_peren}}",
                "reviewCount": "{{($rank_count==0)?1:$rank_count}}"
            },
            "review": {
				"@type": "Review",
				"author": "Tran Le Hai",
				"reviewRating": {
					"@type": "Rating",
					"bestRating": "5",
					"ratingValue": "1",
					"worstRating": "1"
				}
			},
            "offers": {
                "@type": "Offer",
                "priceCurrency": "VND",
                "price": "{{number_format(number_in_string(api_custom_price($fit->price)),0,',','')??0}}",
                "priceValidUntil": "2019-12-31",
                "availability": "http://schema.org/InStock",
                "url": "{{route('web.fits.show',$fit->slug)}}",
                "warranty": {
                    "@type": "WarrantyPromise",
                    "durationOfWarranty": {
                        "@type": "QuantitativeValue",
                        "value": "6 tháng",
                        "unitCode": "ANN"
                    }
                },
                "itemCondition": "mới",
                "seller": {
                    "@type": "Organization",
                    "name": "Cityphone"
                }
            }
        },
        {
            "@context": "http://schema.org",
            "@type": "WebSite",
            "name": "Cityphone",
            "url": "{{config('app.url')}}"
        }
    ]
}
</script>
@endif
@endsection
@section('script')
<script src="{{ asset('assets/js/slick.min.js') }}"></script>
<script type="text/javascript" src="/assets/libs/fancybox/jquery.fancybox.pack.min.js"></script>
@endsection
@section('content')

<div class="service_detail default">
    <div class="wrap">
        <div class="title">

            <h1>{{ $fit->name }}</h1>
            @php
                $avg = round($rating_star->avg * 2) / 2;
            @endphp
            <div class="show-rate">
               
                <ul>
                    @for($i = 0; $i < 5; $i++)
                        @if($i < $avg && $i + 0.5 == $avg)
                            <li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
                        @elseif($i < $avg)
                        <li> <i class="fa fa-star" aria-hidden="true"></i></li>
                        @else
                        <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                        @endif
                    @endfor
                </ul>
                <span>{{ round($rating_star->avg,1) }} / ({{ $rating_star->count }} đánh giá)</span>
                {{--  <a href="javascript:;" class="scroll_cmt"><span>({{ $rating_star->count }} đánh giá)</span></a>  --}}
            </div>
            <div class="share_facebook">
                <div id="fb-root"></div>
                <script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v6.0&appId=535054533725628&autoLogAppEvents=1"></script>
                <div class="fb-like" data-href="{{ $fit->getUrl() }}" data-width="" data-layout="button_count" data-action="like" data-size="small" data-share="true"></div>
            </div>
        </div>
        <div class="content_service">
            <div class="service_detail_left">
                <div class="detail_slide">
                    <div class="slide">
                        <div class="detailProduct-left__top" >
                            <img class="lazy" src="{{ $fit->getImage('large') }}" class="cloudzoom" data-zoom="{{ @$fit->image }}" data-original="{{$fit->getImage('large')}}" alt="{{$fit->slug}}">
                            @if($slides != '')
                                @foreach ($slides as $v)
                                    <img class="lazy" src="{{ $v }}" data-zoom="{{ @$v }}"  class="cloudzoom" data-original="{{$v}}" >
                                @endforeach
                            @endif
                        </div>
                        <div class="detailProduct-left__bottom slide_list">
                            <img class="lazy" src="{{ $fit->getImage('small') }}"  data-original="{{$fit->getImage('small')}}" alt="{{$fit->slug}}">
                            @if($slides != '')
                                @foreach ($slides as $v1)
                                    <img class="lazy" src="{{ $v1 }}" data-original="{{$v1}}">
                                @endforeach
                            @endif
                        </div>
                    </div>
                    <div class="detail">
                        <div id="price">
                            <strong>{{ $fit->getPrice() }}</strong>
                             <del>{!! $fit->getPriceOld() !!}</del> 
                        </div>
						<p id="descript">{!! $fit->info !!}</p>
						@if(isset($promotion) && empty($promotion))
							<div class="promotion">
								<p class="title">
									<strong>Khuyến mãi độc quyền</strong>
								</p>
								<div class="info_promotion">
									<ul>
										@foreach($promotion as $info)
											@if($info != "")
												<li><i>{{ $key+1 }}</i> &nbsp;{!! $info !!}
												</li>
											@endif
										@endforeach
									</ul>
								</div>
							</div>
						@endif
                        <p class="order">

                            <div class="order">
                                <div href="javascript:;" data-id="{!! $fit->id !!}" data-type="fits" class="add_to_cart item" data-token="{!! csrf_token() !!}">
                                    <i class="fa fa-shopping-cart"></i> 
                                   <ul>
                                        <li> Đặt hàng</li>
                                        <li>(Không chờ đợi)</li>
                                   </ul>
                                </div>
                                <div href="{{ route('web.contact.index') }}" class="item">
                                    <img src="/assets/images/icon/facebook-messenger-brands.png" alt="">
                                    <ul>
                                        <li><a href="{!! @$config_general['messaging_via_facebook'] !!}"> Nhắn tin qua facebook</a></li>
                                        <li><a href="{!! @$config_general['messaging_via_facebook'] !!}">(Hỗ trợ 247)</a></li>
                                    </ul>
                                </div>
                            </div>

                            {{--  <a href="javascript:;" data-id="{!! $fit->id !!}" class="add_to_cart" data-type="fits" data-token="{!! csrf_token() !!}">Đặt hàng</a>
                            <a href="{{ route('web.contact.index') }}">Liên hệ</a>  --}}
                        </p>
                        <div class="advisory" >
                            <span>Gọi <a href="tel:{!! @$config_general['phone_destop_tu_van'] !!} ">{!! @$config_general['phone_destop_tu_van'] !!}</a> để được tư vấn , hỗ trợ </span>
                        </div>
                        @php
                            $infor = preg_split('/\n|\r\n/',$fit->infor);
                        @endphp
                        @if($fit->infor != null)
                            <ul class="service">
                                @foreach ($infor as $key=>$item)
                                    <li><i class="fa fa-check-square-o"></i>{!! $item !!}</li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
				</div>
				@if (isset($fit->option) && $fit->option!=null)
					@php
						$option = json_decode(base64_decode($fit->option));
					@endphp
					<div class="price_list">
                        <div class="content_title">
                            <h4 id="title_content">Bảng giá {{ $fit->name }}</h4>
                        </div>

                        <table id="price_list_home" style="width: 100%;">
                            <tbody>
                                <tr>
                                    <td style="width: 153px;"><strong>Tên</strong></td>
                                    <td style="width: 186px;"><strong>Giá</strong></td>
                                    <td style="width: 169px;"><strong>Bảo hành</strong></td>
                                </tr>
                                @if(isset($option))
                                    @foreach ($option as $value)
                                        <tr>
                                            <td style="width: 153px;">{{$value->name}}</td>
                                            <td style="width: 186px;">{{format_price(number_in_string(api_custom_price($value->price)))}}</td>
                                            <td style="width: 169px;">{{$value->warranty}}</td>
                                        </tr>
                                    @endforeach
								@endif
                            </tbody>
                        </table>

						
					</div>
				@endif
                <div class="specifications">
                    @if($fit->detail != '')
                        <div class="content_title">
                            <h4 id="title_content">Thông số kỹ thuật</h4>
                        </div>
                        <div class="css-content" id="specifications_detail">
                            {!! $fit->detail !!}
                        </div>
                        <div class="product-detail-show">
                            <button class="product-detail-view product-detail-viewall">
                                Đọc thêm chi tiết <i class="fa fa-long-arrow-down"></i>
                            </button>
                            <button class="product-detail-view product-detail-viewdefault">
                                Ẩn bớt chi tiết <i class="fa fa-long-arrow-up"></i>
                            </button>
                        </div>
                    @endif
                </div>
            
                @php
                $type_id = $fit->id;
                $type = 'fits';
            @endphp
                @include('web.layouts.star',compact('type','type_id'))
                <div id="detail-comment-main">
                   
                    @include('web.comments.comments',compact('type_id','type'))
                    <div class="clear"></div>
                </div>
            </div>
            <div class="news_right">
                @include('web.layouts.detail_right')
            </div>
        </div>
    </div>
</div>
{{--  @include('web.layouts.form')  --}}



@if ($is_mobile)
<footer id="footer_infor_mobile" >
    <div class="wrap">
        <div class="footer-support ui-grid-a" style="float: left; width: 100% !important;">
            <div class="footer-support-item ui-block-a">
                <p>Tư vấn (24/7)</p>
                <a href="tel:{!! @$config_general['phone_mobile_1'] !!}"><i class="fa fa-phone"></i> {!! @$config_general['phone_mobile_1'] !!}</a>
            </div>
            <div class="footer-support-item ui-block-b">
                <p>Góp ý, phản ánh</p>
                <a href="tel:{!! @$config_general['phone_mobile_2'] !!}"><i class="fa fa-phone"></i> {!! @$config_general['phone_mobile_2'] !!}</a>
            </div>
            <div class="footer-support-item ui-block-a">
               <a href="javascript:;" id="other_information" style="color: #288ad6 !important;font-size: 16px !important;">Thông tin khác <i class="fa fa-angle-down" aria-hidden="true"></i></a>
            </div>
            <div class="footer-support-item ui-block-a">
                <a href="javascript:;" id="store_address" style="color: #288ad6 !important;font-size: 16px !important;">Địa chỉ của hàng <i class="fa fa-angle-down" aria-hidden="true"></i></a>
            </div>
        </div>
        <div class="m_footer_2_mobile product_top" style="display: none" >
            @if(isset($menu_primary))
                @foreach($menu_primary as $value)
                    <div class="item">
                        <h4 id="title">{{ $value->name }}</h4>
                        <ul>
                            @foreach($value->children as $children)
                                <li><a href="{{ $children->link }}">{{ $children->name }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                @endforeach
            @endif
        </div>
        <div class="m_address_2_mobile product_top" style="display: none" >
            @foreach($locations as $location)
                <h3 id="title">{{$location->name}}</h3>
                @foreach($address as $locate)
                    @if($locate->location_id == $location->id)
                        <div class="item">
                            <p id="adrress"><i class="fa fa-map-marker"></i>{{ $locate->name }}</p>
                            <p id="phone">
                                <a href="tel:{{$locate->mobile}}"><i class="fa fa-phone"></i> {{$locate->mobile}}</a>
                                <a href="{{ $locate->map }}" target="_blank"><i class="fa fa-location-arrow"></i> Vị trí bản đồ
                                </a>
                            </p>
                        </div>
                    @endif
                @endforeach
            @endforeach 
        </div>
    </div>
</footer>
@endif


@endsection
