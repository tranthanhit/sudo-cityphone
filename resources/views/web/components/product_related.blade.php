<div class="product-related-box">
	<div class="product-related-list">
		@foreach($value as $k => $v)
		@php
			$link = $v->getUrl();
		@endphp
		<div class="product-related-item">
			<div class="product-related-image">
				<a target="_blank" href="{{$link}}">
					@if($v->image)
						<img class="lazy" src="{{getImageDefault('load')}}" data-original="{{$v->getImage('small')}}" alt="{{getAlt($v->getImage('small'))}}">
					@else
						<img class="lazy" src="{{getImageDefault('load')}}" data-original="{{getImageDefault('')}}" alt="{{getAlt(getImageDefault(''))}}">
					@endif
				</a>
			</div>
			<div class="product-related-content">
				<p class="name"><a target="_blank" href="{{$link}}">{{$v->name}}</a></p>
				<p class="price">{{$v->getPrice()}}</p>
			</div>
		</div>
		@endforeach
		
	</div>
	@if($forum != null)
	<div class="product-support-box">
		<div class="product-support-title">Hỗ trợ phần mềm</div>
		<div class="product-support-content">
			<ul>
				@foreach($forum as $k => $v)
				<li><a href="{{$v->link}}">{{$v->title}}</a></li>
				@endforeach
			</ul>
		</div>
	</div>
	@endif
</div>