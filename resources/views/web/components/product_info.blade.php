<div class="product-info-box">
	<div class="product-info-title">Thông số kỹ thuật</div>
	<div class="product-info-content">
		<table>
			<tbody>
				<tr>
					<td>Màn hình:</td>
					<td>{{$value->display_standard}}</td>
				</tr>
				<tr>
					<td>Hệ điều hành:</td>
					<td>{{$value->general_os}}</td>
				</tr>
				<tr>
					<td>Camera sau:</td>
					<td>{{$value->cam_primary}} , {{$value->cam_filming}}</td>
				</tr>
				<tr>
					<td>Camera trước:</td>
					<td>{{$value->cam_second}}</td>
				</tr>
				<tr>
					<td>CPU:</td>
					<td>{{$value->cpuram_chipset}} , {{$value->cpuram_core}}</td>
				</tr>
				<tr>
					<td>RAM:</td>
					<td>{{$value->cpuram_ram}}</td>
				</tr>
				<tr>
					<td>Bộ nhớ trong:</td>
					<td>{{$value->storage_rom}}</td>
				</tr>
				<tr>
					<td>Thẻ SIM:</td>
					<td>{{$value->connect_duasim}} , {{$value->connect_simtype}}</td>
				</tr>
				<tr>
					<td>Dung lượng pin:</td>
					<td>{{$value->pin_capacity}}</td>
				</tr>
				<tr>
					<td>Thiết kế:</td>
					<td>{{$value->design_style}}</td>
				</tr>
			</tbody>
		</table>
		<span id="product-show-lightbox" class="show-lightbox-btn">Xem thêm cấu hình chi tiết</span>
	</div>
	<div class="product-info-lightbox lightbox">
		<span id="product-close-lightbox" class="close-lightbox-btn"><i class="fa fa-times" aria-hidden="true"></i></span>
		<marquee class="product-lightbox-title">
			<p>{{$value->name}}</p>
		</marquee>
		<div class="product-lightbox-content">
			<table>
				<tbody>
					<tr>
						<td colspan="2">Thông tin chung</td>
					</tr>
					<tr>
						<td>Hệ điều hành:</td>
						<td>{{$value->general_os}}</td>
					</tr>
					<tr>
						<td>Ngôn ngữ:</td>
						<td>{{$value->general_language}}</td>
					</tr>
					
					<tr >
						<td colspan="2">Màn hình</td>
					</tr>
					<tr>
						<td>Loại màn hình:</td>
						<td>{{$value->display_type}}</td>
					</tr>
					<tr>
						<td>Màu màn hình:</td>
						<td>{{$value->display_color}}</td>
					</tr>
					<tr>
						<td>Chuẩn màn hình:</td>
						<td>{{$value->display_standard}}</td>
					</tr>
					<tr>
						<td>Độ phân giải:</td>
						<td>{{$value->display_resolution}}</td>
					</tr>
					<tr>
						<td>Màn hình rộng:</td>
						<td>{{$value->display_width}}</td>
					</tr>
					<tr>
						<td>Công nghệ cảm ứng:</td>
						<td>{{$value->display_induction}}</td>
					</tr>
					<tr>
						<td colspan="2">Chụp hình &amp; Quay phim</td>
					</tr>
					<tr>
						<td>Camera sau:</td>
						<td>{{$value->cam_primary}}</td>
					</tr>
					<tr>
						<td>Camera trước:</td>
						<td>{{$value->cam_second}}</td>
					</tr>
					<tr>
						<td>Đèn Flash:</td>
						<td>{{($value->cam_flash == 1)?"Có":"Không"}}</td>
					</tr>
					<tr>
						<td>Tính năng camera:</td>
						<td>{{$value->cam_feature}}</td>
					</tr>
					<tr>
						<td>Quay phim:</td>
						<td>{{$value->cam_filming}}</td>
					</tr>
					<tr>
						<td>Videocall:</td>
						<td>{{($value->cam_videocall == 1)?"Có":"Không"}}</td>
					</tr>
					<tr>
						<td colspan="2">CPU &amp; RAM</td>
					</tr>
					<tr>
						<td>Tốc độ CPU:</td>
						<td>{{$value->cpuram_speed}}</td>
					</tr>
					<tr>
						<td>Số nhân:</td>
						<td>{{$value->cpuram_core}}</td>
					</tr>
					<tr>
						<td>Chipset:</td>
						<td>{{$value->cpuram_chipset}}</td>
					</tr>
					<tr>
						<td>RAM:</td>
						<td>{{$value->cpuram_ram}}</td>
					</tr>
					<tr>
						<td>Chip đồ họa (GPU):</td>
						<td>{{$value->cpuram_graphic}}</td>
					</tr>
					
					<tr>
						<td colspan="2">Bộ nhớ &amp; Lưu trữ</td>
					</tr>
					<tr>
						<td>Danh bạ:</td>
						<td>{{$value->storage_contacts}}</td>
					</tr>
					<tr>
						<td>Bộ nhớ trong (ROM):</td>
						<td>{{$value->storage_rom}}</td>
					</tr>
					<tr>
						<td>Thẻ nhớ ngoài:</td>
						<td>{{$value->storage_memorycard}}</td>
					</tr>
					<tr>
						<td>Hỗ trợ thẻ tối đa:</td>
						<td>{{$value->storage_memorycardsup}}</td>
					</tr>
					<tr>
						<td colspan="2">Thiết kế &amp; Trọng lượng</td>
					</tr>
					<tr>
						<td>Kiểu dáng:</td>
						<td>{{$value->design_style}}</td>
					</tr>
					<tr>
						<td>Kích thước:</td>
						<td>{{$value->design_size}}</td>
					</tr>
					<tr>
						<td>Trọng lượng (g):</td>
						<td>{{$value->design_weight}}</td>
					</tr>
					
					<tr>
						<td colspan="2">Thông tin pin</td>
					</tr>
					<tr>
						<td>Loại pin:</td>
						<td>{{$value->pin_type}}</td>
					</tr>
					<tr>
						<td>Dung lượng pin:</td>
						<td>{{$value->pin_capacity}}</td>
					</tr>
					<tr>
						<td>Pin có thể tháo rời:</td>
						<td>{{($value->pin_removable == 1)?"Có":"Không"}}</td>
					</tr>
					<tr>
						<td colspan="2">Kết nối &amp; Cổng giao tiếp</td>
					</tr>
					<tr>
						<td>3G:</td>
						<td>{{$value->connect_3g}}</td>
					</tr>
					<tr>
						<td>4G:</td>
						<td>{{$value->connect_4g}}</td>
					</tr>
					<tr>
						<td>Loại Sim:</td>
						<td>{{$value->connect_simtype}}</td>
					</tr>
					<tr>
						<td>Khe gắn Sim:</td>
						<td>{{$value->connect_duasim}}</td>
					</tr>
					<tr>
						<td>Wifi:</td>
						<td>{{$value->connect_wifi}}</td>
					</tr>
					<tr>
						<td>GPS:</td>
						<td>{{$value->connect_gps}}</td>
					</tr>
					<tr>
						<td>Bluetooth:</td>
						<td>{{$value->connect_bluetouth}}</td>
					</tr>
					<tr>
						<td>GPRS/EDGE:</td>
						<td>{{($value->connect_gprs == 1)?"Có":"Không"}}</td>
					</tr>
					<tr>
						<td>Jack tai nghe:</td>
						<td>{{$value->connect_jack}}</td>
					</tr>
					<tr>
						<td>NFC:</td>
						<td>{{($value->connect_nfc == 1)?"Có":"Không"}}</td>
					</tr>
					<tr>
						<td>Kết nối USB:</td>
						<td>{{$value->connect_usb}}</td>
					</tr>
					<tr>
						<td>Kết nối khác:</td>
						<td>{{$value->connect_other}}</td>
					</tr>
					<tr>
						<td>Cổng sạc:</td>
						<td>{{$value->connect_chargprot}}</td>
					</tr>
					<tr>
						<td colspan="2">Giải trí &amp; Ứng dụng</td>
					</tr>
					<tr>
						<td>Xem phim:</td>
						<td>{{$value->entertainment_movie}}</td>
					</tr>
					<tr>
						<td>Nghe nhạc:</td>
						<td>{{$value->entertainment_player}}</td>
					</tr>
					<tr>
						<td>Cổng sạc:</td>
						<td>{{$value->connect_chargprot}}</td>
					</tr>
					<tr>
						<td>Ghi âm:</td>
						<td>{{($value->entertainment_recording == 1)?"Có":"Không"}}</td>
					</tr>
					<tr>
						<td>FM radio:</td>
						<td>{{($value->entertainment_fm == 1)?"Có":"Không"}}</td>
					</tr>
					<tr>
						<td>Chức năng khác:</td>
						<td>{{$value->entertainment_other}}</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>