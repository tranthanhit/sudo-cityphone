<div class="product-slide-image">
	<div class="product_image">
		<?php 
			$img_array= [];
			foreach($data_attribute as $attribute){
				if (!in_array($attribute['color_image'], $img_array)) {
					if ($attribute['color_image'] != "") {
						array_push($img_array, $attribute['color_image']);
					}
				}
			}
		?>
        @if($value->image)
			<li class="active"><img class="lazy" src="{{getImageDefault('load')}}" data-original="{{$value->getImage()}}" alt="{{getAlt($value->getImage())}}" /></li>
        @else
        	<li class="active"><img class="lazy" src="{{getImageDefault('load')}}" data-original="{{getImageDefault('')}}" alt="{{getAlt(getImageDefault(''))}}" /></li>
        @endif
		@foreach($img_array as $img)
			<li><img src="{{getImage('',$img)}}" alt="{{getAlt(getImage('',$img))}}" /></li>
		@endforeach
	</div>
	<?php 
		$img_slide_array= explode(',', $value->slides);
	?>
	@if($img_slide_array[0] != "")
		<div class="product_slide">
			<ul id="image-gallery" class="gallery list-unstyled">
				@foreach($img_slide_array as $image)
					<li data-src="{{getImage('',$image)}}" data-thumb="{{getImage('tiny',$image)}}" data-sub-html="<h4>{{$value->name}}</h4>">
                    	<a href="#">
                        	<img src="{{getImage('tiny',$image)}}" alt="{{getAlt(getImage('tiny',$image))}}" />
                        </a>
                    </li>
				@endforeach
			</ul>
		</div>
	@endif
</div>