<div class="product-video-box">
    <div class="product-video-title">{{$title??'Video đánh giá'}}</div>
    @if(count($videos) != 0)
        <div class="product-video-content" id="slider_video_home">
            <div class="iframe" data-iframe="{{$videos[0]}}"></div>
            @php $serial = 1; @endphp
            @if (count($videos) > 1)            
                <div class="slide_video owl-carousel" id="slide_video">
                    @foreach($videos as $k => $v)
                        @if($v != "")
                            <div class="item" data-iframe="{{$v}}">
                                <span class="item_serial">{{$serial++}}</span>
                            </div>
                        @endif
                    @endforeach
                </div>
            @endif
        </div>
    @endif
</div>