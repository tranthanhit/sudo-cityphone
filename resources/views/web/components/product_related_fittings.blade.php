<div class="accessories-related-box">
	<div class="accessories-related-title">Phụ kiện liên quan</div>
	<div class="accessories-related-content">
		@foreach($related_fit as $k => $v)
		<div class="accessories-related-item">
			<div class="accessories-related-item-image">
				@if($v->image)
				<a target="_blank" href="{{$v->getUrl()}}">
					<img class="lazy" src="{{getImageDefault('load')}}" data-original="{{$v->getImage('small')}}" alt="{{getAlt($v->getImage('small'))}}">
				</a>
				@else
				<a target="_blank" href="{{$v->getUrl()}}">
					<img class="lazy" src="{{getImageDefault('load')}}" data-original="{{getImageDefault('')}}" alt="{{getAlt(getImageDefault(''))}}">
				</a>
				@endif
			</div>
			<div class="accessories-related-item-info">
				<p class="name"><a target="_blank" href="{{$v->getUrl()}}">{{$v->name}}</a></p>
				<p class="price">{{$v->getPrice()}}</p>
				<a target="_blank" href="{{$v->getUrl()}}" class="buy">Mua</a>
			</div>
		</div>
		@endforeach
	</div>
</div>