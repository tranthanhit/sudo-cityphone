<div class="news-related-box">
	<div class="news-related-title">Tin tức liên quan</div>
	<div class="news-related-content">
		@foreach($related_news as $k => $v)
		<div class="news-related-item">
			<div class="news-related-image">
				<a href="{{$v->getUrl()}}">
					@if($v->image)
                        <img class="lazy" src="{{getImageDefault('load')}}"  data-original="{{$v->getImage('medium')}}" alt="{{getAlt($v->getImage('medium'))}}"/>
                    @else
                        <img class="lazy" src="{{getImageDefault('load')}}" data-original="{{getImageDefault('')}}" alt="{{getAlt(getImageDefault(''))}}">
                    @endif
				</a>
			</div>
			<p class="name"><a target="_blank" href="{{$v->getUrl()}}">{{$v->name}}</a></p>
		</div>
		@endforeach
	</div>
</div>