@if (isset($data) && count($data) > 0)
	<div class="product-garelly">
		<div class="product-garelly__title">
			<h4>{{$title??''}}</h4>
		</div>
		<div class="product-garelly__list" data-open_box id="garelly-content">
			@foreach ($data as $item)
				<a href="{{image_by_link($item)}}" data-sub-html=""  class="product-garelly__list-image">
					<img class="lazy" src="{{getImageDefault('load')}}" data-original="{{image_by_link($item,'medium')}}" alt="">
				</a>
			@endforeach
		</div>
		<div class="product-garelly__btn">
			<button class="button" data-open="product-garelly"><i class="fa fa-long-arrow-down"></i> Xem thêm</button>
		</div>
	</div>
@endif