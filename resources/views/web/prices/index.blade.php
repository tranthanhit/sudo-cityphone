@extends('web.layouts.app')
@section('content')
<div class="content default product_top">
    <div class="wrap">
        <div class="content_title">
            <h4 id="title_content">{!! $price->name !!}</h4>
        </div>
        <div class="css-content price-content">
            {!! $price->detail !!}
        </div>
    </div>
</div> 
@endsection
