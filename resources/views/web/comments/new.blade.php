<div class="comment_item" id="comment_item_{{$data->id}}">
    <div class="comment_item_author">
        <div class="comment_item_author_avatext">{{getCharacterAvatar($data->name)}}</div>
        <span class="comment_item_author_name">{{$data->name}}</span>
        @if($data->admin_id > 0) <span class="comment_item_author_admin">Quản trị viên</span> @endif
    </div>
    <div class="comment_item_content css-content">{!! nl2br($data->content) !!}</div>
    @if($data->images)
    @php
    $images = explode(',',$data->images);
    @endphp
    <div class="comment_item_image">
        @foreach($images as $img)
        <a class="comment_item_image_link" href="{!!$img!!}"><img src="{!!image_by_link($img,'x80')!!}"></a>
        @endforeach
    </div>
    @endif
    <div class="comment_item_meta">
        <span class="comment_item_time" data="{{$data->created_at}}">{{getSimpleTextTime($data->created_at)}}</span>
    </div>
</div>