<div class="comment_form comment_form_addnew">
    <textarea class="comment_editor" placeholder="Nhập nội dung trả lời ..."></textarea>
    <input type="file" name="files_upload" class='files_upload' multiple="multiple" style='display: none;'>
    <div class="comment_form_footer">
        <div class="comment_form_footer_left">
            @if(request()->cookie('comment_name'))
                <span class="comment_form_author_name">{{ request()->cookie('comment_name') }}</span> <a href="javascript:void(0)" class="comment_form_author_remove">(Xóa)</a>
            @endif
        </div>
        <div class="comment_form_footer_right">
            <a href="javascript:void(0)" class="comment_btn comment_addnew_btn" data-author="{{(isset($_COOKIE['comment_name']) && $_COOKIE['comment_name'] != '') ? '1' : '0'}}">Gửi</a>
        </div>
    </div>
    <div class="comment_author_info">
        <p class="comment_author_title">Thông tin bình luận</p>
        <div class="comment_author_info_item">
            @php $rand = rand(0,999999999) @endphp
            <label><input type="radio" name="comment_author_gender_{{$rand}}" class="comment_author_gender" value="1" checked="checked"> Anh</label>
            <label><input type="radio" name="comment_author_gender_{{$rand}}" class="comment_author_gender" value="2"> Chị</label>
        </div>
        <div class="comment_author_info_item">
            <input type="text" name="comment_author_name" class="comment_author_name" value="" placeholder="Họ và tên">
        </div>
        <div class="comment_author_info_item">
            <input type="text" name="comment_author_email" class="comment_author_email" value="" placeholder="Địa chỉ email">
        </div>
        <div class="comment_author_info_item">
            <input type="text" name="comment_author_phone" class="comment_author_phone" value="" placeholder="Số điện thoại">
        </div>
        <div class="comment_author_info_item">
            <a href="javascript:void(0)" class="comment_btn comment_author_addnew_btn" id="putComment">Gửi bình luận</a>
        </div>
    </div>
</div>