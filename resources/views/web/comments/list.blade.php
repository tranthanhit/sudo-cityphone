
@if(isset($comments) && count($comments)>0)
    @foreach($comments as $value)
        <div class="comment_item comment_item_parent" data-parent="{{$value->id}}" id="comment_item_{{$value->id}}">
            <div class="comment_item_author">
                <div class="comment_item_author_avatext">{{getCharacterAvatar($value->name)}}</div>
                <span class="comment_item_author_name">{{$value->name}}</span>
                @if($value->admin_id > 0) <span class="comment_item_author_admin">Quản trị viên</span>
                 @endif
            </div>
            <div class="comment_item_content css-content">{!! nl2br($value->content) !!}</div>
            <div class="comment_item_meta">
                <a href="javascript:void(0)" class="comment_item_reply"  onclick="btn_reply({{ $value->id }})">Trả lời</a> &nbsp;&nbsp;&nbsp; 
                <span class="comment_item_time" data="{{$value->created_at}}">{{getSimpleTextTime($value->created_at)}}</span>
            </div>
            @php
                if(isset($comment_parent_admin_collect) && count($comment_parent_admin_collect)>0){
                    $comment_parent_admin = $comment_parent_admin_collect->where('parent_id',$value->id);
                }
            @endphp
            @if(isset($comment_parent_admin) && count($comment_parent_admin)>0)
            <div class="comment_item_reply_wrap @if(count($comment_parent_admin)) has_child @endif ">
                @if(count($comment_parent_admin))
                    @foreach($comment_parent_admin as $v)
                        <div class="comment_item comment_item_child" id="comment_item_{{$v->id}}">
                            <div class="comment_item_author">
                                <div class="comment_item_author_avatext">{{getCharacterAvatar($v->name)}}</div>
                                <span class="comment_item_author_name">{{$v->name}}</span>
                                @if($v->admin_id > 0) <span class="comment_item_author_admin">Quản trị viên</span> @endif
                            </div>
                            <div class="comment_item_content css-content">{!! strip_tags(nl2br($v->content),'<br><a><img>') !!}</div>
                         
                            <div class="comment_item_meta">
                                <span class="comment_item_time" data="{{$v->created_at}}">{{getSimpleTextTime($v->created_at)}}</span>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
            @else
                <div class="comment_item_reply_wrap"></div>
            @endif

            <div class="comment_form comment_form_addnew form-import-{{ $value->id }}">
                <form action="" class="create_comment_{{ $value->id }}" name="myform" method="post" role="form">
                    <input type="hidden" name="type_id" value="{{ @$type_id }}">
                    <input type="hidden" name="type" value="{{ @$type }}">
                    <input type="hidden" name="parent_id" value="{{ @$value->id }}">
                    <div class="form-import " id="form-import">
                        <textarea class="comment_editor" name="comment_editor" placeholder="Nhập nội dung trả lời ..."></textarea>
                    </div>
                    <div class="comment_form_footer">
                        <div class="comment_form_footer_left">
                            @if(!$is_mobile)
                                <a href=" {!! @$config_general['terms_of_use'] !!} " id="you_agree_to_the_rules">Bạn đồng ý với quy định nội dung của Cityphone</a>
                            @endif
                            @if(request()->cookie('comment_name'))
                                <span class="comment_form_author_name">{{ request()->cookie('comment_name') }}</span> <a href="javascript:void(0)" class="comment_form_author_remove">(Xóa)</a>
                            @endif
                        </div>
                        <div class="comment_form_footer_right">
                            <a href="javascript:void(0)" class="comment_btn " onclick="display_information({{ $value->id }})
                            " data-author="{{(isset($_COOKIE['comment_name']) && $_COOKIE['comment_name'] != '') ? '1' : '0'}}">Gửi</a>
                        </div>
                    </div>
                    <div class="comment_author_info comment_author_info-{{ $value->id }}">
                        <p class="comment_author_title">Thông tin bình luận</p>
                        <div class="comment_author_info_item">
                            @php $rand = rand(0,999999999) @endphp
                            <label><input type="radio" name="comment_author_gender" class="comment_author_gender" value="1" checked="checked"> Anh</label>
                            <label><input type="radio" name="comment_author_gender" class="comment_author_gender" value="2"> Chị</label>
                        </div>
                        <div class="comment_author_info_item">
                            <input type="text" name="comment_author_name" class="comment_author_name" value="" placeholder="Họ và tên">
                        </div>
                        <div class="comment_author_info_item">
                            <input type="text" name="comment_author_email" class="comment_author_email" value="" placeholder="Địa chỉ email">
                        </div>
                        <div class="comment_author_info_item">
                            <input type="text" name="comment_author_phone" class="comment_author_phone" value="" placeholder="Số điện thoại">
                        </div>
                        <div class="comment_author_info_item">
                            <a href="javascript:void(0)" class="comment_btn comment_author_addnew_btn" id="putComment">Gửi bình luận</a>
                        </div>
                    </div>
                </form>
            </div>
        
        </div>
    @endforeach    
@endif