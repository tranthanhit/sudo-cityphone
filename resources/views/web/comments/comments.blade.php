@php
    $comments = \App\Comment::where('type',$type)->where('status',1)->where('parent_id',0)->where('type_id',$type_id)->orderBy('created_at','desc')->paginate(10);
    $comment_parent_admin_collect =collect( \App\Comment::where('status',1)->whereIn('parent_id',$comments->pluck('id','id')->toArray())->get() ); 
@endphp
<div id="sudo_comments" data-url="{!! @$comment_url !!}">
    <form method="post" id="form_comment_{{ @$type_id }}" class="create_comment_{{ @$type_id }}" enctype="multipart/form-data">
        <div class="comment_form comment_form_addnew">
            <textarea class="comment_editor" name="comment_editor" placeholder="Mời bạn để lại bình luận ..."></textarea>
            <div class="comment_form_footer">
                <div class="comment_form_footer_left">
                    @if(!$is_mobile)
                        <a href=" {!! @$config_general['terms_of_use'] !!} " id="you_agree_to_the_rules">Bạn đồng ý với quy định nội dung của Cityphone</a>
                    @endif
                    @if(request()->cookie('comment_name'))
                        <span class="comment_form_author_name">{{ request()->cookie('comment_name') }}</span> <a href="javascript:void(0)" class="comment_form_author_remove">(Xóa)</a>
                    @endif
                </div>
                <div class="comment_form_footer_right">                
                    <a href="javascript:void(0)" class="comment_btn comment_addnew_btn" data-author="{{(isset($_COOKIE['comment_name']) && $_COOKIE['comment_name'] != '') ? '1' : '0'}}">Gửi</a>
                </div>
            </div>
            <div class="comment_author_info">
                <p class="comment_author_title">Thông tin bình luận</p>
                <input type="hidden" name="type_id" value="{{ @$type_id }}">
                <input type="hidden" name="type" value="{{ @$type }}">
                <div class="comment_author_info_item">
                   
                    <label><input type="radio" name="comment_author_gender" class="comment_author_gender" value="1" checked="checked"> Anh</label>
                    <label><input type="radio" name="comment_author_gender" class="comment_author_gender" value="2"> Chị</label>
                </div>
                <div class="comment_author_info_item">
                    <input type="text" name="comment_author_name" class="comment_author_name" value="" placeholder="Họ và tên">
                </div>

                <div class="comment_author_info_item">
                    <input type="text" name="comment_author_email" class="comment_author_email" value="" placeholder="Địa chỉ email">
                </div>
                <div class="comment_author_info_item">
                    <input type="text" name="comment_author_phone" class="comment_author_phone" value="" placeholder="Số điện thoại">
                </div>
                <div class="comment_author_info_item">
                    <a href="javascript:void(0)" data-type_id="{{ @$type_id }}" class="comment_btn comment_author_addnew_btn" id="putComment">Gửi bình luận</a>
                </div>
            </div>
        </div>
    </form>
    <div id="comment_list" class="comment_list">
        @include('web.comments.list')
        <div class="show_comments">

        </div>
        @if($comments->count()>10)
            <div class="clear"></div>
            <a  href="javascript:;" class="comments_more" id="view_more_comment" data-type="{{@$type}}"  data-type_id="{{@$type_id}}"  data-count="{{$comments->count()}}">Tải thêm 10 bình luận</a>
        @endif
    </div>
</div>
<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function(event) {
        $('.comment_btn_image').on('click',function(){
            $(this).closest('.comment_form').find('.files_upload').trigger('click');
            $('.comment_preview_image').remove();
            //$(this).closest('.comment_form').find('.files_upload').unbind('change');
        });
        $('.files_upload').on('change', function(e) {
            var current_file = $(this);
            var files = this.files;
            if (files.length == 0) {
                alert('Bạn chưa chọn ảnh để đính kèm');
                return;
            }else if(files.length > 4) {
                alert('Vui lòng đính kèm tối đa 4 ảnh');
                return;
            }else {
                var str = '<div class="comment_preview_image"><span>Ảnh đính kèm:</span>';
                for (var i = 0; i < files.length; i++)
                {
                    if(files[i].size > 5242880 || files[i].fileSize > 5242880) {
                        alert('Ảnh đính kèm phải có kích thước nhỏ hơn 5MB');
                        return;
                    }else if($.inArray(files[i].type, ["image/gif", "image/jpeg", "image/png"]) < 0){
                        alert('Ảnh đính kèm phải không đúng định dạng');
                        return;
                    }else {
                        var tmppath = URL.createObjectURL(e.target.files[i]);
                        str += '<img src="'+tmppath+'">';
                    }
                }
                str += '</div>';
                current_file.closest('.comment_form').find('.comment_form_footer').after(str);
            }
        });
        // $("a.comment_item_image_link").fancybox().hover(function() {
        //     $(this).click();
        // });
        $("a.comment_item_image_link").fancybox();
    });
    /*
    document.addEventListener("DOMContentLoaded", function(event) {
        tinymce.init({
            selector: 'textarea.comment_editor',
            branding: false,
            content_css : "/public/assets/css/tinymce.css",
            min_height: 150,
            menubar: false,
            statusbar: false,
            plugins: [
                'link image autoresize'
            ],
            toolbar: 'bold italic underline | alignleft aligncenter alignright alignjustify | customeImage',
            autoresize_bottom_margin: 20,
            setup: function(editor) {
                function customeImage() {
                    $("#fileupload").trigger("click");
                    $("#fileupload").unbind('change');
                    $("#fileupload").on("change", function() {
                        var filePath = this.value;
                        var fileName = filePath.match(/[^\/\\]+$/);
                        var fileExtension = filePath.split('.').pop().toUpperCase();
                        var fileSize = this.files[0].size;


                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $('.media-upload-process .image img').attr('src', e.target.result);
                        };
                        reader.readAsDataURL(this.files[0]);

                        if($.inArray(fileExtension, ['PNG','JPG','JPEG','GIF']) == -1) {
                            console.log(fileExtension);
                            alert('File sai định dạng (chỉ chấp nhận jpg, jpeg, png, gif)');
                        }else if(fileSize > 5242880){
                            alert('File có kích thước quá lớn (chỉ chấp nhận file có dung lượng < 5MB)');
                        }else {
                            var token = $('meta[name="csrf-token"]').val();
                            var data = new FormData();
                            data.append('file',$('#fileupload')[0].files[0]);
                            data.append('_token',token);

                            var filename = "";
                            $.ajax({
                                type:'post',
                                dataType: 'json',
                                url:'/ajax/upload-image',
                                data:data,
                                processData: false,
                                contentType: false,
                                success:function(result){
                                    if(result.status == 1) {
                                        editor.insertContent('<img class="comment-image-item" src="'+result.filename+'">');
                                    }else {
                                        alert(result.message);
                                    }
                                    tinymce.activeEditor.setMode('design'); 
                                },
                                beforeSend:function () {
                                    tinymce.activeEditor.setMode('readonly');
                                }
                            });                  
                        }                        
                    });
                    //var html = 'abc';
                    //editor.insertContent(html);
                }
                editor.addButton('customeImage', {
                    icon: 'image',
                    tooltip: "Chèn ảnh vào comment",
                    onclick: customeImage
                });
            }
        });
    });*/
</script>