
@if(!$is_mobile)
{{--  <div id="location">
    <div class="wrap">
        <div class="location">
            <div class="location_list">
                <div class="location_list_item">
                    <b>Khu vực:</b>

                    <select  class="location-option"  @if(isset($home_color) && $home_color==1)  style="background-color: #f4f4f4;" @endif>
                        @foreach($locations as $key=>$location)
                            <option @if(isset($_COOKIE['location']) && $_COOKIE['location'] == $location->id){{'selected'}}@endif value="{{ $location->id }}"  >{{ $location->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="location_list_item">
                    <b>Hotline:</b>
                    <a href="tel:{!! @$config_general['hotline_head'] !!}"> <i class="fa fa-phone"></i> {!! @$config_general['hotline_head'] !!}</a>
                </div>
            </div>
        </div>
    </div>
</div>  --}}
@endif
<header id="header" class="header default">
    <div class="wrap">
        
        <div class="header_logo">
            <a href="/"><img src="{!! @$config_general['logo_header'] !!}" alt=""></a>
        </div>
        <div class="header_search">
            <form action="{{ route('web.search.show') }}" method="get">
                <input type="text" name="keyword" value="{{ @$_GET['keyword'] }}" id="keyword" autocomplete="off" placeholder="Tìm kiếm sản phẩm ...">
                <button type="submit" class="btn-search">
                    <i class="fa fa-search"></i>
                </button>
            </form>
        </div>
        <div class="menu">
            <ul>
                @if(isset($menu_header))
                @php
              
                    $menu = json_decode(base64_decode($menu_header->value),true);
                
                @endphp
                    @foreach ($menu['name_menu_header'] as $key=>$val)
                        <li><a href="{{ $menu['link_menu_header'][$key] }}">
                            <div class="all-icon icon-dependency">
                                <img src="{{ $menu['image_menu_header'][$key] }}" alt="">
                            </div>
                            {{ $val }}</a>
                        </li>
                    @endforeach
                @endif
               
            </ul>
            <div class="phone">
                <div class="phone_item">
                    <a href="tel:{!! @$config_general['hotline_head'] !!}">
                        <i class="fa fa-phone" aria-hidden="true"></i>
                    </a>
                </div>
                <div class="phone_item">
                    <a href="tel:{!! @$config_general['hotline_head'] !!}">{!! @$config_general['hotline_head'] !!}</a>
                    <span>Tư vấn miễn phí (24/7)</span>
                </div>
            </div>
        </div>
    </div>
</header>

<div id="header_mobile" class="header_mobile default">
    <div class="wrap">
        <div class="header_mobile_list">
            <div class="header_mobile_left">
                <a href="/"><img src="{!! @$config_general['logo_header'] !!}" alt=""></a>
            </div>
            <div class="header_mobile_right">
                <div class="cart">
                    <a href="{!! route('web.orders.cart') !!}"><img src="/assets/images/icon/cart.png" alt="">
                        <span> <i class="fa fa-circle"></i></span>
                        <span id="count_cart" style="left: 18.5px;">{{Cart::count()}}</span>
                    </a>
                </div>
                <div class="menu_click">
                    <a href="javascript:;"><i class="fa fa-bars"></i></a>
                </div>
            </div>
        </div>
        <div class="header_mobile_search">
            <form action="{{ route('web.search.show') }}" method="get">
                <input type="text" name="keyword" id="keyword" value="{{ @$_GET['keyword'] }}" autocomplete="off" placeholder="Tìm kiếm sản phẩm ...">
                <button type="submit" class="btn-search">
                    <i class="fa fa-search"></i>
                  
                </button>
            </form>
        </div>
        <nav class="header_mobile_menu">
            <ul>

                
                @if(isset($menu_header))
                @php
              
                    $menu = json_decode(base64_decode($menu_header->value),true);
                
                @endphp
                    @foreach ($menu['name_menu_header'] as $key=>$val)
                        <li><a href="{{ $menu['link_menu_header'][$key] }}">
                            <div class="all-icon icon-dependency">
                                <img src="{{ @$menu['image_mobile_menu_header'][$key] }}" alt="">
                            </div>
                            {{ $val }}</a>
                        </li>
                    @endforeach
                @endif
            </ul>
            <div class="my-ten" style="    text-align: center;margin-top: 10px;">
                <a href="javascript:;">
                    <img src="/assets/images/icon_mobiles/mui-ten.png" height="20px" alt="">
                </a>
            </div>
        </nav>
    </div>
</div>
@if($is_mobile)
{{-- <div id="location_mobile">
    <div class="wrap">
        <div class="location_mobile">
            <div class="location_mobile_list">
                <div class="location_mobile_list_item">
                    <b>Khu vực:</b>
                    <select  class="location-option" id="loca_head"  @if(isset($home_color) && $home_color==1)  style="background-color: #f4f4f4;" @endif>
                        @foreach($locations as $location)
                        <option @if(isset($_COOKIE['location']) && $_COOKIE['location'] == $location->id){{'selected'}}@endif value="{{ $location->id }}"  >{{ $location->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="location_mobile_list_item">
                    <b>Hotline:</b>
                    <a href="tel:{!! @$config_general['hotline_head'] !!}" style="display: flex"> 
                        <i class="fa fa-phone" style="margin-right: 5px"></i> 
                        {!! @$config_general['hotline_head'] !!}
                    </a>
                </div>
            </div>
        </div>
    </div>
</div> --}}
@endif