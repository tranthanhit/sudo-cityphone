@php
    $news_highlight = \App\News::where('status',1)->orderBy('type_id','desc')
->leftJoin('pins', 'news.id', '=', 'pins.type_id')->where('type','news')->where('place','highlight')
->limit(5)->get();

    $list_services_highlight = \App\Service::where('status',1)->orderBy('created_at','DESC')
->leftJoin('pins', 'services.id', '=', 'pins.type_id')->where('type','services')->where('place','highlight')->orderBy('pins.value','asc')
->limit(5)->get();
@endphp
<div class="news_right_list">
    @if(isset($news_highlight) && count($news_highlight)>0)
        <h4 id="title_content">Tin tức nổi bật</h4>
        @foreach( $news_highlight as $value)
            @include('web.layouts.news.news_item')
        @endforeach
    @endif
  
</div>
<div class="news_right_list product_top">
    @if(isset($list_services_highlight) && count($list_services_highlight)>0)
        <h4 id="title_content">Dịch vụ nổi bật</h4>
        @foreach( $list_services_highlight as $value)
            @include('web.layouts.news.news_item')
        @endforeach
    @endif
</div>