<div class="item" data-aos="fade-up" data-aos-duration="1000">
    <a href="{{ $value->getUrl() }}">
        <div class="img">
            <img class="lazy" src="{{ $value->getImage('medium') }}" data-original="{{$value->getImage('medium')}}" alt="{{$value->slug}}">
        </div>
        <h3 id="title">{{ $value->name }}</h3>
    </a>
</div>