<div class="news_list_item"  data-aos="fade-up" data-aos-anchor-placement="top-bottom" data-aos-duration="1000">
    <div class="img">
        <a href="{{ $value->getUrl() }}">
            <img class="lazy" src="{{ $value->getImage('large') }}" data-original="{{$value->getImage('large')}}" alt="{{$value->slug}}"></a>
    </div>
    <div class="infor">
        <p>
            <span><i class="fa fa-calendar"></i> </span>
            <span> 
                {{ date('d/m/Y',strtotime($value->created_at)) }} | {{ date('h:s A',strtotime($value->created_at)) }}
            </span>
        </p>
        <h3><a href="{{ $value->getUrl() }}">{{ $value->name }}</a></h3>
        <a href="{{ $value->getUrl() }}">xem thêm <i class="fa fa-angle-double-right"></i></a>
    </div>
</div> 