@php
    $list_services_highlight = \App\Service::where('status',1)->orderBy('created_at','DESC')
->leftJoin('pins', 'services.id', '=', 'pins.type_id')->where('type','services')->where('place','highlight')->orderBy('pins.value','asc')
->limit(5)->get();

    $news_technology = \App\News::where('status',1)->where('category_id',4)->limit(5)->get();
@endphp
<div class="news_right_list">
    @if(isset($news_technology) && count($news_technology)>0)
        <h4 id="title_content">Tin tức công nghệ</h4>
        @foreach( $news_technology as $value)
            @include('web.layouts.news.news_item')
        @endforeach
    @endif
</div>
<div class="news_right_list product_top">
    @if(isset($list_services_highlight) && count($list_services_highlight)>0)
        <h4 id="title_content">Dịch vụ nổi bật</h4>
        @foreach( $list_services_highlight as $value)
            @include('web.layouts.news.news_item')
        @endforeach
    @endif
</div>