<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="vi" xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#"
      xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{!! @$config_general['image_favicon'] !!}" type="image/x-icon" rel="shortcut icon"/>
    @include('web.layouts.seo')

    <link href="/assets/css/style.min.css" rel="stylesheet">
    <link href="/assets/css/responsive.min.css" rel="stylesheet">

    <link href="/assets/css/t.desktop.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/assets/font-awesome/css/font-awesome.min.css">
  

    <script type="text/javascript" src="/js/jquery-3.3.1.min.js"></script>



    <link href="/assets/libs/aos/aos.min.css" rel="stylesheet">

  
    @yield('head')

    @if(isset($config_metacode['insert_head']))
        {!!$config_metacode['insert_head']!!}
    @endif
</head>
<body   @if(isset($home_color) && $home_color==1)  style="background-color: #f4f4f4;" @endif>

    <img class="loading1" src="/assets/images/loading1.gif" alt=""
    style="    
    position: fixed;
    display: none;
    z-index: 9999;
    left: 50%;
    transform: translateX(-50%);
    top: 40%;
    width: 100px;
    "
    >
@if(!$is_mobile)
    @include('web.layouts.adminbar')
@endif
@if(isset($config_metacode['insert_open_body']))
    {!!$config_metacode['insert_open_body']!!}
@endif
@include('web.layouts.header')
@include('web.layouts.breadcrumb')
@yield('content')

@include('web.layouts.footer')

<div class="mobile_footer">
    <div class="wrap">
        <a href="tel:{!! @$config_general['hotline_head'] !!}">
            <div class="mobile_footer_icon">
                <img src="/assets/images/icon/goi-dien.png" alt="">
            </div>
            <span style="font-size: 11px">Gọi ngay</span>
        </a>  
        <a href="javascript:;" onclick="price_list_mobile()">
            <div class="mobile_footer_icon">
                <img src="/assets/images/icon/bang-gia.png" alt="">
            </div>
            <span style="font-size: 11px">Bảng giá</span>
        </a> 
        <a href="{!! @$config_general['link_facebook'] !!}">
            <div class="mobile_footer_icon">
                <img src="/assets/images/icon/Chinhanh.png" alt="">
            </div>
            <span style="font-size: 11px">Facebook chat</span>
        </a> 
        <a href="{{ route('web.contact.index') }}">
            <div class="mobile_footer_icon">
                <img src="/assets/images/icon/lien-he.png" alt="">
            </div>
            <span style="font-size: 11px">Liên hệ</span>
        </a> 
    </div>
</div>

<!-- script phân riêng từng trang -->
@if(isset($config_metacode['insert_close_body']))
    {!!$config_metacode['insert_close_body']!!}
@endif

<div style="display:none">
    <div id="fancy_data">
        <div class="layer_cart_product">
            <h4> Đã thêm thành công sản phẩm vào giỏ hàng của bạn </h4>
            
                <div class="col-md-4" style="width: 100px;float: left;padding-right: 20px;">
                    <div class="layer_cart_img" style="border: 1px solid #ebebeb;">
                        <img src="" alt="" width="100%">
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="layer_cart_product_info">
                        <p style="max-height:60px;overflow:hidden" class="product_name"></p>
                        <div>
                             <strong class="dark">Số lượng</strong>
                            <span class="layer_cart_product_quantity"></span>
                        </div>
                        <div> 
                            <strong class="dark">Thành tiền</strong>
                             <span class="layer_cart_product_price"></span>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="layer_cart_cart">
                    <h4>
                        <span class="ajax_cart_product_txt_s "> Có <span class="ajax_cart_quantity"></span> sản phầm trong giỏ hàng của bạn. </span>
                    </h4>
                    {{-- <div class="layer_cart_row"> 
                        <strong class="dark"> Tổng tiền </strong>
                         <span class="ajax_block_cart_total"></span>
                    </div> --}}
                </div>
                <div class="go_to_cart">
                    <a href="{{ route('web.orders.cart') }}" class="btn">Đi đến giỏ hàng</a>
                </div>
        </div>
    </div>
</div>

</body>
</html>
<script src="/assets/libs/lazyload.min.js"></script>
<script src="{{ asset('assets/js/script.min.js') }}"></script>
<script type="text/javascript" src="/assets/libs/jquery.sticky-kit.min.js"></script>

<script src="/assets/libs/owl.carousel.min.js"></script>
<script src="/assets/libs/owl.carousel2.thumbs.js"></script>





<script src="/assets/libs/aos/aos.min.js"></script>


@yield('script')

<script>    
        $('.owl-carousel').owlCarousel({
            loop: false,
            margin: 0,
           autoplay: true,
            nav: false,
           items: 1,
           dots: false,
           smartSpeed: 500,
          mouseDrag: true,
          pullDrag: true,
            touchDrag: true,
            thumbs: true,
            thumbImage: false,
            thumbsPrerendered: true,
            thumbContainerClass: 'owl-thumbs',
            thumbItemClass: 'owl-thumb-item',
    
        });
        $(".owl-prev ").html('<i class="fa fa-angle-left" aria-hidden="true"></i>');
        $(".owl-next ").html('<i class="fa fa-angle-right" aria-hidden="true"></i>');    
</script>
<script>
    AOS.init();
  </script>

  <div class="__b-popup1-menu__" style="background-color: rgb(0, 0, 0); position: fixed; top: 0px; right: 0px; bottom: 0px; left: 0px; opacity: 0;display: none; z-index: 1; cursor: pointer;"></div>
