@if (isset($breadcrumbs))
    <div id="breadcrumb" class="breadcrumb default" style="    padding: 5px 0;">
        <div class="wrap">
            <div id="breadcrumb__menu">
                <span><a href="{{ route('web.home') }}" style="color: #368ad3">Trang chủ <i style="margin-left: 10px;margin-right: 5px" class="fa fa-angle-right" aria-hidden="true"></i> </a>
                    @foreach($breadcrumbs as $key=>$value)
                        @if ($key == count($breadcrumbs)-1)
                            <a href="{{$value['url']}}" style="color: #368ad3">{{$value['name']}}</a>
                        @else
                            <a  href="{{$value['url']}}" style="color: #368ad3">{{$value['name']}} <i style="margin-left: 10px;margin-right: 5px" class="fa fa-angle-right" aria-hidden="true"></i> </a>
                        @endif
                    @endforeach
                </span>

            </div>
        </div>
    </div>
@endif