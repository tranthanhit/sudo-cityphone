<div class="rating-show-start">
	@php 
		$rank_peren = floor($rank_peren * 2) / 2;
	@endphp
	@for($i=0;$i<floor($rank_peren);$i++)
		<i class="fa fa-star" aria-hidden="true"></i>
	@endfor
	@if($rank_peren - floor($rank_peren) == 0.5)
		<i class="fa fa-star-half-o" aria-hidden="true"></i>
	@endif
	@for($i=0;$i<5-ceil($rank_peren);$i++)
		<i class="fa fa-star-o" aria-hidden="true"></i>
	@endfor
	<a href="#" class="rating-show-count">(<span>{{$rank_count}}</span> đánh giá)</a>
</div>