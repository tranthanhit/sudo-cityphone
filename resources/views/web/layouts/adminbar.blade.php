@if(Auth::guard('admin')->check())
    <link rel="stylesheet" type="text/css" href="/template-admin/css/admin-bar.css">
    <div id="admin-bar">
        <div class="wrap">
            <a id="admin-bar-backend" href="{!!route('admin.dashboard')!!}">&#9619; Trang quản trị</a>
            @if(isset($admin_bar_edit) && $admin_bar_edit != '') {{--truyền link edit nội dung vào biến view có tên $admin_bar_edit--}}
                <a id="admin-bar-edit" target="_blank" href="{!!$admin_bar_edit!!}">&#9998; Sửa nội dung này</a>
            @endif
            <a id="admin-bar-user">Chào, {!! Auth::guard('admin')->user()->name !!} !</a>
        </div>
    </div>
@endif