<div class="customer default product_top">
    <div class="wrap">
        <div class="customer_form" >
            <div class="customer_form_title">Không tìm thấy dịch vụ bạn cần?</div>
            <div class="customer_form_content" >
                <div class="sff-gender">
                    <label><input type="radio" name="gender" value="1" class="sff-gender"> Anh</label>
                    <label><input type="radio" name="gender" value="2" class="sff-gender"> Chị</label>
                </div>
                <div class="sff_list">
                    <div class="sff_list_item">
                        <input type="text" name="name" class="sff-name" placeholder="Họ và tên">
                        <input type="text" name="phone" class="sff-phone" placeholder="Số điện thoại">
                    </div>
                    <div class="sff_list_item">
                        <input type="text" name="service" class="sff-service" placeholder="Dịch vụ cần hỗ trợ">
                        <button class="sff-button">
                            <span class="sff-button-text-1">Gọi lại cho tôi</span>
                            <span class="sff-button-text-2">(Tôi cần hỗ trợ)</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="customer_check" >
            <div class="title">Kiểm tra tình trạng sửa máy, bảo hành</div>
            <div class="customer_check_form clearfix">
                <form action="" method="POST" role="form">
                    <input type="hidden" name="_token" value="aO1QSt3DBtHBoG7o9bgh866NhRfyhzf3dB6QSzi2">
                    <select class="gcf-location" name="location_check">
                        @foreach($locations as $k => $v)
                            <option value="{{$v->id}}"
                                @if($location_id == $v->id) {{'selected'}} @endif
                            >{{$v->name}}</option>
                        @endforeach
                    </select>
                    <input class="gcf-info" type="text" required="" name="info" placeholder="Số điện thoại hoặc IMEI">
                    <input class="gcf-submit" type="button"  name="submit" value="Kiểm tra">
                </form>
            </div>
            <div class="note">Ghi chú: Số IMEI ghi trên hóa đơn (5 số cuối của máy)
                <a href="">Xem hướng dẫn</a></div>
        </div>
    </div>
</div>
@if ($is_mobile)
<footer id="footer_infor_mobile" >
    <div class="wrap">
        <div class="footer-support ui-grid-a" style="float: left; width: 100% !important;">
            <div class="footer-support-item ui-block-a">
                <p>Tư vấn (24/7)</p>
                <a href="tel:{!! @$config_general['phone_mobile_1'] !!}"><i class="fa fa-phone"></i> {!! @$config_general['phone_mobile_1'] !!}</a>
            </div>
            <div class="footer-support-item ui-block-b">
                <p>Góp ý, phản ánh</p>
                <a href="tel:{!! @$config_general['phone_mobile_2'] !!}"><i class="fa fa-phone"></i> {!! @$config_general['phone_mobile_2'] !!}</a>
            </div>
            <div class="footer-support-item ui-block-a">
               <a href="javascript:;" id="other_information" style="color: #288ad6 !important;font-size: 16px !important;">Thông tin khác <i class="fa fa-angle-down" aria-hidden="true"></i></a>
            </div>
            <div class="footer-support-item ui-block-a">
                <a href="javascript:;" id="store_address" style="color: #288ad6 !important;font-size: 16px !important;">Địa chỉ của hàng <i class="fa fa-angle-down" aria-hidden="true"></i></a>
            </div>
        </div>
        <div class="m_footer_2_mobile product_top" style="display: none" >
            @if(isset($menu_primary))
                @foreach($menu_primary as $value)
                    <div class="item">
                        <h4 id="title">{{ $value->name }}</h4>
                        <ul>
                            @foreach($value->children as $children)
                                <li><a href="{{ $children->link }}">{{ $children->name }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                @endforeach
            @endif
        </div>
        <div class="m_address_2_mobile product_top" style="display: none" >
            @foreach($locations as $location)
                <h3 id="title">{{$location->name}}</h3>
                @foreach($address as $locate)
                    @if($locate->location_id == $location->id)
                        <div class="item">
                            <p id="adrress"><i class="fa fa-map-marker"></i>{{ $locate->name }}</p>
                            <p id="phone">
                                <a href="tel:{{$locate->mobile}}"><i class="fa fa-phone"></i>{{$locate->mobile}}</a>
                                <a href="{{ $locate->map }}" target="_blank"><i class="fa fa-location-arrow"></i> Vị trí bản đồ
                                </a>
                            </p>
                        </div>
                    @endif
                @endforeach
            @endforeach 
        </div>
    </div>
</footer>
@endif

