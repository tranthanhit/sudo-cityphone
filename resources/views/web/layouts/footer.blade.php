@php
$Multiple_viewing_services = \App\Service::where('amount','>',0)->OrderBy('amount','DESC')->limit(6)->get();
@endphp
<div class="footer">
    @if(isset($Multiple_viewing_services) && count($Multiple_viewing_services) > 0)
        <div class="content default product_top" style="background: #ebebeb">
            <div class="wrap">
                <p id="Interested_much" ><span style="color: #999;">Quan tâm nhiều:</span>
                    @foreach ($Multiple_viewing_services as $value)
                        <a title="{{ $value->getName() }}" href="{!! $value->getUrl() !!}">{{ cutString($value->getName(),26) }},</a>
                    @endforeach
                </p>
            </div>
        </div>
    @endif
</div>

@if(!$is_mobile)
<div id="redirect" class="flex-column">
    <div class="row-center gotoTop" style="visibility: visible;">
        <i class="fa fa-arrow-up" aria-hidden="true"></i>
        <p>Lên đầu trang</p>
    </div>
    <div class="row-center cart_item_show">
        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
        <p>Giỏ hàng</p>
    </div>
    {{--  <div class="row-center search">
        <i class="fa fa-search" aria-hidden="true"></i>
        <p>Tìm kiếm</p>
    </div>  --}}
    {{--  <div class="row-center call">
        <i class="fa fa-mobile" aria-hidden="true"></i>
        <p>Đường dây nóng</p>
    </div>  --}}
</div>


<div id="basket" class="flex-column popup_t">
    <p>{{ __("Giỏ hàng") }}</p>
    <div class="closeBtn"></div>
    @php
        $cart_content = Cart::content();
    @endphp
    @if(Cart::count() > 0)
        @foreach ($cart_content as $value)
            @php
                $get_item_cart = Cart::get($value->rowId);
            @endphp
            <div class="item flex-row cart_item_{{$value->rowId}}" id="cart_item_{{$value->rowId}}" >
                <div class="img">
                    <img src="{{image_by_link($value->options->image)}}" alt="">
                </div>

                <div class="detail flex-column">
                    <h3>{{$value->name}}</h3>
                    <span>{{number_format($value->price)}} đ</span>
                    <p>{{ __("Số lượng") }}: <strong>{{$value->qty}}</strong></p>
                </div>
                <a href="javascript:;" data-rowid="{{$value->rowId}}" class="cart_quantity_delete">
                    <i class="fa fa-trash-o"  aria-hidden="true"></i>
                </a>
            </div>
        @endforeach
        <div class="action flex-row">
            <a href="{!! route('web.orders.cart') !!}" class="phe_btn">{{ __("Giỏ hàng") }}</a>
            <a href="{!! route('web.orders.checkout') !!}" class="phe_btn">{{ __("Thanh toán") }}</a>
        </div>
    @endif

    <section class="section your_cart"  @if(Cart::count() > 0) style="display: none" @endif>
        <div class="empty_cart" style="display:block ; text-align: center;
        padding-bottom: 200px;
        padding-top: 150px;">
            <p class="title" style="    color: #757575;
            font-size: 14px;
            line-height: 18px;
            margin-bottom: 27px;">Không có sản phẩm nào trong giỏ hàng của bạn!</p>
            <div class="continue">
                <a style="color: #f57224;
                border: 1px solid #f57224;
                text-decoration: none;
                margin: 0;
                height: 48px;
                padding: 0 36px;
                font-size: 14px;
                line-height: 46px;
                display: inline-block;
                transition: 0.3s all;" href="/">Tiếp tục mua sắm</a>
            </div>
        </div>
    </section>

</div>


<div id="contact" class="flex-column">
    <div class="message">
        <a href="{{ route('web.contact.index') }}">
            <img src="/assets/images/icon/lien-he.png" alt="">
        </a>
    </div>
    <div class="zalo">
        <a href="{!! @$config_general['link_facebook'] !!}">
            <img src="/assets/images/icon/Chinhanh.png" alt="">
        </a>
    </div>
    <div class="facebook">
        <a href="tel:{!! @$config_general['hotline_head'] !!}">
            <img src="/assets/images/icon/goi-dien.png" alt="">
        </a>
    </div>
</div>

@if(isset($home_color) && $home_color==1)
{{--  <div id="behavior" class="flex-column">
    <a href="javascript:;" class="row-center" onclick="outstanding_service()
    ">
        <i class="fa fa-television" aria-hidden="true"></i>
        <p>Dịch vụ nổi bật</p>
    </a>
    <a href="javascript:;" class="row-center" onclick="Price_list_for_repair_services()
    ">
        <i class="fa fa-television" aria-hidden="true"></i>
        <p>Bảng giá dịch vụ sửa chữa</p>
    </a>
    <a href="javascript:;" class="row-center" onclick="Repair_service_yet_iphone()
    ">
        <i class="fa fa-television" aria-hidden="true"></i>
        <p>Dịch vụ sửa chưa iphone</p>
    </a>
    <a href="javascript:;" class="row-center" onclick="parts_accessories()
    ">
        <i class="fa fa-television" aria-hidden="true"></i>
        <p>Linh kiện, phụ kiện</p>
    </a>
   
    <a href="javascript:;" class="row-center" onclick="technology_news()
    ">
        <i class="fa fa-television" aria-hidden="true"></i>
        <p>Tin tức công nghệ</p>
    </a>
</div>  --}}
@endif
@endif

<footer class="footer default  ">
    <div class="wrap">

        <div class="m_footer_2 product_top">
            @if(isset($menu_primary))
                @foreach($menu_primary as $value)
                    <div class="item" >
                        <h4 id="title">{{ $value->name }}</h4>
                        <ul>
                            @if(isset($value->children) && count($value->children) >0)
                                @foreach($value->children as $children)
                                    <li><a href="{{ $children->link }}">{{ $children->name }}</a></li>
                                @endforeach
                            @endif
                        </ul>
                    </div>
                @endforeach
            @endif
            <div class="item" >
                <h4 id="title">Hỗ trọ thành toán</h4>
               {{--  <p>
                   <a href=""><img src="/assets/images/icon/visa-atm.png" alt=""></a>
               </p>  --}}
               <footer_c class="default" style="background-color: #fff;display: flex;align-items: center;margin-top: 5px">
            
                    <a href="{!! @$config_general['link_facebook_desktop'] !!}"><img src="/assets/images/icon/facebook.png" style="height: 45px ;width:45px" alt=""></a>
                    <a href="{!! @$config_general['link_youtube_desktop'] !!}"><img src="/assets/images/icon/youtube.png" style="height: 45px ;width:45px" alt=""></a>
                    <a href="{!! @$config_general['link_thong_bao_bct'] !!}"><img src="/assets/images/icon/certi.gif" height="40px" width="110px" alt=""></a>
               
            </footer_c>  
            </div>
        </div>
       
        <div class="m_footer_1 product_top"   style="padding-bottom: 10px;">
            @foreach($locations as $location)
                <div class="item">
                    <h4 id="title">{{$location->name}}</h4>
                    @foreach($address as $locate)
                        @if($locate->location_id == $location->id)

                            <p id="adrress"> {{ $locate->name }}</p>
                            <p id="time">Giờ làm việc: {{ $locate->work_time }} </p>
                            <p id="phone">
                                <a href="tel:{{$locate->mobile}}"><i
                                            class="fa fa-phone"></i> {{$locate->mobile}}</a> 
                                <a href="{{ $locate->map }}" target="_blank"><i class="fa fa-location-arrow"></i> Vị trí bản đồ
                                </a>
                            </p>
        
                        @endif
                    @endforeach
                </div>
            @endforeach
        </div>
      
    </div>
</footer>
@if(!$is_mobile)
    {{--  <footer_c class="default" style="background-color: #fff">
        <div class="wrap" style="display: flex;align-items: center">
            <a href="{!! @$config_general['link_facebook_desktop'] !!}"><img src="/assets/images/icon/facebook.png" style="height: 45px ;width:45px" alt=""></a>
            <a href="{!! @$config_general['link_youtube_desktop'] !!}"><img src="/assets/images/icon/youtube.png" style="height: 45px ;width:45px" alt=""></a>
            <a href="{!! @$config_general['link_thong_bao_bct'] !!}"><img src="/assets/images/icon/certi.gif" height="40px" width="110px" alt=""></a>
        </div>
    </footer_c>    --}}

<footer-bottom class="default">
    <div class="wrap">
        <p>{!! @$config_general['text_footer'] !!}</p>
        <div class="terms_policies">
            <a href="{!! @$config_general['terms_of_use'] !!}">Điều khoản sử dụng</a>
            <a href="{!! @$config_general['privacy_policy'] !!}">Chính sách bảo mật</a>
        </div>

    </div>
</footer-bottom>
@endif  