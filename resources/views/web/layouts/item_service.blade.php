<div class="content_list_item" data-aos="fade-up" data-aos-duration="1000">
   
        <div class="img">
            <a href="{{ $value->getUrl() }}">
                <img class="lazy" src="{{ $value->getImage('medium') }}" data-original="{{$value->getImage('medium')}}" alt="{{$value->slug}}">
            </a>
            @if($promotion)
                <div class="hover">
                    <h3 id="title">
                        <a href="{{ $value->getUrl() }}">
                            {{ $value->name }}
                        </a>
                    </h3>
                    <div class="promotion">
                        @foreach($promotion as $val)
                            @if($value != "")
                                <p>- {!! $val !!}</p>
                            @endif
                        @endforeach
                    </div>
                
                </div>
            @endif
        </div>
        <a href="{{ $value->getUrl() }}">
            <div class="infor">
                <h3 id="title">{{ $value->name }}</h3>
                <span> Xem chi tiết</span>
            </div>
        
            
        </a>
</div>