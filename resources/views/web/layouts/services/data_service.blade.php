<div class="content_list_item" >
    <a href="{{ $value->getUrl() }}">
        <div class="img">
            <img class="lazy" src="{{ $value->getImage('medium') }}" data-original="{{$value->getImage('medium')}}" alt="{{$value->slug}}">
        </div>
        <div class="infor">
            <h3 id="title">{{ $value->name }}</h3>
            <i class="fa fa-angle-double-up"></i>
            <p id="view" >Xem ngay</p>
        </div>
        <div class="shopping_cart">
            <p id="phone"><i class="fa fa-volume-control-phone"></i></p>
            <p id="cart"><i class="fa fa-shopping-basket"></i></p>
           
        </div>
        @if(isset($promotion))
            <div class="hover">
                <h3 id="title">{{ $value->name }}</h3>
                @foreach($promotion as $value)
                    @if($value != "")
                        <p>- {!! $value !!}</p>
                    @endif
                @endforeach
            </div>
        @endif
    </a>
</div>