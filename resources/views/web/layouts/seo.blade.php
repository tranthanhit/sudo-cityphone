<title>@if(!empty($meta_seo['title'])){{$meta_seo['title']}}@else{{''}}@endif</title>
@if(!empty($meta_seo['description']))
<meta name="description" content="{{$meta_seo['description']}}"/>
@endif
@if(!empty($meta_seo['robots']) && isset($config_general['robots']))
<meta name="robots" content="{{$meta_seo['robots']}}"/>
@endif
@if(!empty($meta_seo['url']))
<link rel="canonical" href="{{$meta_seo['url']}}" />
@endif
<meta property="og:locale" content="vi_VN" />
<meta property="og:site_name" content="{{ config('app.name') }}" />
<meta property="og:type" content="@if(!empty($meta_seo['type'])){{$meta_seo['type']}}@else{{'website'}}@endif" />
<meta property="og:title" content="@if(!empty($meta_seo['title'])){{$meta_seo['title']}}@else{{''}}@endif" />
@if(!empty($meta_seo['description']))
<meta property="og:description" content="{{$meta_seo['description']}}" />
@endif
@if(!empty($meta_seo['url']))
<meta property="og:url" content="{{$meta_seo['url']}}" />
@endif
@if(!empty($meta_seo['image']))
<meta property="og:image" content="{{$meta_seo['image']}}" />
@endif
<link rel="alternate" type="application/rss+xml" title="Cityphone điện thoại xách tay iPhone, Sony, LG, HTC, SamSung; Feed" href="{{route('rss')}}">