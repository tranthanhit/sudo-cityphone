@extends('web.layouts.app')
@section('content')
<section id="container" class="default product_bottom">
    <div class="wrap">
        @if(Cart::count() > 0)
            <div class="page_order">
                <div class="row form_order">
                    <form action="{{ route('web.orders.post_order') }}" method="POST" role="form">
                        {{ csrf_field() }}
                        <div class="row_list">
                            <div class="col-md-4">
                                <div class="box_order box_left">
                                    <div class="top">
                                        Thông tin mua hàng
                                    </div>
                                    <div class="form form_user">
                                        <div class="list_radio">
                                            <input type="radio" id="radio1" name="sex" value="1" checked="">
                                            <label for="radio1">Anh</label>
                                            &nbsp;&nbsp;
                                            <input type="radio" id="radio2" name="sex" value="2">
                                            <label for="radio2">Chị</label>
                                        </div>
                                        <div class="list_input">
                                            <input type="text" name="name" id="" required="" placeholder="Họ và tên" required="">
                                            <input type="number" name="phone" id="" required="" placeholder="Số điện thoại" required="">
                                            <input type="email" name="email" id="" placeholder="Email">
                                            <textarea name="address" id="address" required="" class="form-control" rows="4" placeholder="Địa chỉ" required=""></textarea>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="box_order box_mid">
                                    <div class="top">
                                        Hình thức thanh toán
                                    </div>
                                    <div class="list_radio">
                                        <p>
                                            <input type="radio" id="radio5" name="pay" value="cod" checked="">
                                            <label for="radio5">Thanh toán khi nhận hàng (COD)</label>
                                        </p>
                                        <br>
                                        <p>
                                            <input type="radio" id="radio6" name="pay" value="ck">
                                            <label for="radio6">Chuyển khoản qua Ngân hàng</label>
                                        </p>
                                    </div>
                                    <textarea name="note" id="inputNote" class="form-control" rows="4" placeholder="Viết ghi chú, yêu cầu gì khác"></textarea>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="box_order box_right">
                                    <div class="top">
                                        Đơn hàng (<span class="number">{{Cart::count()}}</span> sản phẩm)
                                    </div>
                                    <div class="list_item">
                                        @foreach($cart_content as $value)
                                        <div class="item">
                                            <div class="img">
                                                <img src="{{image_by_link($value->options->image)}}" alt="">
                                            </div>
                                            <div class="info">
                                                <p class="name">{{$value->name}}</p>
                                                @if($value->price != 0)
                                                <div class="price">
                                                    <p class="price_sale">{{number_format($value->price)}} đ</p>
                                                </div>
                                                @else
                                                <div class="price">
                                                    <p class="price_sale">Liên hệ</p>
                                                </div>
                                                @endif
                                                <div class="sluong">
                                                    <span>Số lượng: <span>{{$value->qty}}</span></span>
                                                    &nbsp;&nbsp;&nbsp;
                                                </div>
                                                
                                            </div>

                                            <div class="clear"></div>
                                        </div>
                                        @endforeach
                                    </div>
                                    <div class="check_out_row">
                                        <div class="checkout-summary-label">Tạm tính</div>
                                        <div class="checkout-summary-value">{{Cart::total()}} ₫</div>
                                    </div>
                                    <div class="check_out_row check_out_row_total" style="border-top: 1px solid #eee">
                                        <div style="font-weight: bold;" class="checkout-summary-label">Thành tiền</div>
                                        <div style="color: red;" class="checkout-summary-value">{{Cart::total()}} ₫</div>
                                    </div> 
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="list_btn">
                                <div class="btn_back_home">
                                    <a href="{{route('web.home')}}">Tiếp tục mua hàng</a>
                                </div>
                                <div class="btn_order">
                                    <button style="cursor: pointer;" type="submit" class="btn btn-primary">Đặt hàng</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        @else
            <section class="section your_cart" >
                <div class="empty_cart" style="display:block ; text-align: center;
                padding-bottom: 200px;
                padding-top: 150px;">
                    <p class="title" style="    color: #757575;
                    font-size: 14px;
                    line-height: 18px;
                    margin-bottom: 27px;">Không có sản phẩm nào trong giỏ hàng của bạn!</p>
                    <div class="continue">
                        <a style="color: #f57224;
                        border: 1px solid #f57224;
                        text-decoration: none;
                        margin: 0;
                        height: 48px;
                        padding: 0 36px;
                        font-size: 14px;
                        line-height: 46px;
                        display: inline-block;
                        transition: 0.3s all;" href="/">Tiếp tục mua sắm</a>
                    </div>
                </div>
            </section>
        @endif
    </div>
</section>
@if ($is_mobile)
    @include('web.layouts.customer_mobile') 
@endif
@endsection