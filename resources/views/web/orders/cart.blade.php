@extends('web.layouts.app')
@section('content')
<div class="order_list default">
    <div class="wrap">
        <h4 id="title"> Dịch vụ và phụ kiện điện thoại đã chọn</h4>
        @if(Cart::count() > 0)
            <div class="list_item_orders" >
                <table>
                    <thead>
                    <tr>
                        <th id="stt_cart">STT</th>
                        <th id="image_cart">Ảnh</th>
                        <th>Sản phẩm</th>
                        <th>Số lượng</th>
                        <th>Đơn giá</th>
                        <th>Thành tiền</th>
                        <th>Xóa</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($cart_content as $value)
                        @php
                            $get_item_cart = Cart::get($value->rowId);
                        @endphp
                        <tr id="cart_item_{{$value->rowId}}" data-id="{{$value->id}}" data-rowid="{{$value->rowId}}" class="cart_item cart_item_{{$value->rowId}}">
                            <td id="stt_cart">1</td>
                            <td id="image_cart">
                                <a href="">
                                    <div class="img">
                                        <a href="{{route('web.service.show',$value->options->slug)}}"><img src="{{image_by_link($value->options->image)}}" alt="" width="100" height="120"></a>
                                    </div>
                                </a>
                            </td>
                            <td style="width: 26%;">
                                <a href="">{{$value->name}}</a>
                            </td>
                            <td>
                                <div class="custom-qty">
                                    <button @if($value->qty == 1) disabled @endif onclick="var result = document.getElementById('qty{{$value->id}}'); var qty{{$value->id}} = result.value; if( !isNaN( qty{{$value->id}} ) &amp;&amp; qty{{$value->id}} > 1 ) result.value--;return false;" class="reduced items increase_reduced_item" type="button"><i class="fa fa-minus"></i>
                                    </button>
                                    <span class="price_span_{{$value->id}}" >{{$value->qty}}</span>
                                    <input style="display: none" type="text" readonly data-rowid="{{$value->rowId}}" class="input-text-qty qty" min="1" max="5" title="Qty" value="{{$value->qty}}" id="qty{{$value->id}}" name="qty[]">
                                    <button onclick="var result = document.getElementById('qty{{$value->id}}'); var qty{{$value->id}} = result.value; if( !isNaN( qty{{$value->id}} )) result.value++;return false;" class="increase items increase_reduced_item" type="button"><i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </td>
                            <td class="price total-price">{{number_format($value->price)}}</td>
                            <td>
                                <span class="price_{{$value->id}}">
                                    {{$get_item_cart->subtotal()}}
                                </span>
                              
                            </td>
                            <td>
                                <a data-rowid="{{$value->rowId}}" class="cart_quantity_delete" data-title="Xóa khỏi giỏ hàng?" href="javascript:;">
                                    <div class="img_delete">
                                        <img src="/assets/images/icon/delete.png" alt="">
                                    </div>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="cart_navigation">
                    <a href="{!! route('web.orders.checkout') !!}">Xác nhận giỏi hàng</a>
                </div>
            </div>
        @endif
        <section class="section your_cart"  @if(Cart::count() > 0) style="display: none" @endif>
            <div class="empty_cart" style="display:block ; text-align: center;
            padding-bottom: 200px;
            padding-top: 150px;">
                <p class="title" style="    color: #757575;
                font-size: 14px;
                line-height: 18px;
                margin-bottom: 27px;">Không có sản phẩm nào trong giỏ hàng của bạn!</p>
                <div class="continue">
                    <a style="color: #f57224;
                    border: 1px solid #f57224;
                    text-decoration: none;
                    margin: 0;
                    height: 48px;
                    padding: 0 36px;
                    font-size: 14px;
                    line-height: 46px;
                    display: inline-block;
                    transition: 0.3s all;" href="/">Tiếp tục mua sắm</a>
                </div>
            </div>
        </section>
    </div>
</div>
@if ($is_mobile)
    @include('web.layouts.customer_mobile') 
@endif
@endsection