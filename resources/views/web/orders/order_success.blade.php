@extends('web.layouts.app')
@section('content')
<div class="order_list default">
    <div class="wrap">
        <section class="section your_cart"  @if(Cart::count() > 0) style="display:none" @else style="display:block" @endif>
            <div class="empty_cart" style=" text-align: center;
            padding-bottom: 200px;
            padding-top: 150px;">
                <p class="title" style="    color: #757575;
                font-size: 14px;
                line-height: 18px;
                margin-bottom: 27px;">Đặt hàng thành công. Chúng tôi sẽ sớm liên hệ với bạn!</p>
                <div class="continue">
                    <a style="color: #f57224;
                    border: 1px solid #f57224;
                    text-decoration: none;
                    margin: 0;
                    height: 48px;
                    padding: 0 36px;
                    font-size: 14px;
                    line-height: 46px;
                    display: inline-block;
                    transition: 0.3s all;" href="/">Tiếp tục mua sắm</a>
                </div>
            </div>
        </section>
    </div>
</div>

@endsection