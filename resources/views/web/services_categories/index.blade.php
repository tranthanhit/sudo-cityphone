@extends('web.layouts.app')

@section('head')
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "WebSite",
        "name": "{{config('app.name')}}",
        "url": "{{config('app.url')}}"
    }, {
        "@context": "http://schema.org",
        "@type": "Organization",
        "url": "{{config('app.url')}}",
        "@id": "{{config('app.url')}}/#organization",
        "name": "Cityphone điện thoại xách tay iPhone, Sony, LG, HTC, SamSung",
        "logo": "{{config('app.url')}}/assets/img/logo.png"
    }
</script>
@endsection
@section('content')

<div class="service default product_top product_bottom">
    <div class="wrap">
        <div class="service_left">
            <div class="service_left_mobile_list_select product_bottom">

                @if(isset($services_categories) && count($services_categories)>0)
                    <select name="service-cate-m" class="service-cate-m" onchange="service_cate_m()" >
                        <option value="">Dịch vụ sửa chữa</option>
                        @foreach($services_categories as $value)
                            <option  @if($slug == $value->slug) selected @endif value="{{ $value->slug }}">{{ $value->name }}</option>
                        @endforeach 
                    </select>
                @endif

                @if(isset($classes) && count($classes)>0)
                    <select  name="request" id="request" class="request"  onchange="request()" data-url="sua-chua-{{ $slug }}">
                        <option value="">Tra theo hãng</option>
                        @foreach ($classes as $value )
                            <option  @if(isset($_GET['request']) &&  $_GET['request']== $value->slug) selected @endif  value="{{ $value->slug }}">{{ $value->name }}</option>
                        @endforeach
                    </select>
                @endif
                
               
            </div>
            <div class="menu_item">
                <h4 id="title"> Dịch vụ sữa chữa</h4>
                <ul>
					@if(isset($services_categories) && count($services_categories)>0)
						@foreach($services_categories as $value)
							<li @if($slug == $value->slug) class="active" @endif><a href="{{ $value->getUrl() }}">{{ $value->name }}</a></li>
						@endforeach
					@endif
                </ul>
            </div>
            @if(isset($classes) && count($classes))
                <div class="menu_item product_top" >
                    <h4 id="title">Tra theo loại</h4>
                    <ul>
                        @foreach ($classes as $value)
                            <li @if(isset($_GET['request']) &&  $_GET['request']== $value->slug) class="active" @endif><a href="?request={{ $value->slug }}">{!! $value->name !!}</a></li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
        <div class="service_right view-more-fit-service">
            <div class="category-content-title" >
                <h1 class="name">{!! $service_category->name ?? '' !!}</h1>
                {{--  <div class="category-search">
                    <form method="GET" action="/tim-kiem/">
                        <input type="text" name="keyword" id="category-keyword" autocomplete="off" placeholder="Tìm dịch vụ">
                        <button id="category-search-btn"><i class="fa fa-search"></i></button>
                    </form>
                </div>  --}}
            </div>
            <div class="content_list">
				@if(isset($services) && count($services)>0)
					@include('web.layouts.services.data_service_category')
				
				@endif
            </div>
            @if($services->count()>10)
                <div class="clear"></div>
                <div class="viewmore" id="viewmore">
                    <button class="view-product-fits-service" href="javascript:;" data-request1="{{ @$_GET['request'] }}" data-cate_id="{{ $service_category->id }}" data-type="services"  data-count="{{$services->count()}}">Xem thêm dịch vụ</button>
                </div>
            @endif
            @if($config_seo_category['title_service'] != '')
                <div class="seo_category" style="padding: 10px;">
                    <div class="">
                        <h1 id="" style="color: #333;font-weight: unset"> {!! @$config_seo_category['title_service'] !!}</h1>
                    </div>
                    <div class="css-content">
                        {!! @$config_seo_category['detail_service'] !!}
                    </div>
                    <a href="javascript:;" class="show-seo-category" data-status="hide" data-up="Thu gọn ▲">Xem thêm ▼</a>
                </div>
            @endif
        </div>
    </div>
</div>

@if ($is_mobile)
    @include('web.layouts.customer_mobile') 
@endif

@endsection