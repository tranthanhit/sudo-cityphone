@extends('web.layouts.app')
@section('head')
@endsection
@section('script')
@section('content')
<div class="service default product_top product_bottom">
    <div class="wrap">
        <div class="service_left">
            <div class="service_left_mobile_list_select product_bottom">
                @if(isset($services_categories) && count($services_categories)>0)
                    <select name="service-cate-m" onchange="service_cate_m()"  class="service-cate-m">
                        <option value="">Dịch vụ sửa chữa</option>
                        @foreach($services_categories as $value)
                            <option value="{{ $value->slug }}">{{ $value->name }}</option>
                        @endforeach 
                    </select>
                @endif
                @if(isset($service_categories_brand) && count($service_categories_brand)>0)
                    <select  name="brand" id="brand" class="brand" onchange="brand()" >
                        <option value="">Tra theo hãng</option>
                        @foreach ($service_categories_brand as $value )
                            <option @if(isset($_GET['brand']) && $_GET['brand'] == $value->brand ) selected  @endif value="{{ $value->brand }}">{{ $value->brand }}</option>
                        @endforeach
                    </select>
                @endif
            </div>
            @if(isset($services_categories) && count($services_categories)>0)
                <div class="menu_item">
                    <h4 id="title"> Dịch vụ sữa chữa</h4>
                    <ul>
                        @foreach($services_categories as $value)
                            <li ><a href="{{ $value->getUrl() }}">{{ $value->name }}</a></li>
                        @endforeach                    
                    </ul>
                </div>
            @endif
            @if(isset($service_categories_brand) && count($service_categories_brand)>0)
                <div class="menu_item product_top" >
                    <h4 id="title">Tra theo hãng</h4>
                    <ul>
                        @foreach ($service_categories_brand as $value )
                            <li @if(isset($_GET['brand']) && $_GET['brand'] == $value->brand ) class="active" @endif><a href="?brand={{ $value->brand }}">{{ $value->brand }}</a></li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
        <div class="service_right view-more-fit-service">
            <div class="category-content-title" >
                <h1 class="name">Sửa chữa điện thoại</h1>
                {{--  <div class="category-search">
                    <form method="GET" action="/tim-kiem/">
                        <input type="text" name="keyword" id="category-keyword" autocomplete="off" placeholder="Tìm dịch vụ">
                        <button id="category-search-btn"><i class="fa fa-search"></i></button>
                    </form>
                </div>  --}}
            </div>
            <div class="content_list">
				@if(isset($services) && count($services)>0)
					
					@include('web.layouts.services.data_service_category')
				
                @endif
            </div>
            @if($services->count()>10)
                <div class="clear"></div>
                <div class="viewmore" id="viewmore">
                    <button class="view-product-fits-service" href="javascript:;" data-brand="{{ @$_GET['brand'] }}" data-cate_id="" data-type="services"  data-count="{{$services->count()}}">Xem thêm dịch vụ</button>
                </div>
            @endif
            @if($config_seo_category['title_service'] != '')
                <div class="seo_category" style="padding: 10px;">
                    <div class="">
                        <h1 id="" style="color: #333;font-weight: unset"> {!! @$config_seo_category['title_service'] !!}</h1>
                    </div>
                    <div class="css-content">
                        {!! @$config_seo_category['detail_service'] !!}
                    </div>
                    <a href="javascript:;" class="show-seo-category" data-status="hide" data-up="Thu gọn ▲">Xem thêm ▼</a>
                </div>
            @endif
        </div>
    </div>
</div>
@if ($is_mobile)
    @include('web.layouts.customer_mobile') 
@endif

@endsection