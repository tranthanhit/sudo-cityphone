@extends('web.layouts.app')
@php
    // $admin_name = $news->getAdminName()??config('app.name')??'';
@endphp
@section('head')
    <link rel="stylesheet" href="/assets/css/pages/profile.css">
@endsection
@section('script')
    <script type="text/javascript" src="/assets/js/pages/news.js"></script>
    @if(isset($config_general['code_chat_all'][$location_id]))
        {!!$config_general['code_chat_all'][$location_id]!!}
    @endif
@endsection
@section('content')
    <section class="profile other wrapped_content container">
        <div class="my-profile">
            <div class="profile-info">
                <aside class="aside a-left">
                    <div class="avt">
                        <a href="javascript:;">
                            <img width="80" height="80" src="/assets/img/avatar7.png"
                                 onerror="this.src='/assets/img/avatar7.png'">
                        </a>
                    </div>
                </aside>
                <aside class="aside a-right">
                    <div class="facebook">
                        <a target="_blank" href=""><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
                    </div>
                    <strong>{!! $user->fullname ?? $user->name !!}</strong>
                    <div class="infoprofile">
                        <span>Tham gia ngày:</span> <span>{!! $user->created_at !!}</span>
                    </div>
                    <div class="description">

                    </div>
                </aside>
            </div>
            <div class="post-box">
                <ul class="lst-topic">
                    <p>Bài đã đăng:</p>
                    <h3 class="titlehome">{!! count($news) !!} bài viết</h3>
                    <div class="content">
                        @if(isset($news))
                            @foreach($news as $value)
                                <li>
                                    <a href="{{route('web.news.show',$value->slug)}}">
                                        <div class="img">
                                            <img class="lazy" src="{{getImageDefault('load')}}" data-original="{{$value->getImage()}}" alt="{{getAlt($value->getImage())}}">
                                        </div>
                                        <div class="title">
                                            <h3>{!! $value->name !!}</h3>
                                            <p>{!! $value->updated_at !!}</p>
                                        </div>
                                    </a>
                                </li>
                            @endforeach
                        @endif
                    </div>
                    <div id="pagination">
                        {!! $news->links() !!}
                    </div>
{{--                    <a href="javascript:;" onclick="loadMorePost(27)" data-page="2" class="viewmore">Xem thêm--}}
{{--                        <b></b></a>--}}
                </ul>
            </div>
        </div>
    </section>
@endsection