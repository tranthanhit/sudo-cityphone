@extends('web.layouts.app')
@section('content')
<div class="content default product_top">
    <div class="wrap">
        <div class="content_title">
            <h4 id="title_content">Dịch vụ sữa chữa ({{ count($services) }})</h4> 
        </div>
        @if(isset($services) && count($services)>0)
            <div class="content_list">
                @foreach ($services as $value)
                @php
                    $promotion = $config_promotion["promotion_default_service"];
                    $promotion = preg_split('/\n|\r\n/',$promotion);
                @endphp
                <div class="content_list_item" data-aos="fade-up" data-aos-duration="1000">
                   
                        <div class="img">
                            <img class="lazy" src="{{ $value->getImage('medium') }}" data-original="{{$value->getImage('medium')}}" alt="{{$value->slug}}">
                            @if($promotion)
                                <div class="hover">
                                    <h3 id="title">
                                        <a href="{{ $value->getUrl() }}">
                                            {{ $value->name }}
                                        </a>
                                    </h3>
                                    <div class="promotion">
                                        @foreach($promotion as $val)
                                            @if($value != "")
                                                <p>- {!! $val !!}</p>
                                            @endif
                                        @endforeach
                                    </div>
                                
                                </div>
                            @endif
                        </div>
                        <a href="{{ $value->getUrl() }}">
                            <div class="infor">
                                <h3 id="title">{{ $value->name }}</h3>
                                <span> Xem chi tiết</span>
                            </div>
                        
                            
                        </a>
                   
                </div>
                @endforeach
            </div>
        @endif
    </div>
</div>

<div class="content default product_top">
    <div class="wrap">
        <div class="content_title">
            <h4 id="title_content">Linh kiện, phụ kiện điện thoại ({{ count($fits) }})</h4>
        </div>
        @if(isset($fits) && count($fits)>0)
            <div class="content_list">
                @foreach($fits as $value)
                    <div class="content_list_accessories" data-aos="fade-up" data-aos-duration="1000">
                        <a href="{{ $value->getUrl() }}">
                            <div class="img">
                                <img class="lazy" src="{{ $value->getImage('medium') }}" data-original="{{$value->getImage('medium')}}" alt="{{$value->slug}}">
                            </div>
                            <div class="infor">
                                <h3 id="title">{{ $value->name }}</h3>
                                <p id="price">
                                    <strong>{{ $value->getPrice() }}</strong>
                                </p>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        @endif
    </div>
</div>

<div class="content default product_top product_bottom">
    <div class="wrap">
        <div class="content_title">
            <h4 id="title_content">Tin tức ({{count($news)}})</h4>
        </div>
        @if(isset($news) && count($news)>0)
            <div class="news" id="new_category">
                <div class="news_left_all">
                    @foreach($news as $key=>$v3)
                        @if($key >= 3)
                            <div class="item" data-aos="fade-up" data-aos-duration="1000">
                                <a href="{{ $v3->getUrl() }}">
                                    <div class="img">
                                        <img class="lazy" src="{{ $v3->getImage('medium') }}" data-original="{{$v3->getImage('medium')}}" alt="{{$v3->slug}}">
                                    </div>
                                    <div class="infor">
                                        <p id="time">
                                            <span><i class="fa fa-calendar"></i></span>
                                            <span>
                                                {{ date('d/m/Y',strtotime($value->created_at)) }} | {{ date('h:s A',strtotime($value->created_at)) }}
                                            </span>
                                        </p>
                                        <h3 id="title">{{ $v3->name }}</h3>
                                        <p id="detail">
                                            {!!cutString(removeHTML($v3->detail), 300)!!}
                                        </p>
                                    </div>
                                </a>
                            </div>
                        @endif
                    @endforeach
                    
                </div>
            </div>
        @endif
    </div>
</div>

@if ($is_mobile)
    @include('web.layouts.customer_mobile') 
@endif

@endsection