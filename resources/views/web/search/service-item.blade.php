<div class="product-list">
@foreach($services as $service)
	<div class="service-list-item">
		<div class="service-item-image">
			<a href="{{route('web.service.show',['slug'=>$service->slug])}}">
				@if($service->image)
					<img class="lazy" src="{{getImageDefault('load')}}" data-original="{{$service->getImage('small')}}" alt="{{$service->slug}}" style="">
				@else
					<img class="lazy" src="{{getImageDefault('load')}}" data-original="{{getImageDefault('')}}" alt="{{$service->slug}}">
				@endif
			</a>
			<div class="mask-service"><a href="{{route('web.service.show',['slug'=>$service->slug])}}">
				@if($service->promotion)
					<?php $promotion = preg_split('/\n|\r\n/',$service->promotion); ?>
					<ul>
						@foreach($promotion as $info)
							@if($info != "")
								<li>{!!$info!!}</li>
							@endif
						@endforeach
					</ul>
				@elseif($promotion_sercice)
					<ul>
						@foreach($promotion_sercice as $info)
							@if($info != "")
								<li>{!!$info!!}</li>
							@endif
						@endforeach
					</ul>
				@endif
			</a></div>
		</div>
		<div class="service-item-info">
			<div class="service-item-left">
	   	    	<h1 class="name"><a href="{{route('web.service.show',['slug'=>$service->slug])}}" title="{{$service->name}}">{{$service->name}}</a></h1>
				<p class="price">{{$service->getPrice()}}</p>
			</div>
			<div class="service-item-right">
				<a href="{{route('web.service.show',['slug'=>$service->slug])}}" class="buy">Mua</a>
			</div>
		</div>
	</div>
@endforeach
</div>