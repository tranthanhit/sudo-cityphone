<div class="product-list">
@foreach($fits as $fits)
	<div class="service-list-item">
		<div class="service-item-image">
			<a href="{{route('web.fits.show',['slug'=>$fits->slug])}}">
				@if($fits->image)
					<img class="lazy" src="{{getImageDefault('load')}}" data-original="{{$fits->getImage('small')}}" alt="{{$fits->slug}}" style="">
				@else
					<img class="lazy" src="{{getImageDefault('load')}}" data-original="{{getImageDefault('')}}" alt="{{$fits->slug}}">
				@endif
			</a>
		</div>
		<div class="service-item-info">
			<div class="service-item-left">
	   	    	<h1 class="name"><a href="{{route('web.fits.show',['slug'=>$fits->slug])}}" title="Thay màn hình, mặt kính cảm ứng iPhone 6, 6S Plus & IP 6S (Tặng dán Cường lực)">{{$fits->name}}</a></h1>
				<p class="price">{{$fits->getPrice()}}</p>
			</div>
			<div class="service-item-right">
				<a href="{{route('web.fits.show',['slug'=>$fits->slug])}}" class="buy">Mua</a>
			</div>
		</div>
	</div>
@endforeach
</div>