<div class="product-list">
@foreach($news as $new)
	<div class="news-list-item">
		<div class="news-list-image">
			<a href="{{route('web.news.show',['slug'=>$new->slug])}}">
				@if($new->image)
					<img class="lazy" src="{{getImageDefault('load')}}" data-original="{{$new->getImage('medium')}}" alt="{{$new->slug}}" style="">
				@else
					<img class="lazy" src="{{getImageDefault('load')}}" data-original="{{getImageDefault('')}}" alt="{{$new->slug}}">
				@endif
			</a>
		</div>
		<h1 class="name"><a href="{{route('web.news.show',['slug'=>$new->slug])}}">{{$new->name}}</a></h1>
	</div>
@endforeach
</div>