<div class="product-list">
@foreach($tablets as $tablet)	
	<div class="product-list-item">
		<div class="product-item-image">
			<div class="hot-and-new">
				@if($tablet->new == 1 && $tablet->hot == 1)
				<span class="new">Mới</span>
				<span class="hot">Hot</span>
				@elseif($tablet->new == 1 && $tablet->hot != 1)
				<span class="new">Mới</span>
				@elseif($tablet->new != 1 && $tablet->hot == 1)
				<span class="hot">Hot</span>
				@endif
			</div>
			<a href="{{route('web.tablet.show',['slug'=>$tablet->slug])}}">
				@if($tablet->image)
					<img class="lazy" src="{{getImageDefault('load')}}" data-original="{{$tablet->getImage('small')}}" alt="{{$tablet->slug}}" style="">
				@else
					<img class="lazy" src="{{getImageDefault('load')}}" data-original="{{getImageDefault('')}}" alt="{{$tablet->slug}}">
				@endif
			</a>
			<div class="mask">
				<a href="{{route('web.tablet.show',['slug'=>$tablet->slug])}}">
                    <div class="mask-title">
                        <div class="icon"></div>
                        <div class="text">Cityphone Care</div>
                    </div>
                    <ul class="mask-list">
                        <li>BH 12 tháng nguồn, màn hình</li>
                        <li>Đổi mới 30 ngày đầu tiên</li>
                        <li>Tặng ốp lưng, dán cường lực</li>
                        <li>Hỗ trợ phần mềm trọn đời máy</li>
                    </ul>
                </a>
                <a class="mask-link" target="_blank" title="Xem chi tiết chính sách bảo hành tại Cityphone" href="#">Bảo hành vàng</a>
			</div>
		</div>
		<div class="product-item-info">
			<div class="product-item-left">
	   	    	<h1 class="name"><a href="{{route('web.tablet.show',['slug'=>$tablet->slug])}}">{{$tablet->name}}</a></h1>
				<p class="price">{{$tablet->getPrice()}}</p>
			</div>
			<div class="product-item-right">
				<div class="all-icon icon-promotion"></div>
				<a href="{{route('web.tablet.show',['slug'=>$tablet->slug])}}" class="buy">Mua</a>
			</div>
			<div class="product-item-full-width">
				@if($tablet->promotion)
					<?php $promotion = preg_split('/\n|\r\n/',$tablet->promotion); ?>
					<ul>
						@foreach($promotion as $tablet)
							@if($tablet != "")
								<li>{!!$tablet!!}</li>
							@endif
						@endforeach
					</ul>
				@elseif($promotion_product)
					<ul>
						@foreach($promotion_product as $tablet)
							@if($tablet != "")
								<li>{!!$tablet!!}</li>
							@endif
						@endforeach
					</ul>
				@endif
			</div>
		</div>
	</div>
@endforeach
</div>