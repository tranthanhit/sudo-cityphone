<div class="product-list">
@foreach($phones as $key => $value)
	<?php
		foreach ($cate_phone as $value_cate) {
			if ($value_cate->id == $value->category_id) {
				$cate_slug = $value_cate->slug;
			}
		}

	?>
	<div class="product-list-item">
		<div class="product-item-image">
			<div class="hot-and-new">
				@if($value->new == 1 && $value->hot == 1)
				<span class="new">Mới</span>
				<span class="hot">Hot</span>
				@elseif($value->new == 1 && $value->hot != 1)
				<span class="new">Mới</span>
				@elseif($value->new != 1 && $value->hot == 1)
				<span class="hot">Hot</span>
				@endif
			</div>
			<a href="{{route('web.phone.show',['category'=>$cate_slug,'slug'=>$value->slug])}}">
				@if($value->image)
					<img class="lazy" src="{{getImageDefault('load')}}" data-original="{{$value->getImage('small')}}" alt="{{$value->slug}}" style="">
				@else
					<img class="lazy" src="{{getImageDefault('load')}}" data-original="{{getImageDefault('')}}" alt="{{$value->slug}}">
				@endif
			</a>
			<div class="mask">
				<a href="{{route('web.phone.show',['category'=>$cate_slug,'slug'=>$value->slug])}}">
                    <div class="mask-title">
                        <div class="icon"></div>
                        <div class="text">Cityphone Care</div>
                    </div>
                    <ul class="mask-list">
                        <li>BH 12 tháng nguồn, màn hình</li>
                        <li>Đổi mới 30 ngày đầu tiên</li>
                        <li>Tặng ốp lưng, dán cường lực</li>
                        <li>Hỗ trợ phần mềm trọn đời máy</li>
                    </ul>
                </a>
                <a class="mask-link" target="_blank" title="Xem chi tiết chính sách bảo hành tại Cityphone" href="{{route('web.phone.show',['category'=>$cate_slug,'slug'=>$value->slug])}}">Bảo hành vàng</a>
			</div>
		</div>
		<div class="product-item-info">
			<div class="product-item-left">
	   	    	<h1 class="name"><a href="{{route('web.phone.show',['category'=>$cate_slug,'slug'=>$value->slug])}}">{{$value->name}}</a></h1>
				<p class="price">{{$value->getPrice()}}</p>
			</div>
			<div class="product-item-right">
				<div class="all-icon icon-promotion"></div>
				<a href="{{route('web.phone.show',['category'=>$cate_slug,'slug'=>$value->slug])}}" class="buy">Mua</a>
			</div>
			<div class="product-item-full-width">
				@if($value->promotion)
					<?php $promotion = preg_split('/\n|\r\n/',$value->promotion); ?>
					<ul>
						@foreach($promotion as $value)
							@if($value != "")
								<li>{!!$value!!}</li>
							@endif
						@endforeach
					</ul>
				@elseif($promotion_product)
					<ul>
						@foreach($promotion_product as $value)
							@if($value != "")
								<li>{!!$value!!}</li>
							@endif
						@endforeach
					</ul>
				@endif
			</div>
		</div>
	</div>
@endforeach
</div>