@extends('web.layouts.app')
@section('head')
<link href="/assets/css/comments.min.css" rel="stylesheet">

<link rel="stylesheet" href="/assets/libs/slick/slick.min.css">
<link rel="stylesheet" href="/assets/libs/slick/slick-theme.min.css">
<script src="/assets/libs/slick/slick.min.js"></script>


    @if(strpos($_SERVER['HTTP_USER_AGENT'], 'coc_coc_browser') === false)
        <script type="application/ld+json">
{
    "@graph": [{
            "@context": "http://schema.org/",
            "@type": "Product",
            "sku": "{{$service->id}}",
            "id": "{{$service->id}}",
            "mpn": "Cityphone",
            "name": "{{$service->name}}",
            "description": "{{$meta_seo['description'] == '' ? cutString(removeHTML($service->detail),150) : $meta_seo['description']}}",
            "image": "{{$service->getImage()}}",
            "brand": "{{$service->name}}",
            "aggregateRating": {
                "@type": "AggregateRating",
                "ratingValue": "{{$rank_peren}}",
                "reviewCount": "{{($rank_count==0)?1:$rank_count}}"
            },
			"review": {
				"@type": "Review",
				"author": "Tran Le Hai",
				"reviewRating": {
					"@type": "Rating",
					"bestRating": "5",
					"ratingValue": "1",
					"worstRating": "1"
				}
			},
            "offers": {
                "@type": "Offer",
                "priceCurrency": "VND",
                "price": "{{(preg_replace('/[^\p{L}\p{N}]+/u', '', api_custom_price($service->price,true))!="Liênhệ")?number_in_string(api_custom_price($service->price,true)):0}}",
                "priceValidUntil": "2019-12-31",
                "availability": "http://schema.org/InStock",
                "url": "{{route('web.service.show',$service->slug)}}",
                "warranty": {
                    "@type": "WarrantyPromise",
                    "durationOfWarranty": {
                        "@type": "QuantitativeValue",
                        "value": "6 tháng",
                        "unitCode": "ANN"
                    }
                },
                "itemCondition": "mới",
                "seller": {
                    "@type": "Organization",
                    "name": "{{config('app.name')}}"
                }
            }
        },
        {
            "@context": "http://schema.org",
            "@type": "WebSite",
            "name": "{{config('app.name')}}",
            "url": "{{config('app.url')}}"
        }
    ]
}



        </script>
        @if (isset($service->schema) && $service->schema != null)
            {!!$service->schema!!}
        @endif

    @endif
@endsection

<?php
if ($service->option) {
    $option_array = json_decode($service->option);
}
?>

@section('script')
<script src="{{ asset('assets/js/slick.min.js') }}"></script>
<script type="text/javascript" src="/assets/libs/jquery.bpopup.min.js"></script>
@endsection
@section('content')

    <div class="service_detail default">
        <div class="wrap">
            <div class="title">

                <h1>{{$service->name}} </h1>

                @php
                    $avg = round($rating_star->avg * 2) / 2;
                @endphp
                <div class="show-rate">
                    
                    <ul>
                        @for($i = 0; $i < 5; $i++)
                            @if($i < $avg && $i + 0.5 == $avg)
                                <li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
                            @elseif($i < $avg)
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                            @else
                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                            @endif
                        @endfor
                    </ul>
                    <span>{{ round($rating_star->avg,1) }} / {{ $rating_star->count }} đánh giá</span>
                    <a href="javascript:;" class="scroll_cmt"><span> </span></a>
                </div>

                <div class="share_facebook">
                    <div id="fb-root"></div>
                    <script async defer crossorigin="anonymous"
                            src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v6.0&appId=535054533725628&autoLogAppEvents=1"></script>
                    <div class="fb-like" data-href="{!! $service->getUrl() !!}" data-width="" data-layout="button_count"
                         data-action="like" data-size="small" data-share="true"></div>
                </div>
            </div>
            <div class="content_service">
                <div class="service_detail_left">
                    <div class="detail_slide">
                        <div class="slide">
                            <div class="detailProduct-left__top">

                                <img class="lazy" src="{{ $service->getImage('large') }}"
                                     data-original="{{$service->getImage('medium')}}" class="cloudzoom" data-zoom="{{ @$service->getImage('medium') }}"  alt="{{$service->slug}}">
                                @if($slides != '')
                                    @foreach ($slides as $v)
                                        <img class="lazy" src="{{ $v }}" class="cloudzoom" data-zoom="{{ @$v }}" data-original="{{$v}}">
                                    @endforeach
                                @endif
                            </div>
                            <div class="detailProduct-left__bottom slide_list">
                                <img class="lazy" src="{{ $service->getImage('large') }}"
                                     data-original="{{$service->getImage('medium')}}" alt="{{$service->slug}}">
                                @if($slides != '')
                                    @foreach ($slides as $v1)
                                        <img class="lazy" src="{{ $v1 }}" data-original="{{$v1}}">
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="detail">
                            <div id="price">
                              
                                    <strong>{{ $service->getPrice() }}</strong>
                            
                            </div>
                            <div class="promotion">
                                <p class="title">
                                    <strong>Khuyến mãi độc quyền</strong>
                                </p>
                                <div class="info_promotion">
                                    <ul>
                                        @foreach($promotion as $key=>$info)
                                            @if($info != "")
                                                <li><i>{{ $key+1 }}</i> &nbsp;{!! $info !!}
                                                </li>
                                            @endif
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            <div class="order">
                                <div href="javascript:;" class="buy-now item">
                            
                                    <img src="/assets/images/icon/Memo.png" alt="">
                                   <ul>
                                        <li> Đặt lịch sửa chữa</li>
                                        <li>(Không chờ đợi)</li>
                                   </ul>
                                </div>
                                <div href="{{ route('web.contact.index') }}" class="item">
                                
                                    <img src="/assets/images/icon/facebook-messenger-brands.png" alt="">
                                    <ul>
                                        <li><a href="{!! @$config_general['messaging_via_facebook'] !!}"> Nhắn tin qua facebook</a></li>
                                        <li><a href="{!! @$config_general['messaging_via_facebook'] !!}">(Hỗ trợ 247)</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="advisory" >
                              
                                <span>Gọi <a href="tel:{!! @$config_general['phone_destop_tu_van'] !!} ">{!! @$config_general['phone_destop_tu_van'] !!}</a> để được tư vấn , hỗ trợ </span>
                            </div>
                            @php
                                $infor = preg_split('/\n|\r\n/',$service->infor);
                            @endphp
                            @if($service->infor != null)
                                <ul class="service">
                                    @foreach ($infor as $key=>$item)
                                        <li><i class="fa fa-check-square-o"></i>{!! $item !!}</li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>
                    </div>
                    @if (isset($service->price_table) && $service->price_table!=null)
                        <div class="price_list">
                            <div class="content_title">
                                <h4 id="title_content">Bảng giá {{ $service->name }}</h4>
                            </div>
                            {!! $service->price_table !!}
                        </div>
                    @endif
                    <div class="specifications">
                        @if($service->detail != '')
                            <div class="content_title">
                                <h4 id="title_content">Thông tin về dịch vụ</h4>
                            </div>
                            <div class="css-content" id="specifications_detail">
                                {!!  $service->detail !!}
                            </div>
                            <div class="product-detail-show">
                                <button class="product-detail-view product-detail-viewall">
                                    Đọc thêm<i class="fa fa-caret-down"></i>
                                </button>
                                <button class="product-detail-view product-detail-viewdefault">
                                    Ẩn bớt<i class="fa fa-caret-up"></i>
                                </button>
                            </div>
                        @endif
                    </div>
                    
                    {{-- <p id="p_admin"><i>Đăng bơi <b>Quản trị viên</b></i> lúc 04:5 06/07/2020</p> --}}
                    @php
                        $type_id = $service->id;
                        $type = 'services';
                    @endphp
                    @include('web.layouts.star',compact('type','type_id'))
                    <div id="detail-comment-main">
                        @include('web.comments.comments',compact('type_id','type'))
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="news_right stick-scroll">

                    <div class="news_right_list">
                        @if(isset($locations) && count($locations)>0)
                           
                            <div class="location product_bottom">
                                <div class="location_title">
                                    <h4>Chọn địa điểm gần bạn</h4>
                                    <p>Để giúp chúng tôi phục vụ bạn tốt hơn</p>
                                </div>
                                @foreach($locations as $location)
                                    <h4 id="title">{{$location->name}}</h4>
                                    @foreach($address as $locate)
                                        @if($locate->location_id == $location->id)
                                            <div class="item_lc" data-aos="fade-up" data-aos-duration="1000">
                                                <p id="adrress"> {{ $locate->name }}</p>
                                                <p id="time">Giờ làm việc: {{ $locate->work_time }}</p>
                                                <p id="phone">
                                                    <a href="tel:{{$locate->mobile}}"><i
                                                                class="fa fa-phone"></i> {{$locate->mobile}}</a> 
                                                    <a href="{{ $locate->map }}" target="_blank"><i class="fa fa-location-arrow"></i> Vị trí bản đồ
                                                      </a>
                                                </p>
                                               
                                            </div>
                                        @endif
                                    @endforeach
                                @endforeach

                                @endif
                            </div>

                            @include('web.layouts.detail_right')
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--  @include('web.layouts.form')  --}}


    @if ($is_mobile)
<footer id="footer_infor_mobile" >
    <div class="wrap">
        <div class="footer-support ui-grid-a" style="float: left; width: 100% !important;">
            <div class="footer-support-item ui-block-a">
                <p>Tư vấn (24/7)</p>
                <a href="tel:{!! @$config_general['phone_mobile_1'] !!}"><i class="fa fa-phone"></i> {!! @$config_general['phone_mobile_1'] !!}</a>
            </div>
            <div class="footer-support-item ui-block-b">
                <p>Góp ý, phản ánh</p>
                <a href="tel:{!! @$config_general['phone_mobile_2'] !!}"><i class="fa fa-phone"></i> {!! @$config_general['phone_mobile_2'] !!}</a>
            </div>
            <div class="footer-support-item ui-block-a">
               <a href="javascript:;" id="other_information" style="color: #288ad6 !important;font-size: 16px !important;">Thông tin khác <i class="fa fa-angle-down" aria-hidden="true"></i></a>
            </div>
            <div class="footer-support-item ui-block-a">
                <a href="javascript:;" id="store_address" style="color: #288ad6 !important;font-size: 16px !important;">Địa chỉ của hàng <i class="fa fa-angle-down" aria-hidden="true"></i></a>
            </div>
        </div>
        <div class="m_footer_2_mobile product_top" style="display: none" >
            @if(isset($menu_primary))
                @foreach($menu_primary as $value)
                    <div class="item">
                        <h4 id="title">{{ $value->name }}</h4>
                        <ul>
                            @foreach($value->children as $children)
                                <li><a href="{{ $children->link }}">{{ $children->name }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                @endforeach
            @endif
        </div>
        <div class="m_address_2_mobile product_top" style="display: none" >
            @foreach($locations as $location)
                <h3 id="title">{{$location->name}}</h3>
                @foreach($address as $locate)
                    @if($locate->location_id == $location->id)
                        <div class="item">
                            <p id="adrress"><i class="fa fa-map-marker"></i>{{ $locate->name }}</p>
                            <p id="phone">
                                <a href="tel:{{$locate->mobile}}"><i class="fa fa-phone"></i> {{$locate->mobile}}</a>
                                <a href="{{ $locate->map }}" target="_blank"><i class="fa fa-location-arrow"></i> Vị trí bản đồ
                                </a>
                            </p>
                        </div>
                    @endif
                @endforeach
            @endforeach 
        </div>
    </div>
</footer>
@endif



    <div class="popup">
        <div class="popup_title">
            <p class="title">{{$service->name}}</p>
            <span class="popup_close"><i class="fa fa-times" aria-hidden="true"></i></span>
        </div>
        <div class="popup_main">
            <div class="popup_product_info">
                <div class="popup-product-image">
                    @if($service->image)
                        <img src="{{$service->getImage('medium')}}" alt="{{getAlt($service->getImage('medium'))}}"/>
                    @else
                        <img src="{{getImageDefault('')}}" alt="{{getAlt(getImageDefault(''))}}"/>
                    @endif
                </div>
                <p class="popup_product_title">{{$service->name}}</p>
                {{-- <p class="popup_product_price" data-realprice="Liên hệ">{{$service->getPrice()}}</p> --}}
                <p class="popup_product_price"
                   data-realprice="Liên hệ">
                  
                        <strong>{{ $service->getPrice() }}</strong>
                  
                </p>
                <div class="clearfix"></div>
                <div class="popup-custom-param-list">
                    @if(isset($option_array))
                        <div class="popup-custom-param-item">
                            <p>Chọn link kiện </p>
                            <select class="popup-param-option option_services">
                                @for($i = 0; $i < count($option_array); $i++)
                                    <option value="{{$option_array[$i][1]}}">{{$option_array[$i][0]}}</option>
                                @endfor
                            </select>
                        </div>
                    @endif
                </div>
            </div>
            <div class="popup_customer_info">
                <p class="popup_customer_info_title">Thông tin người mua</p>
                <form action="">
                    <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
                    <input type="hidden" id="type_order" value="services">
                    <select id="customer-gender" name="customer-gender">
                        <option value="1">Anh</option>
                        <option value="2">Chị</option>
                    </select>
                    <input type="text" id="customer-name" name="customer-name" placeholder="Họ tên bạn (bắt buộc)">
                    <input type="text" id="customer-phone" name="customer-phone"
                           placeholder="Số điện thoại (bắt buộc)">
                    <input type="text" id="customer-email" name="customer-email"
                           placeholder="Địa chỉ email (bắt buộc)">
                    <select id="customer-location" name="customer-location">
                        @foreach($locations as $k => $v)
                            <option value="{{$v->id}}"
                            @if($location_id == $v->id) {{'selected'}} @endif
                            >{{$v->name}}</option>
                        @endforeach
                    </select>
                    <textarea id="customer-address" name="customer-address"
                              placeholder="Địa chỉ nhận hàng"></textarea>
                    <textarea id="order-note" name="order-note" placeholder="Ghi chú đơn hàng"></textarea>
                    <button type="button" class="btn-submit-buynow" data-type="services"
                            data-price="{{ $service->price ?? 0 }}" data-id="{{$service->id}}">Đặt lịch hẹn
                    </button>
                </form>
            </div>
        </div>
    </div>



@endsection


