@extends('web.layouts.app')
@section('head')
@if(strpos($_SERVER['HTTP_USER_AGENT'], 'coc_coc_browser') === false)
<script type="application/ld+json">
{
    "@graph": [{
            "@context": "http://schema.org/",
            "@type": "Product",
            "sku": "{{$unlock->id}}",
            "id": "{{$unlock->id}}",
            "mpn": "Cityphone",
            "name": "{{$unlock->name}}",
            "description": "{{$meta_seo['description'] == '' ? cutString(removeHTML($unlock->detail),150) : $meta_seo['description']}}",
            "image": "{{$unlock->getImage()}}",
            "brand": "{{$unlock->name}}",
            "aggregateRating": {
                "@type": "AggregateRating",
                "ratingValue": "{{$rank_peren}}",
                "reviewCount": "{{($rank_count==0)?1:$rank_count}}"
            },
			"review": {
				"@type": "Review",
				"author": "Tran Le Hai",
				"reviewRating": {
					"@type": "Rating",
					"bestRating": "5",
					"ratingValue": "1",
					"worstRating": "1"
				}
			},
            "offers": {
                "@type": "Offer",
                "priceCurrency": "VND",
                "price": "{{(preg_replace('/[^\p{L}\p{N}]+/u', '', api_custom_price($unlock->price,true))!="Liênhệ")?number_in_string(api_custom_price($unlock->price,true)):0}}",
                "priceValidUntil": "2019-12-31",
                "availability": "http://schema.org/InStock",
                "url": "{{route('web.unlocks.show',$unlock->slug)}}",
                "warranty": {
                    "@type": "WarrantyPromise",
                    "durationOfWarranty": {
                        "@type": "QuantitativeValue",
                        "value": "6 tháng",
                        "unitCode": "ANN"
                    }
                },
                "itemCondition": "mới",
                "seller": {
                    "@type": "Organization",
                    "name": "{{config('app.name')}}"
                }
            }
        },
        {
            "@context": "http://schema.org",
            "@type": "WebSite",
            "name": "{{config('app.name')}}",
            "url": "{{config('app.url')}}"
        }
    ]
}
</script>
@if (isset($unlock->schema) && $unlock->schema != null)
	{!!$unlock->schema!!}
@endif

@endif
@endsection

<?php 

if ($unlock->option) {
	$option_array = json_decode($unlock->option);
}
?>

@section('content')

<div class="service_detail default">
    <div class="wrap">
        <div class="title">

            <h1>{{$unlock->name}}</h1>

            <div class="show-rate">
                <span>5.0/5</span>
                <ul>
                    <li><i class="fa fa-star star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star star" aria-hidden="true"></i></li>
                </ul>
                <a href="javascript:;" class="scroll_cmt"><span>(1 đánh giá)</span></a>
            </div>
            <div class="share_facebook">

            </div>
        </div>
        <div class="content_service">
            <div class="service_detail_left">
                <div class="detail_slide">
                    <div class="slide">
                        <div class="owl-carousel" data-slider-id="3">
                            <div class="mySlides">
                                <img class="lazy" src="{{ $unlock->getImage('large') }}" data-original="{{$unlock->getImage('large')}}" alt="{{$unlock->slug}}">
                            </div>
                        </div>
                        <div class="slide_list owl-thumbs" data-slider-id="3">
                            <span class="owl-thumb-item"> <img class="lazy" src="{{ $unlock->getImage('large') }}" data-original="{{$unlock->getImage('large')}}" alt="{{$unlock->slug}}"></span>
                        </div>
                    </div>
                    <div class="detail">
                        <div id="price">
                            <strong>{{ $unlock->getPrice() }}</strong>
                            <del>{{ $unlock->price_old }}</del>
                        </div>
                        <p id="descript">{!! $unlock->info !!} </p>
                        <div class="promotion">
                            <p class="title">
                                <strong>Khuyến mãi độc quyền</strong>
                            </p>
                            <div class="info_promotion">
                                <ul>
                                    @foreach($promotion as $info)
										@if($info != "")
											<li><i class="fa fa-circle" aria-hidden="true"></i> &nbsp;{!! $info !!}
											</li>
										@endif
									@endforeach
                                </ul>
                            </div>
                        </div>
                        <p class="order">
                            <a href="javascript:;" class="buy-now">Đặt lịch hẹn</a>
                            <a href="">Liên hệ </a>
                        </p>
                        <a class="advisory" href="tel:19006388">
                            <img src="assets/images/icon/phone.png" alt="">
                            <span>Tư vấn: 19006388 - 19005521(7h - 22h30)</span>
                        </a>
                        <ul class="service">
                            <li><i class="fa fa-check-square-o"></i> Nguon gốc rõ ràng</li>
                            <li><i class="fa fa-check-square-o"></i> Nguon gốc rõ ràng</li>
                            <li><i class="fa fa-check-square-o"></i> Nguon gốc rõ ràng</li>
                            <li><i class="fa fa-check-square-o"></i> Nguon gốc rõ ràng</li>
                            <li><i class="fa fa-check-square-o"></i> Nguon gốc rõ ràng</li>
                            <li><i class="fa fa-check-square-o"></i> Nguon gốc rõ ràng</li>
                        </ul>
                    </div>
                </div>
				@if (isset($unlock->option) && $unlock->option!=null)
				@php
					$option = json_decode(base64_decode($unlock->option));
				@endphp
				<div class="price_list">
					<h4 id="title">Bảng giá {{ $unlock->name }}</h4>
					<table>
						<thead>
						<tr>
							<th>Tên</th>
							<th>Giá</th>
							<th>Bảo hành</th>
						</tr>
						</thead>
						<tbody>
							
							@if(isset($option))
								@foreach ($option as $value)
									<tr>
										<td>{{$value->name}}</td>
										<td>{{format_price(number_in_string(api_custom_price($value->price)))}}</td>
										<td>{{$value->warranty}}</td>
									</tr>
								@endforeach
							@endif
						</tbody>
					</table>
					<p>Rất nhiều máy chỉ bị hư mặt kính không cần thay thế màn hình. ĐRất nhiều máy chỉ bị hư mặt kính
						không cần thay thế màn hình. ĐRất nhiều máy chỉ bị hư mặt kính không cần thay thế màn hình.
						Đ</p>
				</div>
			@endif
                <div class="specifications">
                    <div class="content_title">
                        <h4 id="title_content">thông số kỹ thuật</h4>
                    </div>
                    <div class="css-content" id="specifications_detail">
                       {!! $unlock->detail !!}
                    </div>
                    <div class="product-detail-show">
                        <button class="product-detail-view product-detail-viewall">
                            Đọc thêm chi tiết <i class="fa fa-long-arrow-down"></i>
                        </button>
                        <button class="product-detail-view product-detail-viewdefault">
                            Ẩn bớt chi tiết <i class="fa fa-long-arrow-up"></i>
                        </button>
                    </div>
                </div>
                <div class="m_detail">
                    <div class="product">
                        <a href="">
                            <div class="img">
								<img class="lazy" src="{{ $unlock->getImage('medium') }}" data-original="{{$unlock->getImage('medium')}}" alt="{{$unlock->slug}}">
                            </div>
                            <div class="infor">
                                <h3 id="title">{{ $unlock->name }}</h3>
                                <div id="price">
                                    <strong>{{ $unlock->getPrice() }}</strong>
                                    <del>{{ $unlock->price_old }}</del>
                                </div>
                            </div>
                        </a>
                        <div class="order">
                            <p id="order">
                                <a href="javascript:;" class="buy-now">Đặt lịch hẹn</a>
                                <a href="">Liên hệ </a>
                            </p>
                            <a id="advisory" href="tel:19006388">
                                <img src="assets/images/icon/phone.png" alt="">
                                <span>Tư vấn: 19006388 - 19005521(7h - 22h30)</span>
                            </a>
                        </div>
                    </div>
                </div>
                @include('web.layouts.star')
                <div id="detail-comment-main">
                    @php
                        $type_id = $unlock->id;
                        $type = 'unlocks';
                    @endphp
                    @include('web.comments.comments',compact('type_id','type'))
                    <div class="clear"></div>
                </div>
            </div>
            <div class="news_right">
               @include('web.layouts.detail_right')
            </div>
        </div>
    </div>
</div>
@include('web.layouts.form')

<div class="popup">
	<div class="popup_title">
		<p class="title">{{$unlock->name}}</p>
		<span class="popup_close"><i class="fa fa-times" aria-hidden="true"></i></span>
	</div>
	<div class="popup_main">
		<div class="popup_product_info">
			<div class="popup-product-image">
				@if($unlock->image)
	                <img src="{{$unlock->getImage('medium')}}" alt="{{getAlt($unlock->getImage('medium'))}}" />
	            @else
	                <img src="{{getImageDefault('')}}" alt="{{getAlt(getImageDefault(''))}}" />
                @endif
			</div>
			<p class="popup_product_title">{{$unlock->name}}</p>
			{{-- <p class="popup_product_price" data-realprice="Liên hệ">{{$service->getPrice()}}</p> --}}
			<p class="popup_product_price" data-realprice="Liên hệ">{{api_custom_price($unlock->price,true)}}</p>
			<div class="clearfix"></div>
			<div class="popup-custom-param-list">
				@if(isset($option_array))
				<div class="popup-custom-param-item">
					<p>Chọn link kiện </p>
					<select class="popup-param-option option_services">
						@for($i = 0; $i < count($option_array); $i++)
							<option value="{{$option_array[$i][1]}}">{{$option_array[$i][0]}}</option>
						@endfor
					</select>
				</div>
				@endif
			</div>
		</div>
		<div class="popup_customer_info">
			<p class="popup_customer_info_title">Thông tin người mua</p>
			<form action="">
				<input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
				<input type="hidden" id="type_order" value="services">
				<select id="customer-gender" name="customer-gender">
					<option value="1">Anh</option>
					<option value="2">Chị</option>
				</select>
				<input type="text" id="customer-name" name="customer-name" placeholder="Họ tên bạn (bắt buộc)">
				<input type="text" id="customer-phone" name="customer-phone" placeholder="Số điện thoại (bắt buộc)">
				<input type="text" id="customer-email" name="customer-email" placeholder="Địa chỉ email (bắt buộc)">
				<select id="customer-location" name="customer-location">
					@foreach($locations as $k => $v)
						<option value="{{$v->id}}"
							@if($location_id == $v->id) {{'selected'}} @endif
						>{{$v->name}}</option>
                    @endforeach
				</select>
				<textarea id="customer-address" name="customer-address" placeholder="Địa chỉ nhận hàng"></textarea>
				<textarea id="order-note" name="order-note" placeholder="Ghi chú đơn hàng"></textarea>
				<button type="button" class="btn-submit-buynow" data-type ="unlocks" data-id="{{$unlock->id}}">Đặt lịch hẹn</button>
			</form>
		</div>
	</div>
</div>

@endsection