@foreach($unlocks as $unlock)
<div class="service-list-item">
	<div class="service-item-image">
		<a href="{{route('web.unlocks.show', ['slug'=>$unlock->slug])}}">
			@if($unlock->image)
				<img class="lazy" src="{{getImageDefault('load')}}" data-original="{{$unlock->getImage('small')}}" alt="{{getAlt($unlock->getImage('small'))}}" style="">
			@else
				<img class="lazy" src="{{getImageDefault('load')}}" data-original="{{getImageDefault('')}}" alt="{{getAlt(getImageDefault(''))}}" style="">
			@endif
		</a>
		
		@if($unlock->promotion)
			<?php $promotion_array = preg_split('/\n|\r\n/',$unlock->promotion); ?>
			<a href="{{$unlock->getUrl()}}">
				<div class="mask-service">
						<ul>
							@foreach($promotion_array as $promotion)
								@if($promotion != "")
									<li>{!!$promotion!!}</li>
								@endif
							@endforeach
						</ul>
				</div>
			</a>
		@elseif($config_promotion)
			<a href="{{$unlock->getUrl()}}">
				<div class="mask-service">
					<ul>
						@foreach($config_promotion as $value)
							@if($value != "")
								<li>{!!$value!!}</li>
							@endif
						@endforeach
					</ul>
				</div>
			</a>
		@endif
	</div>
	<div class="service-item-info">
		<div class="service-item-left">
   	    	<p class="name"><a href="{{$unlock->getUrl()}}" title="{{$unlock->name}}">{{$unlock->name}}</a></p>
			<p class="price">{{$unlock->getPrice()}}</p>
		</div>
		<div class="service-item-right">
			<a href="{{$unlock->getUrl()}}" class="buy">Mua</a>
		</div>
	</div>
</div>
@endforeach