@extends('web.layouts.app')
@section('head')
@endsection
@section('script')

@if(isset($config_boxchat['unlock'][$location_id]) && $config_boxchat['unlock'][$location_id]["location_code"] != null)
{!!filter_code_chat($config_boxchat['unlock'][$location_id]["location_code"])!!}
@else
	@if(isset($config_general['code_chat_all'][$location_id]))
	{!!filter_code_chat($config_general['code_chat_all'][$location_id])!!}
	@endif
@endif
@endsection

@section('content')

<div class="service default">
    <div class="wrap">
        <div class="service_left">
            <div class="menu_item">
                <h4 id="title">Danh mục điện thoại</h4>
                <ul>
					@if(isset($unlock_categories) && count($unlock_categories)>0)
						@foreach($unlock_categories as $value)
							<li><a href="{{ $value->getUrl() }}">{{ $value->name }}</a></li>
						@endforeach
					@endif
                </ul>
            </div>
            <div class="menu_item product_top" >
                <h4 id="title">Tra theo hãng</h4>
                <ul>
                    <li><a href="">Apple</a></li>
                    <li><a href="">Apple</a></li>
                    <li><a href="">Apple</a></li>
                    <li><a href="">Apple</a></li>
                    <li><a href="">Apple</a></li>
                    <li><a href="">Apple</a></li>
                    <li><a href="">Apple</a></li>
                    <li><a href="">Apple</a></li>
                </ul>
            </div>
        </div>
        <div class="service_right">
            @if(isset($unlocks) && count($unlocks)>0)
				<div class="content_list">
					@foreach($unlocks as $value)
						<div class="content_list_accessories" id="list_product">
							<a href="{{ $value->getUrl() }}">
								<div class="img">
									<img class="lazy" src="{{ $value->getImage('medium') }}" data-original="{{$value->getImage('medium')}}" alt="{{$value->slug}}">
								</div>
								<div class="infor">
									<h3 id="title">{{ $value->name }}</h3>
									<p id="price">
										<strong>{{ $value->getPrice() }}</strong>
									</p>
								</div>
							</a>
						</div>
					@endforeach
				</div>
			@endif  
        </div>
    </div>
</div>


@endsection