<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Thêm thuộc tính giá</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
	      	<a style="margin-bottom: 20px;" id="add_option" class="btn btn-primary btn-xs"><i class="fa fa-plus" aria-hidden="true"></i> Thêm </a>
	      	<div id="result_option">
	      		@if(isset($option))
	      		@foreach($option as $key => $val)
	      		<div class="row option_item" style="padding: 10px;border:1px solid #ccc;margin-bottom: 5px;">
	    			<div class="col-xs-3">
	    				<input type="text" class="form-control" name="option[{{$key}}][name]" value="{{$val->name}}" placeholder="Tên phụ kiện">
	    			</div>
	    			<div class="col-xs-3">
	    				<input type="text" class="form-control" name="option[{{$key}}][price]" value="{{$val->price}}" placeholder="Giá">
	    			</div>
	    			<div class="col-xs-3">
	    				<input type="text" class="form-control" name="option[{{$key}}][warranty]" value="{{$val->warranty}}" placeholder="Bảo hành">
	    			</div>
	    			<div class="col-md-3" style="text-align: center;">
	    				<a class="btn btn-sm btn-danger remove_option" href="" style=""><i class="fa fa-times" aria-hidden="true"> Xóa</i></a>
	    			</div>
	    		</div>
	    		@endforeach
	    		@endif
	    	</div>
	    </div>
	    
	</div>
<script type="text/javascript">
	$(document).ready(function() {
			@if(isset($option))
            var i = {{count((array)$option)}};
            @else
            var i = 0;
            @endif
            $("#add_option").click(function(event) {
                event.preventDefault();
                i++;
                var option_item =
                '<div class="row option_item" style="padding: 10px;border:1px solid #ccc;margin-bottom: 5px;">' + 
                '<div class="col-xs-3">'+
                '<input type="text" class="form-control" name="option['+ i +'][name]" value="" placeholder="Tên phụ kiện">'+
                '</div>'+
                '<div class="col-xs-3">'+
                '<input type="text" class="form-control" name="option['+ i +'][price]" value="" placeholder="Giá">'+
                '</div>'+
                '<div class="col-xs-3">'+
                '<input type="text" class="form-control" name="option['+ i +'][warranty]" value="" placeholder="Bảo hành">'+
                '</div>'+
                '<div class="col-xs-3" style="text-align: center;">'+
                '<a class="btn btn-sm btn-danger remove_option" href="" style=""><i class="fa fa-times" aria-hidden="true"> Xóa</i></a>'+
                '</div>'+
                '</div>';
                $(this).closest('.controls').find("#result_option").append(option_item);
            });
            $("#result_option").on('click', '.remove_option', function(event) {
                event.preventDefault();
                $(this).closest('.option_item').remove();
            });
            
	});
</script>