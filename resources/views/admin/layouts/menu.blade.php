<span class="section" style="margin-top:30px;width: 100%;float: left;clear: both;">{!! $value['title'] !!}</span>
<div id="{!! $value['name'] !!}" class="clearfix" style="margin-bottom: 20px;">
<div class="action-menu">
	<div class="custom-link option-menu" onclick="toggle_menu('custom-link', '{!! $value['name'] !!}')">
		<p>Liên kết tự tạo <i class="fa fa-caret-right" aria-hidden="true"></i></p>
	</div>
	<div class="add-menu add-menu-custom-link">
    	<h5>Thêm mới menu</h5>
    	<div class="form-group">
    		<label>Tên menu</label>
    		<input type="text" class="name-menu">
    	</div>
    	<div class="form-group">
    		<label>Link</label>
    		<input type="text" class="link-menu">
    	</div>
    	<div class="form-group">
    		<label>Mở tab mới</label>
    		<input style="height: 15px;width: 20px;" type="checkbox" class="taget-menu">
    	</div>
    	<div class="form-group">
    		<label>Nofollow</label>
    		<input style="height: 15px;width: 20px;" type="checkbox" class="rel-menu">
    	</div>
		<p class="add">
			<img class="img1" src="/template-admin/images/spinner.gif" alt="">
			<a href="javascript:;" onclick="add_menu('{!! $value['name'] !!}');">Thêm mới</a>
		</p>
	</div>
	@foreach($value['data_table'] as $k => $v)
		@php
			$data_select = DB::table($v)->where('status',1)->get();
		@endphp
		<div class="{!! $v !!} option-menu" onclick="toggle_menu('{!! $v !!}', '{!! $value['name'] !!}')">
			<p>{!! config('modules.name')[$v] !!}<i class="fa fa-caret-right" aria-hidden="true"></i></p>
		</div>
		<div class="add-menu add-menu-{!! $v !!}">
	    	<h5>Thêm mới menu</h5>
	    	<div class="form-group">
	    		<label>Tên menu</label>
	    		<input type="text" class="name-menu-{!! $v !!}">
	    	</div>
	    	<div class="form-group">
	    		<label>Đường dẫn đến</label>
	    		<select class="link-menu-{!! $v !!}" id="">
		    		@foreach($data_select as $item)
		    			<option value="{!! $item->id !!}" data-slug="{!! route('web.'.$v.'.show',$item->slug) !!}">{!! $item->name !!}</option>
		    		@endforeach
	    		</select>
	    	</div>
	    	<div class="form-group">
	    		<label>Mở tab mới</label>
	    		<input style="height: 15px;width: 20px;" type="checkbox" class="taget-menu-{!! $v !!}">
	    	</div>
	    	<div class="form-group">
	    		<label>Nofollow</label>
	    		<input style="height: 15px;width: 20px;" type="checkbox" class="rel-menu-{!! $v !!}">
	    	</div>
			<p class="add">
				<img class="img1" src="/template-admin/images/spinner.gif" alt="">
				<a href="javascript:;" onclick="add_menu1('{!! $value['name'] !!}','{!! $v !!}');">Thêm mới</a>
			</p>
		</div>
	@endforeach
	<div class="add-menu edit-menu-custom edit-menu" style="margin-top: 50px;display: none;">
    	<h5>Sửa menu</h5>
    	<div class="form-group">
    		<label>Tên menu</label>
    		<input type="text" class="name-menu-edit" value="">
    	</div>
    	<div class="form-group">
    		<label>Link</label>
    		<input type="text" class="link-menu-edit" value="">
    	</div>
    	<div class="form-group">
    		<label>Mở tab mới</label>
    		<input style="height: 15px;width: 20px;" type="checkbox" class="taget-menu-edit">
    	</div>
    	<div class="form-group">
    		<label>Nofollow</label>
    		<input style="height: 15px;width: 20px;" type="checkbox" class="rel-menu-edit">
    	</div>
		<p class="add">
			<img class="img2" src="/template-admin/images/spinner.gif" alt="">
			<a href="javascript:;" onclick="update_menu('{!! $value['name'] !!}');">Cập nhật</a>
		</p>
		<input type="hidden" class="edit-menu-btn" value="">
	</div>
	@foreach($value['data_table'] as $k => $v)
		@php
			$data_select = DB::table($v)->where('status',1)->get();
		@endphp
		<div class="add-menu edit-menu-{!! $v !!} edit-menu" style="margin-top: 50px;display: none;">
	    	<h5>Sửa menu</h5>
	    	<div class="form-group">
	    		<label>Tên menu</label>
	    		<input type="text" class="name-menu-edit-{!! $v !!}">
	    	</div>
	    	<div class="form-group">
	    		<label>Đường dẫn đến</label>
	    		<select class="link-menu-edit-{!! $v !!}" id="">
		    		@foreach($data_select as $item)
		    			<option value="{!! $item->id !!}" data-slug="{!! $item->slug !!}">{!! $item->name !!}</option>
		    		@endforeach
	    		</select>
	    	</div>
	    	<div class="form-group">
	    		<label>Mở tab mới</label>
	    		<input style="height: 15px;width: 20px;" type="checkbox" class="taget-menu-edit-{!! $v !!}">
	    	</div>
	    	<div class="form-group">
	    		<label>Nofollow</label>
	    		<input style="height: 15px;width: 20px;" type="checkbox" class="rel-menu-edit-{!! $v !!}">
	    	</div>
			<p class="add">
				<img class="img2" src="/template-admin/images/spinner.gif" alt="">
				<a href="javascript:;" onclick="update_menu1('{!! $value['name'] !!}','{!! $v !!}');">Cập nhật</a>
			</p>
			<input type="hidden" class="edit-menu-btn-{!! $v !!}" value="">
		</div>
	@endforeach
</div>
<input type="hidden" name="{!! $value['name'] !!}" class="{!! $value['name'] !!}" value="">
<div class="manager-menu">
	@php
		$data_menu = json_decode($value['value'], 1);
	@endphp
	<h5>Cây menu</h5>
	<div class="content-menu">
		<div class="dd" id="nestable-{!! $value['name'] !!}">
			@if(!empty($value['value']))
				{!! recursive_setting_menu($data_menu,0, $value['name'],true) !!}
			@else
				<ol class="dd-list" id="ol-first">
				</ol>
			@endif
		</div>
	</div>
</div>
</div>
<script>
	$(document).ready(function(){
        var updateOutput = function(e)
        {
            var list   = e.length ? e : $(e.target),
                output = list.data('output');
            if (window.JSON) {
                output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
            } else {
                output.val('JSON browser support required for this demo.');
            }
        };
        $('#nestable-{!! $value['name'] !!}').nestable({
            group: 1
        })

        $('#submit-form').on('click',function(){
            updateOutput($('#nestable-{!! $value['name'] !!}').data('output', $('.{!! $value['name'] !!}')));
            $('#form-menu').submit();
        });
    });
	
	
</script>
