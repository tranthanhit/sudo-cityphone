<div class="alert alert-info" data-toggle="collapse" data-target="#{{$name}}">
	<strong>{{__($title??'')}}</strong>
</div>
<div id="{{$name}}" class="collapse" style="height: auto;">
	
	@include('admin.layouts.form.table.2_input',[
		'full' => 'false',
		'name' => $name,
		'slug' => $name,
		'value' => $data[$name]??'',
		'label' => 'Danh sách slogan',
		'placeholder_input_1' => 'Icon slogan',
		'placeholder_input_2' => 'Tên slogan hiển thị',
	])
	
	@php $banner_text = 'banner_text'; @endphp
	
</div>