<div class="alert alert-info" data-toggle="collapse" data-target="#detail_config">
	<strong>Cấu hình nội dung</strong>
</div>
<div id="detail_config" class="collapse" style="height: auto;">
	<div class="form-group" style="margin-bottom: 10px;">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Nội dung</label>
		{{-- <div class="controls col-md-9 col-sm-10 col-xs-12"></div> --}}
	</div>
	<div class="detail_config_block">
		@if (isset($detail_config))
			@if (isset($detail_config->detail_config_title) && count($detail_config->detail_config_title) > 0)
				@for ($i = 0; $i < count($detail_config->detail_config_title); $i++)
					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12 box-number">Box: {{$i+1}}</label>
						<div class="controls col-md-9 col-sm-10 col-xs-12">
							<input type="text" class="form-control" name="detail_config_title[]" value="{{$detail_config->detail_config_title[$i]??''}}" placeholder="Nhập tiêu đề block">
							<span class="btn btn-danger btn-sm delete_item"><i class="fa fa-trash"></i></span>
							<textarea name="detail_config_content[]" id="detail_config_content_{{$i+1}}" class="form-control" style="height: 200px;">{{$detail_config->detail_config_content[$i]}}</textarea>
						</div>
					</div>
				@endfor
			@endif
		@endif
	</div>
	<div class="form-group" style="margin-bottom: 10px;">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"></label>
		<div class="controls col-md-9 col-sm-10 col-xs-12"><button class="btn btn-primary btn-sm" id="add_detail_config" type="button">Thêm block nội dung</button></div>
	</div>
</div>

<script>
	$('body').on('click','.delete_item',function() {
		if(confirm('Bạn muốn xóa box này!')) {
			$(this).parent().parent().remove();
			$('.box-number').each(function(index) {
				$(this).text('Box: '+(index+1));
			});
		}
	})
	$('body').on('click','#add_detail_config',function() {
		stt = $('input[name^=detail_config_title]').length;
		detail_config_form = `
			<div class="form-group">
				<label class="control-label col-md-2 col-sm-2 col-xs-12 box-number">Box: ${stt+1}</label>
				<div class="controls col-md-9 col-sm-10 col-xs-12">
					<input type="text" class="form-control" name="detail_config_title[]" value="" placeholder="Nhập tiêu đề block">
					<span class="btn btn-danger btn-sm delete_item"><i class="fa fa-trash"></i></span>
					<textarea name="detail_config_content[]" id="detail_config_content_${stt+1}" class="form-control" style="height: 200px;"></textarea>
				</div>
			</div>
		`;
		$('.detail_config_block').append(detail_config_form);
		addTinyMCE(`#detail_config_content_${stt+1}`);
	});
	document.addEventListener("DOMContentLoaded", function(event) {
		$('textarea[name="detail_config_content[]').each(function(index,item) {
			id = $(this).attr('id');
        	addTinyMCE('#'+id);
		})
    });
</script>