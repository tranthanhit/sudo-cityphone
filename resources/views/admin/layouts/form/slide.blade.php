{{-- 
	@include('admin.layouts.form.slide',[
		'name' => 'text',
		'value' => 'text',
		'title' => 'text',
		'required' => 1,
		'title_btn' => 'text',
		'helper_text' => 'text',
	])
 --}}
 <div class="form-group">
	<label class="control-label col-md-2 col-sm-2 col-xs-12">@if($required==1)<span class="form-asterick">* </span>@endif {!! $title??'' !!}</label>
	<div class="controls col-md-9 col-sm-10 col-xs-12">
		<div id="{!! $name??'' !!}" class="slide-wrap slide-check">
			<a href="javascript:;" onclick="media_popup('add','slide','{!!$name??''!!}','{!! $title_btn??'' !!}');">
				<button class="btn btn-success btn-sm" type="button"><i class="fa fa-plus-square"></i> Thêm ảnh </button>
			</a>
			<div class="clear"></div>
			@php
				$str_val = '';
				$str_id = $name??'';
				if(!empty($value)) {
					foreach($value as $v) {
						if($v != '') {
							$str_val .= '<div class="result_image_item">
										<input type="hidden" name="'.$str_id.'[]" value="'.$v.'">
										<img src="'.$v.'" alt="Không có ảnh"/>
										<a href="javascript:;" class="del_img" onclick="return media_remove_item(this);"><i class="fa fa-times"></i></a>
									</div>';
						}
					}
				}
			@endphp
			<div class="result_image">
				{!!$str_val!!}
			</div>
		</div>
		@if($helper_text != '')<p class="help-block">{!! $helper_text??'' !!}</p>@endif
	</div>
</div>