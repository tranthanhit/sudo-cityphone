{{-- 
	@include('admin.layouts.form.action',[
		'type' => 'text',
		'preview' => 'text',
		'table_name' => 'text',
	])
 --}}
@switch($type)

    @case('edit')
        <div class="form-actions">
			<button type="submit" name="redirect" value="edit" class="btn btn-success">Lưu lại</button>&nbsp;
			<button type="submit" name="redirect" value="index" class="btn btn-info">Lưu lại & thoát</button>&nbsp;
			@if($value['preview'] != '')
				<a href="{!! $value['preview'] !!}" target="_blank" class="btn btn-primary"><i class="fa fa-eye"></i> Xem</a>
			@endif
			<button type="button" onclick="window.location='{!! route($table_name.'.index') !!}'" class="btn btn-danger">Thoát</button>&nbsp;
		</div>
    @break

    @case('editconfig') 
		<div class="form-actions">
			@if($value['preview'] != '')
				<a href="{!! $value['preview'] !!}" target="_blank" class="btn btn-primary"><i class="fa fa-eye"></i> Xem</a>
			@endif
			<button type="submit" name="submit" id="submit-form" value="edit" class="btn btn-success">Lưu cấu hình</button>&nbsp;
		</div>
    @break

    @case('add')
		<div class="form-actions">
			<button type="submit" name="redirect" value="edit" class="btn btn-success">Thêm mới</button>&nbsp;
			<button type="submit" name="redirect" value="index" class="btn btn-info">Thêm & thoát</button>&nbsp;
			<button type="reset" class="btn">Nhập lại</button>&nbsp;
			<button type="button" onclick="window.location='{!! route($table_name.'.index') !!}'" class="btn btn-danger">Thoát</button>&nbsp;
		</div>
    @break

@endswitch
