{{-- 
	@include('admin.layouts.form.table.1_image_1_input',[
		'full' => 'false',
		'name' => $name.'[list]',
		'slug' => $name.'_list',
		'value' => $data[$name]['list']??'',
		'label' => 'Danh sách (tối đa 4)',
		'placeholder_input' => 'Tiêu đề',
	])
--}}
<div class="form-group">
	@if ($full == 'false')
	<label class="control-label col-md-2 col-sm-2 col-xs-12"> {{__($label??'Box')}}</label>
	<div class="controls col-md-9 col-sm-10 col-xs-12">
	@else 
	<div class="controls col-md-12 col-sm-12 col-xs-12">
	@endif
		<table class="module_table_option has_image" border="1">
			<thead>
				<tr>
					@if ($full == 'false')
						<th colspan="4"><button type="button" class="btn-add add_{{$slug}}"><i class="fa fa-plus"></i></button></th>
					@else
						<td colspan="3"  style="padding: 0 10px;">{{__($label??'Box')}}</td>
						<th style="width: 30px;"><button type="button" class="btn-add add_{{$slug}}"><i class="fa fa-plus"></i></button></th>
					@endif
				</tr>
			</thead>
			<tbody>
				@if (isset($value['image']) && count($value['image']) > 0)
					@for ($i = 0; $i < count($value['image']); $i++)
						<tr draggable="true">
				            <td class="module_table_image">
								<div class="form-group">
									<a class="btn btn-primary {{$slug}}_image" href="javascript:;" onclick="media_popup('add','single','{{$slug}}_image_{{$i}}','{!! __('Ảnh') !!}');" title="{{__('Anh có kích thước')}} 168x550">Ảnh</a>
									<img src="{{$value['image'][$i]??''}}" class="thumb-img" alt="">
									<input id="{{$slug}}_image_{{$i}}" type="hidden" name="{{$name}}[image][]" value="{{$value['image'][$i]??''}}">
								 </div>
							</td>
							<td>
								<textarea name="{{$name}}[text][]" class="form-control" placeholder="{{__($placeholder_input??'')}}">{{$value['text'][$i]??''}}</textarea>
							</td>
				            <td><button type="button" class="btn-delete delete_option"><i class="fa fa-trash-o"></i></button></td>
				        </tr>
					@endfor
				@endif
			</tbody>
		</table>
	</div>
</div>

<script>
$(document).ready(function() {
	$(document).ready(function() {
		$('.module_table_option').on('click','.delete_option',function(e) {
	        e.preventDefault();
	        $(this).closest('tr').remove();
	    });
	});
	window.addEventListener('DOMContentLoaded', (event) => {
		$('.module_table_option tbody').sortable();
	});
	$('body').on('click','.add_{{$slug}}',function() {
		{{$slug}}_image_number = $('.{{$slug}}_image').length;
		{{$slug}}_content = `
			<td class="module_table_image">
				<div class="form-group">
					<a class="btn btn-primary {{$slug}}_image" href="javascript:;" onclick="media_popup('add','single','{{$slug}}_image_${ {{$slug}}_image_number }','{!! __('Ảnh') !!}');" title="{{__('Anh có kích thước')}} 168x550">Ảnh</a>
					<img src="" class="thumb-img" alt="">
					<input id="{{$slug}}_image_${ {{$slug}}_image_number }" type="hidden" name="{{$name}}[image][]" value="">
				 </div>
			</td>
			<td>
				<textarea name="{{$name}}[text][]" class="form-control" placeholder="{{__($placeholder_input??'')}}" ></textarea>
			</td>
		`;
		$(this).closest('table').find('tbody').append(`
			<tr draggable="true">
	            ${ {{$slug}}_content }
	            <td><button type="button" class="btn-delete delete_option"><i class="fa fa-trash-o"></i></button></td>
	        </tr>
		`);
		$('.module_table_option tbody').sortable();
	});
});
</script>