{{-- 
	@include('admin.layouts.form.table.2_image_1_input',[
		'full' => 'false',
		'name' => $name.'[list]',
		'slug' => $name.'_list',
		'value' => $data[$name]['list']??'',
		'label' => 'Danh sách (tối đa 4)',
		'placeholder_input' => 'Tiêu đề',
	])
--}}
<div class="form-group">
	@if ($full == 'false')
	<label class="control-label col-md-2 col-sm-2 col-xs-12"> {{__($label??'Box')}}</label>
	<div class="controls col-md-9 col-sm-10 col-xs-12">
	@else 
	<div class="controls col-md-12 col-sm-12 col-xs-12">
	@endif
		<table class="module_table_option has_image" border="1">
			<thead>
				<tr>
					@if ($full == 'false')
						<th colspan="4"><button type="button" class="btn-add add_{{$slug}}"><i class="fa fa-plus"></i></button></th>
					@else
						<td colspan="3"  style="padding: 0 10px;">{{__($label??'Box')}}</td>
						<th style="width: 30px;"><button type="button" class="btn-add add_{{$slug}}"><i class="fa fa-plus"></i></button></th>
					@endif
				</tr>
			</thead>
			<tbody>
				@if (isset($data[$name]['text']) && count($data[$name]['text']) > 0)
					@for ($i = 0; $i < count($data[$name]['text']); $i++)
						<tr>
							<td class="module_table_image">
								<div class="form-group">
									<a class="btn btn-primary {{$slug}}_pc_image" href="javascript:;" onclick="media_popup('add','single','{{$slug}}_pc_image_{{$i}}','{!! __('Ảnh') !!}');" title="Ảnh cho PC">Ảnh</a>
									<img src="{{getImage($data[$name]['image']['pc'][$i]??'')}}" class="thumb-img" alt="">
									<input id="{{$slug}}_pc_image_{{$i}}" type="hidden" name="{{$name}}[image][pc][]" value="{{$data[$name]['image']['pc'][$i]??''}}">
								 </div>
							</td>
							<td class="module_table_image">
								<div class="form-group">
									<a class="btn btn-primary {{$slug}}_mobile_image" href="javascript:;" onclick="media_popup('add','single','{{$slug}}_mobile_image_{{$i}}','{!! __('Ảnh') !!}');" title="Ảnh cho điện thoại">Ảnh</a>
									<img src="{{getImage($data[$name]['image']['mobile'][$i]??'')}}" class="thumb-img" alt="">
									<input id="{{$slug}}_mobile_image_{{$i}}" type="hidden" name="{{$name}}[image][mobile][]" value="{{$data[$name]['image']['mobile'][$i]??''}}">
								 </div>
							</td>
							<td>
								<textarea rows="2" name="{{$name}}[text][]" id="{{$slug}}[text][]" class="form-control" placeholder="{{$placeholder_input??''}}">{{$data[$name]['text'][$i]??''}}</textarea>
							</td>
							<td><button type="button" class="btn-delete delete_option"><i class="fa fa-trash-o"></i></button></td>
						</tr>
					@endfor
				@endif

			</tbody>
		</table>
	</div>
</div>

<script>
$(document).ready(function() {
	$('body').on('click','.add_{{$slug}}',function() {
		{{$slug}}_number = $('.{{$slug}}_pc_image').length;
		template_{{$slug}} = `
			<td class="module_table_image">
				<div class="form-group">
					<a class="btn btn-primary {{$slug}}_pc_image" href="javascript:;" onclick="media_popup('add','single','{{$slug}}_pc_image_${ {{$slug}}_number }','{!! __('Ảnh') !!}');" title="Ảnh cho PC">Ảnh</a>
					<img src="" class="thumb-img" alt="">
					<input id="{{$slug}}_pc_image_${ {{$slug}}_number }" type="hidden" name="{{$name}}[image][pc][]" value="">
				 </div>
			</td>
			<td class="module_table_image">
				<div class="form-group">
					<a class="btn btn-primary {{$slug}}_mobile_image" href="javascript:;" onclick="media_popup('add','single','{{$slug}}_mobile_image_${ {{$slug}}_number }','{!! __('Ảnh') !!}');" title="Ảnh cho điện thoại">Ảnh</a>
					<img src="" class="thumb-img" alt="">
					<input id="{{$slug}}_mobile_image_${ {{$slug}}_number }" type="hidden" name="{{$name}}[image][mobile][]" value="">
				 </div>
			</td>
			<td>
				<textarea rows="2" name="{{$name}}[text][]" id="{{$slug}}[text][]" class="form-control" placeholder="{{$placeholder_input??''}}"></textarea>
			</td>
		`;
		$(this).closest('.module_table_option').children('tbody').append(`
            <tr draggable="true">
                ${ template_{{$slug}} }
                <td><button type="button" class="btn-delete delete_option"><i class="fa fa-trash-o"></i></button></td>
            </tr>
        `);
		$('.module_table_option tbody').sortable();
	});
});
</script>