<div class="form-group">
	<label class="control-label col-md-2 col-sm-2 col-xs-12">Forum - Hỗ trợ phần mềm</label>
	<div class="controls col-md-9 col-sm-10 col-xs-12">
		<a style="margin-bottom: 20px;" id="add_forum" class="btn btn-primary btn-xs"><i class="fa fa-plus" aria-hidden="true"></i> Thêm Link</a>
		<div id="result_forum">
            @if(isset($forums))
            @foreach($forums as $key => $val)
			<div class="row forum_item" style="padding: 10px;padding-left:0;background: #ddd;margin-bottom:5px;position:relative;">
				<div class="col-xs-12">
					<input type="text" class="form-control" name="forums[{{$key}}][title]" value="{{$val->title}}" placeholder="Tiêu đề, vd: Rom quốc tế Android 7.0 Galaxy S7 Mỹ G930U">
				</div>
				<div class="col-xs-12">
					<input type="text" class="form-control" name="forums[{{$key}}][link]" value="{{$val->link}}" placeholder="Đường dẫn,">
				</div>
				<a href="" class="remove_forum" style="position: absolute;top: 0;right:0;font-size: 20px;color: red;"><i class="fa fa-times" aria-hidden="true"></i></a>
			</div>
            @endforeach
            @endif
		</div>
	</div>

</div>

<script type="text/javascript">
	$(document).ready(function() {
			@if(isset($forums))
            var i = {{count((array)$forums)}};
            @else
            var i = 0;
            @endif
            $("#add_forum").click(function(event) {
                event.preventDefault();
                i++;
                var forum_item =
                '<div class="row forum_item" style="padding: 10px;padding-left:0;background: #ddd;margin-bottom:5px;position:relative;">' + 
                '<div class="col-xs-12">'+
                '<input type="text" class="form-control" name="forums['+ i +'][title]" value="" placeholder="Tiêu đề, vd: Rom quốc tế Android 7.0 Galaxy S7 Mỹ G930U">'+
                '</div>'+
                '<div class="col-xs-12">'+
                '<input type="text" class="form-control" name="forums['+ i +'][link]" value="" placeholder="Đường dẫn, ">'+
                '</div>'+
                '<a href="" class="remove_forum" style="position: absolute;top: 0;right:0;font-size: 20px;color: red;"><i class="fa fa-times" aria-hidden="true"></i></a>'+
                '</div>';
                $(this).closest('.controls').find("#result_forum").append(forum_item);
            });
            $("#result_forum").on('click', '.remove_forum', function(event) {
                event.preventDefault();
                $(this).closest('.forum_item').remove();
            });
            
	});
</script>