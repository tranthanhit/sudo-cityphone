<div class="form-group">
	<label class="control-label col-md-2 col-sm-2 col-xs-12">Tùy biến chế độ bảo hành</label>
	<div class="controls col-md-9 col-sm-10 col-xs-12">
		<a style="margin-bottom: 20px;" id="add_warranty" class="btn btn-primary btn-xs"><i class="fa fa-plus" aria-hidden="true"></i> Thêm bảo hành</a>
		<div id="result_warranty">
            @if(isset($warrantys))
            @foreach($warrantys as $key => $val)
			<div class="row warranty_item" style="padding: 10px;padding-left:0;border:1px solid #ccc;margin-bottom:5px;position:relative;">
				<div class="col-xs-3">
					<input type="text" class="form-control" name="warrantys[{{$key}}][name]" value="{{$val->name}}" placeholder="Tên chế độ bảo hành">
				</div>
				<div class="col-xs-3">
					<input type="text" class="form-control" name="warrantys[{{$key}}][price]" value="{{$val->price}}" placeholder="Giá thêm VD: 300000">
				</div>
				<a href="" class="btn btn-danger remove_warranty btn-xs" style="position: absolute;top: 15px;font-size: 14px;"><i class="fa fa-times" aria-hidden="true"></i> Xóa </a>
			</div>
            @endforeach
            @endif
		</div>
	</div>

</div>

<script type="text/javascript">
	$(document).ready(function() {
            @if(isset($warrantys))
            var i = {{count((array)$warrantys)}};
            @else
            var i = 0;
            @endif
            $("#add_warranty").click(function(event) {
                event.preventDefault();
                i++;
                var warranty_item =
                '<div class="row warranty_item" style="padding: 10px;padding-left:0;border:1px solid #ccc;;margin-bottom:5px;position:relative;">' + 
                '<div class="col-xs-3">'+
                '<input type="text" class="form-control" name="warrantys['+ i +'][name]" value="" placeholder="Tên chế độ bảo hành">'+
                '</div>'+
                '<div class="col-xs-3">'+
                '<input type="text" class="form-control" name="warrantys['+ i +'][price]" value="" placeholder="Giá thêm VD: 300000">'+
                '</div>'+
                '<a href="" class="btn btn-danger btn-xs remove_warranty" style="position: absolute;top: 15px;font-size: 14px;"><i class="fa fa-times" aria-hidden="true"></i> Xóa</a>'+
                '</div>';
                $(this).closest('.controls').find("#result_warranty").append(warranty_item);
            });
            $("#result_warranty").on('click', '.remove_warranty', function(event) {
                event.preventDefault();
                $(this).closest('.warranty_item').remove();
            });
            
	});
</script>