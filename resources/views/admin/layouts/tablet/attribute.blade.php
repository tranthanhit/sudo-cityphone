
<div class="alert alert-info" data-toggle="collapse" data-target="#attribute">
	<strong>Thêm thuộc tính cho máy</strong>
</div>
<div id="attribute" class="collapse" style="height: auto;">
	{{-- <div style="text-align: center;font-size: 16px;color: red;">Phải có ít nhất 1 Màu, Dung Lượng và Tình trạng để có thể tạo bảng giá. Tình trạng có thể để trống</div> --}}

	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Màu - Hình ảnh</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
	      	<a style="margin-bottom: 20px;" id="add_color" class="btn btn-primary btn-xs"><i class="fa fa-plus" aria-hidden="true"></i> Thêm </a>
	      	<div id="result_color">
	      		@if(isset($color))
	      		@php
	      		$k = 0;
	      		@endphp
	      		@foreach($color as $key => $value)
	      		@php
	      		$color_code = App\Phone_attribute::where('color_name',$value)->where('phone_id',$id)->first();
	      		$k++;
	      		@endphp
	    		<div class="row color_item" style="padding: 10px;border:1px solid #ccc;margin-bottom: 5px;">
	    			<div class="col-xs-2">
	    				<input type="text" class="form-control tags_color_name" name="color_name[]" value="{{$value["color_name"]}}" placeholder="Màu - VD: Đen">
	    			</div>
	    			<div class="col-xs-2">
	    				<input type="color" class="form-control tags_color_code" name="color_code[]" value="{{$value["color_code"]}}">
	    			</div>
	    			
	    			<div class="col-xs-2" style="text-align: center;">
	    				
	    				<img style="width: 50px;" src="{{$value["color_image"]}}" alt="">
	    				
	    				<a href="javascript:;" class="btn btn-xs btn-success add_color_image" data-id="{{$k}}" onclick="media_popup('add','single','color_image_{{$k}}','Chèn ảnh')">Hình ảnh</a>
	    				<input type="hidden" name="color_image[]" id="color_image_{{$k}}" value="{{$value["color_image"]}}">
	    			</div>
	    			<div class="col-xs-2">
	    				<input type="number" class="form-control number" name="color_price[]" value="{{$value["price"]}}" placeholder="Giá chênh lệch">
	    				<p class="show_number">{{number_format($value["price"])}}</p>
	    			</div>
	    			<div class="col-md-2" style="text-align: center;">
	    				<a class="btn btn-sm btn-danger remove_color" href="" style=""><i class="fa fa-times" aria-hidden="true"> Xóa</i></a>
	    			</div>
	    		</div>
	    		@endforeach
	    		@endif
	    	</div>
	    </div>
	    
	</div>
	
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Dung lượng </label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
	      	<a style="margin-bottom: 20px;" id="add_storage" class="btn btn-primary btn-xs"><i class="fa fa-plus" aria-hidden="true"></i> Thêm </a>
	      	<div id="result_storage">
	    		<div class="row">
	      		@if(isset($storage))
	      		@foreach($storage as $key => $value)
	    			<div class="col-xs-3 storage_item" style="margin-bottom: 20px;">
						<input type="text" class="form-control" name="storage_name[]" value="{{$value["storage_name"]}}" placeholder="VD: 16GB">
						<input type="number" class="form-control number" name="storage_price[]" value="{{$value["price"]}}" placeholder="Giá chênh lệch">
						<p class="show_number">{{number_format($value["price"])}}</p>
						<a href="" class="remove_storage btn btn-xs btn-danger" style=""><i class="fa fa-times" aria-hidden="true"> Xóa</i></a>
					</div>
	    		@endforeach
	    		@endif
	    		</div>
	    	</div>
	    </div>
	    
	</div>

	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Tình trạng </label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
	      	<a style="margin-bottom: 20px;" id="add_tt" class="btn btn-primary btn-xs"><i class="fa fa-plus" aria-hidden="true"></i> Thêm </a>
	      	<div id="result_tt">
	    		<div class="row">
	      		@if(isset($aspect))
	      		@foreach($aspect as $key => $value)
	    			<div class="col-xs-3 tt_item" style="margin-bottom: 20px;">
						<input type="text" class="form-control" name="tt_name[]" value="{{$value["tt_name"]}}" placeholder="VD: 99%">
						<input type="number" class="form-control number" name="tt_price[]" value="{{$value["price"]}}" placeholder="Giá chênh lệch">
						<p class="show_number">{{number_format($value["price"])}}</p>
						<a href="" class="remove_tt btn btn-xs btn-danger" style=""><i class="fa fa-times" aria-hidden="true"> Xóa</i></a>
					</div>
	    		@endforeach
	    		@endif
	    		</div>
	    	</div>
	    </div>
	    
	</div>
	
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Ram </label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
	      	<a style="margin-bottom: 20px;" id="add_ram" class="btn btn-primary btn-xs"><i class="fa fa-plus" aria-hidden="true"></i> Thêm </a>
	      	<div id="result_ram">
	    		<div class="row">
	      		@if(isset($ram))
	      		@foreach($ram as $key => $value)
	    			<div class="col-xs-3 ram_item" style="margin-bottom: 20px;">
						<input type="text" class="form-control" name="ram_name[]" value="{{$value["ram_name"]}}" placeholder="VD: 2GB">
						<input type="number" class="form-control number" name="ram_price[]" value="{{$value["price"]}}" placeholder="Giá chênh lệch">
						<p class="show_number">{{number_format($value["price"])}}</p>
						<a href="" class="remove_ram btn btn-xs btn-danger" style=""><i class="fa fa-times" aria-hidden="true"> Xóa</i></a>
					</div>
	    		@endforeach
	    		@endif
	    		</div>
	    	</div>
	    </div>
	    
	</div>
	
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Xuất Xứ </label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
	      	<a style="margin-bottom: 20px;" id="add_country" class="btn btn-primary btn-xs"><i class="fa fa-plus" aria-hidden="true"></i> Thêm </a>
	      	<div id="result_country">
	    		<div class="row">
	      		@if(isset($country))
	      		@foreach($country as $key => $value)
	    			<div class="col-xs-3 country_item" style="margin-bottom: 20px;">
						<input type="text" class="form-control" name="country_name[]" value="{{$value["country_name"]}}" placeholder="VD: Mỹ">
						<input type="number" class="form-control number" name="country_price[]" value="{{$value["price"]}}" placeholder="Giá chênh lệch">
						<p class="show_number">{{number_format($value["price"])}}</p>
						<a href="" class="remove_country btn btn-xs btn-danger" style=""><i class="fa fa-times" aria-hidden="true"> Xóa</i></a>
					</div>
	    		@endforeach
	    		@endif
	    		</div>
	    	</div>
	    </div>
	    
	</div>
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Xem bảng giá</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
	      	<a style="margin-bottom: 20px;" id="add_price" class="btn btn-primary btn-sm"> Xem bảng giá </a>
	      	<a style="margin-bottom: 20px;" id="hide_table_price" class="btn btn-primary btn-sm"> Ẩn bảng giá </a>
	      	<div id="result_price">
	    		<table class="table table-hover table-striped">
	    			<thead>
	    				<tr>
	    					<th>STT</th>
	    					<th>Màu</th>
	    					<th>Mã màu</th>
	    					<th>Hình ảnh</th>
	    					<th>Dung lượng</th>
	    					<th>Tình trạng máy</th>
	    					<th>RAM</th>
	    					<th>Xuất Xứ</th>
	    					<th>Giá</th>
	    				</tr>
	    			</thead>
	    			<tbody>
	    				
	    			</tbody>
	    		</table>
	    	</div>
	    </div>
	    
	</div>
	
</div>

<script type="text/javascript">
		
	    function price_format(number){
	    	var number = number,
			thousand_separator = ',';

			var	reverse   = number.toString().split('').reverse().join(''),
			thousands = reverse.match(/\d{1,3}/g);
			result 	  = thousands.join(thousand_separator).split('').reverse().join('');
			return result;
	    }

		$(document).ready(function() {
		
		$("#hide_table_price").click(function(event) {
			$(".tr_product_item").remove();
		});
		$("#add_price").click(function(event) {
			// color

			var price_basic = ($('input[name="price"]').val() === "")?0:$('input[name="price"]').val();
			var color_name = $('input[name="color_name[]"]');
			var color_code = $('input[name="color_code[]"]');
			var color_image = $('input[name="color_image[]"]');
			var color_price = $('input[name="color_price[]"]');
			console.log(price_basic);
			var color_array = [];
			for(var i = 0; i < color_name.length; i++){
				// color_array.push([ color_name[i]["value"],color_code[i]["value"],color_image[i]["value"],color_price[i]["value"] ]);
				color_array.push({
					"color_name":color_name[i]["value"],
					"color_code":color_code[i]["value"],
					"color_image":color_image[i]["value"],
					"price":(color_price[i]["value"] != "" && color_price[i]["value"] > 0)?color_price[i]["value"]:0

				})
			}
			
			// Dung luong

			var storage_name = $('input[name="storage_name[]"]');
			var storage_price = $('input[name="storage_price[]"]');

			var storage_array = [];

			for(var i = 0; i < storage_name.length; i++){
				// storage_array.push([ storage_name[i]["value"],storage_price[i]["value"] ]);
				storage_array.push({
					"storage":storage_name[i]["value"],
					"price":(storage_price[i]["value"] != "" && storage_price[i]["value"] > 0)?storage_price[i]["value"]:0
				});
			}

			// Tinh trang

			var tt_name = $('input[name="tt_name[]"]');
			var tt_price = $('input[name="tt_price[]"]');

			var tt_array = [];

			for(var i = 0; i < tt_name.length; i++){
				// storage_array.push([ storage_name[i]["value"],storage_price[i]["value"] ]);
				tt_array.push({
					"tt":tt_name[i]["value"],
					"price":(tt_price[i]["value"] != "" && tt_price[i]["value"] > 0)?tt_price[i]["value"]:0
				});
			}

			// Ram

			var ram_name = $('input[name="ram_name[]"]');
			var ram_price = $('input[name="ram_price[]"]');

			var ram_array = [];

			for(var i = 0; i < ram_name.length; i++){
				// storage_array.push([ storage_name[i]["value"],storage_price[i]["value"] ]);
				ram_array.push({
					"ram":ram_name[i]["value"],
					"price":(ram_price[i]["value"] != "" && ram_price[i]["value"] > 0)?ram_price[i]["value"]:0
				});
			}


			// Country

			var country_name = $('input[name="country_name[]"]');
			var country_price = $('input[name="country_price[]"]');

			var country_array = [];

			for(var i = 0; i < country_name.length; i++){
				// storage_array.push([ storage_name[i]["value"],storage_price[i]["value"] ]);
				country_array.push({
					"country":country_name[i]["value"],
					"price":(country_price[i]["value"] != "" && country_price[i]["value"] > 0)?country_price[i]["value"]:0
				});
			}

			var data_attribute = [];
			if(color_array.length > 0){
				data_attribute = color_array;
			}
			if(storage_array.length > 0){
				var test1 = [];
				$.map(storage_array, function(value, index) {
					test1.push([value,data_attribute]);
				});
				var all1 = [];
				$.map(test1, function(value, key) {
					$.map(value[1], function(val, k) {
						var new_price  = parseInt(value[0].price) + parseInt(val.price);
						var obj = $.extend(false, value[0], val);
						obj.price = new_price;
						all1.push(obj);
					});
				});
				data_attribute = all1;
			}
			if(tt_array.length > 0){
				var test2 = [];
				$.map(tt_array, function(value, index) {
					test2.push([value,data_attribute]);
				});
				var all2 = [];
				$.map(test2, function(value, key) {
					$.map(value[1], function(val, k) {
						var new_price  = parseInt(value[0].price) + parseInt(val.price);
						var obj = $.extend(false, value[0], val);
						obj.price = new_price;
						all2.push(obj);
					});
				});
				data_attribute = all2;
			}
			if(ram_array.length > 0){
				var test3 = [];
				$.map(ram_array, function(value, index) {
					test3.push([value,data_attribute]);
				});
				var all3 = [];
				$.map(test3, function(value, key) {
					$.map(value[1], function(val, k) {
						var new_price  = parseInt(value[0].price) + parseInt(val.price);
						var obj = $.extend(false, value[0], val);
						obj.price = new_price;
						all3.push(obj);
					});
				});
				data_attribute = all3;
			}
			if(country_array.length > 0){
				var test4 = [];
				$.map(country_array, function(value, index) {
					test4.push([value,data_attribute]);
				});
				var all4 = [];
				$.map(test4, function(value, key) {
					$.map(value[1], function(val, k) {
						var new_price  = parseInt(value[0].price) + parseInt(val.price);
						var obj = $.extend(false, value[0], val);
						obj.price = new_price;
						all4.push(obj);
					});
				});
				data_attribute = all4;
			}
			// console.log(data_attribute);
			$(".tr_product_item").remove();
			$.map(data_attribute, function(item, index) {
				var stt = index + 1;
				var color_name = (item["color_name"] != undefined)?item["color_name"]:"";
				var color_code = (item["color_code"] != undefined)?item["color_code"]:"";
				var color_image = (item["color_image"] != undefined)?item["color_image"]:"";
				var storage = (item["storage"] != undefined)?item["storage"]:"";
				var aspect = (item["tt"] != undefined)?item["tt"]:"";
				var country = (item["country"] != undefined)?item["country"]:"";
				var ram = (item["ram"] != undefined)?item["ram"]:"";
				var price = (item["price"] != undefined)?item["price"]:0
				var price = parseInt(price) + parseInt(price_basic);
				var price = price_format(price);
				var tr_item = 
				'<tr class="tr_product_item">'+
				'<td>'+stt+'</td>'+
				'<td>'+color_name+'</td>'+
				'<td>'+color_code+'</td>'+
				'<td><img src="'+color_image+'" alt="" style="width:50px;"></td>'+
				'<td>'+storage+'</td>'+
				'<td>'+aspect+'</td>'+
				'<td>'+ram+'</td>'+
				'<td>'+country+'</td>'+
				'<td>'+price+'</td>';
				$("#result_price").find('tbody').append(tr_item);
			});
		});
		
		@if(isset($color))
		var i = {{count($color)}};
		@else
		var i = 0;
		@endif
		$("#add_color").click(function(event) {
			event.preventDefault();
			// i++;
			// var color_item = 
			// '<div class="row color_item" style="padding: 10px;border:1px solid #ccc;margin-bottom: 5px;">' + 
			// '<div class="col-xs-2">' + 
			// '<input type="text" class="form-control tags_color_name" name="color_name[]" value="" placeholder="Màu - VD: Đen">' +
			// '</div>' + 
			// '<div class="col-xs-2">' +
			// '<input type="color" class="form-control tags_color_code" name="color_code[]" value="">' + 
			// '</div>' + 
			// '<div class="col-xs-2" style="text-align: center;">' +
			// '<img style="width: 50px;" src="" alt="">' +
			// '<a href="javascript:;" class="btn btn-xs btn-success add_color_image" data-id="'+i+'">Hình ảnh</a>'+
			// '<input type="hidden" name="color_image[]" id="color_image_'+i+'">' +
			// '</div>' +
			// '<div class="col-xs-2">' +
			// '<input type="number" class="form-control number" name="color_price[]" placeholder="Giá chênh lệch" value="">' + 
			// '<p class="show_number">fdsa</p>'+
			// '</div>' + 
			// '<div class="col-md-2" style="text-align: center;">' +
			// '<a class="btn btn-sm btn-danger remove_color" href="" style=""><i class="fa fa-times" aria-hidden="true"> Xóa</i></a>' +
			// '</div>';

			// $(this).closest('.controls').find("#result_color").append(color_item);

		});
		
		
        $(".form-group").on('click', '#add_color', function(event) {
        	event.preventDefault();
        	i++;
			var color_item = 
			'<div class="row color_item" style="padding: 10px;border:1px solid #ccc;margin-bottom: 5px;">' + 
			'<div class="col-xs-2">' + 
			'<input type="text" class="form-control tags_color_name" name="color_name[]" value="" placeholder="Màu - VD: Đen">' +
			'</div>' + 
			'<div class="col-xs-2">' +
			'<input type="color" class="form-control tags_color_code" name="color_code[]" value="">' + 
			'</div>' + 
			'<div class="col-xs-2" style="text-align: center;">' +
			'<img style="width: 50px;" src="" alt="">' +
			'<a href="javascript:;" class="btn btn-xs btn-success add_color_image" data-id="'+i+'">Hình ảnh</a>'+
			'<input type="hidden" name="color_image[]" id="color_image_'+i+'">' +
			'</div>' +
			'<div class="col-xs-2">' +
			'<input type="number" class="form-control number" name="color_price[]" placeholder="Giá chênh lệch" value="">' + 
			'<p class="show_number"></p>'+
			'</div>' + 
			'<div class="col-md-2" style="text-align: center;">' +
			'<a class="btn btn-sm btn-danger remove_color" href="" style=""><i class="fa fa-times" aria-hidden="true"> Xóa</i></a>' +
			'</div>';

			$(this).closest('.controls').find("#result_color").append(color_item);
			$(".number").keyup(function(event) {
            	var price = $(this).val();
            	if(price != ""){
            		$(this).closest('.col-xs-2').find('.show_number').html(price_format(price));
            	}else{
            		$(this).closest('.col-xs-2').find('.show_number').html("");
            	}
	        });

        });
        $(".color_item .number").keyup(function(event) {
        	var price = $(this).val();
        	if(price != ""){
        		$(this).closest('.col-xs-2').find('.show_number').html(price_format(price));
        	}else{
        		$(this).closest('.col-xs-2').find('.show_number').html("");
        	}
        });
        $("#result_color").on('click', '.remove_color', function(event) {
            event.preventDefault();
            $(this).closest('.color_item').remove();
        });
        $("#result_color").on('click', '.add_color_image', function(event) {
            var id = $(this).data('id');
            $(this).attr({
            	'onclick': media_popup('add','single','color_image_'+id,'Chèn ảnh'),
            });
        });

		// tinh trang
			
			$(".form-group").on('click', '#add_tt', function(event) {
				event.preventDefault();
				var tt_item =
	            '<div class="col-xs-3 tt_item" style="margin-bottom: 20px;">'+
	            '<input type="text" class="form-control" name="tt_name[]" value="" placeholder="VD: 99%">'+
	            '<input type="number" class="form-control number" name="tt_price[]" value="" placeholder="Giá chênh lệch">'+
	            '<p class="show_number"></p>'+
	            '<a href="" class="remove_tt btn btn-xs btn-danger"><i class="fa fa-times" aria-hidden="true"></i> Xóa</a>'+
	            '</div>';
	            $(this).closest('.controls').find("#result_tt .row").append(tt_item);
	            $(".number").keyup(function(event) {
	            	var price = $(this).val();
	            	if(price != ""){
	            		$(this).closest('.tt_item').find('.show_number').html(price_format(price));
	            	}else{
	            		$(this).closest('.tt_item').find('.show_number').html("");
	            	}
		        });
			});
			$(".tt_item .number").keyup(function(event) {
            	var price = $(this).val();
            	if(price != ""){
            		$(this).closest('.tt_item').find('.show_number').html(price_format(price));
            	}else{
            		$(this).closest('.tt_item').find('.show_number').html("");
            	}
	        });
	        // $("#add_tt").click(function(event) {
	        //     event.preventDefault();
	        //     var tt_item =
	        //     '<div class="col-xs-3 tt_item" style="margin-bottom: 20px;">'+
	        //     '<input type="text" class="form-control" name="tt_name[]" value="" placeholder="VD: 99%">'+
	        //     '<input type="number" class="form-control zz" name="tt_price[]" value="" placeholder="Giá chênh lệch">'+
	        //     '<p class="show_number">fds</p>'+
	        //     '<a href="" class="remove_tt btn btn-xs btn-danger"><i class="fa fa-times" aria-hidden="true"></i> Xóa</a>'+
	        //     '</div>';
	        //     $(this).closest('.controls').find("#result_tt .row").append(tt_item);
	        // });
	        
	        $("#result_tt").on('click', '.remove_tt', function(event) {
	            event.preventDefault();
	            $(this).closest('.tt_item').remove();
	        });

	        // dung luong
	        // $("#add_storage").click(function(event) {
	        //     event.preventDefault();
	        //     var storage_item =
	        //     '<div class="col-xs-3 storage_item" style="margin-bottom: 20px;">'+
	        //     '<input type="text" class="form-control" name="storage_name[]" value="" placeholder="VD: 16GB">'+
	        //     '<input type="number" class="form-control" name="storage_price[]" value="" placeholder="Giá chênh lệch">'+
	        //     '<a href="" class="remove_storage btn btn-xs btn-danger"><i class="fa fa-times" aria-hidden="true"></i> Xóa</a>'+
	        //     '</div>';
	        //     $(this).closest('.controls').find("#result_storage .row").append(storage_item);
	        // });
	        $(".form-group").on('click', '#add_storage', function(event) {
				event.preventDefault();
				var storage_item =
	            '<div class="col-xs-3 storage_item" style="margin-bottom: 20px;">'+
	            '<input type="text" class="form-control" name="storage_name[]" value="" placeholder="VD: 16GB">'+
	            '<input type="number" class="form-control number" name="storage_price[]" value="" placeholder="Giá chênh lệch">'+
	            '<p class="show_number"></p>'+
	            '<a href="" class="remove_storage btn btn-xs btn-danger"><i class="fa fa-times" aria-hidden="true"></i> Xóa</a>'+
	            '</div>';
	            $(this).closest('.controls').find("#result_storage .row").append(storage_item);
	            $(".number").keyup(function(event) {
	            	var price = $(this).val();
	            	if(price != ""){
	            		$(this).closest('.storage_item').find('.show_number').html(price_format(price));
	            	}else{
	            		$(this).closest('.storage_item').find('.show_number').html("");
	            	}
	            	console.log(price);
		        });
			});
			$(".storage_item .number").keyup(function(event) {
            	var price = $(this).val();
            	if(price != ""){
            		$(this).closest('.storage_item').find('.show_number').html(price_format(price));
            	}else{
            		$(this).closest('.storage_item').find('.show_number').html("");
            	}
	        });
	        $("#result_storage").on('click', '.remove_storage', function(event) {
	            event.preventDefault();
	            $(this).closest('.storage_item').remove();
	        });

	        // Ram

	        // $("#add_ram").click(function(event) {
	        //     event.preventDefault();
	        //     var ram_item =
	        //     '<div class="col-xs-3 ram_item" style="margin-bottom: 20px">'+
	        //     '<input type="text" class="form-control" name="ram_name[]" value="" placeholder="VD: 2GB">'+
	        //     '<input type="number" pattern="[0-9]*" class="form-control" name="ram_price[]" value="" placeholder="Giá chênh lệch">'+
	        //     '<p class="show_number">fdas</p>'
	        //     '<a href="" class="remove_ram btn btn-xs btn-danger"><i class="fa fa-times" aria-hidden="true"></i> Xóa</a>'+
	        //     '</div>';
	        //     $(this).closest('.controls').find("#result_ram .row").append(ram_item);
	        // });
	        $(".form-group").on('click', '#add_ram', function(event) {
				event.preventDefault();
				var ram_item =
	            '<div class="col-xs-3 ram_item" style="margin-bottom: 20px">'+
	            '<input type="text" class="form-control" name="ram_name[]" value="" placeholder="VD: 2GB">'+
	            '<input type="number" pattern="[0-9]*" class="form-control number" name="ram_price[]" value="" placeholder="Giá chênh lệch">'+
	            '<p class="show_number"></p>'+
	            '<a href="" class="remove_ram btn btn-xs btn-danger"><i class="fa fa-times" aria-hidden="true"></i> Xóa</a>'+
	            '</div>';
	            $(this).closest('.controls').find("#result_ram .row").append(ram_item);
	            
	            $(".number").keyup(function(event) {
	            	var price = $(this).val();
	            	if(price != ""){
	            		$(this).closest('.ram_item').find('.show_number').html(price_format(price));
	            	}else{
	            		$(this).closest('.ram_item').find('.show_number').html("");
	            	}
		        });
			});
			$(".ram_item .number").keyup(function(event) {
            	var price = $(this).val();
            	if(price != ""){
            		$(this).closest('.ram_item').find('.show_number').html(price_format(price));
            	}else{
            		$(this).closest('.ram_item').find('.show_number').html("");
            	}
	        });
	        $("#result_ram").on('click', '.remove_ram', function(event) {
	            event.preventDefault();
	            $(this).closest('.ram_item').remove();
	        });

	        // country
	        // $("#add_country").click(function(event) {
	        //     event.preventDefault();
	        //     var country_item =
	        //     '<div class="col-xs-3 country_item" style="margin-bottom: 20px;">'+
	        //     '<input type="text" class="form-control" name="country_name[]" value="" placeholder="VD: Mỹ">'+
	        //     '<input type="number" class="form-control" name="country_price[]" value="" placeholder="Giá chênh lệch">'+
	        //     '<a href="" class="remove_country btn btn-xs btn-danger"><i class="fa fa-times" aria-hidden="true"></i> Xóa</a>'+
	        //     '</div>';
	        //     $(this).closest('.controls').find("#result_country .row").append(country_item);
	        // });
	        $(".form-group").on('click', '#add_country', function(event) {
				event.preventDefault();
				 var country_item =
	            '<div class="col-xs-3 country_item" style="margin-bottom: 20px;">'+
	            '<input type="text" class="form-control" name="country_name[]" value="" placeholder="VD: Mỹ">'+
	            '<input type="number" class="form-control number" name="country_price[]" value="" placeholder="Giá chênh lệch">'+
	            '<p class="show_number"></p>'+
	            '<a href="" class="remove_country btn btn-xs btn-danger"><i class="fa fa-times" aria-hidden="true"></i> Xóa</a>'+
	            '</div>';
	            $(this).closest('.controls').find("#result_country .row").append(country_item);
	            
	            $(".number").keyup(function(event) {
	            	var price = $(this).val();
	            	if(price != ""){
	            		$(this).closest('.country_item').find('.show_number').html(price_format(price));
	            	}else{
	            		$(this).closest('.country_item').find('.show_number').html("");
	            	}
		        });
			});
			$(".country_item .number").keyup(function(event) {
            	var price = $(this).val();
            	if(price != ""){
            		$(this).closest('.country_item').find('.show_number').html(price_format(price));
            	}else{
            		$(this).closest('.country_item').find('.show_number').html("");
            	}
	        });
	        $("#result_country").on('click', '.remove_country', function(event) {
	            event.preventDefault();
	            $(this).closest('.country_item').remove();
	        });

        
	});
	
</script>