@extends('admin.layouts.app')
@section('title')
    <h3>{{ $title }}</h3>
@endsection()
@section('content')
    @include('errors.error')

    @php
    $array_valid = [];
    foreach($data_form as $value) {
        if(isset($value['required']) && $value['required'] == 1) {
            $array_valid[] = $value['name'];
        }
    }
    $string_valid = '';
    if(count($array_valid) > 0) {
        $string_valid = 'onsubmit="validForm(this,\''.implode(',',$array_valid).'\');return false;"';
    }
    @endphp

    <form action="" class="form-horizontal form-label-left" enctype="multipart/form-data" method="post" {!! $string_valid !!}>

        @include('admin.layouts.form')
    </form>

@endsection()
@section('css')
<link rel="stylesheet" href="/template-admin/css/menu.css?v=1234">
@endsection
@section('script')
    <script src="/template-admin/js/jquery.nestable.js"></script>
    <script>
        function toggle_menu(classs, name){
            $('#'+name+' .add-menu-'+classs).slideToggle(400);
        }
        function add_menu(id){
            var link =$('#'+id+' .link-menu').val();
            var name =$('#'+id+' .name-menu').val();
            if ($('#'+id+' .taget-menu').is(":checked")){
                var taget = 1;
            }else{
                var taget = 0;
            }
            if ($('#'+id+' .rel-menu').is(":checked")){
                var rel = 1;
            }else{
                var rel = 0;
            }

            if(name == ''){
                alert('Tên menu không được để trống')
            }else{
                var html = '<li class="dd-item" data-name = "'+name+'" data-link = "'+link+'" data-table="" data-id="0" data-taget="'+taget+'" data-rel="'+rel+'"><div class="dd-handle">'+name+'</div></li>';
                $('#'+id+' .add img.img1').css('display','inline');
                setTimeout(function(){
                    $('#'+id+' .add img.img1').css('display','none');
                    $('#'+id+' .link-menu').val('');
                    $('#'+id+' .name-menu').val('');
                    $('#'+id+' #ol-first').append(html)
                },500);
            }
        }
        function add_menu1(id,table){
            var name =$('#'+id+' .name-menu-'+table).val();
            var id_table =$('#'+id+' .link-menu-'+table).val();
            var link =$('#'+id+' .link-menu-'+table).find(':selected').attr('data-slug');
            if ($('#'+id+' .taget-menu-'+table).is(":checked")){
                var taget = 1;
            }else{
                var taget = 0;
            }
            if ($('#'+id+' .rel-menu-'+table).is(":checked")){
                var rel = 1;
            }else{
                var rel = 0;
            }
            if(name == ''){
                alert('Tên menu không được để trống')
            }else{
                var html = '<li class="dd-item" data-name = "'+name+'" data-link = "'+link+'" data-table="'+table+'" data-id="'+id_table+'" data-taget="'+taget+'" data-rel="'+rel+'"><div class="dd-handle">'+name+'</div></li>';
                $('#'+id+' .add img.img1').css('display','inline');
                setTimeout(function(){
                    $('#'+id+' .add img.img1').css('display','none');
                    $('#'+id+' .link-menu').val('');
                    $('#'+id+' .name-menu').val('');
                    $('#'+id+' #ol-first').append(html)
                },500);
            }
        }
        function show_edit_menu(name, id){
            $('.edit-menu').css('display', 'none');
            var data_name = $('#'+name+' #li-'+id).attr('data-name');
            var data_link = $('#'+name+' #li-'+id).attr('data-link');
            var data_taget = $('#'+name+' #li-'+id).attr('data-taget');
            var data_rel = $('#'+name+' #li-'+id).attr('data-rel');
            if(data_taget == 0){
                $('#'+name+' .taget-menu-edit').prop('checked', false);
            }else{
                $('#'+name+' .taget-menu-edit').prop('checked', true);
            }
            if(data_rel == 0){
                $('#'+name+' .rel-menu-edit').prop('checked', false);
            }else{
                $('#'+name+' .rel-menu-edit').prop('checked', true);
            }
            $('#'+name+' .edit-menu-custom').fadeIn();
            $('#'+name+' .link-menu-edit').val(data_link);
            $('#'+name+' .name-menu-edit').val(data_name);
            $('#'+name+' .edit-menu-btn').val(id);
        }

        function show_edit_menu1(name,id,table, id_table){
            $('.edit-menu').css('display', 'none');
            var data_name = $('#'+name+' #li-'+id).attr('data-name');
            var data_taget = $('#'+name+' #li-'+id).attr('data-taget');
            var data_rel = $('#'+name+' #li-'+id).attr('data-rel');
            if(data_taget == 0){
                $('#'+name+' .taget-menu-edit-'+table).prop('checked', false);
            }else{
                $('#'+name+' .taget-menu-edit-'+table).prop('checked', true);
            }
            if(data_rel == 0){
                $('#'+name+' .rel-menu-edit-'+table).prop('checked', false);
            }else{
                $('#'+name+' .rel-menu-edit-'+table).prop('checked', true);
            }
            $('#'+name+' .edit-menu-'+table).fadeIn();
            $('#'+name+' .name-menu-edit-'+table).val(data_name);
            $('#'+name+' .link-menu-edit-'+table).val(id_table);
            $('#'+name+' .edit-menu-btn-'+table).val(id);
        }
        function update_menu(name){
            var link = $('#'+name+' .link-menu-edit').val();
            var data_name =$('#'+name+' .name-menu-edit').val();
            var id =$('#'+name+' .edit-menu-btn').val();

            if ($('#'+name+' .taget-menu-edit').is(":checked")){
                var taget = 1;
            }else{
                var taget = 0;
            }
            if ($('#'+name+' .rel-menu-edit').is(":checked")){
                var rel = 1;
            }else{
                var rel = 0;
            }

            if(data_name == ''){
                alert('Tên menu không được để trống')
            }else{
                $('.add img.img2').css('display','inline');
                setTimeout(function(){
                    $('.add img.img2').css('display','none');
                    $('#'+name+' #li-'+id).attr('data-name',data_name);
                    $('#'+name+' #li-'+id).attr('data-link',link);
                    $('#'+name+' #li-'+id).attr('data-taget',taget);
                    $('#'+name+' #li-'+id).attr('data-rel',rel);
                    $('#'+name+' #li-'+id+' .handle-'+id).html(data_name);
                    $('#'+name+' .edit-menu-custom').fadeOut();
                },500);
            }
        }
        function update_menu1(name, table){
            var data_name =$('#'+name+' .name-menu-edit-'+table).val();
            var id =$('#'+name+' .edit-menu-btn-'+table).val();
            var id_table =$('#'+name+' .link-menu-edit-'+table).val();
            var link =$('#'+name+' .link-menu-edit-'+table).find(':selected').attr('data-slug');
            if ($('#'+name+' .taget-menu-edit-'+table).is(":checked")){
                var taget = 1;
            }else{
                var taget = 0;
            }
            if ($('#'+name+' .rel-menu-edit-'+table).is(":checked")){
                var rel = 1;
            }else{
                var rel = 0;
            }

            if(data_name == ''){
                alert('Tên menu không được để trống')
            }else{
                $('.add img.img2').css('display','inline');
                setTimeout(function(){
                    $('.add img.img2').css('display','none');
                    $('#'+name+' #li-'+id).attr('data-name',data_name);
                    $('#'+name+' #li-'+id).attr('data-link',link);
                    $('#'+name+' #li-'+id).attr('data-id',id_table);
                    $('#'+name+' #li-'+id).attr('data-taget',taget);
                    $('#'+name+' #li-'+id).attr('data-rel',rel);
                    $('#'+name+' #li-'+id+' .handle-'+id).html(data_name);
                    $('#'+name+' .edit-menu-'+table).fadeOut();
                },500);
            }
        }
        function remove_menu(name,id){
            $('#'+name+' #li-'+id).remove();
        }
    </script>
@endsection