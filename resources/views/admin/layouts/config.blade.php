
<div class="alert alert-info" data-toggle="collapse" data-target="#ttc">
	<strong>Thông tin chung</strong>
</div>
<div id="ttc" class="collapse" style="height: auto;">
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Hệ điều hành</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
	      	<input class="form-control " type="text" name="general_os" id="general_os" value="{{(isset($phone)?$phone->general_os:"")}}" placeholder="Vd: IOS / Android 6.0 (Marshmallow) / ...">
	    </div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Ngôn ngữ</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
	      	<input class="form-control " type="text" name="general_language" id="general_language" value="{{(isset($phone)?$phone->general_language:"")}}" placeholder="Vd: Tiếng Việt, Tiếng Anh, Tiếng Nhật">
	    </div>
	</div>
</div>
<div class="alert alert-info" data-toggle="collapse" data-target="#display">
	<strong>Màn hình</strong>
</div>
<div id="display" class="collapse" style="height: auto;">
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Loại màn hình</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
	      	<input class="form-control " type="text" name="display_type" id="display_type" value="{{(isset($phone)?$phone->display_type:"")}}" placeholder="Vd: LED-backlit IPS LCD">
	    </div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Màu màn hình</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
	      	<input class="form-control " type="text" name="display_color" id="display_color" value="{{(isset($phone)?$phone->display_color:"")}}" placeholder="Vd: 16 triệu màu">
	    </div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Chuẩn màn hình</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
	      	<input class="form-control " type="text" name="display_standard" id="display_standard" value="{{(isset($phone)?$phone->display_standard:"")}}" placeholder="Vd: DVGA">
	    </div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Độ phân giải</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
	      	<input class="form-control " type="text" name="display_resolution" id="display_resolution" value="{{(isset($phone)?$phone->display_resolution:"")}}" placeholder="Vd: 640 x 1136 pixels">
	    </div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Màn hình rộng</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
	      	<input class="form-control " type="text" name="display_width" id="display_width" value="{{(isset($phone)?$phone->display_width:"")}}" placeholder="Vd: 5">
	    </div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Công nghệ cảm ứng</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
	      	<input class="form-control " type="text" name="display_induction" id="display_induction" value="{{(isset($phone)?$phone->display_induction:"")}}" placeholder="Vd: Cảm ứng điện dung đa điểm">
	    </div>
	</div>
</div>
<div class="alert alert-info" data-toggle="collapse" data-target="#camera">
	<strong>Chụp hình - Quay Phim</strong>
</div>
<div id="camera" class="collapse" style="height: auto;">
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Camera sau</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
	      	<input class="form-control " type="text" name="cam_primary" id="cam_primary" value="{{(isset($phone)?$phone->cam_primary:"")}}" placeholder="Vd: 16 MP">
	    </div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Camera trước</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
	      	<input class="form-control " type="text" name="cam_second" id="cam_second" value="{{(isset($phone)?$phone->cam_second:"")}}" placeholder="Vd: 2 MP">
	    </div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Đèn Flash</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
	      	<select name="cam_flash" id="cam_flash" class="form-control">
				<option value="1" @if(isset($phone) && $phone->cam_flash == 1) selected="selected"  @endif>Có</option>
				<option value="0" @if(isset($phone) && $phone->cam_flash == 0) selected="selected"  @endif>Không</option>
			</select>
	    </div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Tính năng camera</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
	      	<input class="form-control " type="text" name="cam_feature" id="cam_feature" value="{{(isset($phone)?$phone->cam_feature:"")}}" placeholder="Vd: Tự động lấy nét, chạm lấy nét. Nhận diện khuôn mặt, nụ cười. Chống rung">
	    </div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Quay phim</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
	      	<input class="form-control " type="text" name="cam_filming" id="cam_filming" value="{{(isset($phone)?$phone->cam_filming:"")}}" placeholder="Vd: Quay phim 4K 2160p@30fps">
	    </div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Video Call</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
	      	<select name="cam_videocall" id="cam_videocall" class="form-control">
				<option value="1"  @if(isset($phone) && $phone->cam_videocall == 1) selected="selected"  @endif>Có</option>
				<option value="0" @if(isset($phone) && $phone->cam_videocall == 0) selected="selected"  @endif>Không</option>
			</select>
	    </div>
	</div>
</div>
<div class="alert alert-info" data-toggle="collapse" data-target="#cpu">
	<strong>CPU - RAM</strong>
</div>
<div id="cpu" class="collapse" style="height: auto;">
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Tốc độ CPU</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
	      	<input class="form-control " type="text" name="cpuram_speed" id="cpuram_speed" value="{{(isset($phone)?$phone->cpuram_speed:"")}}" placeholder="Vd: Quad-core 1.3 GHz Cortex-A53 & 1.9GHz quad-core Cortex-A57">
	    </div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Số nhân</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
	      	<input class="form-control " type="text" name="cpuram_core" id="cpuram_core" value="{{(isset($phone)?$phone->cpuram_core:"")}}" placeholder="Vd: 8 nhân">
	    </div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Chipset</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
	      	<input class="form-control " type="text" name="cpuram_chipset" id="cpuram_chipset" value="{{(isset($phone)?$phone->cpuram_chipset:"")}}" placeholder="Vd: Exynos 5433">
	    </div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> RAM</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
	      	<input class="form-control " type="text" name="cpuram_ram" id="cpuram_ram" value="{{(isset($phone)?$phone->cpuram_ram:"")}}" placeholder="Vd: 2 GB">
	    </div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Chip đồ họa (GPU)</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
	      	<input class="form-control " type="text" name="cpuram_graphic" id="cpuram_graphic" value="{{(isset($phone)?$phone->cpuram_graphic:"")}}" placeholder="Vd: Mali-T760">
	    </div>
	</div>
</div>
<div class="alert alert-info" data-toggle="collapse" data-target="#storage">
	<strong>Bộ nhớ - Lưu trữ</strong>
</div>
<div id="storage" class="collapse" style="height: auto;">
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Danh bạ</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
	      	<input class="form-control " type="text" name="storage_contacts" id="storage_contacts" value="{{(isset($phone)?$phone->storage_contacts:"")}}" placeholder="Vd: Không giới hạn">
	    </div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Bộ nhớ trong(ROM)</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
	      	<input class="form-control " type="text" name="storage_rom" id="storage_rom" value="{{(isset($phone)?$phone->storage_rom:"")}}" placeholder="Vd: 32GB">
	    </div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Thẻ nhớ ngoài</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
	      	<input class="form-control " type="text" name="storage_memorycard" id="storage_memorycard" value="{{(isset($phone)?$phone->storage_memorycard:"")}}" placeholder="Vd: MicroSD (T-Flash)">
	    </div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Hỗ trợ thẻ tối đa</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
	      	<input class="form-control " type="text" name="storage_memorycardsup" id="storage_memorycardsup" value="{{(isset($phone)?$phone->storage_memorycardsup:"")}}" placeholder="Vd: 128 GB">
	    </div>
	</div>
</div>
<div class="alert alert-info" data-toggle="collapse" data-target="#design">
	<strong>Thiết Kế - Trọng Lượng</strong>
</div>
<div id="design" class="collapse" style="height: auto;">
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Kiểu dáng</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
	      	<input class="form-control " type="text" name="design_style" id="design_style" value="{{(isset($phone)?$phone->design_style:"")}}" placeholder="Vd: Thanh + Cảm ứng">
	    </div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Kích thước</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
	      	<input class="form-control " type="text" name="design_size" id="design_size" value="{{(isset($phone)?$phone->design_size:"")}}" placeholder="Vd: 153.5 x 78.6 x 8.5 mm">
	    </div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Trọng lượng(g)</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
	      	<input class="form-control " type="text" name="design_weight" id="design_weight" value="{{(isset($phone)?$phone->design_weight:"")}}" placeholder="Vd: 100">
	    </div>
	</div>
</div>
<div class="alert alert-info" data-toggle="collapse" data-target="#pin">
	<strong>PIN</strong>
</div>
<div id="pin" class="collapse" style="height: auto;">
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Loại</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
	      	<input class="form-control " type="text" name="pin_type" id="pin_type" value="{{(isset($phone)?$phone->pin_type:"")}}" placeholder="Vd: Pin chuẩn Li-Ion">
	    </div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Dung lượng</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
	      	<input class="form-control " type="text" name="pin_capacity" id="pin_capacity" value="{{(isset($phone)?$phone->pin_capacity:"")}}" placeholder="Vd: 3220mAh">
	    </div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Có thể tháo rời</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
	      	<select name="pin_removable" id="pin_removable" class="form-control">
				<option value="1" @if(isset($phone) && $phone->pin_removable == 1) selected="selected" @endif>Có</option>
				<option value="0" @if(isset($phone) && $phone->pin_removable == 0) selected="selected" @endif>Không</option>
			</select>
	    </div>
	</div>
</div>
<div class="alert alert-info" data-toggle="collapse" data-target="#kn">
	<strong>Kết Nối</strong>
</div>
<div id="kn" class="collapse" style="height: auto;">
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> 3G</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
	      	<input class="form-control " type="text" name="connect_3g" id="connect_3g" value="{{(isset($phone)?$phone->connect_3g:"")}}" placeholder="Vd: HSDPA, 42 Mbps; HSUPA, 5.76 Mbps">
	    </div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> 4G</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
	      	<input class="form-control " type="text" name="connect_4g" id="connect_4g" value="{{(isset($phone)?$phone->connect_4g:"")}}" placeholder="Vd: Có">
	    </div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Loại sim</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
	      	<input class="form-control " type="text" name="connect_simtype" id="connect_simtype" value="{{(isset($phone)?$phone->connect_simtype:"")}}" placeholder="Vd: Micro SIM">
	    </div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> 2 sim 2 sóng</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
	      	<input class="form-control " type="text" name="connect_duasim" id="connect_duasim" value="{{(isset($phone)?$phone->connect_duasim:"")}}" placeholder="Vd: 1 Sim">
	    </div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Wifi</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
	      	<input class="form-control " type="text" name="connect_wifi" id="connect_wifi" value="{{(isset($phone)?$phone->connect_wifi:"")}}" placeholder="Vd: Wi-Fi 802.11 a/b/g/n/ac, dual-band, DLNA, Wi-Fi Direct, Wi-Fi hotspot">
	    </div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> GPS</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
	      	<input class="form-control " type="text" name="connect_gps" id="connect_gps" value="{{(isset($phone)?$phone->connect_gps:"")}}" placeholder="Vd: A-GPS">
	    </div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Bluetoutch</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
	      	<input class="form-control " type="text" name="connect_bluetouth" id="connect_bluetouth" value="{{(isset($phone)?$phone->connect_bluetouth:"")}}" placeholder="Vd: Có, V4.0 với A2DP, EDR">
	    </div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> GPRS/EDGE</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
			<select name="connect_gprs" id="connect_gprs" class="form-control">
				<option value="1" @if(isset($phone) && $phone->connect_gprs == 1) selected="selected" @endif>Có</option>
				<option value="0" @if(isset($phone) && $phone->connect_gprs == 0) selected="selected" @endif>Không</option>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> NFC</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
			<select name="connect_nfc" id="connect_nfc" class="form-control">
				<option value="1" @if(isset($phone) && $phone->connect_nfc == 1) selected="selected" @endif>Có</option>
				<option value="0" @if(isset($phone) && $phone->connect_nfc == 0) selected="selected" @endif>Không</option>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Jack tai nghe</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
	      	<input class="form-control " type="text" name="connect_jack" id="connect_jack" value="{{(isset($phone)?$phone->connect_jack:"")}}" placeholder="Vd: 3.5 mm">
	    </div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Kết nối USB</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
	      	<input class="form-control " type="text" name="connect_usb" id="connect_usb" value="{{(isset($phone)?$phone->connect_usb:"")}}" placeholder="Vd: Có">
	    </div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Kết nối khác</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
	      	<input class="form-control " type="text" name="connect_other" id="connect_other" value="{{(isset($phone)?$phone->connect_other:"")}}" placeholder="Vd: NFC">
	    </div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Cổng sạc</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
	      	<input class="form-control " type="text" name="connect_chargprot" id="connect_chargprot" value="{{(isset($phone)?$phone->connect_chargprot:"")}}" placeholder="Vd: Micro USB">
	    </div>
	</div>
</div>
<div class="alert alert-info" data-toggle="collapse" data-target="#gt">
	<strong>Giải Trí - Ứng Dụng</strong>
</div>
<div id="gt" class="collapse" style="height: auto;">
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Xem phim</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
	      	<input class="form-control " type="text" name="entertainment_movie" id="entertainment_movie" value="{{(isset($phone)?$phone->entertainment_movie:"")}}" placeholder="Vd: MP4, WMV, H.263, H.264(MPEG4-AVC), Xvid, DivX">
	    </div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Nghe nhạc</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
	      	<input class="form-control " type="text" name="entertainment_player" id="entertainment_player" value="{{(isset($phone)?$phone->entertainment_player:"")}}" placeholder="Vd: MP3, WAV, WMA, eAAC+, AC3, FLAC">
	    </div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Ghi Âm</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
			<select name="entertainment_recording" id="entertainment_recording" class="form-control">
				<option value="1" @if(isset($phone) && $phone->entertainment_recording == 1) selected="selected" @endif>Có</option>
				<option value="0" @if(isset($phone) && $phone->entertainment_recording == 0) selected="selected" @endif>Không</option>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> FM Radio</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
			<select name="entertainment_fm" id="entertainment_fm" class="form-control">
				<option value="1" @if(isset($phone) && $phone->entertainment_fm == 1) selected="selected" @endif>Có</option>
				<option value="0" @if(isset($phone) && $phone->entertainment_fm == 0) selected="selected" @endif>Không</option>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Chức năng khác</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
			<textarea class="form-control " name="entertainment_other" id="entertainment_other" value="{{(isset($phone)?$phone->entertainment_other:"")}}" class="form-entertainment_other" rows="3">{{(isset($phone)?$phone->entertainment_other:"")}}</textarea>
		</div>
		
	</div>
</div>