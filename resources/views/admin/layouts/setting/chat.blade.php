<div class="alert alert-info" data-toggle="collapse" data-target="#phone">
	<strong>Code Chat Điện thoại</strong>
</div>
<div id="phone" class="collapse" style="height: auto;">
	@foreach($location as $k => $v)
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> {{$v->name}}</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
			<input type="hidden" name="phone[{{$v->id}}][location_id]" value="{{$v->id}}">
	      	<textarea name="phone[{{$v->id}}][location_code]" class="form-control" rows="5">{{isset($data_phone[$v->id])?$data_phone[$v->id]["location_code"]:""}}</textarea>
	    </div>
	</div>
	@endforeach
</div>
<div class="alert alert-info" data-toggle="collapse" data-target="#tablet">
	<strong>Code Chat Máy tính bảng</strong>
</div>
<div id="tablet" class="collapse" style="height: auto;">
	@foreach($location as $k => $v)
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> {{$v->name}}</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
			<input type="hidden" name="tablet[{{$v->id}}][location_id]" value="{{$v->id}}">
	      	<textarea name="tablet[{{$v->id}}][location_code]" class="form-control" rows="5">{{isset($data_tablet[$v->id])?$data_tablet[$v->id]["location_code"]:""}}</textarea>
	    </div>
	</div>
	@endforeach
</div>
<div class="alert alert-info" data-toggle="collapse" data-target="#fit">
	<strong>Code Chat Phụ kiện</strong>
</div>
<div id="fit" class="collapse" style="height: auto;">
	@foreach($location as $k => $v)
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> {{$v->name}}</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
			<input type="hidden" name="fit[{{$v->id}}][location_id]" value="{{$v->id}}">
	      	<textarea name="fit[{{$v->id}}][location_code]" class="form-control" rows="5">{{isset($data_fit[$v->id])?$data_fit[$v->id]["location_code"]:""}}</textarea>
	    </div>
	</div>
	@endforeach
</div>
<div class="alert alert-info" data-toggle="collapse" data-target="#service">
	<strong>Code Chat Sửa chữa</strong>
</div>
<div id="service" class="collapse" style="height: auto;">
	@foreach($location as $k => $v)
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> {{$v->name}}</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
			<input type="hidden" name="service[{{$v->id}}][location_id]" value="{{$v->id}}">
	      	<textarea name="service[{{$v->id}}][location_code]" class="form-control" rows="5">{{isset($data_service[$v->id])?$data_service[$v->id]["location_code"]:""}}</textarea>
	    </div>
	</div>
	@endforeach
</div>