@foreach($location as $k => $v)
<div class="form-group">
	<label class="control-label col-md-2 col-sm-2 col-xs-12">Code Chat - {{$v->name}}</label>
	<div class="controls col-md-9 col-sm-10 col-xs-12">
		{{-- <input type="hidden" name="code_chat_all[{{$v->id}}]" value="{{$v->id}}"> --}}
      	<textarea name="code_chat_all[{{$v->id}}]" class="form-control" rows="5">{{(isset($code_chat_all[$v->id]) && $code_chat_all[$v->id] != null)?$code_chat_all[$v->id]:""}}</textarea>
    </div>
</div>
@endforeach