@php
    $product = isset($value['value']) ? $value['value']: '';
@endphp
<div class="form-group" style="padding-bottom: 20px;">
    <label class="control-label col-md-2 col-sm-2 col-xs-12">Cấu hình menu heaer</label>
    <div class="controls col-md-9 col-sm-10 col-xs-12" style="">
        <div class="row">
            <div class="col-md-12 form-group">
                <a href="javascript:;" id="add-setting-address" class="btn btn-info">+ Thêm menu</a>
            </div>
        </div>
        @if(isset($product['name_menu_header']) )
            @foreach($product['name_menu_header'] as $key => $value)

                <div class="row"
                    style="margin-top: 20px;box-shadow: 0 0 4px 0 rgba(0, 0, 0, 0.2);padding:10px ;    background: #ededed;">
                    <div class="col-md-10 form-group">

                        <div class="col-md-12 form-group">
                            <div class="controls col-sm-12">
                                <input type="text" name="name_menu_header[]" placeholder="Tên menu"
                                    style="    border: 1px solid #e1e1e1;width: calc(100%);height: 35px;padding-left: 10px;font-size: 16px;"
                                    id="price" value="{{ $value }}">
                            </div>
                        </div>

                        <div class="col-md-12 form-group">
                            <div class="controls col-sm-12">
                                <input type="text" name="link_menu_header[]" placeholder="Url"
                                    style="    border: 1px solid #e1e1e1;width: calc(100%);height: 35px;padding-left: 10px;font-size: 16px;"
                                    id="price" value="{{ $product['link_menu_header'][$key] }}">
                            </div>
                        </div>

                        <div class="col-md-12 form-group">
                            <div class="controls col-sm-12">
                                <a style="padding: 7px 10px;" class="btn btn-primary btn-xs" href="javascript:;" onclick="media_popup('add','single','image_menu_header_{!! $key !!}','Chọn ảnh');">Chọn ảnh cho desktop</a>
                                <div class="clear"></div>
                                <input id="image_menu_header_{!! $key !!}" type="hidden" name="image_menu_header[]" value="{!! $product['image_menu_header'][$key] !!}">
                                <img src="{!! $product['image_menu_header'][$key] !!}" class="thumb-img" style="width: 100px;margin-top: 6px;">
                            </div>
                        </div>

                        <div class="col-md-12 form-group">
                            <div class="controls col-sm-12">
                                <a style="padding: 7px 10px;" class="btn btn-primary btn-xs" href="javascript:;" onclick="media_popup('add','single','image_mobile_menu_header_{!! $key !!}','Chọn ảnh');">Chọn ảnh cho mobile</a>
                                <div class="clear"></div>
                                <input id="image_mobile_menu_header_{!! $key !!}" type="hidden" name="image_mobile_menu_header[]" value="{!! @$product['image_mobile_menu_header'][$key] !!}">
                                <img src="{!! @$product['image_mobile_menu_header'][$key] !!}" class="thumb-img" style="width: 100px;margin-top: 6px;">
                            </div>
                        </div>

                    </div>

                    <div class="col-md-2 form-group">
                        <a href="javascript:;" style="background: #a30803;"
                        class="btn btn-xs btn-warning remove-color">Xóa</a>
                    </div>
                </div>

            @endforeach
        @endif
</div>
</div>
<script>
    function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    $('#add-setting-address').on('click', function () {
        var rand = getRandomInt(1, 999);
        var str = '<div class="row" style="margin-top: 20px;box-shadow: 0 0 4px 0 rgba(0, 0, 0, 0.2);padding:10px ;    background: #ededed;">' +

            '<div class="col-md-10 form-group">' +

            '<div class="col-md-12 form-group">' +
            '<div class="controls col-sm-12">' +
            '<input type="text" name="name_menu_header[]" placeholder="Tên menu" style="    border: 1px solid #e1e1e1;width: calc(100%);height: 35px;padding-left: 10px;font-size: 16px;" id="price" value="">' +
            '</div>' +
            '</div>' +

            '<div class="col-md-12 form-group">' +
            '<div class="controls col-sm-12">' +
            '<input type="text" name="link_menu_header[]" placeholder="Url" style="    border: 1px solid #e1e1e1;width: calc(100%);height: 35px;padding-left: 10px;font-size: 16px;" id="price" value="">' +
            '</div>' +
            '</div>' +

            '<div class="col-md-12 form-group">' +
            '<div class="controls col-sm-12">' +
                '<a style="padding: 7px 10px;" class="btn btn-primary btn-xs" href="javascript:;" onclick="media_popup(\'add\',\'single\',\'image_menu_header_'+rand+'\',\'Chọn ảnh\');">Chọn ảnh cho desktop</a>' +
                '<div class="clear"></div>' +
                '<input id="image_menu_header_'+rand+'" type="hidden" name="image_menu_header[]" value="">' +
                '<img src="" class="thumb-img" style="width: 100px;margin-top: 6px;">' +
            '</div>' +
            '</div>' +

            '<div class="col-md-12 form-group">' +
                '<div class="controls col-sm-12">' +
                    '<a style="padding: 7px 10px;" class="btn btn-primary btn-xs" href="javascript:;" onclick="media_popup(\'add\',\'single\',\'image_mobile_menu_header_'+rand+'\',\'Chọn ảnh\');">Chọn ảnh cho mobile</a>' +
                    '<div class="clear"></div>' +
                    '<input id="image_mobile_menu_header_'+rand+'" type="hidden" name="image_mobile_menu_header[]" value="">' +
                    '<img src="" class="thumb-img" style="width: 100px;margin-top: 6px;">' +
                '</div>' +
                '</div>' +

            '</div>' +

            '<div class="col-md-2 form-group">' +
            '<a href="javascript:;" style="background: #a30803;" class="btn btn-xs btn-warning remove-color">Xóa</a>' +
            '</div>' +
            '</div>';
        var btn_group = $(this).closest('.controls');
        btn_group.append(str);
    });
    $('body').on('click', '.remove-color', function () {
        $(this).closest('.row').remove();
    });

</script>
