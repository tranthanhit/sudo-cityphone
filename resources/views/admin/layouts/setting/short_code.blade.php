<?php
	$option = DB::table('options')->select('value')->where('name','short_code')->first();
    if(empty($option)){
        $data = [];
    }else{
        $data = json_decode(base64_decode($option->value),true);
    }
?>

<button type="button" class="btn btn-success btn-xs" id="add_short_code"><i class="fa fa-plus"></i> Thêm</button>
<input type="hidden" name="key" value="short_code">
<div class="short_code_wrap">
	<div class="short_code_list">
        @if(isset($data['short_code_key']))
    		@for($i = 0; $i < count($data['short_code_key']);$i++)
    		<div class="short_code_item">
    			<div class="short_code_key">
    				<input type="text" name="short_code_key[]" placeholder="Mã Code" class="form-control" value="{{$data['short_code_key'][$i]}}">
    				<button class="btn btn-danger btn-xs delete_short_code">Xóa</button>
    			</div>
    			<div class="short_code_content">
    				<textarea name="short_code_content[]" cols="30" rows="10">{{$data['short_code_content'][$i]}}</textarea>
    				
    			</div>
    		</div>
    		@endfor
        @endif
	</div>
</div>
<script>
    document.addEventListener("DOMContentLoaded", function(event) {
        addTinyMCE();
    });
    
    $(document).ready(function() {
    	$('#add_short_code').click(function() {
    		$('.short_code_list').append('<div class="short_code_item"><div class="short_code_key"><input type="text" name="short_code_key[]" placeholder="Mã Code" class="form-control"><input type="hidden" name="key" value="short_code"><button class="btn btn-danger btn-xs delete_short_code">Xóa</button></div><div class="short_code_content"><textarea name="short_code_content[]" cols="30" rows="10"></textarea></div></div>');
            addTinyMCE();
    	});

        $('.short_code_list').on('click','.delete_short_code',function() {
            $(this).parent().parent().remove();
        });
    });

    function addTinyMCE() {
        tinymce.init({
            path_absolute : "/",
            selector:'textarea[name="short_code_content[]"]',
            branding: false,
            hidden_input: false,
            relative_urls: false,
            convert_urls: false,
            height : 150,
            autosave_ask_before_unload:true,
            autosave_interval:'10s',
            autosave_restore_when_empty:true,
            entity_encoding : "raw",
            fontsize_formats: "8pt 9pt 10pt 11pt 12pt 13pt 14pt 15pt 16pt 17pt 18pt 19pt 20pt 22pt 24pt 26pt 28pt 30pt 32pt 36pt 40pt 46pt 52pt 60pt",
            plugins: [
                "textcolor",
                "advlist autolink lists link image imagetools charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table autosave contextmenu paste wordcount"
            ],
            wordcount_countregex: /[\w\u2019\x27\-\u00C0-\u1FFF]+/g,
            language: "vi_VN",
            autosave_retention:"30m",
            autosave_prefix: "tinymce-autosave-{path}{query}-{id}-",
            wordcount_cleanregex: /[0-9.(),;:!?%#$?\x27\x22_+=\\\/\-]*/g,
            toolbar: "insertfile undo redo table sudomedia charmap | styleselect | sizeselect | bold italic | fontselect |  fontsizeselect | forecolor " +
                "backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent " +
                "indent | link unlink fullscreen restoredraft filemanager",
            setup: function (editor) {
            editor.addButton('sudomedia', {
            text: 'Tải ảnh',
            icon: 'image',
            label:'Nhúng ảnh vào nội dung',
            onclick: function () {
                media_popup("add","tinymce","short_code_content[]","Chèn ảnh vào bài viết");
                }
            });
        },
            file_picker_callback: function() {
                media_popup("add","tinymce","short_code_content[]","Chèn ảnh vào bài viết");
            }
        });
    }
</script>