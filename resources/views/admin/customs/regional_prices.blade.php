@php
	$locations = DB::table('location')->where('status',1)->get();
	$regional_prices = [];
	$service = isset($value['value']) ? $value['value']: '';
	if(!empty($service)){
		$regional_prices = DB::table('regional_prices')->where('service_id', $service->id)->get();
		$locations_collect =collect( DB::table('location')->whereIn('id',$regional_prices->pluck('location_id','location_id')->toArray())->where('status',1)->get() );
	}

@endphp
<div class="form-group" style="padding-bottom: 20px;">
    <label class="control-label col-md-2 col-sm-2 col-xs-12">Chọn giá theo từng khu vực</label>
    <div class="controls col-md-9 col-sm-10 col-xs-12" style="">
    	<div class="row">
            <div class="col-md-12 form-group">
                <a href="javascript:;" id="add-colors" class="btn btn-info">+ Thêm giá</a>
            </div>
        </div>
		@foreach($regional_prices as $key => $value)
		@php
			$loca = $locations_collect->where('id',$value->location_id)->first();
		@endphp
	        <div class="row" style="margin-top: 10px;">
	        	<div class="col-md-4 form-group">
		            <select name="localtion[]" class="choose-color" style="width: calc(100% - 50px);height: 35px;padding-left: 10px;font-size: 16px;">
		            	<option data-code="#fff" value="0">Chọn khu vực</option>
		            	@foreach($locations as $val)
		            		<option data-code="{!! $val->name !!}" @if($val->id == $loca->id) selected @endif value="{!! $val->id !!}">{!! $val->name !!}</option>
		            	@endforeach
		            </select>
		        </div>
		        <div class="col-md-2 form-group">
		            <div class="controls col-sm-12">
		               <input type="text" class="form-control" name="price_regional[]" value="{{ $value->price }}" placeholder="Giá">
		            </div>
		        </div>
		        <div class="col-md-2 form-group">
		            <a href="javascript:;" class="btn btn-xs btn-warning remove-color">Xóa</a>
		        </div>
		    </div>
	    @endforeach
    </div>
</div>
<script>
	function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
	$('#add-colors').on('click', function(){
		var rand = getRandomInt(1,999);
		var str = '<div class="row" style="margin-top: 10px;">' +
                '<div class="col-md-4 form-group">' +
                '<select name="localtion[]" class="choose-color" style="width: calc(100% - 50px);height: 35px;padding-left: 10px;font-size: 16px;">' +
                '<option data-code="#fff" value="0">Chọn khu vực</option>';
                @foreach($locations as $value)
                str += '<option data-code="{!! $value->name !!}" value="{!! $value->id !!}">{!! $value->name !!}</option>';
                @endforeach
            str += '</select>' +
               
                '</div>' +
                '<div class="col-md-2 form-group">' +
                '<div class="controls col-sm-12">' +

				'<div class="clear"></div>' +
				' <input type="text" class="form-control" name="price_regional[]" value="" placeholder="Giá">'+
				
                '</div>' +
                '</div>' +
                '<div class="col-md-2 form-group">' +
                '<a href="javascript:;" class="btn btn-xs btn-warning remove-color">Xóa</a>' +
                '</div>' +
                '</div>';
        var btn_group = $(this).closest('.controls');
        btn_group.append(str);
	});
	$('body').on('click','.remove-color',function () {
        $(this).closest('.row').remove();
    });
	$('body').on('change','.choose-color',function () {
        var code = $('option:selected', this).attr('data-code');
		var btn_group = $(this).closest('.row');
		$(btn_group).find('.color-demo').css('background', code);
    });
</script>