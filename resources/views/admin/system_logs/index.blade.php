<td>
    {{$array_admins[$value->admin_id]}}
</td>
<td>
    {{$value->ip}}
</td>
<td>
    {{date('H:i:s d/m/Y',strtotime($value->time))}}
</td>
<td>
    {{$value->title}}
</td>
<td>
    {!! config('app.log_type')[$value->type]  ?? 'Chưa cập nhật'!!}
</td>
<td>
    {!! config('app.log_table')[$value->table] ?? 'Chưa cập nhật' !!}
</td>
<td>
	@if($value->type != 'login')
    	<a href="/admin/system_logs/{{$value->id}}" target="_blank">Chi tiết</a>
    @endif
    @if($value->type != 'sync_order_fail')
		<a href="/admin/orders/{{$value->table_id}}">Xem đơn lỗi</a>
    @endif
</td>