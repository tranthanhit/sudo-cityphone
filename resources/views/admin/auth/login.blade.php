

<html lang="{{ config('app.locale') }}">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Administrator Managerment</title>
<link href="{!! asset('/template-admin/css/style.css') !!}" rel="stylesheet">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="{!! asset('/template-admin/css/style-responsive.css') !!}" rel="stylesheet">
<script src="{!! asset('/template-admin/js/jquery-1.10.2.min.js') !!}"></script>
<script src="{!! asset('/template-admin/js/bootstrap.min.js') !!}"></script>
<style>

.help-block {
    display: block;
    margin-top: 5px;
    margin-bottom: 10px;
    color: #737373;
    text-align: center;
    background: #f2dede;
    padding: 12px 0;
    border-radius: 5px;
    width: 87%;
    margin: 0 auto;
}
i.fa.fa-times {
    position: relative;
    top: -8px;
    right: 8px;
}
p.close-alert {
    width: 20px;
    height: 20px;
    position: relative;
    position: absolute;
    top: 10px;
    font-size: 13px;
    right: 20px;
    text-align: center;
    background: #fff;
    border-radius: 50%;
    cursor: pointer;
    color: black;
}
</style>
</head>
<body class="login-body" cz-shortcut-listen="true">

<div class="container">

    <form class="form-signin" action="{{ route('admin.login') }}" method="post">
    {{ csrf_field() }}
        <div class="form-signin-heading text-center">
            <h1 class="sign-title">Đăng nhập</h1>
            <img src="{!! asset('/template-admin/images/login-logo.png') !!}" alt="">
        </div>
        @include('errors.alert')
        @if ($errors->has('error'))
            <span class="help-block">
                    <button type="button" class="close close-sm" data-dismiss="alert"><i class="fa fa-times"></i></button>
                    <strong>{{ $errors->first('error') }}</strong>
                </span>
        @endif
        <div class="login-wrap">
            
            <input type="text" class="form-control" value="{{ old('login') }}" placeholder="Tên đăng nhập hoặc email" autofocus="" name="login">
            <input type="password" class="form-control" placeholder="Mật khẩu" id="password" name="password">

            <button class="btn btn-lg btn-login btn-block" type="submit">
            <input type="hidden" name="action" value="login"/>
            <i class="fa fa-check"></i>
            </button>
            <label class="checkbox">
                <span class="pull-right">
                    <a data-toggle="modal" href="#myModal"> Quên mật khẩu?</a>

                </span>
            </label>

        </div>

        <!-- Modal -->
        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">Forgot Password ?</h4>
                    </div>
                    <div class="modal-body">
                        <p>Enter your e-mail address below to reset your password.</p>
                        <input type="text" name="reset-email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">

                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                        <button class="btn btn-primary" type="button">Submit</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- modal -->

    </form>

</div>
<script>
    $(document).ready(function(){
            $('.close-alert').on('click',function(){
                $('.alert-success').fadeOut();
            });

            $('.close-error').on('click',function(){
                $('.alert-danger').fadeOut();
            });

            $('#edit_post_date').on('click',function(){
                $(this).parent().html('<input type="date" name="post_date" id="post_date" class="form-control">');
            });
        });
</script>

</body>
</html>