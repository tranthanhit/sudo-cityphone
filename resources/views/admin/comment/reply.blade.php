@extends('admin.layouts.app')
@section('title')
    <h3>Trả lời {{$module_name}}</h3>
@endsection()
@section('title2')
    <h2>Những trường đánh dấu (<span style="color:red;">*</span>) là bắt buộc nhập</h2>
@endsection()
@section('content')
    @include('errors.alert')
    @include('errors.error')

    <div class="row">
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="comment_item comment_item_parent">
                <div class="comment_item_author" style="display: flex;align-items: center">
                    <div class="comment_item_author_avatext" style="float: left;
                                        width: 26px;
                                        height: 26px;
                                        line-height: 26px;
                                        background: #ddd;
                                        margin-right: 7px;
                                        text-align: center;
                                        color: #666;
                                        text-transform: uppercase;
                                        font-size: 12px;
                                        font-weight: 600;
                                        text-shadow: 1px 1px 0 rgba(255,255,255,.2);">{{getCharacterAvatar($data['comment']->name)}}</div>
                    <span class="comment_item_author_name" style="font-weight: bold;color: #333">{{$data['comment']->name}}</span>
                    @if($data['comment']->admin_id > 0) <span class="comment_item_author_admin">Quản trị viên</span> @endif
                </div>
                <div class="comment_item_content" style="padding: 10px;">{{$data['comment']->content}}</div>
                <div class="comment_item_meta">
                    <span class="comment_item_time" data="{{$data['comment']->created_at}}">{{($data['comment']->created_at)}}</span>
                </div>
                @if(count($data['child']))
                    <div class="comment_item_reply_wrap @if(count($data['child'])) has_child   @endif" @if(count($data['child'])) style="    background: #f8f8f8;
                    border: 1px solid #dfdfdf;
                    position: relative;
                    margin-top: 10px;padding: 10px;margin-bottom: 20px;"  @endif>
                        @if(count($data['child']))
                            @foreach($data['child'] as $v)
                                <div class="comment_item comment_item_child" style="    margin-top: 10px;
                                padding-bottom: 8px;
                                border-bottom: 1px solid #e1e1e1;">
                                    <div class="comment_item_author" style="display: flex;align-items: center">
                                        <div class="comment_item_author_avatext" style="float: left;
                                        width: 26px;
                                        height: 26px;
                                        line-height: 26px;
                                        background: #ddd;
                                        margin-right: 7px;
                                        text-align: center;
                                        color: #666;
                                        text-transform: uppercase;
                                        font-size: 12px;
                                        font-weight: 600;
                                        text-shadow: 1px 1px 0 rgba(255,255,255,.2);">{{getCharacterAvatar($v->name)}}</div>
                                        <span class="comment_item_author_name" style="margin-right: 10px;font-weight: bold;color: #333" >{{$v->name}}</span>
                                        @if($v->admin_id > 0)
                                            <span class="comment_item_author_admin" style="display: inline-block; margin-left: 10px;margin-right: 10px;font-weight: normal;color: #fff; background: #c69a39;padding: 3px 5px;border-radius: 4px;font-size: 13px;">Quản trị viên</span>
                                        @endif
                                        <select data-id={{$v->id}} class="form-control comment_success" style="display: inline-block;width: unset;" required="required">
                                            <option value="1" @if($v->status == 1) selected="" @endif>Duyệt</option>
                                            <option value="3" @if($v->status != 1) selected="" @endif>Ẩn</option>
                                        </select> 
                                    </div>
                                    <div class="comment_item_content" style="padding: 10px">{{$v->content}}</div>
                                    <div class="comment_item_meta">
                                         <span class="comment_item_time" data="{{$v->created_at}}">{{($v->created_at)}}</span>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                @else
                    <div class="comment_item_reply_wrap"></div>
                @endif
            </div>
        </div>
    </div>

    <form action="{!! route($table_name.'.postReply',$data['comment']->id) !!}" class="form-horizontal form-label-left" enctype="multipart/form-data" method="post">
        {!! csrf_field() !!}
        <div class="form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12">Quản trị viên</label>
            <div class="controls col-md-9 col-sm-10 col-xs-12">
                <input class="form-control" type="text" name="name" id="name" value="{{Auth::guard('admin')->user()->fullname??'Cityphone'}}" placeholder="" required="">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12">Nội dung trả lời</label>
            <div class="controls col-md-9 col-sm-10 col-xs-12">
                <textarea rows="6" name="content" id="content" class="form-control" placeholder="" required=""></textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12">&nbsp;</label>
            <div class="controls col-md-9 col-sm-10 col-xs-12">
                <button type="submit" name="redirect" value="edit" class="btn btn-success">Trả lời</button>&nbsp;
                <button type="reset" class="btn">Nhập lại</button>&nbsp;
                <button type="button" onclick="window.location='{!! route($table_name.'.index') !!}'" class="btn btn-danger">Thoát</button>&nbsp;
                <a href="{!!$products->getUrl()!!}" target="_blank">
                    <button type="button" onclick="" class="btn btn-primary"><i class="fa fa-eye"></i> Xem</button>&nbsp;
                </a>
            </div>
        </div>
    </form>

@endsection()

@section('script')
    <link href="{!! url('public/template-admin/css/comments.css') !!}" rel="stylesheet">
    <script type="text/javascript">
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $(".comment_success").change(function(event) {
                var id = $(this).data('id');
                var val = $(this).val();
                $.ajax({
                    type: "POST",
                    url: "{{route('web.admin.comment_success')}}",
                    data: {id:id,val:val},
                    dataType: "JSON",
                    success: function (response) {
                       if(response.val == 1){
                            alert("Đã duyệt trả lời");
                       }else{
                            alert("Đã ẩn trả lời");
                       }
                    }
                });
            });

        });
    </script>
@endsection()