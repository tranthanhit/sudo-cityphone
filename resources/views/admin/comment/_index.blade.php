@php
$that_cmt = DB::table('comment')->where('status','<>',4)->where('id',$value->id)->first();
$url = "";
if($that_cmt){
    if($that_cmt->parent_id == 0){
        $type = $that_cmt->type;
        $type_id = $that_cmt->type_id;
        switch ($type) {
            case 'services':
                $service = App\Service::where('status',1)->where('id',$type_id)->first();
                if($service){
                    $url = route('web.service.show',$service->slug);
                }
                else{
                    $url = "";
                }
                break;
            case 'fits':
                $fit = App\Fit::where('status',1)->where('id',$type_id)->first();
                if($fit){
                    $url = route('web.fits.show',$fit->slug);    
                }else{
                    $url = "";
                }
                break;
            
            case 'tablets':
                $tablet = App\Tablet::where('status',1)->where('id',$type_id)->first();
                if($tablet){
                    $url = route('web.tablet.show',$tablet->slug);
                }else{
                    $tablet = "";
                }
                break;
            case 'phones':
                $phone = App\Phone::where('status',1)->where('id',$type_id)->first();
                if($phone){
                    $cate = App\PhoneCategory::where('status',1)->where('id',$phone->category_id)->first();
                    $url = route('web.phone.show',['category'=>$cate->slug,'slug'=>$phone->slug]);   
                }else{
                    $url = "";
                }
                
                break;
        }
    }else{
        $rep = DB::table('comment')->where('status','<>',4)->where('id',$that_cmt->parent_id)->first();
        $type = $rep->type;
        $type_id = $rep->type_id;
        switch ($type) {
            case 'services':
                $service = App\Service::where('status',1)->where('id',$type_id)->first();
                $url = route('web.service.show',$service->slug);
                break;
            case 'fits':
                $fit = App\Fit::where('status',1)->where('id',$type_id)->first();
                $url = route('web.fits.show',$fit->slug);
                break;
            
            case 'tablets':
                $tablet = App\Tablet::where('status',1)->where('id',$type_id)->first();
                $url = route('web.tablet.show',$tablet->slug);
                break;
            case 'phones':
                $phone = App\Phone::where('status',1)->where('id',$type_id)->first();
                if($phone) {
                    $cate = App\PhoneCategory::where('status',1)->where('id',$phone->category_id)->first();
                    $url = route('web.phone.show',['category'=>$cate->slug,'slug'=>$phone->slug]);
                } else {
                    $url = "";
                }
                
                break;
        }
    }
}
$rep = DB::table('comment')->where('status','<>',4)->where('parent_id','<>',0)->where('parent_id',$value->id)->first();
@endphp
<td style="width: 200px;">
    <select name="status" class="form-control quick-edit" onchange="check_edit(this)">
        @foreach($arr_stt as $k => $v)
        <option value="{{$k}}" @if($value->status == $k) selected="" @endif">{{$v}}</option>
        @endforeach
    </select>
    
</td>
<td>
    <a href="{!! route('comment.reply',['id'=>$value->id]) !!}" class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i> Trả lời</a>
    @if($url != "")
    <a target="_blank" href="{{$url}}" class="btn btn-xs btn-info"><i class="fa fa-eye"></i> Link</a>
    @endif
    @if($rep)
    <span class="label label-success">Đã trả lời</span>
    @endif
</td>
<td>
    <div style="max-width: 400px;">{!! nl2br($value->content) !!}</div>
</td>
<td>
    {{$value->name}} @if($value->admin_id > 0) <b>(QTV)</b> @endif
</td>
<td>{{$value->email}}</td>
<td>{{$value->phone}}</td>
<td>
    <strong>Thêm lúc:</strong> {!! $value->created_at !!}
    <br>
    <strong>Cập nhật:</strong> {!! $value->updated_at !!}
    <br>
   {{--  @if($value->status == 1)
        <span class="label label-success">Đã duyệt</span>
    @elseif($value->status == 2)
        <span class="label label-warning">Chờ duyệt</span>
    @elseif($value->status == 3)
        <span class="label label-danger">Từ chối</span>
    @else
        <span class="label label-info">Không xác định</span>
    @endif --}}
</td>