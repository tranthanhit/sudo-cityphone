<td style="width: 200px;">
    <select name="status" class="form-control edit_quick" data-table="{{$table}}" style="margin-bottom:10px;">
        @foreach($arr_stt as $k => $v)
        <option value="{{$k}}" @if($value->status == $k) selected="" @endif">{{$v}}</option>
        @endforeach
    </select>
    <p>
    	@php
    		switch ($value->type) {
                case 'fits':
                    foreach ($fittings as $fitting) {
                    	if ($fitting->id == $value->type_id) {
                    		@endphp
                    			<p><a target="_blank" href="{{$fitting->getUrl()}}">{{$fitting->name}}</a></p>
                    		@php
                    	}
                    }
                break;
                case 'services':
                    foreach ($services as $service) {
                    	if ($service->id == $value->type_id) {
                    		@endphp
                    			<p><a target="_blank" href="{{$service->getUrl()}}">{{$service->name}}</a></p>
                    		@php
                    	}
                    }
                break;
            }
    	@endphp
    </p>
</td>
<td class="comment_content">   
	<p>{!!nl2br($value->content)!!}</p>
	@foreach ($comment_admins as $comment_admin)
		@if ($comment_admin->parent_id == $value->id)
			<p class="comment_admin">
                <strong>{{$comment_admin->fullname??'Cityphone'}}:</strong> 
                {{$comment_admin->content}}
               
            </p>
		@endif
    @endforeach
</td>
<td>
	<p>Tên: {!!$value->name??'Không xác định'!!}</p>
	<p>Điện thoại: {!!$value->phone??'Không xác định'!!}</p>
	<p>Email: {!!$value->email??'Không xác định'!!}</p>
</td>
<td style="width:150px;">
	<p>Số sao: {!!$value->rank!!}<i class="fa fa-star"></i></p>
    <p><strong>Thêm lúc:</strong> {!! $value->created_at !!}</p>    
    <p><strong>Cập nhật:</strong> {!! $value->updated_at !!}</p>
</td>
<td style="width:50px;">
	<a href="{!! route('comment.reply',['id'=>$value->id]) !!}" target="_blank" class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i> Trả lời</a>
    <a class="btn btn-success btn-xs" href='#modal-comment'>Trả lời ngay</a>
    <input type="checkbox" class="check_has_call" data-id="{{$value->id}}" data-type="comment" @if($value->has_call == 1) checked="checked" @endif> đã gọi
</td>