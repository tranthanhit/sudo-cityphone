@php
	$location_id = ($value->location_id == 0 || $value->location_id == null)?1:$value->location_id;
	$location = DB::table('location')->where('status',1)->where('id',$location_id)->first();
@endphp

<td style="width: 200px;">
	@if($value->type == 'fits')
		<select name="status" class="form-control quick-edit" onchange="check_edit(this)">
			@foreach($arr_stt as $k => $v)
				<option value="{{$k}}" @if($value->status == $k) selected="" @endif>{{$v}}</option>
			@endforeach
		</select>
	@endif
</td>
<td>
    <strong><a class="row-title" href="{!! route($table.'.show', $value->id) !!}" title="Thông tin đơn hàng: {!! $value->name !!}">{!! $value->name !!}</a> </strong>
</td>
<td>
    {{$value->phone}}
</td>
<td>
    {{$value->email}}
</td>
<td>
    {{$value->address}}
</td>
<td style="width: 100px;">
	@if($location)
		{{$location->name}}
	@endif
</td>
<td>
	{{ $value->type }}
	@if(isset($arr_type[$value->type]))
	{{$arr_type[$value->type]}}
	@endif
</td>
<td>
	<strong>{{ api_custom_price(@$value->price,true) }}</strong>
</td>
<td>
    {{$value->note}}
</td>
<td>
    {!! $value->created_at !!}
</td>
<td style="text-align: center;">
	@if($value->type == 'fits')
		<a href="{!! route($table.'.show', $value->id) !!}"><i class="fa fa-eye" aria-hidden="true"></i>
		</a>
	@endif
</td>