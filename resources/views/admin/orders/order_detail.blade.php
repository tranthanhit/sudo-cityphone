@extends('admin.layouts.app')
@section('title')
<h3>Chi tiết đơn hàng của khách hàng: {{$order->name}}</h3>
@endsection
@section('content')
<table class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>Thông tin sản phẩm</th>
            <th>Loại sản phẩm</th>
            <th>Link sản phẩm</th>
            <th>Giá</th>
        </tr>
    </thead>
    <tbody>
            <th>{{$order_detail->info}}</th>
            <th>{{$type}}</th>
            <th>
                <a href="{{$link}}" target="_blank">{{$link}}</a>
            </th>
            <th>{{number_format($order_detail->price)}}</th>
    </tbody>
</table>
@endsection