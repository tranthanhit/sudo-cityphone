<td>
	@if(isset($array_categories[$value->category_id]))
    {{$array_categories[$value->category_id]}}
    @else
    {{"Không xác định"}}
    @endif
</td>
<td>
	@if(isset($array_class[$value->class_id]))
    {{$array_class[$value->class_id]}}
    @else
    {{"Không xác định"}}
    @endif
</td>
{{-- <td>
    <img style="height: 100px;" src="{!! getImage('',$value->image) !!}" onerror="this.src='/template-admin/images/no-image.png'">
</td> --}}
<td>
    <strong><a class="row-title" href="{!! route($table.'.edit', $value->id) !!}" title="Sửa bài: {!! $value->name !!}">{!! $value->name !!}</a> </strong>
</td>
<td>
    <strong>{!! $value->price !!}</strong>
</td>
<td>
    <strong>{!! $value->warranty !!}</strong>
</td>
<td>
    <strong>Thêm lúc:</strong> {!! $value->created_at !!}
    <br>
    <strong>Cập nhật:</strong> {!! $value->updated_at !!}
    <br>
    @php
        $system_logs = $system_logs_collect->where('table_id',$value->id)->first();
        if (isset($system_logs)) {
            $user = $user_collect->where('id',$system_logs->admin_id)->first();
        }

    @endphp
    @if (isset($user->name))
        <strong>Cập nhật cuối: </strong> {{$user->name}}
    @else
        <strong>Cập nhật cuối: </strong>Chưa rõ
    @endif
</td>


