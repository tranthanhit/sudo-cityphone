<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('sync-phone-to-software','API\SyncController@phoneToSoftware');
Route::get('sync-fitting-to-software','API\SyncController@fittingToSoftware');
Route::get('find-phone/{id}','API\SyncController@findPhone');
Route::get('find-fitting/{id}','API\SyncController@findFitting');

Route::middleware('auth:api')->group(function () {
	
});

// api cho MCnews Nuxt
// lấy 5 bài viết đc ghim hot

// Route::get('news_hots','API\MCNewsController@news_hots');
// Route::get('get_news/{slug}','API\MCNewsController@getNews');
// Route::get('categories','API\MCNewsController@categories');
// Route::get('categories/{slug}','API\MCNewsController@news_categories');