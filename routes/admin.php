<?php
Route::group(['prefix' => 'admin'], function() {
    Route::get('/', 'Admin\AdminController@index')->middleware('auth-admin')->name('admin.dashboard');
    Route::get('/login','Admin\LoginController@showLoginForm')->name('admin.login');
    Route::post('/login','Admin\LoginController@login')->name('admin.login.submit');
    Route::get('logout','Admin\LoginController@logout')->name('admin.logout');
    Route::get('/permission-denied','Admin\Controller@permissionDenied')->name('admin.permission.denied');
    // route cho admin user admin
    Route::group(['prefix' => 'admin_users','middleware'=>'auth-admin'], function() {
        Route::get('change-password','Admin\AdminUserController@changePassword')->name('admin.admin_user.changePassword');
        Route::post('change-password','Admin\AdminUserController@postChangePassword');
    });
    Route::resource('admin_users','Admin\AdminUserController',['middleware'=>'auth-admin']);
    //route cho settings
    Route::group(['prefix' => 'settings','middleware'=>'auth-admin'], function() {
        Route::match(['GET', 'POST'],'/general','Admin\SettingController@general')->name('admin.setting.general');
        Route::match(['GET', 'POST'],'/home','Admin\SettingController@home')->name('admin.setting.home');
        Route::match(['GET', 'POST'],'/menu_header','Admin\SettingController@menuHeader')->name('admin.setting.menu_header');
        Route::match(['GET', 'POST'],'/promotion','Admin\SettingController@promotion')->name('admin.setting.promotion');
        Route::match(['GET', 'POST'],'/meta_code','Admin\SettingController@meta_code')->name('admin.setting.meta_code');
        Route::match(['GET', 'POST'],'/seo_category','Admin\SettingController@seo_category')->name('admin.setting.seo_category');
        Route::match(['GET', 'POST'],'/google_shopping','Admin\SettingController@google_shopping')->name('admin.setting.google_shopping');
    });
    //route cho media
    Route::group(['prefix' => 'media','middleware'=>'auth-admin'], function() {
        Route::get('library','Admin\MediaController@library')->name('admin.media.library');
        Route::post('store', 'Admin\MediaController@store')->name('admin.media.store');
        Route::post('update', 'Admin\MediaController@update')->name('admin.media.update');
        Route::post('search','Admin\MediaController@search')->name('admin.media.search');
    });
    // route cho ajax
    Route::group(['prefix' => 'ajax'], function() {
        Route::post('get-slug', 'Admin\Controller@getSlug');
        Route::post('delete-all', 'Admin\Controller@deleteAll');
        Route::post('trash-all', 'Admin\Controller@trashAll');
        Route::post('deactive-all', 'Admin\Controller@deactiveAll');
        Route::post('save-one', 'Admin\Controller@saveOne');
        Route::post('save-all', 'Admin\Controller@saveAll');
        Route::post('relate-suggest', 'Admin\Controller@relateSuggest');
        Route::post('pins', 'Admin\Controller@pins');
        Route::post('duplicate','Admin\AjaxController@duplicate')->name('web.admin.duplicate');
        Route::post('check_new','Admin\AjaxController@check_new')->name('web.admin.check_new');
        Route::post('check_hot','Admin\AjaxController@check_hot')->name('web.admin.check_hot');
        Route::post('has_call','Admin\AjaxController@has_call')->name('admin.comment.has_call');
        Route::post('comment_success','Admin\AjaxController@comment_success')->name('web.admin.comment_success');
        Route::get('export_order','Admin\AjaxController@export_order')->name('admin.export_order');

        Route::post('quick_comment', 'Admin\CommentController@quick_comment');
        Route::post('quick_edit', 'Admin\AjaxController@quick_edit')->name('admin.quick_edit');

        Route::post('delete-cache', 'Admin\AjaxController@DeleteCache');
    });
    Route::resource('system_logs','Admin\SystemLogController',['middleware'=>'auth-admin']);

    //Các module resource
    Route::resource('products_categories','Admin\ProductsCategoryController',['middleware'=>'auth-admin']);
    Route::resource('classes','Admin\ClassController',['middleware'=>'auth-admin']);
    Route::resource('products','Admin\ProductController',['middleware'=>'auth-admin']);
    Route::resource('news_categories','Admin\NewsCategoryController',['middleware'=>'auth-admin']);
    Route::resource('news','Admin\NewsController',['middleware'=>'auth-admin']);
    Route::resource('pages','Admin\PageController',['middleware'=>'auth-admin']);

    Route::resource('prices','Admin\PriceController',['middleware'=>'auth-admin']);

    Route::resource('fit_categories','Admin\FitCategoryController',['middleware'=>'auth-admin']);
    Route::resource('fits','Admin\FitController',['middleware'=>'auth-admin']);
    Route::post('fits/export','Admin\FitController@export',['middleware'=>'auth-admin'])->name('admin.fits.export');
    Route::resource('service_categories','Admin\ServiceCategoryController',['middleware'=>'auth-admin']);
    Route::resource('services','Admin\ServiceController',['middleware'=>'auth-admin']);
    Route::resource('slides','Admin\SlideController',['middleware'=>'auth-admin']);
    Route::resource('location','Admin\LocationController',['middleware'=>'auth-admin']);
    Route::resource('address','Admin\AddressController',['middleware'=>'auth-admin']);
    Route::resource('sync','Admin\SyncController',['middleware'=>'auth-admin']);
    Route::resource('internal_link','Admin\InternalController',['middleware'=>'auth-admin']);
    Route::resource('orders','Admin\OrderController',['middleware'=>'auth-admin']);
    Route::post('orders/export','Admin\OrderController@export')->name('admin.orders.export');
    Route::resource('comment','Admin\CommentController',['middleware'=>'auth-admin']);
    Route::post('comment/export','Admin\CommentController@export')->name('admin.comment.export');
    Route::group(['prefix' => 'comment','middleware'=>'auth-admin'], function() {
        Route::get('/{id}/reply', 'Admin\CommentController@reply')->name('comment.reply');
        Route::post('/reply/{id}', 'Admin\CommentController@postReply')->name('comment.postReply');
    });
    
});