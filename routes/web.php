<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// bảng giá
Route::get('/bang-gia/{slug}.html','Web\PriceController@index')->name('web.price.index');
Route::get('/', 'Web\HomeController@index')->name('web.home');
Route::get('/tao-fits', 'Web\TestController@CreateFit')->name('web.fit');
Route::get('/tao-services', 'Web\TestController@CreatedService')->name('web.service');
Route::get('/test', 'Web\TestController@index')->name('web.test');
// site map
Route::get('datafeeds','Web\ServiceController@datafeeds');


Route::get('sitemap.xml','Web\SitemapController@index');
Route::get('sitemap-misc.xml','Web\SitemapController@misc');
Route::get('sitemap-contact.xml','Web\SitemapController@contact');
Route::get('sitemap-categories.xml','Web\SitemapController@categories');
Route::get('sitemap-detail.xml','Web\SitemapController@detail');
Route::get('sitemap-pages.xml','Web\SitemapController@pages');
Route::get('sitemap-{slug}-page-{page}.xml','Web\SitemapController@show')->where(['slug' => '([^/]*)', 'page' => '[0-9]+']);

Route::get('404.html','Web\HomeController@page_not_found')->name('web.page_not_found');
// RSS
Route::get('rss','Web\RssController@index')->name('rss');
Route::get('rss/dien-thoai.rss','Web\RssController@phone')->name('rss.phone');
Route::get('rss/may-tinh-bang.rss','Web\RssController@talbet')->name('rss.tablet');
Route::get('rss/phu-kien.rss','Web\RssController@fit')->name('rss.fit');
Route::get('rss/sua-chua.rss','Web\RssController@service')->name('rss.service');
Route::get('rss/tin-tuc.rss','Web\RssController@news')->name('rss.news');
//

Route::get('tin-tuc/{slug}.html','Web\NewsController@show')->name('web.news.show');
Route::get('tin-tuc','Web\NewsController@showall')->name('web.news_categories.showall');
Route::get('tin-tuc-{slug?}','Web\NewsController@index')->name('web.news_categories.show');

// Sảm phẩm phụ kiện
Route::get('phu-kien/{slug?}.html','Web\FitController@index')->name('web.fits.show');
// Danh mục phụ kiện
Route::get('phu-kien','Web\FitCategoryController@showall')->name('web.fits_categories.showall');
Route::get('phu-kien-{slug?}','Web\FitCategoryController@index')->name('web.fits_categories.show');
// Sảm phẩm dịch vụ
Route::get('dich-vu/{slug?}','Web\ServiceController@index')->name('web.service.show');
// danh mục dịch vụ
Route::get('sua-chua-dien-thoai','Web\ServiceCategoryController@showall')->name('web.services_categories.showall');
Route::get('sua-chua-{slug?}','Web\ServiceCategoryController@index')->name('web.services_categories.show');

// profile
Route::get('profile/{id}','Web\ProfileController@index')->name('web.profile.show');

//sản phẩm unlock
Route::get('unlock-mo-mang/{slug?}.html','Web\UnlockController@index')->name('web.unlocks.show');
// unlock mở mạng
Route::get('unlock-mo-mang','Web\UnlockCategoryController@showall')->name('web.unlock_categories.showall');
Route::get('unlock-mo-mang-{slug?}','Web\UnlockCategoryController@index')->name('web.unlock_categories.show');

// Trang đơn
Route::get('page/{slug?}.html','Web\PageController@show')->name('web.pages.show');

//ajax
Route::group(['prefix' => 'ajax'], function() {
    Route::get('phu-kien','AjaxController@fits_categories_all')->name('web.ajax.fits_all');
    Route::get('phu-kien-{slug?}','AjaxController@fits_categories')->name('web.ajax.fits');

    Route::get('sua-chua-dien-thoai','AjaxController@services_categories_all')->name('web.ajax.services_all');
    Route::get('sua-chua-{slug?}','AjaxController@services_categories')->name('web.ajax.services');

    Route::get('unlock-mo-mang','AjaxController@unlock_categories_all')->name('web.ajax.unlock_all');
    Route::get('unlock-mo-mang-{slug?}','AjaxController@unlock_categories')->name('web.ajax.unlock');
    
    // comment
    Route::get('comment/{comment_id}/like','Web\CommentController@CommentLike')->name('web.ajax.commentlike');
    Route::get('comment/{comment_id}/dislike','Web\CommentController@CommentDisLike')->name('web.ajax.commentdislike');
    Route::get('comment/{type}/{type_id}','Web\CommentController@getComment')->name('web.ajax.getComment');
    Route::post('comment/{type}/{type_id}/add','Web\CommentController@addComment')->name('web.ajax.addComment');
    Route::post('comment/search/{type}/{type_id}','Web\CommentController@searchComment')->name('web.ajax.searchComment');
    // search
    Route::post('search','AjaxController@search_ajax')->name('web.ajax.search');

    Route::post('vote_star','Web\NewsController@vote_star')->name('web.ajax.vote_star');
    //đặt lịch sửa chữa dịch vụ
    Route::post('dat-lich-sua-chua','AjaxController@orderService')->name('web.ajax.order_service');
    Route::post('dat-hang','AjaxController@Order')->name('web.ajax.order');
    // trả lời bình luận
    Route::post('reply-comment','AjaxController@replyComment')->name('web.ajax.reply_comment');

    // xóa giỏi hàng
    Route::post('remove-item-cart','Web\OrderController@removeItemCart')->name('web.orders.remove_item_cart');
//thêm bớt vào giỏi hàng
    Route::post('update-item-cart','Web\OrderController@updateItemCart')->name('web.orders.update_item_cart');
    //đánh giá sao
    Route::post('rating', 'AjaxController@Rating');
    //call me 
    Route::post('callme', 'AjaxController@callMe');

     //đặt lịch sửa chữa dịch vụ
     Route::post('lien-he','AjaxController@Contact')->name('web.ajax.contact');
     // gửi form ưu đãi
     Route::post('thong-bao-uu-dai','AjaxController@InforEndow')->name('web.ajax.InforEndow');
     /**
      * show thêm bình luận 
      */
      
      Route::post('view-more-comment','AjaxController@viewMoreComment')->name('web.ajax.view_more_comment');
      /**
       * xem thêm phụ kiện , dịch vụ
       */
      Route::post('view-more-product','AjaxController@viewFitService')->name('web.ajax.product');
    /**
     * lưu session
     */
    Route::post('save-session-modal','AjaxController@saveSessionModal')->name('web.ajax.save.session');

});

Route::post('home_view_more_product','Web\HomeController@home_view_more_product')->name('web.home.home_view_more_product');
Route::post('sync_image','AjaxController@sync_image')->name('web.ajax.sync_image');


Route::post('ajax/orders','Web\OrderController@store')->name('web.ajax.addorder');

Route::get('tim-kiem','Web\SearchController@index')->name('web.search.show');

//giỏi hàng
Route::get('/gio-hang.html','Web\OrderController@cart')->name('web.orders.cart');
//dat hang
Route::get('/dat-hang.html','Web\OrderController@checkout')->name('web.orders.checkout');
//dat hang
Route::post('/post-order','Web\OrderController@postOrder')->name('web.orders.post_order');
/**
 * trang lien he
 */
Route::get('/lien-he.html','Web\ContactController@index')->name('web.contact.index');
