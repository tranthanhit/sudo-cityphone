
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$('.product-detail-viewall').click(function() {
    $(this).css('display', 'none');
    $(this).parent().css('position', 'relative');
    $(this).parent().children('.product-detail-viewdefault').css('display', 'inline-block');
    $(this).parent().parent().children('#specifications_detail').css('height','auto');
});
$('.product-detail-viewdefault').click(function() {
    $(this).css('display', 'none');
    // $(this).parent().css('position', 'absolute');
    $(this).parent().children('.product-detail-viewall').css('display', 'inline-block');
    $(this).parent().parent().children('#specifications_detail').animate({height: '520px'}, 500);
});
$("img.lazy").lazyload({
    effect : "fadeIn"
});

function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}
function isPhone(phone) {
    var regex =/((09|03|07|08|05)+([0-9]{8})\b)/g;
    return regex.test(phone);
}

$(document).ready(function() {
    openPopup('.buy-now', '.popup');
    closePopup('.popup_close', '.popup'); 
})
function openPopup(clickShowBtn, popupShow) {
    $(clickShowBtn).on('click',function(){
        $(popupShow).bPopup({
            speed: 450,
            transition: 'slideDown',
            zIndex:99999,
            onOpen: function() { 
                $(popupShow).css('visibility', 'visible'); 
            },
            onClose: function() { 
                $(popupShow).css('visibility', 'hidden'); 
            }
        });
    }); 
}
function closePopup(clickCloseBtn, popupClose) {
    $(clickCloseBtn).on('click' ,function() {
        $(popupClose).css('visibility', 'hidden');
        $(popupClose).bPopup().close();
    })
}
/**
 * đặ lịch sửa chữa
 */
var price_serice = 0;
$('.btn-submit-buynow').on("click",function(){
    var type = $(this).attr('data-type');
    var id = $(this).attr('data-id');
    var gender = $('#customer-gender').val();
    var name = $('#customer-name').val();
    var phone = $('#customer-phone').val();
    var email = $('#customer-email').val();
    var location = $('#customer-location').val();
    var address = $('#customer-address').val();
    var note = $('#order-note').val();
    var link = window.location.href;
    var price_serice = $(this).attr('data-price');
    if(gender == ''){
         alert('Không để trống giới tính');
         return false;
    }
    if(name == ''){
        alert('Không để trống Tên');
         return false;
    }
    if(phone == ''){
        alert('Không để trống Số điện thoại');
         return false;
    }
    if(!isPhone(phone)){
        alert('Nhập sai Số điện thoại');
        return false;
    }
    if(email == ''){
        alert('Không để trống địa chỉ email');
         return false;
    }
    if(!isEmail(email)){
       alert('Nhập sai định dạng email!');
        return false;
    }
    if(location == ''){
        alert('Không để trống Thành phố');
         return false;
    }
    if(address == ''){
        alert('Không để trống địa chỉ nhận hàng');
         return false;
    }
    $.ajax({
        type:'post',
        url:'/ajax/dat-lich-sua-chua',
        dataType:'json',
        data:{
            _token: $('#_token').val(),
            type:type,
            id:id,
            name:name,
            phone:phone,
            email:email,
            gender:gender,
            location:location,
            address:address,
            note:note,
            link:link,
            price:price_serice,

        },success:function(data){
            alert('Đặt lịch hẹn thành công');
            $('#customer-name').val('');
            $('#customer-phone').val('');
            $('#customer-email').val('');
            $('#customer-address').val('');
            $('#order-note').val('');
            $(".popup").bPopup().close();
            $('img.loading1').css('display','none');
            $('body').css({'opacity':1});
        }, 
        error: function (error) {
            $('img.loading1').css('display','none');
            $('body').css({'opacity':1});
            alert('Có lỗi xảy ra! Vui lòng thử lại.');
        }, beforeSend:function(){
            $('img.loading1').css('display','inline');
            $('body').css({'opacity':0.5});
        }

    });

})
/**
 * Thêm vào giỏi hàng
 */
$('.add_to_cart').on("click",function(event){
    event.preventDefault();
    var id = $(this).data('id');
    var type = $(this).data('type');
    var qty = 1;
    var token = $(this).data('token');
    var link =  window.location.href;
    $.ajax({
        type:"post",
        dataType:"json",
        url:"/ajax/dat-hang",
        data:{
            _token: token,
            id:id,
            type:type,
            qty :qty,
            link:link,
        },
        success:function(response){
            $('img.loading1').css('display','none');
            $('body').css({'opacity':1});
            $("#fancy_data").find(".layer_cart_img img").attr("src",response.image);
            $("#fancy_data").find(".product_name").html(response.name);
            $("#fancy_data").find(".ajax_cart_quantity").html(response.cart_count);
            $("#fancy_data").find(".ajax_block_cart_total").html(response.cart_total);
            $("#fancy_data").find(".layer_cart_product_quantity").html(response.qty);
            $("#fancy_data").find(".layer_cart_product_price").html(response.price);
            $(".cart .total_cart").html(response.cart_count);
            $.fancybox({
                maxWidth:455,
                minWidth:450,
                minHeight:210,
                href: "#fancy_data",
                helpers:  {
                    title : {
                        type : 'inside'
                    },
                    overlay : {
                        showEarly : false
                    },
                    autohide : 1
                }
            });
        },
        error: function (error) {
            $('img.loading1').css('display','none');
            $('body').css({'opacity':1});
            alert('Có lỗi xảy ra! Vui lòng thử lại.');
        }, beforeSend:function(){
            $('img.loading1').css('display','inline');
            $('body').css({'opacity':0.5});
        }
    });
});

//trả lời bình luận

$('.comment_form').on('click','.comment_addnew_btn',function () {
    var currentBtn = $(this);
    currentBtn.closest('.comment_form').find('.comment_author_info').slideToggle();
});
function display_information(id){
    $('.comment_author_info-'+id).slideToggle();
}
$('.comment_form').on('click','#putComment',function(){
    var currentBtn = $(this);
    var id = $(this).attr('data-type_id');
    var editor = $(this).closest('form').find("textarea[name='comment_editor']").val();
    var name = $(this).closest('form').find("input[name='comment_author_name']").val();
    var phone = $(this).closest('form').find("input[name='comment_author_phone']").val();
    var email = $(this).closest('form').find("input[name='comment_author_email']").val();
    if (editor == "") {
        alert('Vui lòng nhập nội dung bình luận', 'error');
        currentBtn.closest('.comment_author_info').closest('.comment_form').find('.comment_editor').focus();
        return false;
    }
    if(name == '') {
        alert('Vui lòng nhập họ tên của bạn');
        currentBtn.closest('.comment_author_info').find('.comment_author_name').focus();
        return false
    }
    if(!isEmail(email)) {
        alert('Vui lòng nhập một email hợp lệ');
        currentBtn.closest('.comment_author_info').find('.comment_author_email').focus();
        return false
    }
    if(phone == '') {
        alert('Vui lòng nhập số điện thoại của bạn');
        currentBtn.closest('.comment_author_info').find('.comment_author_phone').focus();
    }
    if(!isPhone(phone)) {
        alert('Vui lòng nhập một số điện thoại hợp lệ');
        currentBtn.closest('.comment_author_info').find('.comment_author_phone').focus();
        return false
    }
    $.ajax({
        type: 'post',
        url: '/ajax/reply-comment',
        async: false,
        processData: false,
        contentType: false,
        data: new FormData($(this).closest('form')[0]),
        success:function(data){
            $('img.loading1').css('display','none');
            $('body').css({'opacity':1});
            $("textarea[name='comment_editor']").val('');
            $("input[name='comment_author_name']").val('');
            $("input[name='comment_author_phone']").val('');
            $("input[name='comment_author_email']").val('');
            alert('Cảm ơn bạn đã bình luận.');
            $('#comment_item_'+id).html(data);
        },
        error: function (error) {
            $('img.loading1').css('display','none');
            $('body').css({'opacity':1});
            alert('Có lỗi xảy ra! Vui lòng thử lại.');
        }, beforeSend:function(){
            $('img.loading1').css('display','inline');
            $('body').css({'opacity':0.5});
        }
    });
});

// $('#comment_list').on('click','.comment_item_reply',function () {
//     var currentBtn = $(this);
//     $('#comment_list .comment_form').hide();
//     currentBtn.closest('.comment_item_parent').find('.comment_form').slideDown().find('textarea').focus();
// });
/**
 * show bình luận con
 */
function btn_reply(id){

    $('#comment_list .comment_form').hide();
   $('.form-import-'+id).slideDown().find('textarea').focus();
}
$('.priceprice').on('click','span',function(){
    $('.listprice').slideToggle();
})
$('.priceprice').on('click','.closefilter', function(){
    $('.listprice').hide();
});
//xóa giỏi hàng

$('.cart_quantity_delete').on('click',function(e){
    e.preventDefault();
    var rowid = $(this).data('rowid');
    var tr_cart = $("#cart_item_"+rowid);
    $.ajax({
        type: "POST",
        url: "/ajax/remove-item-cart",
        data: {rowid:rowid},
        dataType: "JSON",
        success:function(data){
            $('img.loading1').css('display','none');
            $('body').css({'opacity':1});
            $(".cart_item_"+rowid).remove();
            tr_cart.fadeOut(500);
            if(data.cart_count == 0){
                $('.list_item_orders').remove();
                $('.flex-row').css('display','none');
                $('.your_cart').css('display','block');
            }
        },
        error: function (error) {
            $('img.loading1').css('display','none');
            $('body').css({'opacity':1});
            alert('Có lỗi xảy ra! Vui lòng thử lại.');
        }, beforeSend:function(){
            $('img.loading1').css('display','inline');
            $('body').css({'opacity':0.5});
        }
    });
   
});

// thêm bớt vào giỏi hàng

$(".increase_reduced_item").click(function (e) { 
    e.preventDefault();
    var rowid = $(this).closest("tr").data('rowid');
    var id = $(this).closest("tr").data('id');
    var qty = $(this).closest("tr").find("#qty"+id).val();
    $.ajax({
        type: "POST",
        url: "/ajax/update-item-cart",
        data: {rowid:rowid,qty:qty},
        dataType: "JSON",
        success: function (response) {
            $('img.loading1').css('display','none');
            $('body').css({'opacity':1});
            $(".price_span_"+id).html(response.qty);
            if(response.subtotal != 0){
               $(".price_"+id).html(response.subtotal);
            }else{
                $(".price_"+id).html('Liên hệ');
            }
        },
        error: function (error) {
            $('img.loading1').css('display','none');
            $('body').css({'opacity':1});
            alert('Có lỗi xảy ra! Vui lòng thử lại.');
        }, beforeSend:function(){
            $('img.loading1').css('display','inline');
            $('body').css({'opacity':0.5});
        }
    });
});


$('.rating-action-btn').click(function () {
       
    var btn = $(this);
    var review = btn.closest('.rating-action');
    var id = review.attr('data-id');
    var point = btn.attr('data-point');
    var type = review.attr('data-type');
    $.ajax({
        type:'post',
        dataType: 'json',
        url:'/ajax/rating',
        data:{id:id,type:type,point:point},
        success:function(result){
            $('img.loading1').css('display','none');
            $('body').css({'opacity':1});
            if(result.status == 1) {
                $('.rating-action-btn i').each(function (i) {
                    if (i < point) {
                        $(this).removeClass('fa-star-o').addClass('fa-star');
                    }
                });
            }
            alert(result.message);
        },
        error: function (error) {
            $('img.loading1').css('display','none');
            $('body').css({'opacity':1});
            alert('Có lỗi xảy ra! Vui lòng thử lại.');
        },
        beforeSend:function () {
            ratingStatus = false;
            $('img.loading1').css('display','inline');
            $('body').css({'opacity':0.5});
        }
    });

});

/**
 * click menu
 */
$('.my-ten').on('click','a',function(){
    $('.header_mobile_menu').hide();
    $('.menu_click a i').addClass('fa fa-bars');
    $('.menu_click a i').removeClass('fa-close');
    $('.__b-popup1-menu__').css({'opacity': 0,'display':'none'});
})
// click menu
$('body').on('click','.menu_click a .fa-bars',function(){
    $('.header_mobile_menu').slideDown();
    $('#header_mobile').css({'z-index':2});
    $('.__b-popup1-menu__').css({'opacity': 0.7,'display':'block'});
    $('.menu_click a i').removeClass('fa-bars');
    $('.menu_click a i').addClass('fa fa-close');
  
})
$('body').on('click','.menu_click a .fa-close',function(){
    $('.header_mobile_menu').hide();
    $('.menu_click a i').addClass('fa fa-bars');
    $('.menu_click a i').removeClass('fa-close');
    $('.__b-popup1-menu__').css({'opacity': 0,'display':'none'});
})

 /**
  *  trang chủ gọi lại cho tôi
  *  */

$('.sff-button').on('click',function () {
    var name = $('.sff-name').val();
    var phone = $('.sff-phone').val();
    var service_name = $('.sff-service').val();
    if (name == '') {
        alert('Vui lòng nhập họ và tên !');
        $('.sff-name').focus();
        return false;
    } 
    if(phone.trim() == ''){
        alert('Vui lòng nhập số điện thoại !');
        $('.sff-phone').focus();
        return false;
    }
    if(!isPhone(phone)){
        alert('Số điện thoại không đúng định dạng !');
        $('.sff-phone').focus();
        return false;
    }
    if(service_name.trim()==''){
        alert('Vui lòng nhập dịch vụ yêu cầu !');
        $('.sff-service').focus();
        return false;
    }
    $.ajax({
        type:'post',
        dataType: 'json',
        url:'/ajax/callme',
        data:{name:name,phone:phone,service_name:service_name},
        success:function(result){
            $('.customer_form_content').html(result.message);
            $('img.loading1').css('display','none');
            $('body').css({'opacity':1});
        },
        error: function (error) {
            $('img.loading1').css('display','none');
            $('body').css({'opacity':1});
            alert('Có lỗi xảy ra! Vui lòng thử lại.');
        },
        beforeSend:function () {
            $(".sff-name,.sff-phone,.sff-service").val('');
            $('img.loading1').css('display','inline');
            $('body').css({'opacity':0.5});
        }
    });
    
});

//
$('.item_filter').on('click','p',function(){
    alert('dc');
});

$('#form_contact').on('click','.contact_buttom',function(){
    var name = $('#name_contact').val();
    var phone = $('#phone_contact').val();
    var email = $('#email_contact').val();
    var address = $('#address_contact').val();
    var note = $('#infor_contact').val();
    if(name.trim() == ''){
        alert('Không bỏ trống Tên');
        $('#name_contact').focus();
        return false;
    }
    if(phone.trim() == ''){
        alert('Không bỏ trống số điện thoại');
        $('#phone_contact').focus();
        return false;
    }
    if(!isPhone(phone)){
        alert('Nhập sai định dạng Số điện thoại');
        $('#phone_contact').focus();
        return false;
    }
    if(email.trim() == ''){
        alert('Không bỏ trống email');
        $('#email_contact').focus();
        return false;
    }
    if(!isEmail(email)){
        alert('Nhập sai định dạng email');
        $('#email_contact').focus();
        return false;
    }
    $.ajax({
        type:"post",
        dataType:"json",
        url:"/ajax/lien-he",
        data:{
            _token: $('#_token').val(),
            name:name,
            phone:phone,
            email :email,
            address:address,
            note:note,
        },
        success:function(response){
            $('img.loading1').css('display','none');
            $('body').css({'opacity':1});
            alert('Gửi thành công!');
            if(response == 1){
                $('#name_contact').val('');
                $('#phone_contact').val('');
                $('#email_contact').val('');
                $('#address_contact').val('');
                $('#infor_contact').val('');
            }
        },
        error: function (error) {
            $('img.loading1').css('display','none');
            $('body').css({'opacity':1});
            alert('Có lỗi xảy ra! Vui lòng thử lại.');
        }, beforeSend:function(){
            $('img.loading1').css('display','inline');
            $('body').css({'opacity':0.5});
        }
    });
})
$('.check_radio').on('click',function(){
    if ($(this).prop("checked") == true) {
        var id = $(this).attr('data-id');
        $('.item_tranthanh').css('background-color','#e31e30');
        $('.check-item-'+id).css('background-color','#0b659d');
    }
})
$('#buttom_modal').on('click',function(){
    var name = $("#form_modal_uu_dai input[name='name_modal']").val();
    var phone = $("#form_modal_uu_dai input[name='phone_modal']").val();
    var email = $("#form_modal_uu_dai input[name='email_modal']").val();
    if(name.trim() == ''){
        alert('Không bỏ trống Tên');
        $("#form_modal_uu_dai input[name='name_modal']").focus();
        return false;
    }
    if(phone.trim() == ''){
        alert('Không bỏ trống số điện thoại');
        $("#form_modal_uu_dai input[name='phone_modal']").focus();
        return false;
    }
    if(!isPhone(phone)){
        alert('Nhập sai định dạng Số điện thoại');
        $("#form_modal_uu_dai input[name='phone_modal']").focus();
        return false;
    }
    if(email.trim() == ''){
        alert('Không bỏ trống email');
        $("#form_modal_uu_dai input[name='email_modal']").focus();
        return false;
    }
    if(!isEmail(email)){
        alert('Nhập sai định dạng email');
        $("#form_modal_uu_dai input[name='email_modal']").focus();
        return false;
    }
    $.ajax({
        type:'post',
        url:'/ajax/thong-bao-uu-dai',
        async: false,
        processData: false,
        contentType: false,
        data: new FormData($('#form_modal_uu_dai')[0]),
        success:function(data){
            $('.modal img.loading').css('display','none');
            $('.modal').css({'opacity':1});
            $('#myModal').css('display','none');
            location.reload();
            if(data == 1){
                alert('Gửi thành công thông tin!');
                $('#modal_km').css('display','none');
                $('.b-modal').css('opacity',1);
                $('.b-modal').css('background-color','unset');

            }
        },
        error: function (error) {
            $('.modal img.loading').css('display','none');
            $('.modal').css({'opacity':1});
            alert('Có lỗi xảy ra! Vui lòng thử lại.');
        }, beforeSend:function(){
            $('.modal img.loading').css('display','inline');
            $('.modal').css({'opacity':0.5});
        }
    });
});
//lọc thương hiệu bản mobile
function brand(){
    var url = $('.brand').val();
    history.pushState({},'hello','sua-chua-dien-thoai?brand='+url);
    location.reload();
}
function service_cate_m(){
   var url = $('.service-cate-m').val();
  
   history.pushState({},'hello','sua-chua-'+url);
   location.reload();
}
function service_cate_iphone(){
    var url = $('.service-cate-iphone').val();
   history.pushState({},'hello','sua-chua-'+url);
   location.reload();
}
function request(){
    var link = $('.request').attr('data-url');
    var url = $('.request').val();
    history.pushState({},'hello',link+'?request='+url);
    location.reload();
}
//end lọc
// danh mục phụ kiện
function fit_category(){
    var url = $('.fit-category').val();
    history.pushState({},'hello','phu-kien-'+url);
    location.reload();
}

/**
 * show thêm bình luận
 */
var page = 1;
$('.comment_list').on('click','.comments_more',function(){
    page++;
    var that = $(this).closest('.comment_list');
    var type = $(this).data('type');
    var count = $(this).data('count');
    var type_id = $(this).data('type_id');
    $.ajax({
        type:'post',
        url:'/ajax/view-more-comment',
        data:{
            type_id:type_id,
            page:page,
            type:type,
            count:count,
        },
        success:function(response){
            $('img.loading1').css('display','none');
            $('body').css({'opacity':1});
            if(response == ""){
                alert('Hết dữ liệu');
                that.find('.comments_more').css('display','none');
            }
            console.log("zz");
            that.find('.show_comments').append(response);

        }, error: function (error) {
            $('img.loading1').css('display','inline');
            $('body').css({'opacity':1});
            alert('Có lỗi xảy ra! Vui lòng thử lại.');
        }, beforeSend:function(){
            $('img.loading1').css('display','inline');
            $('body').css({'opacity':0.5});
        }
    });
})
/**
 * xem thêm phụ kiện và dịch vụ
 * 
 */
var page_product = 1;
$('.view-more-fit-service').on('click','.view-product-fits-service',function(e){
    e.preventDefault();
    var brand ='';
    var request1 ='';
    var filter_price ='';
    var sort ='';
    var that = $(this).closest('.view-more-fit-service');
    var type = $(this).data('type');
    var count = $(this).data('count');
    var cate_id = $(this).data('cate_id');
    if(type == 'services'){
        var brand = $(this).data('brand');
        var request1 = $(this).data('request1');
    }
    if(type == 'fits'){
        var filter_price = $(this).data('filter_price');
        var sort = $(this).data('sort');
    }
    page_product++;
    $.ajax({
        type:'post',
        url:'/ajax/view-more-product',
        data:{
            page:page_product,type:type,count:count,cate_id:cate_id,brand:brand,request1:request1,filter_price:filter_price,sort:sort
        },
        success:function(response){
            $('img.loading1').css('display','none');
            $('body').css({'opacity':1});
            if(response == ""){
                alert('Hết dữ liệu');
                that.find('#viewmore').css('display','none');
            }
            console.log("zz");
            that.find('.content_list').append(response);

        }
        , error: function (error) {
            $('img.loading1').css('display','inline');
            $('body').css({'opacity':1});
            alert('Có lỗi xảy ra! Vui lòng thử lại.');
        }, beforeSend:function(){
            $('img.loading1').css('display','inline');
            $('body').css({'opacity':0.5});
        }
    });
})
/**
 * kiểm tra bảo hành
 */
$('.gcf-submit').on('click',function(){
    alert('Tính năng này đang được nâng cấp');
})



/**
 * show modal
 */
var modal = document.getElementById("myModal");
var btn = document.getElementById("myBtn");
var span = document.getElementsByClassName("close")[0];

/**
 * tắt modal 
 */
function modal_close(){
 
    $.ajax({
        type:'post',
        url :'/ajax/save-session-modal',
        data:{},
        success:function(data){
            $('#myModal').css('display','none');
        }
    });
}
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
     $.ajax({
        type:'post',
        url :'/ajax/save-session-modal',
        data:{},
        success:function(data){
            modal.style.display = "none";
        }
    });
  }
}
/**
 * hiển thị thông tin khác
 */
$('body').on('click','.ui-block-a a#other_information',function(){
    $('.m_footer_2_mobile').css('display','block');
    $('.m_address_2_mobile').css('display','none');
    $('html, body').animate({scrollTop: $(".m_footer_2_mobile").offset().top}, 1000);
})
$('body').on('click','.ui-block-a a#store_address',function(){
    $('.m_footer_2_mobile').css('display','none');
    $('.m_address_2_mobile').css('display','block');
    $('html, body').animate({scrollTop: $(".m_address_2_mobile").offset().top}, 1000);
})
/**
 * xem bảng giá
 */
function price_list_mobile(){
    $('html, body').animate({scrollTop: $("#price_list_home").offset().top}, 1000);
}
/**
 * thanh cuộn
 */

var script = function(){

    var win = $(window);
    var html = $('html');
    var body = $('body');
    var stick_scroll = function(){
        stick_act();
        win.resize(function(e) {
            stick_act();
        });
    
        function stick(){
            $('.stick-scroll').stick_in_parent({
                offset_top: 68,
            });
        }
        function un_stick(){
            $('.stick-scroll').trigger("sticky_kit:detach");
        }
        function stick_act(){
            (win.width() < 992) ? un_stick() : stick();
        }
    }
    return {

        uiInit: function($fun){
            switch ($fun) {
                case 'stick_scroll':
                    stick_scroll();
                    break;
                    default:
                    
                        stick_scroll();
                    
            }
        }
    }
}


/**
 * chọn địa điểm
 */
function location_head_onchange(){
    
}

$('.location-option').on('change',function () {
    var location = $(this).val();
    var currentUrl = window.location.href;
    currentUrl += '?location='+location;
    window.location.href = currentUrl;
    // alert(currentUrl);
});

$(".gotoTop").on("click", function(event) {
    event.preventDefault();
    $("html,body").animate({ scrollTop: 0 }, "slow");
  });

  $("#redirect .cart_item_show").on("click", function(){
    $("#basket").addClass("active");
})
$(".closeBtn").on("click", function(){
    $(this).closest(".popup_t").removeClass("active");
})

$('.class-nut-click').click(function(){
    $('html, body').animate({scrollTop: $("#id-div-chay-den").offset().top}, 1000);
});
/**
 * dịch vụ sửa chữa nổi bật
 */
function outstanding_service(){
    $('html, body').animate({scrollTop: $("#outstanding_service").offset().top}, 1000);
}
/**
 * bảng gí
 */
function Price_list_for_repair_services(){
    $('html, body').animate({scrollTop: $("#price_list_home").offset().top}, 1000);
}
/**
 * dich vu sua chua iphone
 */
function Repair_service_yet_iphone(){
    $('html, body').animate({scrollTop: $("#Repair_service_yet_iphone").offset().top}, 1000);
}
/**
 * link kien dien tu
 */
function parts_accessories(){
    $('html, body').animate({scrollTop: $("#parts_accessories").offset().top}, 1000);
}
/**
 * tin tức công nghệ
 */
function technology_news(){
    $('html, body').animate({scrollTop: $("#technology_news").offset().top}, 1000);
}

/**
 * xem thêm seo
 */
$('.show-seo-category').click(function() {
    var check = $(this).attr('data-status');
    if(check == 'hide'){
        $(this).removeAttr('data-status');
        $(this).parent().css('position', 'relative');
        $(this).html('Thu gọn ▲');
        $(this).parent().parent().children('.seo_category').css('height','auto');
    }else{
        $(this).attr('data-status', 'hide')
        $(this).parent().css('position', 'relative');
        $(this).html('Xem thêm ▼');
        $(this).parent().parent().children('.seo_category').animate({height: '200px'}, 500);
        $(this).parent().parent().children('.seo_category').css('overflow','hidden');
    }
});
