/**
 * slide ảnh
 */

$('.detailProduct-left__top').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    asNavFor: '.detailProduct-left__bottom'
});
$('.detailProduct-left__bottom').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.detailProduct-left__top',
    dots: false,
    centerMode: false,
    focusOnSelect: true
});